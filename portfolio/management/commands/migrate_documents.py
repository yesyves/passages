#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.core.management.base import BaseCommand

from portfolio.models import Document, DocumentType


class Command(BaseCommand):
    help = """Migrate specific documents to new types model.
        Load fixture: 'uqam_document_types' before running this program
        """

    def handle(self, *args, **options):

        document_types = DocumentType.available_objects.all()
        types = {}

        for document_type in document_types:
            if document_type.name == 'Curriculum Vitae':
                types['A'] = document_type
            elif document_type.name == 'Ligne de vie':
                types['B'] = document_type
            elif document_type.name == "Fiche de préparation à l'entretien":
                types['C'] = document_type
            else:
                print(f'Missing document type: {document_type}')

        documents = Document.available_objects.filter(indice_1=0)
        migrated_documents = 0

        for document in documents:
            document.type = types[document.indice_2]
            document.save()
            migrated_documents += 1

        print(f'{migrated_documents} documents updated')