#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.core.management.base import BaseCommand

from portfolio.models import AchievementType, EditorBlock


class Command(BaseCommand):
    help = 'Copy all editor blocks from a milestone to create a new one'

    def add_arguments(self, parser):
        parser.add_argument(
            '-o',
            '--origin',
            type=int,
            default=0,
            help='Specify milestone to copy',
        )
        parser.add_argument(
            '-n',
            '--new',
            type=int,
            default=0,
            help='Specify new milestone',
        )

    def handle(self, *args, **options):

        origin_milestone = options['origin']
        new_milestone = options['new']

        if new_milestone == 0:
            last_block = EditorBlock.available_objects.all().last()
            new_milestone = last_block.milestone + 1
        if origin_milestone == 0:
            origin_milestone = new_milestone - 1

        print('Copying milestone ' + str(origin_milestone) + ' to create milestone ' + str(new_milestone))

        editor_blocks = EditorBlock.available_objects.filter(milestone=origin_milestone)

        for editor_block in editor_blocks:
            # Legacy
            # if editor_block.number == 0:
            #     editor_block.number = editor_block.id
            #     editor_block.save()
            new_block = EditorBlock(
                number=editor_block.number,
                instruction=editor_block.instruction,
                explanation=editor_block.explanation,
                target_words=editor_block.target_words,
                assignment=editor_block.assignment,
                milestone=new_milestone,
            )
            new_block.save()
            print(new_block)

        for achievement_type in AchievementType.available_objects.all():
            for block in achievement_type.blocks.filter(milestone=origin_milestone):
                add_block = EditorBlock.available_objects.get(number=block.number,
                                                              milestone=new_milestone)
                achievement_type.blocks.add(add_block)
                achievement_type.save()
                print(str(achievement_type) + " : " + str(add_block))
