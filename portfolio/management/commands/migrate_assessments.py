#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.core.management.base import BaseCommand
from django.forms.models import model_to_dict

from portfolio.models import Assessment, PortfolioAssessment, UserAssessment


class Command(BaseCommand):
    help = """Migrate portfolio assessments to new model.
        Load fixtures : assessment_rating portfolio_type assessment
            before running this program
        """

    def handle(self, *args, **options):

        old_assessments = PortfolioAssessment.available_objects.all()

        assessments = Assessment.available_objects.filter(milestone=1)

        for old_assessment in old_assessments:
            old_dict = model_to_dict(old_assessment)
            # print(old_dict)
            print(old_assessment.portfolio.user)
            for assessment in assessments:
                print(assessment.code)
                if assessment.code in old_dict:
                    grade = old_dict[assessment.code]
                    if assessment.code == 'total':
                        comment = ''
                    else:
                        comment = old_dict[f'{assessment.code}_comment']
                elif assessment.code == 'e1':
                    grade = None
                    comment = "Évalué de manière intégrée aux autres éléments"
                else:
                    continue
                user_assessment = UserAssessment(
                    portfolio=old_assessment.portfolio,
                    assessment=assessment,
                    grade=grade,
                    comment=comment,
                )
                user_assessment.save()
                print(user_assessment)
