#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import shutil

from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from portfolio.models import Portfolio


class Command(BaseCommand):
    help = 'Remove users media directories for portfolio certified since 50 days or more and mark them as Archived'

    def handle(self, *args, **options):

        media_root = settings.MEDIA_ROOT
        remove = timezone.now() - timedelta(days=90)

        portfolios = Portfolio.available_objects.filter(status='Certified', status_changed__lte=remove)

        for portfolio in portfolios:
            user = portfolio.user
            directory = media_root + '/' + str(user.id)
            shutil.rmtree(directory)
            portfolio.status = 'Archived'
            portfolio.save()
            self.stdout.write(self.style.SUCCESS('Successfully removed user directory for %s' % user))
