# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE


from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from program.models import CustomCompetency
from utils.models import MetaData


class FormTargetedSkill(forms.Form):
    """
    Form to set targeted skill for an achievement
    """

    def __init__(self, skill_list, *args, **kwargs):
        super(FormTargetedSkill, self).__init__(*args, **kwargs)

        competencies = (('NONE', _('Select Targeted Skill')),)
        for skill in skill_list:
            skill_label = str(skill.code) + '. ' + skill.name

            # if IS PARENT (in 2-level competencies, only parent skill can be selected)
            if not skill.parent:
                ajout = ((skill.id, skill_label),)
                competencies = competencies + ajout

        self.fields['targeted_skill'] = forms.ChoiceField(
            label=_('Targeted skill'),
            required=False,
            choices=competencies,
            widget=forms.Select(attrs={'onchange': ("setTargetedSkill();"
                                                    "$('#changeTargetCompetence').modal('hide');")})
        )


class FormDevelopmentLevel(forms.Form):
    """
    Form to set development level for targeted skill
    """

    def __init__(self, *args, **kwargs):
        super(FormDevelopmentLevel, self).__init__(*args, **kwargs)

        levels = (
            ('NONE', _('Select development level')),
            (1, _('1. Mastery')),
            (2, _('2. Expertise')),
            (3, _('3. Excellence')),
        )

        self.fields['development_level'] = forms.ChoiceField(
            label=_('Development level'),
            required=False,
            choices=levels,
            widget=forms.Select(attrs={'onchange': ("setDevelopmentLevel();"
                                                    "$('#changeDevelopmentLevel').modal('hide');")})
        )



# TODO NOT USED; kept there for reference in case we need to reuse some of this logic for displaying
#   2-level hierarchy of skills in Django dropdown models
class FormTargetedSkillHierarchical(forms.Form):
  """
  Form to set targeted skill for an achievement
  """

  def __init__(self, skill_list, *args, **kwargs):
    super(FormTargetedSkillHierarchical, self).__init__(*args, **kwargs)

    competencies = list([('NONE', _('Select Targeted Skill'))])

    # allow for optional new hierarchical structure of original skills (competency)
    groups = {}

    # helper function to create parent group if it does not exist to prevent errors if child is found
    # before its parent in skill_list
    def create_group(_skill):
      if not groups.get(_skill.id):
        _skill_label = str(_skill.code) + '. ' + _skill.name
        groups[_skill.id] = list([_skill_label, list()])

    for skill in skill_list:
      skill_label = str(skill.code) + '. ' + skill.name

      # if IS PARENT
      if not skill.parent:
        # 1 - look for children
        children = [x for x in skill_list if x.parent and x.parent.id == skill.id]
        # if HAS children, then create entry w/ list as child
        if len(children) > 0:
          create_group(skill)
        # if NO children, then simple tuple/list of ID + label
        else:
          groups[skill.id] = list([skill.id, skill_label])
      # if IS CHILD, append tuple (id+label) to proper group based on parent.id
      else:
        create_group(skill.parent)
        groups[skill.parent.id][1].append(list([skill.id, skill_label]))

    # iterate over the groups we just filled in, and populate the final <select> choices
    for grp_key in sorted(groups.keys()):
      competencies.append(groups.get(grp_key))

    self.fields['targeted_skill'] = forms.ChoiceField(
      label=_("Targeted skill"),
      required=False,
      choices=competencies,
      widget=forms.Select(attrs={'onchange': ("setTargetedSkill();"
                                              "$('#changeTargetCompetence').modal('hide');")})
    )


class FormAchievementVersionApplicant(forms.Form):
    """
    Applicant form to register an achievement version
    """

    achievement_id = forms.CharField(widget=forms.HiddenInput())
    status = forms.CharField(widget=forms.HiddenInput(),
                             required=False,
                             initial='Submitted')
    logMessage = forms.CharField(required=False,
                                 widget=forms.Textarea,
                                 label=_('Log message (optional)'))


class FormAchievementVersionReviewer(forms.Form):
    """
    Reviewer form to register an achievement version
    """

    STAT_LIST = (('Select status', _('Select status')),)

    achievement_id = forms.CharField(widget=forms.HiddenInput())
    portfolio_id = forms.CharField(widget=forms.HiddenInput())
    status = forms.ChoiceField(required=True,
                               label=_('Status'),
                               choices=MetaData.STATUS_REVIEWER)
    logMessage = forms.CharField(required=True,
                                 widget=forms.Textarea,
                                 label=_('Log message'))

class FormQuestionnaireMotivationUQAM(forms.Form):
    """
    Questionnaire for achievement motivation
    """
    A1 = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={'rows': 1,
                                     'onfocus': 'setCurrentQuestion("A1")',
                                     'onblur': 'saveQuestionnaire("A1")'}),
        label="1. Quel titre ai-je envie de donner à cette réalisation professionnelle ? "
    )
    A2 = forms.CharField(
        required=False,
        max_length=700,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("A2")',
                                     'onblur': 'saveQuestionnaire("A2")'}),
        label=("2. Pour quelles raisons cette réalisation a-t-elle sa place dans mon portfolio ? "
               "Qu’est-ce que j’en ai retiré ?")
    )


class FormQuestionnaireMotivation(forms.Form):
    """
    Questionnaire for achievement motivation
    """
    A1 = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={'rows': 1,
                                     'onfocus': 'setCurrentQuestion("A1")',
                                     'onblur': 'saveQuestionnaire("A1")'}),
        label="1. Quel titre ai-je envie de donner à cette réalisation professionnelle ? "
    )


class FormQuestionnaireContext(forms.Form):
    """
    Questionnaire for achievement context
    """
    B3 = forms.CharField(
        required=False,
        max_length=100,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B3")',
                                     'onblur': 'saveQuestionnaire("B3")'}),
        label=("3. En quelle année se déroule la réalisation, "
               "pour qui je travaille à ce moment et depuis combien de temps ?")
    )
    B4 = forms.CharField(
        required=False,
        max_length=100,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B4")',
                                     'onblur': 'saveQuestionnaire("B4")'}),
        label="4. De qui émane cette tâche ? Est-ce une initiative personnelle ou une commande d’un supérieur ?"
    )
    B5 = forms.CharField(
        required=False,
        max_length=200,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B5")',
                                     'onblur': 'saveQuestionnaire("B5")'}),
        label=("5. En quelques mots, en quoi consistera la réalisation : Quelle en sera la principale tâche ? "
               "Quel est l’objectif poursuivi ? Combien de temps dure cette réalisation ?")
    )
    B6 = forms.CharField(
        required=False,
        max_length=200,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B6")',
                                     'onblur': 'saveQuestionnaire("B6")'}),
        label="6. En quoi cette tâche constitue-t-elle un défi en termes de complexité, de nouveauté ou d’ampleur ?"
    )
    B7 = forms.CharField(
        required=False,
        max_length=200,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B7")',
                                     'onblur': 'saveQuestionnaire("B7")'}),
        label=("7. Quelles seront les principales contraintes auxquelles je dois faire face ? "
               "(Une contrainte n’est pas liée à la tâche elle-même, mais au contexte)")
    )
    B8 = forms.CharField(
        required=False,
        max_length=200,
        widget=forms.Textarea(attrs={'rows': 2,
                                     'onfocus': 'setCurrentQuestion("B8")',
                                     'onblur': 'saveQuestionnaire("B8")'}),
        label=("8. Quelles ressources suis-je amené(e) à mobiliser ? "
               "Il peut s’agir de ressources externes (collègues / outils / documentation) "
               "et/ou de ressources internes (qualités / forces / valeurs / attitudes)")
    )
    # B9 = FormTargetedSkill()


class FormQuestionnaireDebut(forms.Form):
    """
    Questionnaire for achievement development start
    """
    C10 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C10")',
                                     'onblur': 'saveQuestionnaire("C10")'}),
        label="10. Comment cette réalisation débute-t-elle de mon point de vue ? Quel est l’élément déclencheur ?"
    )
    C11 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C11")',
                                     'onblur': 'saveQuestionnaire("C11")'}),
        label=("11. À ce moment, quelles sont mes premières impressions ? "
               "Que se passe-t-il dans ma tête ? Qu’est-ce que je me dis ?")
    )
    C12 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C12")',
                                     'onblur': 'saveQuestionnaire("C12")'}),
        label="12. Qu’est-ce que cette situation a de particulier ou d’inhabituel ?"
    )
    C13 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C13")',
                                     'onblur': 'saveQuestionnaire("C13")'}),
        label="13. Quelles sont mes premières actions ?"
    )


cyclic = {
    2: "Combien de temps s’est déroulé depuis le précédent moment clé (en minutes, heures, jours, semaines) ?",
    3: "Qu’est-ce qui me revient principalement de ce moment ?",
    4: ("À quoi est-ce que je pense à ce moment ? Qu’est-ce que je me dis ? "
        "Quelle décision suis-je amené(e) à prendre ?"),
    5: "Quelle est l’action décisive que je pose à la suite de cette décision ?",
    6: ("À quoi je fais attention lorsque je pose cette action ? "
        "De quelle façon suis-je amené(e) à ajuster mon action à cette situation spécifique ?"),
    7: ("Quel savoir (connaissance) et/ou savoir-être (attitude professionnelle) "
        "suis-je alors amené(e) à développer ou à approfondir ?"),
    8: "Quel constat suis-je alors en mesure de faire de la situation ?",
}


class FormQuestionnaireM1(forms.Form):
    """
    Questionnaire for achievement development first moment
    """
    C14 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C14")',
                                     'onblur': 'saveQuestionnaire("C14")'}),
        label="14. Que se passe-t-il lors de ce premier moment formateur ?"
    )
    C15 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C15")',
                                     'onblur': 'saveQuestionnaire("C15")'}),
        label=("15. Combien de temps s’est déroulé depuis le début de la réalisation "
               "(en minutes, heures, jours, semaines) ?")
    )
    C16 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C16")',
                                     'onblur': 'saveQuestionnaire("C16")'}),
        label="16. " + cyclic[3]
    )
    C17 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C17")',
                                     'onblur': 'saveQuestionnaire("C17")'}),
        label="17. " + cyclic[4]
    )
    C18 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C18")',
                                     'onblur': 'saveQuestionnaire("C18")'}),
        label="18. " + cyclic[5]
    )
    C19 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C19")',
                                     'onblur': 'saveQuestionnaire("C19")'}),
        label="19. " + cyclic[6]
    )
    C20 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C20")',
                                     'onblur': 'saveQuestionnaire("C20")'}),
        label="20. " + cyclic[7]
    )
    C21 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C21")',
                                     'onblur': 'saveQuestionnaire("C21")'}),
        label="21. " + cyclic[8]
    )


class FormQuestionnaireM2(forms.Form):
    """
    Questionnaire for achievement development second moment
    """
    C22 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C22")',
                                     'onblur': 'saveQuestionnaire("C22")'}),
        label="22. Que se passe-t-il lors de ce second moment formateur ?"
    )
    C23 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C23")',
                                     'onblur': 'saveQuestionnaire("C23")'}),
        label="23. " + cyclic[2]
    )
    C24 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C24")',
                                     'onblur': 'saveQuestionnaire("C24")'}),
        label="24. " + cyclic[3]
    )
    C25 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C25")',
                                     'onblur': 'saveQuestionnaire("C25")'}),
        label="25. " + cyclic[4]
    )
    C26 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C26")',
                                     'onblur': 'saveQuestionnaire("C26")'}),
        label="26. " + cyclic[5]
    )
    C27 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C27")',
                                     'onblur': 'saveQuestionnaire("C27")'}),
        label="27. " + cyclic[6]
    )
    C28 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C28")',
                                     'onblur': 'saveQuestionnaire("C28")'}),
        label="28. " + cyclic[7]
    )
    C29 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C29")',
                                     'onblur': 'saveQuestionnaire("C29")'}),
        label="29. " + cyclic[8]
    )


class FormQuestionnaireM3(forms.Form):
    """
    Questionnaire for achievement development third moment
    """
    C30 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C30")',
                                     'onblur': 'saveQuestionnaire("C30")'}),
        label="30. Que se passe-t-il lors de ce dernier moment formateur ?"
    )
    C31 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C31")',
                                     'onblur': 'saveQuestionnaire("C31")'}),
        label="31. " + cyclic[2]
    )
    C32 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C32")',
                                     'onblur': 'saveQuestionnaire("C32")'}),
        label="32. " + cyclic[3]
    )
    C33 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C33")',
                                     'onblur': 'saveQuestionnaire("C33")'}),
        label="33. " + cyclic[4]
    )
    C34 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C34")',
                                     'onblur': 'saveQuestionnaire("C34")'}),
        label="34. " + cyclic[5]
    )
    C35 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C35")',
                                     'onblur': 'saveQuestionnaire("C35")'}),
        label="35. " + cyclic[6]
    )
    C36 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C36")',
                                     'onblur': 'saveQuestionnaire("C36")'}),
        label="36. " + cyclic[7]
    )
    C37 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C37")',
                                     'onblur': 'saveQuestionnaire("C37")'}),
        label="37. " + cyclic[8]
    )


class FormQuestionnaireBilan(forms.Form):
    """
    Questionnaire for achievement development appraisal
    """
    C38 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C38")',
                                     'onblur': 'saveQuestionnaire("C38")'}),
        label="38. Comment se termine cette réalisation ? Quelle en est la conclusion ?"
    )
    C39 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C39")',
                                     'onblur': 'saveQuestionnaire("C39")'}),
        label="39. Comment en jugez-vous la réussite ? À partir de quels critères, de quelles informations ?"
    )
    C40 = forms.CharField(
        required=False,
        max_length=300,
        widget=forms.Textarea(attrs={'rows': 5,
                                     'onfocus': 'setCurrentQuestion("C40")',
                                     'onblur': 'saveQuestionnaire("C40")'}),
        label=("40. Que retenez-vous principalement de cette expérience ? "
               "Quels impacts a-t-elle eu pour la suite de ma pratique ?")
    )


def get_facet_choices():
    # UQAR: get facets for this achievement's development level
    facets = CustomCompetency.available_objects.filter(
        type__code="facets",
    )
    facet_choices = (
        ('', _('Choose ...')),
    )
    for facet in facets:
        id_parts = facet.id.split(".")
        len_id_parts = len(id_parts)

        if len_id_parts == 2:
            facet_choices += ( (_(mark_safe(facet.id[2] + '. ' + facet.name)), []), )
        elif len_id_parts == 3:
            for facet_choice in facet_choices:
                if facet_choice[0].startswith(facet.id[2]):
                    facet_choice[1].append( (facet.id, _(mark_safe(facet.id[2:] + '. ' + facet.name))) )
                    break

    return facet_choices


class FormProgressAssessment(forms.Form):
    """
    Form used by expert/supervisor to assess a candidate's competency (progress assessment)
    """

    # field is hidden because it is only used in the backend to differentiate which form (progress or summative) is being submitted
    form_progress_assessment = forms.BooleanField(
        widget=forms.HiddenInput,
        initial=True
    )

    progress_candidate_level = forms.ChoiceField(
        required=True,
        label=_("1. Facet of development level targeted by the candidate"),
        widget=forms.Select(),
    )

    progress_expert_level = forms.ChoiceField(
        required=True,
        label=_("2. Facet of development level targeted by the subject expert (if possible at this point in the assessment)"),
        widget=forms.Select(),
    )

    facet_discoverability = forms.ChoiceField(
        required=True,
        label=_("3. Can I identify the meaning of the development facet and its indicators in the text?"),
        choices=(
            ('', _('Choose ...')),
            ('no', _('No')),
            ('partly', _('In part')),
            ('yes', _('Yes')),
        ),
        widget=forms.Select(),
    )

    easy_indicators = forms.CharField(
        required=True,
        label=_('4. Write in this space the elements of meaning and indicators that you identify in the work situation(s) described and selected.'),
        widget=forms.Textarea(attrs={'rows': 4}),
    )

    hard_indicators = forms.CharField(
        required=True,
        label=_('5. In this space, write down the elements of meaning and indicators that you find most difficult to identify in the work situation(s) described and selected. Then, formulate some open-ended questions to ask the candidate in order to obtain more information that could facilitate your assessment.'),
        widget=forms.Textarea(attrs={'rows': 4}),
    )

    other_questions = forms.CharField(
        required=True,
        label=_('6. In this space, draft additional questions to ask the candidate to facilitate your evaluation.'),
        widget=forms.Textarea(attrs={'rows': 4}),
    )

    def __init__(self, selected_competency, view_permission, edit_permission, *args, **kwargs):
        super(FormProgressAssessment, self).__init__(*args, **kwargs)

        self.fields['progress_candidate_level'].choices = get_facet_choices()
        self.fields['progress_expert_level'].choices = get_facet_choices()

        # set initial value for each field
        if selected_competency and view_permission:
            self.fields['progress_candidate_level'].initial = selected_competency.progress_candidate_level
            self.fields['progress_expert_level'].initial = selected_competency.progress_expert_level
            self.fields['facet_discoverability'].initial = selected_competency.progress_facet_discoverability
            self.fields['easy_indicators'].initial = selected_competency.progress_easy_indicators
            self.fields['hard_indicators'].initial = selected_competency.progress_hard_indicators
            self.fields['other_questions'].initial = selected_competency.progress_other_questions
        else:
            self.fields['progress_candidate_level'].initial = ''
            self.fields['progress_expert_level'].initial = ''
            self.fields['facet_discoverability'].initial = ''
            self.fields['easy_indicators'].initial = ''
            self.fields['hard_indicators'].initial = ''
            self.fields['other_questions'].initial = ''

        # disable fields according to user permissions
        self.fields['progress_candidate_level'].disabled = not edit_permission
        self.fields['progress_expert_level'].disabled = not edit_permission
        self.fields['facet_discoverability'].disabled = not edit_permission
        self.fields['easy_indicators'].disabled = not edit_permission
        self.fields['hard_indicators'].disabled = not edit_permission
        self.fields['other_questions'].disabled = not edit_permission


class FormSummativeAssessment(forms.Form):
    """
    Form used by expert/supervisor to assess a candidate's competency (summative assessment)
    """

    # field is hidden because it is only used in the backend to differentiate which form (progress or summative) is being submitted
    form_summative_assessment = forms.BooleanField(
        widget=forms.HiddenInput,
        initial=True
    )

    summative_candidate_level = forms.ChoiceField(
        required=True,
        label=_("1. Facet of development level targeted by the candidate"),
        widget=forms.Select(),
    )

    summative_expert_level = forms.ChoiceField(
        required=True,
        label=_("2. Facet of the development level targeted by the subject expert"),
        widget=forms.Select(),
    )

    credits = forms.ChoiceField(
        required=True,
        label=_("3. Granted credits"),
        choices=(
            ('', _('Choose ...')),
            ('2', '2'),
            ('4', '4'),
            ('6', '6')
        ),
        widget=forms.Select(),
    )

    comments = forms.CharField(
        required=True,
        label=_('4. Comments'),
        widget=forms.Textarea(attrs={'rows': 4}),
    )

    def __init__(self, selected_competency, view_permission, edit_permission, *args, **kwargs):
        super(FormSummativeAssessment, self).__init__(*args, **kwargs)

        self.fields['summative_candidate_level'].choices = get_facet_choices()
        self.fields['summative_expert_level'].choices = get_facet_choices()

        # initial value per field
        if selected_competency and view_permission:
            self.fields['summative_candidate_level'].initial = selected_competency.progress_candidate_level
            self.fields['summative_expert_level'].initial = selected_competency.summative_expert_level
            self.fields['credits'].initial = selected_competency.summative_credits
            self.fields['comments'].initial = selected_competency.summative_comments
        else:
            self.fields['summative_candidate_level'].initial = ''
            self.fields['summative_expert_level'].initial = ''
            self.fields['credits'].initial = ''
            self.fields['comments'].initial = ''

        # disable fields according to user permissions
        self.fields['summative_candidate_level'].disabled = not edit_permission
        self.fields['summative_expert_level'].disabled = not edit_permission
        self.fields['credits'].disabled = not edit_permission
        self.fields['comments'].disabled = not edit_permission