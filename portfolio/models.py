# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import locale
import os.path
import uuid
from typing import List
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from utils.models import MetaData, UserType
from program.models import Program, Competency, CustomCompetencyType, Concentration, CustomCompetency


# required by [XFormField]
class XFieldType(models.TextChoices):
    LONG_TEXT = 'LONG_TEXT', _('Long text')
    SHORT_TEXT = 'SHORT_TEXT', _('Short text')
    NUMBER = 'NUMBER', _('Number')


class AssessmentStatuses(models.TextChoices):
    CREATED = 'Created'
    VALIDATED = 'Validated'
    FINAL = 'Final'
    UNDER_REVIEW = 'Underreview'


class PortfolioStatuses(models.TextChoices):
    DRAFT = 'Draft'
    FINAL = 'Final'
    UNDER_REVIEW = 'Underreview'
    PRE_REVIEWED = 'PreReviewed'
    PRE_CERTIFIED = 'PreCertified'
    REVIEWED = 'Reviewed'
    CERTIFIED = 'Certified'


#
# Type definitions : Achievement
#

class AchievementTemplate(MetaData):
    """
    A portfolio is a collection of achievements
    The portfolio is always linked to a program
    An applicant can have only one portfolio at any given time
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        verbose_name=_('Type name'),
        max_length=255
    )
    number = models.IntegerField(
        default=0,  # For setting first achievement's number
        verbose_name=_('Type number'),
    )
    tags_options = models.ManyToManyField(
        CustomCompetencyType,
        verbose_name=_('Editor options / tags'),
        blank=True,
        help_text=_('Which custom competency types to show as tags in text editor <br/>')
    )
    has_show_tags = models.BooleanField(
        verbose_name=_('Allow \'Show Tags\' button'),
        help_text=_('Allow user to toggle visibility of tags'),
        default=True
    )

    class Meta:
        ordering = ['number']
        verbose_name = _('Achievement Template')
        verbose_name_plural = _('Achievement Templates')

    def __str__(self):
        return str(self.number) + '. ' + self.name


class EditorBlock(MetaData):
    """
    Represents an individual Quill editor block w/ instruction and expected word count;
    Modes: milestone & continuous
    """
    number = models.IntegerField(
        default=0,
        verbose_name=_('Editor block number'),
    )
    instruction = models.TextField(
        default='',
        blank=True,
        verbose_name=_('Instruction')
    )
    explanation = models.TextField(
        verbose_name=_('Explanation'),
        blank=True
    )
    target_words = models.CharField(
        verbose_name=_('Expected word count'),
        max_length=255
    )
    tags_options = models.ManyToManyField(
        CustomCompetencyType,
        verbose_name=_('Editor options / tags'),
        blank=True,
        help_text=_('Which custom competency types to show as tags for this question <br/>')
    )
    assignment = models.IntegerField(
        default=1,
        verbose_name=_('Group assignment'),
        help_text=_('Relates to Group.assignment')
    )
    milestone = models.IntegerField(
        default=1,
        verbose_name=_('Group milestone'),
        help_text=_('Relates to Group.milestone (tabs in achievement type)')
    )
    has_example = models.BooleanField(default=False)

    class Meta:
        ordering = ['milestone', 'number']
        verbose_name = _('Editor Block')
        verbose_name_plural = _('Editor Blocks')

    def __str__(self):
        return mark_safe(str(self.milestone) + '-' + str(self.number) + ' - ' + self.instruction)

    def get_comp_types(self) -> list[str]:
        tags_opts: List[CustomCompetencyType] = self.tags_options.all()
        return sorted(list(map(lambda x: x.code, tags_opts)))


class AchievementType(MetaData):
    """
    MODE=MILESTONE
    Represents a type of achievement that many users will use w/ specified editor blocks and Quill options template
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    number = models.IntegerField(
        default=0,  # For setting first achievement's number
        verbose_name=_('Achievement type number'),
    )
    name = models.TextField(
        verbose_name=_('Achievement type name'),
    )
    sub_title = models.TextField(
        verbose_name=_('Achievement sub-title'),
        blank=True,
        null=True,
    )
    template = models.ForeignKey(
        AchievementTemplate,
        verbose_name=_('Template to be used for this achievement'),
        blank=False,
        null=True,
        default=None,
        on_delete=models.CASCADE
    )
    blocks = models.ManyToManyField(
        EditorBlock,
        verbose_name=_('Editor blocks'),
        blank=True,
    )
    targeted_competency = models.ForeignKey(
        Competency,
        verbose_name=_('Targeted skill'),
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

    # TODO Delete these items ?
    # questionnaire = models.OneToOneField(
    #     Questionnaire,
    #     null=True,
    #     blank=True,
    #     on_delete=models.SET_NULL,
    #     verbose_name=_('Questionnaire'),
    # )
    # version = models.IntegerField(
    #     default=0,
    #     verbose_name=_('Achievement version'),
    # )

    class Meta:
        ordering = ['number']
        verbose_name = _('Achievement Type')
        verbose_name_plural = _('Achievement Types')

    def __str__(self):
        return str(self.number) + '. ' + self.name

    def editor_blocks(self):
        return self.blocks.all()

    # IMPORTANT: #2 can be used to check if achievement ALLOWS specific competency (tag) EVEN if used in ONLY
    #   a single editor block; otherwise, 'template.tags_options' could have specific tags, BUT individual
    #   editor blocks could allow specific tags in addition, which would be missed at achievement level.
    def _comp_types_objects(self):
        # 1 - get the tags for the main achievement type (convert QuerySet to List so 'append' will work)
        tags_opts: List[CustomCompetencyType] = list(self.template.tags_options.all())
        # 2 - get the tags for each block associated with this achievement type
        for block in self.blocks.all():
            tags_opts.extend(block.tags_options.all())
        return tags_opts

    # Get allowed competency types (slug) ONLY FOR TEMPLATE, EXCLUDING EditorBlock children
    def get_comp_types_template(self):
        tags_opts: List[CustomCompetencyType] = list(self.template.tags_options.all())
        return sorted(set(map(lambda x: x.code, tags_opts)))

    # slugs: includes parent template tags + tags in any of the editor blocks in that achievement
    def get_comp_types(self):
        return sorted(set(map(lambda x: x.code, self._comp_types_objects())))

    # ids: same as `get_comp_types` except ID vs SLUG
    def get_comp_types_ids(self):
        return sorted(set(map(lambda x: x.id, self._comp_types_objects())))


#
# Type definitions : Assessment
#

class AssessmentRating(MetaData):
    """
    RATING_CHOICES = (
        (1, _('Obvious | 100%')),
        (0.8, _('Revealing | 80%')),
        (0.6, _('Emerging | 60%')),
        (0.55, _('Insufficient | 55%')),
        (0, _('Missing | 0%')),
    )
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    weight = models.FloatField(
        default=0.0,
        verbose_name=_('Rating weight')
    )
    description = models.CharField(
        max_length=15,
        verbose_name=_('Rating description')
    )

    class Meta:
        ordering = ['-weight']
        verbose_name = _('(M) Assessment rating')
        verbose_name_plural = _('(M) Assessment ratings')

    def base100(self):
        return format(self.weight * 100, '.0f')

    def __str__(self):
        return self.description + ' | ' + self.base100() + '%'


class Assessment(MetaData):
    """

    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    milestone = models.IntegerField(
        default=1,
        verbose_name=_('Milestone of this assessment')
    )
    """Parent = grade-group : a, b, c, ...
        Defines Grade weight and relevant achievements
    Child = rubric : a1, a2, b1, c1, ...
        Defines assessment's object, indicators and criterias
    """
    code = models.CharField(
        max_length=5,
        verbose_name=_('Assessment number')
    )
    weight = models.FloatField(
        verbose_name=_('Decimal weight'),
        null=True,
        blank=True,
    )
    assessment_object = models.TextField(
        # Parent = Theme description
        verbose_name=_('Assessment object'),
        blank=True,
        default='',
    )
    assessment_indicator = models.TextField(
        # Parent = Targeted achievements
        verbose_name=_('Assessment indicators'),
        blank=True,
        default='',
    )
    # This might eventually be a ManyToMany Field :
    # class Criteria
    #   FK -> AssessmentRating
    criteria_100 = models.TextField(
        verbose_name=_('Obvious assessment criteria'),
        default='',
        blank=True,
    )
    criteria_80 = models.TextField(
        verbose_name=_('Revealing assessment criteria'),
        default='',
        blank=True,
    )
    criteria_60 = models.TextField(
        verbose_name=_('Emerging assessment criteria'),
        default='',
        blank=True,
    )
    criteria_55 = models.TextField(
        verbose_name=_('Insufficient assessment criteria'),
        default='',
        blank=True,
    )
    criteria_0 = models.TextField(
        verbose_name=_('Absent assessment criteria'),
        default='',
        blank=True,
    )
    parent = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        verbose_name=_('Parent assessment'),
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['milestone', 'code']
        verbose_name = _('(M) Assessment')
        verbose_name_plural = _('(M) Assessments')

    def __str__(self):
        return str(self.milestone) + "-" + self.code + " : " + self.assessment_object

    def base100(self):
        if self.weight:
            return format(self.weight * 100, '.0f')


#
# Type definitions : Portfolio
#

class DocumentType(MetaData):
    """
    This approach vs private choices class lets portfolio admin defines his own types and
    avoid the problem of having either to force specific naming convention
    or having redundant types with similar names predefined
    It also associates specific documents to specific milestones

    UQAM :
        CV / Ligne de vie / Fiche de préparation à l'entretien
    UQAR :
        Assermentation
        Artéfact / Preuve
    CEMP :
        Carte d’enregistrement de l’OTIMROEPMQ / Certification du lavage des mains
        Normes et politiques / Preuve / Référence personnelle
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        max_length=255,
        blank=False,
        verbose_name=_('Name'),
    )
    milestone = models.IntegerField(
        default=1,
        verbose_name=_('Milestone of this document'),
        help_text=_('Specify the milestone relevant to this specific document'),
    )
    is_specific = models.BooleanField(
        verbose_name=_('Is a specific document?'),
        help_text=_('Whether the document type has a specific, pre-defined function in the portfolio (ex: CV)'),
        default=False,
    )
    url = models.CharField(
        max_length=255,
        default='',
        blank=True,
        verbose_name=_('Form URL'),
        help_text=_('The URL of the form linked to this type'),
    )
    accepted_format = models.CharField(
        max_length=255,
        default='',
        blank=True,
        verbose_name=_('Accepted file format for this specific document'),
        help_text=_('An optional coma-separated list of allowed file extensions (ex: "doc,jpg,pdf") \
                    for this specific document type'),
    )

    class Meta:
        ordering = ['-milestone', 'is_specific', 'name']
        verbose_name = _('Document type')
        verbose_name_plural = _('Document types')

    def __str__(self):
        return self.name


class HelpLink(MetaData):
    """
    Represent help link displayed in different pages via the '?' icon.
    MODE: GENERAL - applies to both milestone & continuous modes
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    keyword = models.CharField(
        max_length=255,
        verbose_name=_('Help keyword'),
        default='',
    )
    link = models.CharField(
        max_length=255,
        verbose_name=_('Help link'),
        default='',
        blank=True,
    )

    class Meta:
        ordering = ['keyword']
        verbose_name = _('Help link')
        verbose_name_plural = _('Help links')

    def __str__(self):
        return f'{self.keyword} : {self.link}'


class PortfolioType(MetaData):
    """
    TODO adjust the description of PortfolioType to reflect new usage
    A portfolio is a collection of achievements
    The portfolio is always linked to a program
    An applicant can have only one portfolio at any given time
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )

    class Flow(models.TextChoices):
        CONTINUOUS = 'Continuous', _('Continuous')
        MILESTONE = 'Milestone', _('Milestone')

    flow = models.CharField(
        max_length=30,
        choices=Flow.choices,
        default=Flow.CONTINUOUS,
        verbose_name=_('Portfolio flow mode'),
    )
    institution = models.CharField(
        max_length=255,
        default='',
        blank=True,
        verbose_name=_('Name of institution'),
        help_text=_('Name of the institution where this portfolio type is used (e.g. UQAM, UQAR, etc.)')
    )
    curriculum_name = models.CharField(
        max_length=255,
        default='',
        blank=True,
        verbose_name=_('Full name of curriculum'),
    )
    curriculum_code = models.CharField(
        max_length=16,
        default='',
        blank=True,
        verbose_name=_('Code name of curriculum'),
    )
    achievement_name = models.CharField(
        max_length=150,
        default='achievement',
        verbose_name=_('Name of achievements in this model'),
    )
    num_milestones = models.IntegerField(
        default=1,
        verbose_name=_('Number of milestones'),
    )
    document_links = models.BooleanField(
        default=False,
        verbose_name=_('Can documents be linked to specific achievements ?')
    )
    share_portfolio = models.CharField(
        max_length=30,
        choices=UserType.choices,
        default=UserType.NONE,
        verbose_name=_('Which user type can share portfolio'),
    )
    credits_editable = models.CharField(
        max_length=30,
        choices=UserType.choices,
        default=UserType.APPLICANT,
        verbose_name=_('Which user type can set credits goal'),
    )

    class ReviewerScope(models.TextChoices):
        APPLICANT = 'Applicant', _('Applicant')
        GROUP = 'Group', _('Group')

    reviewer_scope = models.CharField(
        max_length=30,
        choices=ReviewerScope.choices,
        default=ReviewerScope.APPLICANT,
        verbose_name=_('Reviewer sees only attributed applicants or whole group'),
    )

    class TargetedSkills(models.TextChoices):
        TARGET = 'Target', _('Target')
        LEVEL = 'Level', _('Level')
        NONE = 'None', _('None')

    targeted_skills = models.CharField(
        max_length=30,
        choices=TargetedSkills.choices,
        default=TargetedSkills.NONE,
        verbose_name=_('How targeted skills are managed'),
    )
    user_data = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_('Ordered fieldnames expected at group creation')
    )

    class NameFormat(models.TextChoices):
        COLUMN = 'Column', _('Family name and first each in a column')
        COMMA = 'Comma', _('Family name and first name separated by a comma')
        SPACE = 'Space', _('First name and family name separated by a space')

    name_format = models.CharField(
        max_length=30,
        choices=NameFormat.choices,
        default=NameFormat.COLUMN,
        verbose_name=_('User names list format'),
    )

    """
    applicant_training = URL / 'view' / ''
    """
    applicant_training = models.CharField(
        default='',
        verbose_name=_("Link to applicants' training"),
    )

    #
    # TODO The following attributes and methods are superfluous in the context of one implementation / type
    #
    # each portfolio is associated with one or more competency types
    competency_types = models.ManyToManyField(
        CustomCompetencyType,
        verbose_name=_('Related competency types')
    )
    achievement_types = models.ManyToManyField(
        AchievementType,
        verbose_name=_('Related achievement types')
    )
    document_types = models.ManyToManyField(
        DocumentType,
        blank=True,
    )
    assessment_ratings = models.ManyToManyField(
        AssessmentRating,
        blank=True,
        verbose_name=_('Related assessment ratings')
    )
    policy_expert = models.TextField(
        default='no_experts',
        blank=True,
        verbose_name=_('Expert policy'),
        help_text=_('Privacy and/or conflict-of-interest policy with regards to student\'s portfolio.')
    )
    policy_applicant = models.TextField(
        default='',
        blank=True,
        verbose_name=_('Applicant policy'),
        help_text=_('Any policy the applicant must consent to.')
    )
    policy_reviewer = models.TextField(
        default='no_reviewers',
        blank=True,
        verbose_name=_('Reviewer policy'),
        help_text=_('Any policy the reviewer must consent to.')
    )

    class Meta:
        ordering = ['id', 'institution', 'curriculum_name', 'curriculum_code']
        verbose_name = _('Portfolio Type')
        verbose_name_plural = _('Portfolio Types')

    def __str__(self):
        return str(self.institution or self.curriculum_name or self.curriculum_code or self.id)

    def get_achievement_types(self):
        """
        return  list of achievements types objects
        """
        return self.achievement_types.all()

    def get_assessment_ratings(self):
        """
        Returns: list of tuples (choices)
        """
        ratings = []
        for assessment_rating in self.assessment_ratings.all():
            ratings.append((assessment_rating.weight, str(assessment_rating)))
        return ratings


#
# Portfolio objects : Portfolio
#

class Portfolio(MetaData):
    """
    A portfolio is a collection of achievements
    The portfolio is always linked to a program
    An applicant can have only one portfolio at any given time
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    program = models.ForeignKey(
        Program,
        verbose_name=_('Program related to'),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    share = models.BooleanField(
        default=False,
        verbose_name=_('Can be shared as an example')
    )

    class Meta:
        ordering = ['program', '-modified']
        verbose_name = _('Portfolio')
        verbose_name_plural = _('Portfolios')

    def __str__(self):
        return str(self.program) + " - " + str(self.user)

    @property
    def concentration(self) -> Concentration:
        return self.program.concentration

    @property
    def expert_consent(self) -> bool:
        return self.user.expert_consent or False

    @property
    def reviewer_consent(self) -> bool:
        return self.user.reviewer_consent or False

    @property
    def personal_consent(self) -> bool:
        if self.user.personal_consent:
            return True
        else:
            return False

    def get_achievements(self, user_type):
        """
        param   user_type:str
        return  list of achievements objects
        """
        if user_type == 'Applicant':
            return Achievement.available_objects.filter(portfolio=self)
        elif user_type == 'Private':
            return Achievement.available_objects.filter(
                portfolio=self,
                number__gt=self.program.concentration.max_num_achievements
            )
        elif user_type == 'Level':
            return Achievement.available_objects.filter(portfolio=self).order_by('targeted_competency__code', 'number')
        else:
            return Achievement.available_objects.filter(
                portfolio=self,
                number__lte=self.program.concentration.max_num_achievements
            )


#
# Portfolio objects : Achievement
#

class QuestionnaireUQAM(MetaData):
    """
    TODO rename class (and db table) to 'AchievementQuestionnaire' to better indicate it's achievement-linked
    A questionnaire is linked to an achievement
    The questionnaire is created when an achievement is created
    The applicant can't access the editor for this achievement
        until the questionnaire is completed at which time
        its answers are transferred to the achievement
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )

    A1 = models.TextField(blank=False, default='')
    A2 = models.TextField(blank=False, default='')
    B3 = models.TextField(blank=False, default='')
    B4 = models.TextField(blank=False, default='')
    B5 = models.TextField(blank=False, default='')
    B6 = models.TextField(blank=False, default='')
    B7 = models.TextField(blank=False, default='')
    B8 = models.TextField(blank=False, default='')
    # B9 = f_target
    C10 = models.TextField(blank=True, default='')
    C11 = models.TextField(blank=True, default='')
    C12 = models.TextField(blank=True, default='')
    C13 = models.TextField(blank=True, default='')
    C14 = models.TextField(blank=True, default='')
    C15 = models.TextField(blank=True, default='')
    C16 = models.TextField(blank=True, default='')
    C17 = models.TextField(blank=True, default='')
    C18 = models.TextField(blank=True, default='')
    C19 = models.TextField(blank=True, default='')
    C20 = models.TextField(blank=True, default='')
    C21 = models.TextField(blank=True, default='')
    C22 = models.TextField(blank=True, default='')
    C23 = models.TextField(blank=True, default='')
    C24 = models.TextField(blank=True, default='')
    C25 = models.TextField(blank=True, default='')
    C26 = models.TextField(blank=True, default='')
    C27 = models.TextField(blank=True, default='')
    C28 = models.TextField(blank=True, default='')
    C29 = models.TextField(blank=True, default='')
    C30 = models.TextField(blank=True, default='')
    C31 = models.TextField(blank=True, default='')
    C32 = models.TextField(blank=True, default='')
    C33 = models.TextField(blank=True, default='')
    C34 = models.TextField(blank=True, default='')
    C35 = models.TextField(blank=True, default='')
    C36 = models.TextField(blank=True, default='')
    C37 = models.TextField(blank=True, default='')
    C38 = models.TextField(blank=True, default='')
    C39 = models.TextField(blank=True, default='')
    C40 = models.TextField(blank=True, default='')

    class Meta:
        ordering = ['A1']
        verbose_name = _('(UQAM) Achievement questionnaire')
        verbose_name_plural = _('(UQAM) Achievement questionnaires')

    def __str__(self):
        return self.A1


class Questionnaire(MetaData):
    """
    A questionnaire is linked to an achievement
    The questionnaire is created when an achievement is created
    The applicant can't access the editor for this achievement
        until the questionnaire is completed at which time
        its answers are transferred to the achievement
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )

    A1 = models.TextField(blank=False, default='')

    class Meta:
        ordering = ['A1']
        verbose_name = _('Achievement questionnaire')
        verbose_name_plural = _('Achievement questionnaires')

    def __str__(self):
        return self.A1


class Achievement(MetaData):
    """
    An achievement consists of
        a motivation for its relevance
        keys elements that compose its context
        a development in which the achievement is unfolded

    An achievement contains
        A name
        A number
        a list of its current development ops:
            [attributes, insert]

    An achievement contains a reference to
        its portfolio
        its targeted competency
        a questionnaire (until it is done and deleted)
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.TextField(
        verbose_name=_('Achievement name'),
    )
    number = models.IntegerField(
        default=0,  # For setting first achievement's number
        verbose_name=_('Achievement number'),
    )
    portfolio = models.ForeignKey(
        Portfolio,
        verbose_name=_('Portfolio that the achievement relates to'),
        blank=True,
        on_delete=models.CASCADE,
    )
    targeted_competency = models.ForeignKey(
        Competency,
        verbose_name=_('Targeted skill'),
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    development_level = models.IntegerField(
        verbose_name=_('Development level'),
        default=1,
        null=True,
        help_text=_('Level of mastery of the targeted skill (aka development level)')
    )
    questionnaire = models.OneToOneField(
        Questionnaire,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_('Questionnaire'),
    )
    ops = ArrayField(
        ArrayField(
            models.TextField(blank=True)
        ),
        null=True,
        blank=True,
        verbose_name=_('Achievement ops')
    )
    comments = models.JSONField(
        default=dict,
        blank=True,
        verbose_name=_('Comments ops')
    )
    version = models.IntegerField(
        default=0,
        verbose_name=_('Achievement version'),
    )

    class Meta:
        ordering = ['number']
        verbose_name = _('Achievement')
        verbose_name_plural = _('Achievements')

    def __str__(self):
        return str(self.number) + '. ' + self.name

    # MODE = CONTINUOUS
    def check_active_user(self, user):
        """
           Validate that an achievement has not been in use for at least the last 15 minutes
           :param user: User instance
           :return: array / bool, message / False
           """
        try:
            active_user = ActiveUser.available_objects.get(achievement=self)
        except ActiveUser.DoesNotExist:
            active_user = None

        if (
                active_user is not None
                and active_user.user is not None
                and active_user.user != user
        ):
            time_elapsed = timezone.now() - active_user.recent_change
            if time_elapsed.total_seconds() < 905:
                return [
                    _('Achievement under review'),
                    _('{user} is currently reviewing achievement {achievement}').format(user=active_user.user,
                                                                                        achievement=self),
                    _('Please come back in a little while'),
                    _('Ignore and open achievement anyway'),
                ]

        self.update_active_user(user)
        return False

    # MODE = CONTINUOUS
    # NOTE: had to rework this because when starting from scratch, if NO existing ActiveUser
    #   manually created, it crashes site w/ ActiveUser.DoesNotExist, so added try/catch to create new
    #   instance if does not exist
    def update_active_user(self, user=None):
        """
        Update Active User with User and Time
        or create it if it does not exist
        :param user:
        :return:
        """
        active_user, created = ActiveUser.available_objects.get_or_create(achievement=self)
        active_user.user = user
        active_user.recent_change = timezone.now()
        active_user.save()
        return active_user


class CompetencyAssessmentUQAR(MetaData):
    """
    This class is originally designed for the use case of UQAR. The assessment of a competency is done in two phases
    using two forms: FormProgressAssessment and FormSummativeAssessment. An instance of this class consists of the
    answers to these two forms. A competency assessment is associated with a portfolio and a competency.
    Note that the expert assesses competencies and not achievements. Since there can be multiple achievements per competency,
    theoretically, there are less competencies to evaluate than the number of achievements. The expert submits progress
    assessments for validation by supervisor in batch. The same is true for summative assessments. The expert can also
    save forms before submitting them to the next stage (Created -> Underreview). In such cases, the status of each kind
    of assessment is updated per competency, passing from Underreview to Final. The supervisor can edit the assessments
    if necessary and then submit them back to the expert for review (Final -> Underreview) or certify them to share them
    with the candidate (Final -> Validated).
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True
    )
    competency = models.ForeignKey(
        Competency,
        on_delete=models.CASCADE,
        verbose_name=_('Competency being assessed')
    )
    portfolio = models.ForeignKey(
        Portfolio,
        on_delete=models.CASCADE,
        null=True,
        verbose_name=_('Portfolio to which belongs this achievement assessment')
    )
    progress_status = models.CharField(
        verbose_name=_("Status of the progress assessment of this portfolio's achievement"),
        blank=True,
        choices=AssessmentStatuses.choices,
        default=AssessmentStatuses.CREATED
    )
    summative_status = models.CharField(
        verbose_name=_("Status of the summative assessment of this portfolio's achievement"),
        blank=True,
        choices=AssessmentStatuses.choices,
        default=AssessmentStatuses.CREATED
    )
    progress_candidate_level = models.CharField(
        verbose_name=_('Progress Assessment - Development level chosen by candidate'),
        default='',
        help_text=_('Level of mastery (aka development level) selected by candidate of the targeted skill')
    )
    progress_expert_level = models.CharField(
        verbose_name=_('Progress Assessment - Development level evaluated by expert'),
        default='',
        help_text=_('Evaluated level of mastery of the targeted skill (aka development level)')
    )
    progress_facet_discoverability = models.CharField(
        verbose_name=_('Progress Assessment - Facet discoverability'),
        blank=True,
        default=''
    )
    progress_easy_indicators = models.TextField(
        verbose_name=_('Progress Assessment - Indicators that are easy to identify'),
        blank=True,
        default=''
    )
    progress_hard_indicators = models.TextField(
        verbose_name=_('Progress Assessment - Indicators that are difficult to identify'),
        blank=True,
        default=''
    )
    progress_other_questions = models.TextField(
        verbose_name=_('Progress Assessment - Other questions to ask to candidate'),
        blank=True,
        default=''
    )
    summative_credits = models.IntegerField(
        verbose_name=_('Summative Assessment - Number of credits assigned to this achievement'),
        default=0
    )
    summative_candidate_level = models.CharField(
        verbose_name=_('Summative Assessment - Development level chosen by candidate'),
        default='',
        help_text=_('Level of mastery (aka development level) selected by candidate of the targeted skill')
    )
    summative_expert_level = models.CharField(
        verbose_name=_('Summative Assessment - Development level evaluated by expert'),
        default='',
        help_text=_('Evaluated level of mastery of the targeted skill (aka development level)')
    )
    summative_comments = models.TextField(
        verbose_name=_('Summative Assessment - Comments'),
        blank=True,
        default=''
    )

    class Meta:
        ordering = ['competency']
        verbose_name = _('(UQAR) Competency assessment')
        verbose_name_plural = _('(UQAR) Competency assessments')

    def __str__(self):
        return str(self.competency) + '. ' + self.portfolio.__str__()

    def get_targeted_facet(self):
        try:
            targeted_facet = CustomCompetency.available_objects.get(id=self.summative_candidate_level)
            return targeted_facet.id[2:] + '. ' + targeted_facet.name
        except CustomCompetency.DoesNotExist as e:
            return 'N/A'

    def get_evaluated_facet(self):
        try:
            evaluated_facet = CustomCompetency.available_objects.get(id=self.summative_expert_level)
            return evaluated_facet.id[2:] + '. ' + evaluated_facet.name
        except CustomCompetency.DoesNotExist as e:
            return 'N/A'


class ActiveUser(MetaData):
    """
    MODE = CONTINUOUS
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement = models.ForeignKey(
        Achievement,
        on_delete=models.CASCADE,
        verbose_name=_('Active achievement')
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        default=None,
        on_delete=models.CASCADE,
        verbose_name=_('Active user')
    )
    recent_change = models.DateTimeField(
        null=True,
        default=None,
        verbose_name=_('Date and time of recent change'),
    )

    class Meta:
        ordering = ['-recent_change']
        verbose_name = _('Active user')
        verbose_name_plural = _('Active users')

    def __str__(self):
        return f'{self.achievement} - {self.user} - {timezone.localtime(self.recent_change).strftime("%Y-%m-%d %H:%M:%S")}'


# determine destination path of uploaded document
def user_directory_path(instance: "Document", filename):
    # document uploaded via Passages 'Documents' section only pass filename w/o path (as sent in HTTP request);
    # document generated from custom forms (PDF) return full local tmp file path, so in both cases, get only filename
    # e.g. /home/passages/dproject/tmp/form-x.pdf > form-x.pdf
    # see portfolio/views/views.py#print_custom_form
    filename = os.path.basename(filename)

    # normal documents: file will be uploaded to MEDIA_ROOT/<user_id>/<filename>
    name = filename.split('.')[:-1]
    ext = filename.split('.')[-1]
    slug = slugify(name)

    # normal case: document is attached to user's portfolio (includes portfolio-global documents)
    if instance.portfolio:
        return f'{instance.portfolio.user.id}/{slug}.{ext}'
    # other cases: special documents available in all applicants' portfolios (not linked to a user's portfolio)
    else:
        print("user_directory_path")
        print(instance.type)

        typename_slug = slugify(instance.type.name.lower())
        return f'{typename_slug}/{slug}.{ext}'


class Document(MetaData):
    """
    A document is linked to a portfolio and to one or more achievements

    Documents are ordered by 2 indices
        indice_1: the number of the first achievement linked to this document
        indice_2: an incremented letter starting from A

    indice_1=0 indicates the CV document which is only linked to the portfolio
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        max_length=255,
        blank=False,
        verbose_name=_('Name'),
    )
    url = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=_('URL'),
    )
    file = models.FileField(
        upload_to=user_directory_path,
        null=True,
        blank=True,
    )
    indice_1 = models.IntegerField(
        null=True,
        blank=True,
        default=None,
    )
    indice_2 = models.CharField(
        max_length=2,
        blank=True,
        default='',
    )
    portfolio = models.ForeignKey(
        Portfolio,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=_('Portfolio that this document relates to'),
    )
    achievements = models.ManyToManyField(
        Achievement,
        blank=True,
        related_name='documents',
        verbose_name=_('Achievement that this document relates to'),
    )
    type = models.ForeignKey(
        DocumentType,
        null=True,
        blank=True,
        default=None,
        verbose_name=_('Passages document type'),
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ['indice_1', 'indice_2', '-type__milestone', 'name']
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')

    # TODO this function is half-working: URL is NOT properly set, which creates problem when showing document link
    # override default save method to auto-set the URL from file.url if user did not manually set it;
    # note: I was getting duplicate pkey errors on Document.create due to improper use of 'save' override
    # - https://stackoverflow.com/questions/38272955/django-save-override-throwing-primary-duplicate-errors
    def save(self, *args, **kwargs):
        # 1 - call parent save method to persist file and get proper file.url from 'user_directory_path';
        # important: check if pk is set, else it throws integrity errors when creating new document
        if self.pk is None:
            super(Document, self).save(*args, **kwargs)
            self.refresh_from_db()

        # TODO figure out way to get real final URL after document is saved w/o causing pkey errors

        # 2 - if .url field not set, use proper file.url as default URL
        if self.file and self.file.url and not self.url:
            self.url = self.file.url

        # 3 - re-save document w/ URL set from file.url
        super(Document, self).save(*args, **kwargs)

    def __str__(self):
        # since portfolio is optional (due to special / global docs), check if portfolio is set to prevent crash
        owner = self.portfolio.user if self.portfolio else "n/d"
        return f'{self.indice_1}-{self.indice_2} {self.name} | {owner}'


#
# Portfolio objects : UserAchievement
#

class UserAchievement(MetaData):
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement_type = models.ForeignKey(
        AchievementType,
        null=True,
        blank=True,
        verbose_name=_('Achievement type'),
        help_text=_('(milestone) Corresponds to an actual achievement. / '
                    '(continuous) Corresponds to a tab in any given achivement.'),
        on_delete=models.CASCADE,
    )
    achievement = models.ForeignKey(
        Achievement,
        null=True,
        blank=True,
        verbose_name=_('Achievement'),
        help_text=_('(continuous) Specific achievement that this content relates to.'),
        on_delete=models.CASCADE,
    )
    block = models.ForeignKey(
        EditorBlock,
        verbose_name=_('Editor block'),
        help_text=_('ID of the editor block in which this content is stored'),
        on_delete=models.CASCADE
    )
    content = models.TextField(
        verbose_name=_('Editor content'),
        help_text=_('JSON object passed to Quill editor when loading each editor per block.')
    )
    attributes = ArrayField(
        ArrayField(
            models.TextField(blank=True)
        ),
        null=True,
        blank=True,
        verbose_name=_('User achievement ops')
    )
    comments = ArrayField(
        ArrayField(
            models.TextField(blank=True)
        ),
        null=True,
        blank=True,
        verbose_name=_('Comments (ops)')
    )
    portfolio = models.ForeignKey(
        Portfolio,
        verbose_name=_('Portfolio'),
        help_text=_('Portfolio that the achievement relates to'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['-created']
        verbose_name = _('Applicant Achievement')
        verbose_name_plural = _('Applicant Achievements')

    def __str__(self):
        # both may be available, but prioritize 'achievement' to 'achievement_type'
        return '{0} - {1} - {2}'.format(
            str(self.achievement),
            str(self.achievement_type),
            str(self.created)
        )


class UserAchievementHistory(MetaData):
    """
    Not used, but keep as a base for UserAchievementVersion
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    user_achievement = models.ForeignKey(
        UserAchievement,
        verbose_name=_('User achievement'),
        help_text=_('User achievement for which we\'re tracking history.'),
        on_delete=models.CASCADE,
    )
    block = models.ForeignKey(
        EditorBlock,
        verbose_name=_('Editor block'),
        help_text=_('ID of the editor block in which this content is stored'),
        on_delete=models.CASCADE
    )
    content = models.TextField()
    # the user who actually made the edit; needed because reviewer will modify users' achievement contents
    author = models.IntegerField(
        blank=False,
        null=False,
        default=0
    )

    class Meta:
        ordering = ['-created']
        verbose_name = _('Applicant Achievement Version')
        verbose_name_plural = _('Applicant Achievements Versions')

    def __str__(self):
        achievement_name = (self.user_achievement.achievement  # continuous
                            or self.user_achievement.achievement_type)  # milestone
        return '{0} - {1} - {2}'.format(
            str(achievement_name),
            str(self.block),
            str(self.created))


class UserAssessment(MetaData):
    """

    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    portfolio = models.ForeignKey(
        Portfolio,
        verbose_name=_('Related portfolio'),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )
    assessment = models.ForeignKey(
        Assessment,
        default=None,
        verbose_name=_('Related assessment'),
        on_delete=models.CASCADE,
    )
    # This should eventually be a FK -> AssessmentRating
    grade = models.FloatField(
        verbose_name=_('Grade (decimal)'),
        null=True,
        default=0.0,
    )
    comment = models.TextField(
        verbose_name=_('Comments'),
        default='',
        blank=True,
    )

    class Meta:
        ordering = ['portfolio', 'assessment__milestone', 'assessment']
        verbose_name = _('(M) Applicant Assessment')
        verbose_name_plural = _('(M) Applicants Assessments')

    def base100(self):
        if self.assessment.code == 'total':
            return locale.format_string('%.1f', self.grade * 100)
        else:
            return format(self.grade * 100, '.0f')

    def __str__(self):
        return str(self.portfolio.user) + " - " + str(self.assessment) + " : " + str(self.grade)


#
# Original V1.0 Portfolio structure
#

class AchievementMotivation(MetaData):
    """
    An achievement motivation is a short text explaining
    the relevance of the achievement for the portfolio
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement = models.ForeignKey(
        Achievement,
        verbose_name=_('Achievement that the motivation applies to'),
        on_delete=models.CASCADE,
    )
    content = models.TextField()

    class Meta:
        ordering = ['-created']
        verbose_name = _('Achievement motivation')
        verbose_name_plural = _('Achievement motivations')

    def __str__(self):
        return str(self.achievement) + ' - ' + str(self.created)


class AchievementContext(MetaData):
    """
    An achievement context is a short text defining
    the key elements making the context of the achievement
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement = models.ForeignKey(
        Achievement,
        verbose_name=_('Achievement that this context applies to'),
        on_delete=models.CASCADE,
    )
    content = models.TextField()

    class Meta:
        ordering = ['-created']
        verbose_name = _('Achievement context')
        verbose_name_plural = _('Achievement contexts')

    def __str__(self):
        return str(self.achievement) + ' - ' + str(self.created)


class AchievementDevelopment(MetaData):
    """
    An achievement development is a text containing development ops
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement = models.ForeignKey(
        Achievement,
        verbose_name=_('Achievement that this development relates to'),
        on_delete=models.CASCADE,
    )
    content = models.TextField()

    class Meta:
        ordering = ['-created']
        verbose_name = _('Achievement development')
        verbose_name_plural = _('Achievement developments')

    def __str__(self):
        return str(self.achievement) + ' - ' + str(self.created)


class AchievementVersion(MetaData):
    """
    Registered version of an achievement

    A version registered by an applicant is a submitted version
    A version registered by any other user type is a registered revision

    Contains a reference to
        its author
        a specific version of an achievement context
        a specific version of an achievement motivation
        a specific version of an achievement development
        the parent version (for a version) or revision (for a revision)
        the child version or revision
    Also contains
        a comment from the author
        a json list of the achievement ops at the moment the version was created

    MODE=MILESTONE
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    achievement = models.ForeignKey(
        Achievement,
        verbose_name=_('Achievement that this version applies to'),
        on_delete=models.CASCADE,
    )
    # snapshots of UserAchievement instances (immutable)
    user_blocks = models.ManyToManyField(
        UserAchievementHistory,
        blank=True,
        verbose_name=_('User achievement history'),
        help_text=_('User achievement blocks associated with this version (block snapshots)'),
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_('Version author'),
    )
    parent = models.ForeignKey(
        'AchievementVersion',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='parents',
        verbose_name=_('Parent version'),
    )
    child = models.ForeignKey(
        'AchievementVersion',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='children',
        verbose_name=_('Child version'),
    )
    comment = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Comment for version'),
    )
    ops = ArrayField(
        ArrayField(
            models.TextField(null=True)
        ),
        null=True,
        verbose_name=_('Achievement version ops')
    )

    class Meta:
        ordering = ['-created']
        verbose_name = _('Achievement version')
        verbose_name_plural = _('Achievement versions')

    def __str__(self):
        return (
                str(self.achievement) + ' - '
                + str(self.created) + ' - '
                + str(self.author) + ' - '
                + str(self.status)
        )


#
# Legacy Code
#

class PortfolioAssessment(MetaData):
    """
    v2.0 Legacy
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    portfolio = models.ForeignKey(
        Portfolio,
        verbose_name=_('Related portfolio'),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )
    a1 = models.FloatField(
        verbose_name=_('Grade A1 (decimal percentage)'),
        default=0.0
    )
    a1_comment = models.TextField(
        verbose_name=_('Grade A1 (comments)'),
        default=''
    )
    a2 = models.FloatField(
        verbose_name=_('Grade A2 (decimal percentage)'),
        default=0.0
    )
    a2_comment = models.TextField(
        verbose_name=_('Grade A2 (comments)'),
        default=''
    )
    a3 = models.FloatField(
        verbose_name=_('Grade A3 (decimal percentage)'),
        default=0.0
    )
    a3_comment = models.TextField(
        verbose_name=_('Grade A3 (comments)'),
        default=''
    )
    a4 = models.FloatField(
        verbose_name=_('Grade A4 (decimal percentage)'),
        default=0.0
    )
    a4_comment = models.TextField(
        verbose_name=_('Grade A4 (comments)'),
        default=''
    )
    b1 = models.FloatField(
        verbose_name=_('Grade B1 (decimal percentage)'),
        default=0.0
    )
    b1_comment = models.TextField(
        verbose_name=_('Grade B1 (comments)'),
        default=''
    )
    c1 = models.FloatField(
        verbose_name=_('Grade C1 (decimal percentage)'),
        default=0.0
    )
    c1_comment = models.TextField(
        verbose_name=_('Grade C1 (comments)'),
        default=''
    )
    d1 = models.FloatField(
        verbose_name=_('Grade D1 (decimal percentage)'),
        default=0.0
    )
    d1_comment = models.TextField(
        verbose_name=_('Grade D1 (comments)'),
        default=''
    )
    total = models.FloatField(
        verbose_name=_('Total Grade (decimal percentage)'),
        default=0.0
    )

    class Meta:
        ordering = ['portfolio', '-modified']
        verbose_name = _('(M) User Portfolio Assessment')
        verbose_name_plural = _('(M) User Portfolio Assessments')

    def __str__(self):
        return str(self.portfolio.user)


class XFormFieldGroup(MetaData):
    """
    Logical container to group form fields per category or whatever. Backend organisation helper only (for now).
    Used to help admin in ordering fields per group when dealing with multiple forms.
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        max_length=255,
        blank=False,
        verbose_name=_('Name of the fields group')
    )

    class Meta:
        ordering = ['name']
        verbose_name = _('Form field group')
        verbose_name_plural = _('Form field groups')

    # https://stackoverflow.com/questions/10569438/how-to-print-unicode-character-in-python#answer-63213938
    def __str__(self):
        return f"{self.name}"


class XFormField(MetaData):
    """
    Single custom form field to be included in a custom form created by admin.
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        max_length=255,
        blank=False,
        verbose_name=_('Name of the form field')
    )
    required = models.BooleanField(
        blank=True,
        verbose_name=_('Is required'),
        help_text=_('Whether this field should be marked as required in the HTML form.')
    )
    type = models.CharField(
        blank=False,
        choices=XFieldType.choices,
        verbose_name=_('Type of field'),
        help_text=_('Long text: multi-line textarea; '
                    'Short text: single-line <=255 characters; '
                    'Number: numeric positive value')
    )
    group = models.ForeignKey(
        XFormFieldGroup,
        verbose_name=_('Field group'),
        help_text=_('This field is part of which group?'),
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
    )
    order = models.IntegerField(
        default=1,
        verbose_name=_('Field order'),
        help_text=_('Order of field within the form it\'s associated with.')
    )
    desc = models.TextField(
        verbose_name=_('Description'),
        help_text=_('Field description.'),
        default='',
        blank=True
    )

    class Meta:
        ordering = ['group', 'order', 'name']
        verbose_name = _('Form field')
        verbose_name_plural = _('Form fields')

    # https://stackoverflow.com/questions/10569438/how-to-print-unicode-character-in-python#answer-63213938
    def __str__(self):
        return f"{self.group}. {self.name} - {self.order} ({self.type})"


class XForm(MetaData):
    """
    Single custom form field to be included in a custom form created by admin.
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    name = models.CharField(
        max_length=255,
        blank=False,
        verbose_name=_('Name of the form')
    )
    desc = models.TextField(
        verbose_name=_('Description'),
        help_text=_('Form description.'),
        default='',
    )
    fields = models.ManyToManyField(
        XFormField,
        verbose_name=_('Form fields'),
        help_text=_('Fields included in this form.')
    )

    class Meta:
        ordering = ['name']
        verbose_name = _('Form')
        verbose_name_plural = _('Forms')

    def __str__(self):
        return f"{self.name}"


class XFormEntry(MetaData):
    """
    Single form submission for a given custom form
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    form = models.ForeignKey(
        XForm,
        verbose_name=_('Form'),
        help_text=_('Form used for this submission'),
        null=False,
        blank=False,
        on_delete=models.PROTECT,
    )
    portfolio = models.ForeignKey(
        Portfolio,
        verbose_name=_('Portfolio'),
        null=False,
        blank=False,
        on_delete=models.PROTECT
    )
    data = models.JSONField(
        verbose_name=_('Submission data')
    )
    # https://www.hacksoft.io/blog/timestamps-in-django-exploring-auto-now-auto-now-add-and-default
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        null=True
    )

    class Meta:
        ordering = ['form', 'created_at']
        verbose_name = _('Form entry')
        verbose_name_plural = _('Form entries')

    def __str__(self):
        return f"{self.form} - {self.portfolio}"
