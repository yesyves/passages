# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale
import string

from random import randrange

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.mail import mail_admins
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader, RequestContext
from django.utils.translation import gettext as _
from django.urls import reverse

import portfolio.views
from config.settings.common import IS_CONTINUOUS, IS_MILESTONE, SESSION_ACTIVE_PORTFOLIO
from portfolio.models import (
    Portfolio, Achievement, AchievementContext, AchievementDevelopment, DocumentType, Document,
    AchievementMotivation, AchievementVersion, ActiveUser, Assessment, UserAssessment, HelpLink, PortfolioType
)
from portfolio.views.achievements import _remove_new_text_attributes_continuous
from portfolio.views.views import export_attribution
from user.models import Attribution, User, ProgramExpert
from utils.views import check_restriction, get_portfolio, get_help_link, active_portfolio_type

locale.setlocale(locale.LC_ALL, '')


@login_required
def profile(request):
    """User entry point after login

    Applicant: Show portfolio if exists, initiate user profile otherwise
    Others: Show Semester and portfolio picker

    Args:
        request: (HttpRequest object)

    Returns:
        redirect

    """

    if request.method != 'GET':
        raise ValueError(request)

    user = request.user

    if user.type == 'Applicant':
        # prevent access to other sections of site if user does NOT consent
        if IS_MILESTONE and not user.personal_consent:
            return redirect('edit_profile')

        # using helper func instead of full query (same in backend, but avoids code duplication)
        # portfolio = Portfolio.available_objects.get(user=user)
        user_portfolio = user.portfolio()

        if user_portfolio is None:
            # edit applicant profile to create portfolio
            return redirect('edit_profile')
        else:
            return redirect('portfolio_index', portfolio_id=user_portfolio.id)

    else:
        # show a portfolio picker
        return redirect('pick_semester')


def create_portfolio(user, program):
    """Create a portfolio for applicant if he does not already have one

    Args:
        user: Object
        program: Object

    Returns:
        Bool

    """

    if Portfolio.available_objects.filter(user=user):
        return False
    else:
        portfolio = Portfolio(
            user=user,
            program=program,
        )
        portfolio.save()

        document_forms = DocumentType.available_objects.all().exclude(url__exact='')
        for document_form in document_forms:
            document = Document(
                name=document_form.name,
                url=document_form.url,
                portfolio=portfolio,
                type=document_form,
            )
            document.save()

        return True


@login_required
def portfolio_index(request, portfolio_id):
    """Shows achievements in portfolio

    Args:
        request: GET
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/index.html / restrict)

    """

    if request.method != 'GET':
        raise ValueError(request)

    # actual user doing the request
    user: User = request.user

    _portfolio: Portfolio | None = None
    try:
        # '_'-prefixed to prevent shadowing global 'portfolio' variable (e.g. portfolio.views.*, portfolio.models.*)
        _portfolio = get_portfolio(portfolio_id)
    except ValidationError:
        # if any error occurs while fetching portfolio by id (e.g. invalid ID format), return to portfolio index
        return redirect("profile")

    # TODO Ugly fix to catch users looking at example portfolio
    if IS_MILESTONE and portfolio_id == 'ed888801-d22f-4505-951b-06951279ee46' and user.id != 2:
        return redirect('profile')

    # default context information generic to both modes
    context = {
        'portfolio': _portfolio,
        'help': get_help_link('create_achievement'),
        'portfolio_active': True,
    }

    # important to set this session variable for consistency and easy access;
    # both users & admin go through this function, so safe place to ensure portfolio ID is available everywhere
    request.session[SESSION_ACTIVE_PORTFOLIO] = str(_portfolio.id)

    if IS_CONTINUOUS:
        if user.type == 'Expert':
            restrict = check_restriction(request, _portfolio)
            if restrict:
                return restrict

        context.update(_portfolio_index_continuous(user, _portfolio))
    else:
        context.update(_portfolio_index_milestone(user, _portfolio))

    template = loader.get_template('portfolio/index.html')
    return HttpResponse(template.render(context, request))


# CONTINUOUS-specific operations
def _portfolio_index_continuous(user, portfolio):
    """

    Args:
        user:
        portfolio:

    Returns:

    """
    targeted_skills = active_portfolio_type().targeted_skills
    private_achievements = None

    if targeted_skills == 'Target':
        public_achievements = portfolio.get_achievements('Reviewer')
        if user.type == 'Applicant':
            private_achievements = portfolio.get_achievements('Private')
    elif targeted_skills == 'Level':
        public_achievements = portfolio.get_achievements('Level')
    else:
        public_achievements = portfolio.get_achievements('Applicant')

    return {
        'public_achievements': public_achievements,
        'private_achievements': private_achievements,
    }


# MILESTONE-specific operations
def _portfolio_index_milestone(user, portfolio):

    # prevent access if Applicant user doest NOT consent (return to profile)
    if user.type == 'Applicant' and not portfolio.user.personal_consent:
        return redirect('edit_profile')

    if portfolio.user.group.status == "Underreview":
        portfolio_under_review = True
    else:
        portfolio_under_review = False

    portfolio_type = active_portfolio_type()
    public_achievements = portfolio_type.achievement_types.all()
    group_milestone = portfolio.user.group.milestone
    for achievement in public_achievements:
        if '\n' in achievement.sub_title:
            achievement.sub_title = achievement.sub_title.split('\n')[group_milestone - 1]

    return {
        'public_achievements': public_achievements,
        'portfolio_under_review': portfolio_under_review,
        'comp_types': portfolio_type.competency_types.all(),
    }


@login_required
def portfolio_example(request):
    """Shows a shared portfolio in the user's discipline if one exists

    Args:
        request: (HttpRequest object)

    Returns:
         HttpResponse (portfolio/index.html)

    """
    program = None
    programs = []
    user = request.user
    user_type = user.type

    if user_type == 'Expert':
        expertises = ProgramExpert.available_objects.filter(expert=user)
        for expertise in expertises:
            programs.append(expertise.program)
        portfolios = Portfolio.available_objects.filter(
            Q(program__in=programs),
            Q(share=True),
            Q(status='Certified') | Q(status='Archived'),
        )
    elif user_type == 'Applicant':
        portfolio = Portfolio.available_objects.filter(user=request.user).first()

        if portfolio is not None:
            program = portfolio.program

            portfolios = Portfolio.available_objects.filter(
                Q(program=program),
                Q(share=True),
                Q(status='Certified') | Q(status='Archived'),
            ).exclude(user=user)

    if not portfolios:
        portfolios = Portfolio.available_objects.filter(
            Q(share=True),
            Q(status='Certified') | Q(status='Archived'),
        )

    if not portfolios:
        message = {
            0: _('No example available'),
        }
        if program:
            message.update({1: _('for program {}').format(program)})
        elif programs:
            message.update({1: _('for programs {}').format(', '.join(programs))})
        if user_type == 'Applicant':
            message.update({2: _('Thank you for considering sharing your portfolio when completed')})
        template = loader.get_template('portfolio/restrict.html')
        return HttpResponse(template.render({'message': message}, request))

    portfolio = portfolios[randrange(portfolios.count())]

    context = {
        'portfolio': portfolio,
        'public_achievements': portfolio.get_achievements('Reviewer'),
        'portfolio_active': True,
        'help': HelpLink.available_objects.get(keyword='examples').link,
    }
    template = loader.get_template('portfolio/index.html')
    return HttpResponse(template.render(context, request))


@login_required
def certify_portfolio(request):
    """Certify portfolio or cancel certification

    Certification by supervisor:
        Clean up achievements history:
            Remove all entries save for the last one
            Remove all registered versions
        Remove Expert association

    Args:
        request: (HttpRequest object)
            POST.portfolio_id: uuid

    Returns:
        HttpResponse (response / redirect)

    """

    if request.method != 'POST':
        raise ValueError(request)

    portfolio_id = request.POST.get('portfolio_id')

    if portfolio_id is None:
        return redirect('profile')

    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    applicant = portfolio.user
    user = request.user

    subject = _('Portfolio certified')
    send_to = applicant.group.supervisor

    response = None

    try:
        attribution = Attribution.available_objects.get(applicant=applicant)
    except ObjectDoesNotExist:
        attribution = Attribution(applicant=applicant)

    if user.type == 'Applicant':
        portfolio.status = 'Final'
        if 'share' in request.POST:
            portfolio.share = True

        subject = _('Portfolio finalized')
        message = [_('has finalized his/hers portfolio.')]

    else:
        if user == portfolio.user.expert and portfolio.status == 'Underreview':
            portfolio.status = 'Reviewed'
            if 'export' in request.POST:
                response = export_attribution(portfolio)

            # Set or update Expert's most recent evaluation
            if not user.group or user.group.id < applicant.group.id:
                user.group = applicant.group
                user.save(update_fields=["group"])

            attribution.expert = str(user)
            attribution.interview = request.POST.get('date')

            message = [_('has certified the portfolio of {}').format(applicant)]

        elif user.type == 'Supervisor':
            portfolio.status = 'Certified'

            # print(request.POST)
            attribution.credits = request.POST.get('credits')

            applicant.expert = None
            applicant.save()

            send_to = applicant
            message = [_(' has certified your portfolio.\n\n'
                         'You can now see your final credits attributions.')]

            # Clean up achievements history
            achievements = Achievement.available_objects.filter(portfolio=portfolio)
            for achievement in achievements:
                ActiveUser.available_objects.filter(achievement=achievement).delete()
                AchievementVersion.available_objects.filter(achievement=achievement).delete()
                entries = AchievementMotivation.available_objects.filter(achievement=achievement)[1:]
                for entry in entries:
                    entry.delete()
                entries = AchievementContext.available_objects.filter(achievement=achievement)[1:]
                for entry in entries:
                    entry.delete()
                entries = AchievementDevelopment.available_objects.filter(achievement=achievement)[1:]
                for entry in entries:
                    entry.delete()

                # Clean up remaining new_text
                _remove_new_text_attributes_continuous(
                    AchievementDevelopment.available_objects.filter(achievement=achievement).first()
                )
                _remove_new_text_attributes_continuous(
                    AchievementMotivation.available_objects.filter(achievement=achievement).first()
                )
                _remove_new_text_attributes_continuous(
                    AchievementContext.available_objects.filter(achievement=achievement).first()
                )

    portfolio.save()
    attribution.save()

    if 'comment' in request.POST:
        message.append(f"{request.POST.get('comment')}")

    if 'flow' in request.POST:
        # link = '/portfolio/evaluations'
        link = reverse('view_assessments')
    else:
        # link = f'portfolio/{portfolio_id}/levels'
        link = reverse('levels', args=[portfolio_id])

    send_to.send_mail(request, subject, message, link)

    if response is not None:
        return response
    else:
        return redirect(link)


@login_required
def submit_portfolio(request):
    """Submit portfolio to expert and mark as submitted

    Args:
        request: (HttpRequest object)
            POST.portfolio_id: uuid

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    portfolio_id = request.POST.get('portfolio_id')

    if portfolio_id is None:
        return redirect('profile')

    comment = request.POST.get('comment')
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    applicant = portfolio.user

    achievements = Achievement.available_objects.filter(portfolio=portfolio)
    for achievement in achievements:
        achievement.update_active_user()

    if portfolio.status != 'Underreview':
        portfolio.status = 'Underreview'
        portfolio.save()

    subject = _('Portfolio for evaluation')
    message = [_('is giving you access to the portfolio of {}').format(applicant)]
    if comment:
        message.extend([_('with the following comment:'), comment])

    message.append(_('By accepting this evaluation, you confirm that you have no conflict of interest with the candidate '
                     'and that you will abide by the rules of confidentiality regarding the narratives and the candidate.'))

    link = reverse('portfolio_index', args=[portfolio_id])

    applicant.expert.send_mail(request, subject, message, link)

    return redirect('pick_semester')


@login_required
def submit_user_assessment(request):
    """

    Args:
        request: (HttpRequest object)

    Raises:
        ValueError if request is not GET

    Returns:
        HttpResponse (user/attribution.html)

    """
    if request.method != 'POST':
        raise ValueError(request)

    # enforce admin-only access
    if request.user.type == 'Applicant':
        return redirect('profile')

    portfolio = Portfolio.available_objects.get(id=request.POST.get('portfolio'))
    milestone = request.POST.get('milestone')
    assessments = Assessment.available_objects.filter(milestone=milestone)
    current_assessment = json.loads(request.POST['assessments'])
    for assessment in assessments:
        if assessment.code in current_assessment and current_assessment[assessment.code][0]:
            user_assessment, created = UserAssessment.available_objects.get_or_create(portfolio=portfolio,
                                                                                      assessment=assessment)
            user_assessment.grade = current_assessment[assessment.code][0]
            user_assessment.comment = current_assessment[assessment.code][1]
            user_assessment.save()

    return HttpResponse(
        json.dumps({
            'success': True
        }),
        content_type='application/json'
    )

@login_required
def set_expert_portfolio_consent(request):
    """

    Args:
        request: (HttpRequest object)

    Raises:
        ValueError if request is not GET

    Returns:
        HttpResponse (portfolio/<portfolio-id>)

    """
    if request.method != 'POST':
        raise ValueError(request)

    print("set_expert_portfolio_consent")
    portfolio_id: string = request.POST.get("portfolio_id")
    print(request.POST)

    # set 'portfolio.expert_consent' to True for the given portfolio
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    if portfolio is not None:
        portfolio.user.expert_consent = True
        portfolio.user.save()

    # then redirect expert to that actual portfolio
    return redirect('portfolio_index', portfolio_id=portfolio_id)