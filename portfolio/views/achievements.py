# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.utils.translation import gettext as _
# from django.forms.models import model_to_dict
from django.urls import reverse

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE
from program.models import Competency
from portfolio.models import (
    Portfolio, Achievement, AchievementContext, AchievementDevelopment,
    AchievementMotivation, AchievementVersion, ActiveUser, Questionnaire, Document,
    AchievementType, UserAchievement, HelpLink, QuestionnaireUQAM, UserAchievementHistory
)
from portfolio.forms import (
    FormAchievementVersionApplicant, FormAchievementVersionReviewer,
    FormQuestionnaireMotivation, FormQuestionnaireContext,
    FormQuestionnaireDebut, FormQuestionnaireM1, FormQuestionnaireM2, FormQuestionnaireM3,
    FormQuestionnaireBilan, FormTargetedSkill, FormDevelopmentLevel
)
from utils.views import get_help_link, active_portfolio_type
# from portfolio.views import sort_documents
from .document import sort_documents

locale.setlocale(locale.LC_ALL, '')


@login_required
def create_achievement(request):
    """Create Achievement and associated Questionnaire and ActiveUser objects

    Args:
        request: (HttpRequest object)
            POST.achievement_name: Str
            POST.portfolio_id: uuid

    Returns:
        redirect (/portfolio/questionnaire/achievement.id)

    """

    if request.method != 'POST' or not request.POST:
        return redirect('profile')

    achievement_name = request.POST.get('achievement_name')
    portfolio_id = request.POST.get('portfolio_id')

    questionnaire = Questionnaire(A1=achievement_name)
    questionnaire.save()

    portfolio_type = active_portfolio_type()
    achievement_number = 1

    if portfolio_type.targeted_skills != 'Level':
        achievements = Achievement.available_objects.filter(portfolio_id=portfolio_id)
        for achievement in achievements:
            if (achievement.number - achievement_number) != 0:
                break
            achievement_number += 1

    achievement = Achievement(
        name=achievement_name,
        number=achievement_number,
        portfolio_id=portfolio_id,
        questionnaire=questionnaire,
        status='Created',
    )
    achievement.save()

    active_user = ActiveUser(
        achievement=achievement,
        user=None,
        recent_change=None,
    )
    active_user.save()

    return redirect('show_questionnaire', achievement_id=achievement.id)


@login_required
def set_achievement_number(request, achievement_id, achievement_number, caller=''):
    """Set achievement number and recursively bump conflicting achievements

    Args:
        achievement_id: uuid
        achievement_number: Int <1 => set to last, >0 => set to number and bump conflicting achievement)
        caller: Str (questionnaire / None)

    Returns:
        HttpResponse (application/json)

    """

    achievement = Achievement.available_objects.get(id=achievement_id)

    if caller == 'questionnaire' and achievement.number < achievement_number:
        # Don't let user increase number in questionnaire
        response = {
            'success': False,
            'message': _('You are only allowed to decrease the achievement number in the questionnaire.'),
        }
    else:
        response = {'success': True}
        achievement.number = achievement_number

        try:
            bump = Achievement.available_objects.get(portfolio=achievement.portfolio,
                                                     number=achievement_number)
            achievement.save(update_fields=["number"])
            set_achievement_number(request, bump.id, achievement_number + 1)
        except ObjectDoesNotExist:
            achievement.save(update_fields=["number"])

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
    )


@login_required
def rename_achievement(request):
    """Rename achievement

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid
            POST.achievement_name: Str

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    achievement_id = request.POST.get('achievement_id')
    achievement_name = request.POST.get('achievement_name')
    achievement = Achievement.available_objects.get(id=achievement_id)

    achievement.name = achievement_name
    achievement.save(update_fields=["name"])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def delete_achievement(request):
    """ Delete an achievement and all related objects

     Delete an achievement and all related objects :
        achievement, context, motivation, developments,
        active_user, questionnaire and all versions

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid

    Returns:
        redirect (home)

    """

    if request.method != 'POST':
        raise ValueError(request)

    achievement_id = request.POST.get('achievement_id')

    if achievement_id is not None:
        achievement = Achievement.available_objects.get(id=achievement_id)

        # Remove documents associations
        portfolio = Portfolio.available_objects.get(achievement=achievement)
        documents = Document.available_objects.filter(portfolio=portfolio)
        resort_documents = False

        for document in documents:
            if achievement in document.achievements.all():
                document.achievements.remove(achievement)
                if document.indice_1:
                    resort_documents = True

        if resort_documents:
            sort_documents(portfolio.id)

        ActiveUser.available_objects.filter(achievement=achievement).delete()
        AchievementVersion.available_objects.filter(achievement=achievement).delete()
        AchievementContext.available_objects.filter(achievement=achievement).delete()
        AchievementMotivation.available_objects.filter(achievement=achievement).delete()
        AchievementDevelopment.available_objects.filter(achievement=achievement).delete()
        achievement.questionnaire.delete()
        achievement.delete()

    return redirect('home')


@login_required
def set_targeted_skill(request):
    """ Set targeted skill for achievement

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid
            POST.targeted_skill: skill ID (Str)

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST' or not request.POST:
        return redirect('profile')

    achievement_id = request.POST.get('achievement_id')

    if achievement_id is None:
        success = False
    else:
        success = True
        competency = request.POST.get('targeted_skill')

        if str(competency) == 'NONE':
            skill = None
        else:
            skill = Competency.available_objects.get(id=competency)

        achievement = Achievement.available_objects.get(id=achievement_id)
        achievement.targeted_competency = skill

        if active_portfolio_type().targeted_skills == 'Level':
            targeted_achievements = Achievement.available_objects.filter(targeted_competency=skill)
            achievement_numbers = []
            achievement_number = 1
            for targeted_achievement in targeted_achievements:
                achievement_numbers.append(targeted_achievement.number)
            if achievement_numbers:
                for number in range(1, max(achievement_numbers)+2):
                    if number not in achievement_numbers:
                        achievement_number = number
                        break
            achievement.number = achievement_number

        achievement.save()

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def set_development_level(request):
    """ Set development level for achievement (based on targeted skill)

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid
            POST.development_level: numeric value representing level of mastery of targeted skill (int)

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    achievement_id = request.POST.get('achievement_id')

    if achievement_id is None:
        success = False
    else:
        success = True
        level = request.POST.get('development_level')
        print(f"set_development_level --> {level}")
        achievement = Achievement.available_objects.get(id=achievement_id)
        achievement.development_level = level
        achievement.save()

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def register_version(request):
    """Register a new achievement version and update achievement status

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid
            POST.status: str
            POST.logMessage: str

    Raises:
        ValidationError (f_version)

    Returns:
        redirect (home)

    """

    if request.method != 'POST':
        raise ValueError(request)

    if IS_CONTINUOUS:
        return _register_version_continuous(request)
    else:
        return _register_version_milestone(request)


def _register_version_continuous(request):
    user = request.user
    user_type = user.type

    if user_type == 'Applicant':
        f_version = FormAchievementVersionApplicant(request.POST)
    else:
        f_version = FormAchievementVersionReviewer(request.POST)

    if f_version.is_valid():
        fstatus = f_version.cleaned_data['status']
        flog_message = f_version.cleaned_data['logMessage']
        achievement_id = f_version.cleaned_data['achievement_id']
        achievement = Achievement.available_objects.get(id=achievement_id)
        achievement_types = active_portfolio_type().achievement_types

        achievement.update_active_user()

        # retrieve all UserAchievementHistory entries for current achievement & its blocks
        user_achievements = UserAchievement.available_objects.filter(achievement=achievement)
        blocks_history = []
        for block in user_achievements:
            latest_block_version: UserAchievementHistory = (UserAchievementHistory.available_objects
                                                            .filter(user_achievement=block).first())
            blocks_history.append(latest_block_version)
            print(latest_block_version)
            print(latest_block_version.block)

        print(blocks_history)

        parent_version = AchievementVersion.available_objects.filter(
            achievement=achievement,
            author=user
        ).first()

        if user_type == 'Applicant':
            if achievement.status == 'Final':
                fstatus = 'Final'
        else:
            for block in blocks_history:
                _remove_new_text_attributes_continuous(block)

        new_version = AchievementVersion(
            achievement=achievement,
            parent=parent_version,
            comment=f_version.cleaned_data['logMessage'],
            ops=achievement.ops,
            status=fstatus,
            author=user,
        )
        new_version.save()
        # ManyToMany field MUST be set after object is saved using either .add() or .set() method
        new_version.user_blocks.set(blocks_history)

        if parent_version is not None:
            parent_version.child = new_version
            parent_version.save(update_fields=['child'])

        if user_type == 'Applicant':
            achievement.version = achievement.version + 1

        # Apply new status to all elements
        achievement.status = fstatus
        achievement.save()

        # The following code is not used anywhere but is a good idea nonetheless
        for block in blocks_history:
            block.status = fstatus
            block.save()

        # Send email notification
        if user_type == 'Applicant':
            subject = _('New version submitted')
            message = [_('has submitted a new version of achievement: ')]
            if user.reviewer:
                to = user.reviewer
            else:
                to = user.group.supervisor
        else:
            if fstatus == 'Final':
                subject = _('Achievement registered as final')
                message = [_('has registered as final achievement: ')]
            else:
                subject = _('New revision registered')
                message = [_('has revised achievement: ')]
            to = achievement.portfolio.user
        message[0] += achievement.name
        message.append(flog_message)
        link = f'editor/{achievement_id}'
        to.send_mail(request, subject, message, link)

        return redirect('profile')

    else:
        raise ValidationError(f_version.errors.as_data())


def _register_version_milestone(request):
    user = request.user
    user_type = user.type

    if user_type == 'Applicant':
        f_version = FormAchievementVersionApplicant(request.POST)
    else:
        f_version = FormAchievementVersionReviewer(request.POST)

    if f_version.is_valid():
        fstatus = f_version.cleaned_data['status']
        flog_message = f_version.cleaned_data['logMessage']
        achievement_id = f_version.cleaned_data['achievement_id']

        if 'portfolio_id' in f_version.cleaned_data:
            portfolio = Portfolio.available_objects.get(id=f_version.cleaned_data['portfolio_id'])
        else:
            portfolio = user.portfolio()
        achievement_type = AchievementType.available_objects.get(id=achievement_id)

        # Apply new status to all elements
        user_achievements = UserAchievement.available_objects.filter(achievement_type=achievement_type,
                                                                     portfolio=portfolio)
        # Do not record version right now in milestone
        # TODO check what history is doing instead
        for user_achievement in user_achievements:
            user_achievement.status = fstatus
            user_achievement.save()
            user_achievement.block.status = fstatus
            user_achievement.block.save()

        # Send email notification
        if user_type == 'Applicant':
            subject = _('New version submitted')
            message = [_('has submitted a new version of achievement: ')]
            if user.reviewer:
                to = user.reviewer
            else:
                to = user.group.supervisor
        else:
            if fstatus == 'Final':
                subject = _('Achievement registered as final')
                message = [_('has registered as final achievement: ')]
            else:
                subject = _('New revision registered')
                message = [_('has revised achievement: ')]
            to = portfolio.user
        message[0] += achievement_type.name
        message.append(flog_message)
        link = f'editor/{achievement_id}/3/{portfolio.id}'
        to.send_mail(request, subject, message, link)

        return redirect('profile')

    else:
        raise ValidationError(f_version.errors.as_data())


def get_f_target(achievement):
    """Get targeted skill for an achievement

    Args:
        achievement: Object

    Returns:
        f_target: targeted skill ID (str)

    """

    portfolio = achievement.portfolio
    competencies = Competency.available_objects.filter(program=portfolio.program)
    f_target = FormTargetedSkill(competencies)
    f_target.initial['achievement_id'] = achievement.id

    f_target.fields['targeted_skill'].label = (
        '2. Compétence ciblée comme centrale en termes de développement '
        'dans cette réalisation (généralement une compétence clé)'
    )
    # UQAM
    # f_target.fields['targeted_skill'].label = ('9. Compétence ciblée comme centrale en termes de développement '
    #                                            'dans cette réalisation (généralement une compétence clé)')

    if achievement.targeted_competency is not None:
        f_target.fields['targeted_skill'].initial = achievement.targeted_competency.id
    return f_target


def get_f_development_level(achievement):
    """Get development level for an achievement

    Args:
        achievement: Object

    Returns:
        f_level: development level for achievement's targeted skill (int)

    """

    f_level = FormDevelopmentLevel()
    f_level.initial['achievement_id'] = achievement.id

    f_level.fields['development_level'].label = (
        '3. Niveau de développement de la compétence ciblée'
    )

    if achievement.development_level > 0:
        f_level.fields['development_level'].initial = achievement.development_level
    return f_level


def _remove_new_text_attributes_continuous(achievement_part):
    """MODE=CONTINUOUS: when we EXIT revision mode, remove all 'new_text' attributes
    from user achievements for group

    Args:
        achievement_part:

    """
    tmp_ops = json.loads(achievement_part.content)['ops']
    for op in tmp_ops:
        if 'attributes' not in op:
            continue

        # check if attributes contain 'new_text' and if so delete that attribute
        attributes = op['attributes']
        if 'new_text' in attributes:
            del attributes['new_text']

    # save updated ops w/ removed 'new_text' attributes
    updated_ops = {"ops": tmp_ops}
    achievement_part.content = json.dumps(updated_ops)
    achievement_part.save(update_fields=["content"])


@login_required
def show_questionnaire(request, achievement_id):
    """Display questionnaire for achievement

    Args:
        achievement_id: uuid

    Returns:
        HttpResponse (portfolio/questionnaire.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    achievement = Achievement.available_objects.get(id=achievement_id)
    portfolio = achievement.portfolio
    questionnaire = achievement.questionnaire
    form_motivation = FormQuestionnaireMotivation()

    # UQAM
    # form_context = FormQuestionnaireContext()
    # form_debut = FormQuestionnaireDebut()
    # form_m1 = FormQuestionnaireM1()
    # form_m2 = FormQuestionnaireM2()
    # form_m3 = FormQuestionnaireM3()
    # form_bilan = FormQuestionnaireBilan()

    form_motivation.fields['A1'].initial = achievement.name

    # UQAM
    # form_motivation.fields['A2'].initial = questionnaire.A2
    # form_context.fields['B3'].initial = questionnaire.B3
    # form_context.fields['B4'].initial = questionnaire.B4
    # form_context.fields['B5'].initial = questionnaire.B5
    # form_context.fields['B6'].initial = questionnaire.B6
    # form_context.fields['B7'].initial = questionnaire.B7
    # form_context.fields['B8'].initial = questionnaire.B8
    # # B9 = f_target
    # form_debut.fields['C10'].initial = questionnaire.C10
    # form_debut.fields['C11'].initial = questionnaire.C11
    # form_debut.fields['C12'].initial = questionnaire.C12
    # form_debut.fields['C13'].initial = questionnaire.C13
    # form_m1.fields['C14'].initial = questionnaire.C14
    # form_m1.fields['C15'].initial = questionnaire.C15
    # form_m1.fields['C16'].initial = questionnaire.C16
    # form_m1.fields['C17'].initial = questionnaire.C17
    # form_m1.fields['C18'].initial = questionnaire.C18
    # form_m1.fields['C19'].initial = questionnaire.C19
    # form_m1.fields['C20'].initial = questionnaire.C20
    # form_m1.fields['C21'].initial = questionnaire.C21
    # form_m2.fields['C22'].initial = questionnaire.C22
    # form_m2.fields['C23'].initial = questionnaire.C23
    # form_m2.fields['C24'].initial = questionnaire.C24
    # form_m2.fields['C25'].initial = questionnaire.C25
    # form_m2.fields['C26'].initial = questionnaire.C26
    # form_m2.fields['C27'].initial = questionnaire.C27
    # form_m2.fields['C28'].initial = questionnaire.C28
    # form_m2.fields['C29'].initial = questionnaire.C29
    # form_m3.fields['C30'].initial = questionnaire.C30
    # form_m3.fields['C31'].initial = questionnaire.C31
    # form_m3.fields['C32'].initial = questionnaire.C32
    # form_m3.fields['C33'].initial = questionnaire.C33
    # form_m3.fields['C34'].initial = questionnaire.C34
    # form_m3.fields['C35'].initial = questionnaire.C35
    # form_m3.fields['C36'].initial = questionnaire.C36
    # form_m3.fields['C37'].initial = questionnaire.C37
    # form_bilan.fields['C38'].initial = questionnaire.C38
    # form_bilan.fields['C39'].initial = questionnaire.C39
    # form_bilan.fields['C40'].initial = questionnaire.C40

    # Validate possibility to transfer to achievement
    validate_transfer = True
    targeted_skills = active_portfolio_type().targeted_skills
    if targeted_skills == 'Target':
        if 1 < achievement.number <= portfolio.concentration.max_num_achievements:
            previous_number = 1 if achievement.number == 2 else 2
            try:
                previous_achievement = Achievement.available_objects.get(
                    portfolio=portfolio,
                    number=previous_number
                )
                if previous_achievement.status != 'Final':
                    validate_transfer = False
            except ObjectDoesNotExist:
                validate_transfer = False

    context = {
        'achievement': achievement,
        'form_motivation': form_motivation,
        # UQAM
        # 'form_context': form_context,
        # 'form_debut': form_debut,
        # 'form_m1': form_m1,
        # 'form_m2': form_m2,
        # 'form_m3': form_m3,
        # 'form_bilan': form_bilan,
        'f_target': get_f_target(achievement),
        'f_level': get_f_development_level(achievement),
        'portfolio': portfolio,
        'validate_transfer': validate_transfer,
        'help': get_help_link('questionnaire'),
    }
    template = loader.get_template('portfolio/questionnaire.html')
    return HttpResponse(template.render(context, request))


@login_required
def save_questionnaire(request):
    """Record an answer

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid
            POST.element_name: Question name (str)
            POST.answer: (str)

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    achievement_id = request.POST.get('achievement_id')

    if achievement_id is None:
        success = False
    else:
        success = True
        achievement = Achievement.available_objects.get(id=achievement_id)
        name = request.POST.get('element_name')

        if name is None:
            success = False
        elif name == 'A1':
            achievement.name = request.POST.get('answer')
            achievement.save()
        else:
            questionnaire = achievement.questionnaire
            setattr(questionnaire, name, request.POST.get('answer'))
            questionnaire.save()

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def transfer_questionnaire(request, achievement_id, transfer):
    """Transfer answers to achievement or show answers as text

    Args:
        achievement_id: uuid
        transfer: Bool

    Returns:
        redirect (editor/achievement.id)
        HttpResponse (portfolio/development.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    achievement = Achievement.available_objects.get(id=achievement_id)
    questionnaire = achievement.questionnaire

    # Context
    # contents_context = ''
    # for number in range(3, 9):
    #     contents_context += getattr(questionnaire, f'B{number}') + ' '

    # Development
    # contents_development = ''
    # for number in range(10, 40):
    #     answer = getattr(questionnaire, f'C{number}')
    #     if answer:
    #         contents_development += answer + ' '

    if transfer:
        # achievement_motivation = AchievementMotivation(
        #     achievement=achievement,
        #     content=json.dumps({'ops': [{'insert': questionnaire.A2}]}),
        # )
        # achievement_motivation.save()

        # achievement_context = AchievementContext(
        #     achievement=achievement,
        #     content=json.dumps({'ops': [{'insert': contents_context}]}),
        # )
        # achievement_context.save()

        # achievement_development = AchievementDevelopment(
        #     achievement=achievement,
        #     content=json.dumps({'ops': [{'insert': contents_development}]}),
        # )
        # achievement_development.save()

        achievement.status = 'Draft'
        achievement.save()

        return redirect('get_achievement', instance_id=achievement.id)

    else:
        # contents = questionnaire.A2
        # contents += f'\n\n{contents_context}'
        # contents += f'\n\n{contents_development}'

        context = {
            'achievement': achievement,
            # 'contents': contents,
            'contents': {},
            'portfolio': achievement.portfolio,
        }
        template = loader.get_template('portfolio/development.html')
        return HttpResponse(template.render(context, request))
