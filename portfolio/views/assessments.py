# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import locale
from enum import Enum
import json

from django.core.exceptions import ObjectDoesNotExist

from portfolio.models import Achievement, CompetencyAssessmentUQAR, Portfolio, AssessmentStatuses, PortfolioStatuses
from program.models import Competency, CustomCompetency
from portfolio.forms import FormProgressAssessment, FormSummativeAssessment
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, QueryDict

from user.models import Attribution
from utils.models import UserType
from utils.views import active_portfolio
from django.db.models import Sum
from django.template import loader
from datetime import datetime, date

locale.setlocale(locale.LC_ALL, '')

DATE_FORMAT = '%Y-%m-%d'


class AssessmentTypes(Enum):
    PROGRESS = 'progress'
    SUMMATIVE = 'summative'


"""
Given the status of an assessment and the role of the user (Expert, Applicant, Supervisor), the function returns
the permissions of the user to view or edit the assessment.
"""


def get_permissions(user_type, assessment_status, portfolio_status=''):
    if user_type == UserType.APPLICANT.value and portfolio_status == 'Certified':
        edit_permission = False
        view_permission = True
    elif user_type == UserType.APPLICANT.value:
        edit_permission = False
        view_permission = (assessment_status == AssessmentStatuses.VALIDATED)
    elif user_type == UserType.EXPERT.value:
        # edit_permission = (assessment_status not in [AssessmentStatuses.FINAL, AssessmentStatuses.VALIDATED])
        edit_permission = (assessment_status != AssessmentStatuses.FINAL)
        view_permission = True
    elif user_type == UserType.SUPERVISOR.value:
        edit_permission = True
        view_permission = True
    elif user_type == UserType.AGENT.value:
        edit_permission = False
        view_permission = True
    else:
        edit_permission = False
        view_permission = False

    return view_permission, edit_permission


# calculate and return the total numbers of credits requested by candidate and granted by expert
def calculate_credits(competency_assessments):
    requested_credits = 0
    recommended_credits = 0
    for assessment in competency_assessments:
        try:
            requested_credits += int(assessment.summative_candidate_level[2]) * 2
        except Exception as e:
            print(e)
        recommended_credits += assessment.summative_credits

    return requested_credits, recommended_credits


# helper function returning selected_competency_id, selected_competency, competencies
def selected_competency_info(competency_id, portfolio_id):
    # retrieve the list of unique competency ids pertaining to the portfolio
    targeted_competency_ids = Achievement.available_objects \
        .filter(portfolio_id=portfolio_id, targeted_competency_id__isnull=False) \
        .values_list('targeted_competency_id', flat=True).distinct()
    # return list of competency ids and names
    competencies = Competency.available_objects.filter(pk__in=targeted_competency_ids).values('id', 'name')

    # if there exist competencies in the candidate's portfolio, return reference to selected
    # competency or first one in user's list of competencies if no one is selected; None is returned if
    # user tries to game system
    if competencies:
        # competency_id=-1 when "Synthesis Table" tab is selected
        if competency_id == '-1':
            return competency_id, None, competencies
        elif competency_id is not None:
            selected_assessment = (CompetencyAssessmentUQAR.available_objects
                                   .filter(portfolio_id=portfolio_id, competency_id=competency_id).first())
        else:
            selected_assessment = (CompetencyAssessmentUQAR.available_objects
                                   .filter(portfolio_id=portfolio_id, competency_id=competencies[0]['id']).first())

        if selected_assessment is not None:
            return selected_assessment.competency_id, selected_assessment, competencies
        else:
            if competency_id in targeted_competency_ids:
                return competency_id, None, competencies
            else:
                return competencies[0]['id'], None, competencies
    else:
        return None, None, competencies


@login_required
def view_assessments(request):
    """
    Return the html page containing the forms for the user to view or edit the competency assessments
    """
    response = HttpResponse()

    # competency being assessed
    competency_id = request.GET.get('competency_id')
    user_type = request.user.type

    # get portfolio object
    portfolio = active_portfolio(request)
    # get applicant's attribution object
    try:
        attribution = Attribution.available_objects.get(applicant_id=portfolio.user.id)
    except ObjectDoesNotExist:
        attribution = Attribution(applicant_id=portfolio.user.id)
        attribution.save()

    selected_competency_id, selected_assessment, competencies = selected_competency_info(competency_id, portfolio.id)

    # indicates if a form has been saved
    progress_submission_success = False
    summative_submission_success = False

    competency_assessment = None

    # if a form is being saved
    if request.method == "POST":
        # look for this field to determine if a progress assessment form is being submitted
        if 'form_progress_assessment' in request.POST:
            form = FormProgressAssessment(None, None, True, request.POST)
            # if valid, save form to database
            if form.is_valid():
                competency_assessment, created = CompetencyAssessmentUQAR.available_objects.get_or_create(
                    portfolio_id=portfolio.id,
                    competency_id=selected_competency_id
                )
                competency_assessment.progress_candidate_level = form.cleaned_data['progress_candidate_level']
                competency_assessment.summative_candidate_level = form.cleaned_data[
                    'progress_candidate_level']  # this field must always correspond to the first one
                competency_assessment.progress_expert_level = form.cleaned_data['progress_expert_level']
                competency_assessment.progress_facet_discoverability = form.cleaned_data['facet_discoverability']
                competency_assessment.progress_easy_indicators = form.cleaned_data['easy_indicators']
                competency_assessment.progress_hard_indicators = form.cleaned_data['hard_indicators']
                competency_assessment.progress_other_questions = form.cleaned_data['other_questions']
                if competency_assessment.progress_status == AssessmentStatuses.CREATED:
                    competency_assessment.progress_status = AssessmentStatuses.UNDER_REVIEW
                competency_assessment.save()
                progress_submission_success = True
            else:
                print(form.errors.as_data())  # here you print errors to terminal
        # look for this field to determine if a summative form is being submitted
        elif 'form_summative_assessment' in request.POST:
            form = FormSummativeAssessment(None, None, True, request.POST)
            # if valid, save form to database
            if form.is_valid():
                competency_assessment, created = CompetencyAssessmentUQAR.available_objects.get_or_create(
                    portfolio_id=portfolio.id,
                    competency_id=selected_competency_id
                )
                competency_assessment.summative_candidate_level = form.cleaned_data['summative_candidate_level']
                competency_assessment.progress_candidate_level = form.cleaned_data[
                    'summative_candidate_level']  # this field must always correspond to the first one
                competency_assessment.summative_expert_level = form.cleaned_data['summative_expert_level']
                competency_assessment.summative_credits = form.cleaned_data['credits']
                competency_assessment.summative_comments = form.cleaned_data['comments']
                if competency_assessment.summative_status == AssessmentStatuses.CREATED:
                    competency_assessment.summative_status = AssessmentStatuses.UNDER_REVIEW
                competency_assessment.save()
                summative_submission_success = True
            else:
                print(form.errors.as_data())  # here you print errors to terminal

    # reference to the latest state of the competency being viewed/updated
    latest_competency_assessment = competency_assessment if progress_submission_success or \
                                                            summative_submission_success else selected_assessment

    # permissions of the user to view/edit the progress and summative assessment forms
    progress_view_permission, progress_edit_permission = get_permissions(
        user_type,
        getattr(latest_competency_assessment, 'progress_status',
                # when None, return a different status depending on if the portfolio has already been certified
                AssessmentStatuses.UNDER_REVIEW if portfolio.status != PortfolioStatuses.CERTIFIED else AssessmentStatuses.VALIDATED),
        portfolio.status
    )

    summative_view_permission, summative_edit_permission = get_permissions(
        user_type,
        getattr(latest_competency_assessment, 'summative_status',
                # when None, return a different status depending on if the portfolio has already been certified
                AssessmentStatuses.UNDER_REVIEW if portfolio.status != PortfolioStatuses.CERTIFIED else AssessmentStatuses.VALIDATED),
        portfolio.status
    )

    # based on provided permissions, return forms w/o data with enabled/disabled fields
    form_progress_assessment = FormProgressAssessment(
        latest_competency_assessment,
        progress_view_permission,
        progress_edit_permission
    )
    form_summative_assessment = FormSummativeAssessment(
        latest_competency_assessment,
        summative_view_permission,
        summative_edit_permission
    )

    competency_assessments = CompetencyAssessmentUQAR.available_objects.filter(portfolio_id=portfolio.id)
    requested_credits, recommended_credits = calculate_credits(competency_assessments)

    context = {
        'selected_competency_id': selected_competency_id,
        'portfolio': portfolio,
        'competencies': competencies,
        'competency_assessments': competency_assessments,
        'user_type': request.user.type,
        'recommended_credits': recommended_credits,
        'requested_credits': requested_credits,
        'form_progress_assessment': form_progress_assessment,
        'form_summative_assessment': form_summative_assessment,
        'progress_edit_permission': progress_edit_permission,
        'summative_edit_permission': summative_edit_permission,
        'progress_view_permission': progress_view_permission,
        'summative_view_permission': summative_view_permission,
        'interview_date': attribution.interview.strftime(DATE_FORMAT) if attribution.interview else '',
        'document_date': attribution.document.strftime(DATE_FORMAT) if attribution.document else '',
    }

    template = loader.get_template('portfolio/assessments.html')
    response.content = template.render(context, request)

    return response


def update_assessment_status(request, portfolio_id, assessment_type, new_status):
    success = True
    try:
        if assessment_type == AssessmentTypes.PROGRESS.value:
            (CompetencyAssessmentUQAR.available_objects
             .filter(portfolio_id=portfolio_id).update(progress_status=new_status))
        elif assessment_type == AssessmentTypes.SUMMATIVE.value:
            (CompetencyAssessmentUQAR.available_objects
             .filter(portfolio_id=portfolio_id).update(summative_status=new_status))
        else:
            success = False
    except Exception as e:
        print(e)
        success = False

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def finalize_assessments(request, assessment_type):
    portfolio = active_portfolio(request)
    if assessment_type == AssessmentTypes.SUMMATIVE.value:
        attribution = Attribution.available_objects.get(applicant_id=portfolio.user.id)
        attribution.document = date.today().strftime(DATE_FORMAT)
        attribution.save()
        portfolio.status = PortfolioStatuses.REVIEWED.value
    elif assessment_type == AssessmentTypes.PROGRESS.value:
        portfolio.status = PortfolioStatuses.PRE_REVIEWED.value
    portfolio.save()
    return update_assessment_status(request, portfolio.id, assessment_type, AssessmentStatuses.FINAL)


@login_required
def certify_assessments(request, assessment_type):
    portfolio = active_portfolio(request)
    if assessment_type == AssessmentTypes.SUMMATIVE.value:
        portfolio.status = PortfolioStatuses.CERTIFIED.value
    elif assessment_type == AssessmentTypes.PROGRESS.value:
        portfolio.status = PortfolioStatuses.PRE_CERTIFIED.value
    portfolio.save()
    return update_assessment_status(request, portfolio.id, assessment_type, AssessmentStatuses.VALIDATED)


@login_required
def review_assessments(request, assessment_type):
    portfolio = active_portfolio(request)
    if assessment_type == AssessmentTypes.SUMMATIVE.value:
        portfolio.status = PortfolioStatuses.REVIEWED.value
    elif assessment_type == AssessmentTypes.PROGRESS.value:
        portfolio.status = PortfolioStatuses.PRE_REVIEWED.value
    portfolio.save()
    return update_assessment_status(request, portfolio.id, assessment_type, AssessmentStatuses.UNDER_REVIEW)


@login_required()
def assess_portfolio(request):
    try:
        portfolio = active_portfolio(request)
        portfolio.status = PortfolioStatuses.UNDER_REVIEW.value
        portfolio.save()
        success = True
    except Exception as e:
        print(e)
        success = False

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def set_interview_assessment_date(request, date):
    """
    How to validate date strings: https://www.geeksforgeeks.org/python-validate-string-date-format/
    """
    try:
        valid_date = bool(datetime.strptime(date, DATE_FORMAT))
    except ValueError:
        valid_date = False

    if valid_date:
        portfolio = active_portfolio(request)
        attribution = Attribution.available_objects.get(applicant_id=portfolio.user.id)
        attribution.interview = date
        attribution.save()
        success = True
    else:
        success = False

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )
