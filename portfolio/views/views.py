# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale
import os
import uuid
from datetime import date
from json import JSONDecodeError
from typing import List

from delta import html
from django import forms
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.core.files.storage import FileSystemStorage
from django.db.models import QuerySet, Sum
from django.forms import Form, model_to_dict
from django.http import HttpResponse, HttpResponseRedirect, QueryDict
from django.shortcuts import redirect, render
from django.template import loader
from django.utils.text import slugify
from django.utils.translation import gettext as _
from urlextract import URLExtract
from weasyprint import HTML

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE
from portfolio.models import AchievementDevelopment, AchievementContext, AchievementMotivation, Document, Portfolio, \
    Assessment, UserAchievement, UserAssessment, AchievementType, XFormField, \
    XFieldType, XForm, XFormEntry, DocumentType, CompetencyAssessmentUQAR
from portfolio.views import calculate_credits
from program.models import Competency, CustomCompetency, CustomCompetencyType, Concentration
from user.models import User, Attribution
from utils.views import check_restriction, get_portfolio, get_help_link, active_portfolio, active_portfolio_type

locale.setlocale(locale.LC_ALL, '')

DATE_FORMAT = '%Y-%m-%d'

def process_associated_op(json_ops, associated_skill_ops, achievement_number):
    """Process an associated skill op

    Add achievement number to an associated_skill_ops entry if associated skill already exists
    Create a new associated_skill_ops entry otherwise

    Args:
        json_ops:Object
        associated_skill_ops:Dictionary
        achievement_number:Int

    Returns:
        Void
    """
    associated_skill = json_ops['attributes']['name']
    # Check if this associated skill already exists
    for entry in associated_skill_ops:
        if entry['associated_skill'] == associated_skill:
            # Already exists: update dictionary and break
            entry[achievement_number] = json_ops['id']
            return
    # Does not exist: create a new entry
    associated_skill_ops.append({
        'associated_skill': associated_skill,
        achievement_number: json_ops['id']
    })
    return


@login_required
def skills_overview(request, portfolio_id):
    """Generate skills view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/skills.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    # Get portfolio object
    portfolio = get_portfolio(portfolio_id)
    # get 'parent' skills ONLY excluding children to avoid confusion w/ 'code' which resets for children
    skills = Competency.available_objects.filter(
        program=portfolio.program,
        parent_id=None)
    program_groups = portfolio.program.groups

    # group skills based on parent (ideal for sorting) and then add children as sub-array under parent
    grouped_skills = []
    children = None
    has_children = True
    for parent_skill in skills:
        # get children based on parent skill's ID
        if has_children:
            children = Competency.available_objects.filter(parent_id=parent_skill.id)
            if not children:
                has_children = False
        # add sub-dict for each parent skill w/ object of parent skill + children as sub-array
        grouped_skills.append({
            "parent": parent_skill,
            "children": children
        })

    context = {
        'portfolio': portfolio,
        'skills': skills, # original 1-level skills (not ideal for sorting)
        'program_groups': program_groups,
        'grouped_skills': grouped_skills,
        'skills_active': True,  # Tell side menu which page is displayed
        'has_children': has_children,    # Skill set has children
    }
    if IS_CONTINUOUS:
        context.update(_skills_overview_continuous(request, portfolio))
    else:
        context.update(_skills_overview_milestone(request, portfolio))

    template = loader.get_template('portfolio/skills.html')
    return HttpResponse(template.render(context, request))


@login_required
def facets_overview(request, portfolio_id):
    """Generate facets view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/facets.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    # Get portfolio object
    portfolio = get_portfolio(portfolio_id)
    portfolio_type = active_portfolio_type()
    tabs = portfolio_type.get_achievement_types()

    competency_type = CustomCompetencyType.available_objects.get(code="facets")
    # get 'parent' skills ONLY excluding children to avoid confusion w/ 'code' which resets for children
    top_skills = CustomCompetency.available_objects.filter(
        type=competency_type,
        parent_id=None
    )

    # group skills based on parent (ideal for sorting) and then add children as sub-array under parent
    # grouped_skills = get_grouped_skills(top_skills)

    # print("grouped_skills")
    # print(grouped_skills)

    context = {
        'portfolio': portfolio,
        'skills': top_skills, # original 1-level skills (not ideal for sorting)
        'program_groups': [],
        # 'grouped_skills': grouped_skills,
        'facets_active': True,  # Tell side menu which page is displayed
        'has_children': True,    # Skill set has children
    }
    if IS_CONTINUOUS:
        context.update(_skills_overview_continuous(request, portfolio))
    else:
        context.update(_skills_overview_milestone(request, portfolio))

    # print("final context")
    # print(context)

    template = loader.get_template('portfolio/facets.html')
    return HttpResponse(template.render(context, request))

# Up to 3 levels deep
def get_grouped_skills(top_skills: list[CustomCompetency]):
    grouped_skills = []
    for parent_skill in top_skills:

        sub_group = []
        for child_skill in parent_skill.children():
            children = child_skill.children()
            sub_group.append({
                "parent": child_skill,
                "children": children,
                "has_children": children.count() > 0
            })

        # add sub-dict for each parent skill w/ object of parent skill + children as sub-array
        grouped_skills.append({
            "parent": parent_skill,
            "children": sub_group,
            "has_children": len(sub_group) > 0
        })
    return grouped_skills


def _skills_overview_continuous(request, portfolio):
    # Get skill list for that user
    user_type = request.user.type
    achievements = portfolio.get_achievements(user_type)

    # Skills dictionaries table
    skill_ops = []
    # Associated skills dictionaries table
    associated_skill_ops = []

    # group UserAchievement instances per Achievement to keep proper order when displaying skills
    blocks_per_achievement = []
    for achievement in achievements:
        blocks = UserAchievement.available_objects.filter(achievement=achievement)
        blocks_per_achievement.append(blocks)

    # 1 loop iteration = 1 achievement
    for idx, blocks in enumerate(blocks_per_achievement):
        achievement_skills = {}

        user_block: UserAchievement
        for user_block in blocks:
            if user_block.attributes is None:
                continue

            achievement = user_block.achievement
            for attributes, insert in user_block.attributes:
                # Get the blots' attributes
                json_ops = json.loads(attributes)
                if json_ops['type'] in ('competency', 'facets'):
                    competency_id = json_ops['attributes']['competency_id']
                    # competency
                    achievement_skills[competency_id] = {
                        "id": json_ops['id'],
                        "tab": user_block.achievement_type.number
                    }
                elif json_ops['type'] == 'associated_skill':
                    # Associated
                    process_associated_op(json_ops, associated_skill_ops, achievement.number)

        # Append list even if empty for straightforward template processing
        skill_ops.append(achievement_skills)

    # print("skills-overview-continuous -> context")
    # print(skill_ops)

    return {
        'achievements': achievements,
        'num_achievements': achievements.count(),
        'user_type': user_type,
        'associated_skill_ops': associated_skill_ops,
        'skill_ops': skill_ops,
    }


def _skills_overview_milestone(request, portfolio):
    """Deprecated"""
    # todo mode=milestone-pilote; hard-coded for now, from 'custom_competency_type' model; however, still a few
    #   things that are specific to this 'skills'; will make it fully generic in next iteration
    competency_type_id = 4

    # Get skill list for that user
    full_competency_type = CustomCompetencyType.available_objects.get(id=competency_type_id)
    user_type = portfolio.user.type
    portfolio_type = active_portfolio_type()
    achievement_types = portfolio_type.get_achievement_types()

    # refer to 'custom_competencies_overview' for more info on why we use this code
    relevant_achievement_types = []
    achievement: AchievementType
    for achievement in achievement_types:
        # keep this achievement to be displayed to user ONLY IF it allows the current 'competency_type'
        if competency_type_id in achievement.get_comp_types_ids():
            relevant_achievement_types.append(achievement)

    # Skills dictionaries table
    skill_ops = []
    for achievement in relevant_achievement_types:
        achievement_skills = {}

        # get the latest block contents for the specified achievement type
        user_achievement_blocks: list[UserAchievement] = UserAchievement.available_objects.filter(
            achievement_type=achievement,
            portfolio=portfolio
        )

        # for each block, track the custom competencies used
        block: UserAchievement
        for block in user_achievement_blocks:
            if block.attributes is None:
                continue

            for attributes, insert in block.attributes:
                # Get the blots' attributes
                json_ops = json.loads(attributes)
                # IMPORTANT: 'vocabulary' does NOT have 'competency_id' which causes a crash, so just catch this
                if json_ops['type'] == 'vocabulary':
                    continue

                achievement_skills[json_ops['attributes']['competency_id']] = {
                    "id": json_ops['id'],
                    "milestone": block.block.milestone # needed to auto-click proper tab when loading specific tag
                }
                # print(f"user-achievement-block :: {block.milestone}")

        # Append list even if empty for straightforward template processing
        skill_ops.append(achievement_skills)

    return {
        'achievements': relevant_achievement_types,
        'num_achievements': len(relevant_achievement_types),
        'user_type': user_type,
        'skill_ops': skill_ops,
    }


@login_required
def custom_competencies_overview(request, portfolio_id, competency_type_id):
    """Generate custom competencies view

    Args:
        portfolio_id: uuid
        competency_type_id: int

    Returns:
        HttpResponse (portfolio/skills.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    # Get portfolio object
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    portfolio_type = active_portfolio_type()
    achievement_types = portfolio_type.get_achievement_types()
    # Get skill list for that user
    competency_type = CustomCompetencyType.available_objects.get(id=competency_type_id)
    skills = CustomCompetency.available_objects.filter(type=competency_type)

    # MODE=MILESTONE; NOT all competencies are available in each achievement, so ONLY keep matching ones;
    #   could have done it directly in HTML template, but cleaner to do it in Python and return ONLY valid achievements
    #   to HTML template
    relevant_achievement_types = []
    for achievement_type in achievement_types:
        # keep this achievement to be displayed to user ONLY IF it allows the current 'competency_type'
        if competency_type_id in achievement_type.get_comp_types_ids():
            relevant_achievement_types.append(achievement_type)

    # Skills dictionaries table
    skill_ops = []

    for achievement_type in relevant_achievement_types:
        # skills for this specific achievement
        achievement_skills = {}

        # get the latest block contents for the specified achievement type
        user_achievements = UserAchievement.available_objects.filter(achievement_type=achievement_type,
                                                                     portfolio=portfolio)

        # for each block, track the custom competencies used
        for user_achievement in user_achievements:
            # skip this iteration IF NO attributes
            if user_achievement.attributes is None:
                continue

            for attributes, insert in user_achievement.attributes:
                # Get the blots' attributes
                json_ops = json.loads(attributes)
                if json_ops['type'] == competency_type.code:
                    op_attributes = json_ops['attributes']
                    competency_id = op_attributes['competency_id']
                    level = op_attributes.get('level')      # NOT always present; depends on custom competency type
                    attribute_id = str(json_ops['id'])      # a timestamp

                    # if level is present in Quill, then append w/ @ to attribute_id; will do substr on client-side
                    # IMPORTANT: suffix @ works great w/ JS since `parseInt` correctly ignores the @<number> part
                    if level:
                        attribute_id += f'@{level}'

                    achievement_skills[f'{user_achievement.block.milestone}:{competency_id}'] = {'id': attribute_id}

        # Append list even if empty for straightforward template processing
        # skill_ops = [{<achievement-skills>},{<achievement-skills>},...] = 1 JSON object per relevant achievement
        skill_ops.append(achievement_skills)

    context = {
        'portfolio': portfolio,
        'competency_type': competency_type,
        'skills': skills,
        'achievements': relevant_achievement_types,
        'num_achievements': len(relevant_achievement_types),
        'skill_ops': skill_ops,
        'active_comp_type': competency_type_id or 0     # used to indicate which menu item is active
    }

    template = loader.get_template('portfolio/custom-competencies.html')
    return HttpResponse(template.render(context, request))


def process_knowledge_op(json_ops, user_block):
    """Returns formatted knowledge dictionary

    Args:
        json_ops: Object
        user_block: UserAchievement

    Returns:
        Dictionary

    """

    # Achievement derived from UserAchievement
    achievement = user_block.achievement
    # (continuous) AchievementType.number == tab number
    tab = user_block.achievement_type.number
    return {
        'id': json_ops['id'],
        'cls': json_ops['class'],
        'name': json_ops['attributes']['name'],
        'explanation': json_ops['attributes']['explanation'],
        'achievement': achievement.name,
        'achievement_number': achievement.number,
        'achievement_id': achievement.id,
        'tab': tab
    }


@login_required
def knowledge(request, portfolio_id, knowledge_type):
    """Generate knowledge or soft-skills view

    Args:
        portfolio_id: uuid
        knowledge_type: Str (knowledge | soft-skill)

    Returns:
        HttpResponse (portfolio/knowledge.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    user_type = request.user.type
    achievements = portfolio.get_achievements(user_type)
    user_achievement_blocks = UserAchievement.available_objects.filter(portfolio_id=portfolio_id)

    knowledge_ops = []
    block: UserAchievement
    for block in user_achievement_blocks:
        if block.attributes is None:
            continue

        for attributes, insert in block.attributes:
            json_ops = json.loads(attributes)
            if json_ops['type'].replace('_', '-') == knowledge_type:
                single_op = process_knowledge_op(json_ops, block)
                knowledge_ops.append(single_op)

    # print("portfolio.views.knowledge")
    # print(knowledge_ops)

    # Does not work on BSD / OSX, should use PyICU
    knowledge_ops_sort_knowledge = sorted(knowledge_ops, key=lambda x: locale.strxfrm(x['name']))
    knowledge_ops_sort_achievement = sorted(knowledge_ops, key=lambda x: x['achievement_number'])
    # knowledge_ops = sorted(knowledge_ops, key=itemgetter('achievement_number'))

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'knowledge_ops_sort_achievement': knowledge_ops_sort_achievement,
        'knowledge_ops_sort_knowledge': knowledge_ops_sort_knowledge,
        'knowledge_type': knowledge_type,
        'user_type': user_type,
    }

    template = loader.get_template('portfolio/knowledge.html')
    return HttpResponse(template.render(context, request))


@login_required
def assessment(request, portfolio_id):
    """Assessment grid for user portfolio (user provided in request)
    MODE=MILESTONE

    Args:
        request
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/assessment.html)
    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    request_user = request.user
    portfolio_user = portfolio.user

    # make sure student is NOT able to view assessment when under review
    if request_user == portfolio_user and portfolio_user.group.status == 'Underreview':
        return redirect('portfolio_index', portfolio_id)

    # Get portfolio assessments structured to create form
    # [milestones
    #     [milestone_assessments
    #         [assessment]
    #     ]
    # ]
    assessments = Assessment.available_objects.filter(milestone__lte=portfolio_user.group.milestone)
    milestones = []
    milestone_assessments = []
    this_milestone = 1
    # Create a dict -> json of all rating criteria to dynamically populate table
    portfolio_type = active_portfolio_type()
    assessment_ratings = portfolio_type.assessment_ratings.all()
    rating_choices = []
    for assessment_rating in assessment_ratings:
        rating_choices.append(assessment_rating.base100())
    criteria_dict = {}

    for assessment in assessments:
        dict_key = f'{assessment.milestone}_{assessment.code}_'
        object_key = 'criteria_'
        for rating_choice in rating_choices:
            criteria = getattr(assessment, f'{object_key}{rating_choice}')
            if criteria:
                criteria_dict[f'{dict_key}{rating_choice}'] = criteria
        if assessment.milestone != this_milestone:
            milestones.append(milestone_assessments)
            milestone_assessments = []
            this_milestone = assessment.milestone
        milestone_assessments.append(assessment)
    milestones.append(milestone_assessments)

    # Put existing user assessments in dict -> json to populate the form
    user_assessments = UserAssessment.available_objects.filter(portfolio=portfolio)
    user_assessments_dict = {}
    for user_assessment in user_assessments:
        key = f'{user_assessment.assessment.milestone}_{user_assessment.assessment.code}'
        user_assessments_dict[key] = user_assessment.grade
        if user_assessment.assessment.code != 'total':
            key = f'{user_assessment.assessment.milestone}_{user_assessment.assessment.code}_comment'
            user_assessments_dict[key] = user_assessment.comment

    # owner CANNOT edit his assessment, ONLY reviewers; NO student can edit any
    read_only = request_user == portfolio_user or request_user.type == 'Applicant'

    context = {
        'portfolio': portfolio,
        # 'achievements': portfolio.type.get_achievement_types(),
        'group_milestone': portfolio_user.group.milestone,
        'milestones': milestones,
        'criterias': json.dumps(criteria_dict),
        'user_assessments': json.dumps(user_assessments_dict),  # None will convert to 'null' in JS via try/catch
        'read_only': read_only,
        # student w/ NOT yet assessed portfolio
        'hide_grades': read_only and not user_assessments,  # ???
        'assessment_active': True,
        'rating_choices': portfolio_type.get_assessment_ratings(),
    }

    template = loader.get_template('portfolio/assessment.html')
    return HttpResponse(template.render(context, request))


def _process_vocabulary_op_continuous(json_ops, insert, user_block, extract=True):
    """Return formatted vocabulary dictionary

    Args:
        json_ops: Object
        insert: Str
        user_block: UserAchievement
        extract: Bool (Does the URL needs to be extracted and formatted)

    Returns:
        Dictionary

    """

    definition = json_ops['attributes']['definition']

    if extract:
        extractor = URLExtract()
        for url in extractor.gen_urls(definition):
            if 'http' in definition:
                href = f'<a href="{url}">{url}</a>'
            else:
                href = f'<a href="https://{url}">{url}</a>'
            definition = definition.replace(url, href)

    definition = definition.replace('\n', '<br>')

    # Achievement derived from UserAchievement
    achievement = user_block.achievement
    # (continuous) AchievementType.number == tab number
    tab = user_block.achievement_type.number
    return {
        'id': json_ops['id'],
        'cls': json_ops['class'],
        'tab': tab,
        'insert': insert.capitalize(),
        'definition': definition,
        'achievement': achievement.name,
        'achievement_number': achievement.number,
        'achievement_id': achievement.id,
    }


def _process_vocabulary_op_milestone(json_ops, insert, achievement: UserAchievement, extract=True):
    """Return formatted vocabulary dictionary

    Args:
        json_ops: Object
        insert: Str
        achievement: UserAchievement
        extract: Bool (Does the URL needs to be extracted and formatted)

    Returns:
        Dictionary

    """

    definition = json_ops['attributes']['definition']

    if extract:
        extractor = URLExtract()
        for url in extractor.gen_urls(definition):
            if 'http' in definition:
                href = f'<a href="{url}">{url}</a>'
            else:
                href = f'<a href="https://{url}">{url}</a>'
            definition = definition.replace(url, href)

    definition = definition.replace('\n', '<br>')

    return {
        'id': json_ops['id'],
        'cls': json_ops['class'],
        'tab': achievement.block.milestone,
        'insert': insert.capitalize(),
        'definition': definition,
        'achievement': achievement.achievement_type.name,
        'achievement_number': achievement.achievement_type.number,
        'achievement_id': achievement.achievement_type_id,
    }


def _process_comment_op_milestone(json_ops, insert, achievement: UserAchievement, extract=True):
    """Return formatted comments dictionary

    Args:
        json_ops: Object
        insert: Str
        achievement: UserAchievement
        extract: Bool (Does the URL needs to be extracted and formatted)

    Returns:
        Dictionary

    """

    comment = json_ops['attributes']['comment']

    if extract:
        extractor = URLExtract()
        for url in extractor.gen_urls(comment):
            if 'http' in comment:
                href = f'<a href="{url}">{url}</a>'
            else:
                href = f'<a href="https://{url}">{url}</a>'
            comment = comment.replace(url, href)

    comment = comment.replace('\n', '<br>')

    return {
        'id': json_ops['id'],
        'cls': json_ops['class'],
        'tab': achievement.block.milestone,
        'block_number': achievement.block.number,
        'insert': insert.capitalize(),
        'comment': comment,
        'author': json_ops['attributes']['author'],
        'date': date.fromtimestamp(json_ops['attributes']['date']/1000),
        'achievement': achievement.achievement_type.name,
        'achievement_number': achievement.achievement_type.number,
        'achievement_id': achievement.achievement_type_id,
    }

@login_required
def glossary_continuous(request, portfolio_id):
    """Generate glossary view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/glossary.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    user_type = request.user.type
    achievements = portfolio.get_achievements(user_type)
    user_achievement_blocks = UserAchievement.available_objects.filter(portfolio_id=portfolio_id)

    glossary_ops = []
    block: UserAchievement
    for block in user_achievement_blocks:
        if block.attributes is None:
            continue

        for attributes, insert in block.attributes:
            json_ops = json.loads(attributes)
            if json_ops['type'] == 'vocabulary':
                single_op = _process_vocabulary_op_continuous(json_ops, insert, block)
                glossary_ops.append(single_op)

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'glossary_ops': sorted(glossary_ops, key=lambda x: locale.strxfrm(x['insert'])),
        'user_type': user_type,
        'glossary_active': True,
    }

    template = loader.get_template('portfolio/glossary.html')
    return HttpResponse(template.render(context, request))


@login_required
def glossary_milestone(request, portfolio_id):
    """Generate glossary view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/glossary.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = get_portfolio(portfolio_id)
    request_user = request.user
    user_type = request_user.type

    achievements = UserAchievement.available_objects.filter(portfolio=portfolio)
    glossary_ops = []
    for achievement in achievements:
        if achievement.attributes is None:
            continue

        for attributes, insert in achievement.attributes:
            json_ops = json.loads(attributes)
            if json_ops['type'] == 'vocabulary':
                glossary_ops.append(
                    _process_vocabulary_op_milestone(json_ops, insert, achievement)
                )

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'glossary_ops': sorted(glossary_ops, key=lambda x: locale.strxfrm(x['insert'])),
        'user_type': user_type,
        'glossary_active': True,
    }

    template = loader.get_template('portfolio/glossary.html')
    return HttpResponse(template.render(context, request))



@login_required
def comments_milestone(request, portfolio_id):
    """Generate comments view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/comments.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = get_portfolio(portfolio_id)

    achievements = UserAchievement.available_objects.filter(portfolio=portfolio)
    comments_ops = []
    for achievement in achievements:
        if achievement.comments is None:
            continue

        for comments, insert in achievement.comments:
            json_ops = json.loads(comments)
            if json_ops['type'] == 'simple_comment':
                comments_ops.append(
                    _process_comment_op_milestone(json_ops, insert, achievement)
                )

    comments_ops = sorted(comments_ops, key=lambda x: (-x['milestone'], x['date'], x['block_number']))

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'comments_ops': comments_ops,
        'comments_active': True,
    }

    template = loader.get_template('portfolio/comments.html')
    return HttpResponse(template.render(context, request))


def process_level_op(json_ops, current_level_2, validations):
    """Process expertise level op

    Args:
        json_ops: Object
        current_level_2: Char
        validations: Dictionary

    Raises:
        ValueError (More than 3 level 2 in achievement)

    Returns:
        current_level_2 (char)

    """

    level = json_ops['attributes']['level']
    if not level:
        return current_level_2
    validation = {
        'score': int(json_ops['attributes']['validation']),
        'id': json_ops['id'],
        'cls': json_ops['class'],
    }
    if ord(level) >= ord('M'):
        if current_level_2 == 'H':
            raise ValueError(json_ops)
        level = current_level_2
        current_level_2 = chr(ord(current_level_2) + 1)

    validations[level] = validation
    return current_level_2


def compute_achievement_points(validations):
    """Compute points for an achievement

    Args:
        validations: Dictionary

    Returns:
        points (Int)

    """

    points = 0
    for value in validations.values():
        if value is not None:
            points += value['score']
    return points


def get_portfolio_attribution(scale, total_points, concentration):
    """Returns credit attribution for portfolio

    Args:
        scale: Array
        total_points: Int
        concentration: Char

    Returns:
        Dictionary

    """

    for step in scale:
        if total_points >= step['min']:
            if concentration == 'E':
                return {'credits': step['credits'], 'milestone': step['milestone']}
            else:
                return {'credits': step['credits']}


def get_portfolio_levels(portfolio, user=None):
    """Computes values required for expertise levels view

    Args:
        portfolio: Object
        user: Object

    Returns:
        Dictionary

    """

    concentration = portfolio.program.concentration
    achievements = portfolio.get_achievements(user_type='Reviewer')


    # Check if CV is included
    cv = False
    # Check if Interview preparation sheet is required and included
    if achievements.count() < 3:
        ips = True
    else:
        ips = False
    # Check if every achievement has at least one document association
    proofed = False

    # Check if there are comments remaining in the portfolio
    comments = []

    # Check if every achievement is marked as final
    check_finalize = False
    finalized = False
    # Check if portfolio is ready to be certified by expert or supervisor
    certify = False

    if user is None:
        user_type = ''
    else:
        user_type = user.type
        if user_type == 'Applicant' and portfolio.status == 'Draft':
            check_finalize = True
            if achievements:
                finalized = True
                proofed = True
                global_docs = Document.available_objects.filter(portfolio=portfolio, indice_1=0)
                for doc in global_docs:
                    if doc.indice_2 == 'A':
                        cv = True
                    elif doc.indice_2 == 'C':
                        ips = True
        elif user == portfolio.user.expert and portfolio.status == 'Underreview':
            certify = True
        elif user_type == 'Supervisor':
            if portfolio.status == 'Reviewed' or (portfolio.status == 'Final' and portfolio.user.expert is None):
                certify = True

    # levels
    total_points = 0
    achievement_validations = []

    for achievement in achievements:
        if check_finalize:
            if achievement.status != 'Final':
                finalized = False
            if not achievement.documents.all():
                proofed = False
            for value in achievement.comments.values():
                if value:
                    comments.append(achievement.number)
                    break

        points = 0
        validations = concentration.get_expertise_levels()
        if achievement.ops is not None:
            current_level_2 = 'E'
            for attributes, insert in achievement.ops:
                # Get the blots' attributes
                json_ops = json.loads(attributes)
                if json_ops['type'] == 'expertise_level':
                    current_level_2 = process_level_op(json_ops, current_level_2, validations)

            # Compute points for this achievement
            points = compute_achievement_points(validations)
            total_points += points

        # Append list even if empty for straightforward template processing
        validations['total'] = {'score': points}
        achievement_validations.append([achievement, validations])

    scale = concentration.get_credits_scale()
    attribution = get_portfolio_attribution(scale, total_points, concentration.type)

    # Check if credits attribution satisfies applicant's target
    if portfolio.user.nb_credits <= attribution['credits']:
        validated = True
    else:
        validated = False

    return {
        # Attribution data
        'achievement_validations': achievement_validations,
        'attribution': attribution,
        'portfolio': portfolio,
        'concentration': concentration,
        'scale': scale,
        'total_points': total_points,
        'user_type': user_type,
        # Validation data
        'certify': certify,
        'comments': comments,
        'cv': cv,
        'finalized': finalized,
        'ips': ips,
        'proofed': proofed,
        'validated': validated,
        # Tell side menu which page is displayed
        'levels_active': True,
        'help': get_help_link('levels'),
    }


@login_required
def levels(request, portfolio_id):
    """Generate expertise levels view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/levels.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    # Get portfolio object
    portfolio = Portfolio.available_objects.get(id=portfolio_id)

    restrict = check_restriction(request, portfolio)
    if restrict:
        return restrict

    context = get_portfolio_levels(portfolio, request.user)

    template = loader.get_template('portfolio/levels.html')
    return HttpResponse(template.render(context, request))


def process_explain_op(json_ops, insert, user_block):
    """Returns explicitation zone op dictionary

    Args:
        json_ops: Object
        insert: Str
        user_block: UserAchievement

    Returns:
        Dictionary

    """

    explanation = json_ops['attributes']['explanation']
    explanation = explanation.replace('\n', '<br>')

    achievement = user_block.achievement
    tab = user_block.achievement_type.number
    return {
        'id': json_ops['id'],
        'cls': json_ops['class'],
        'tab': tab,
        'explanation': explanation,
        'insert': insert,
        'achievement': achievement.name,
        'achievement_number': achievement.number,
        'achievement_id': achievement.id,
    }


@login_required
def explicitation(request, portfolio_id):
    """Generate explicitation zones view

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (portfolio/explicitation.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    user_type = request.user.type
    achievements = portfolio.get_achievements(user_type)
    user_achievement_blocks = UserAchievement.available_objects.filter(portfolio=portfolio)

    explicitation_zones = []
    block: UserAchievement
    for block in user_achievement_blocks:
        if block.attributes is None:
            continue

        for attributes, insert in block.attributes:
            json_ops = json.loads(attributes)
            if json_ops['type'] == 'explain':
                single_op = process_explain_op(json_ops, insert, block)
                explicitation_zones.append(single_op)

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'explicitation_zones': explicitation_zones,
        'explicitation_active': True,
    }

    template = loader.get_template('portfolio/explicitation.html')
    return HttpResponse(template.render(context, request))


@login_required
def print_portfolio_continuous(request, portfolio_id, mode):
    """Print portfolio

    Args:
        portfolio_id: uuid
        mode: str (read / print / pdf)

    Returns:
        HttpResponse    (print/portfolio.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    user: User = request.user

    # Models objects
    user_type = user.type
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    concentration = portfolio.program.concentration
    skills = Competency.available_objects.filter(
        program=portfolio.program,
        parent_id=None)
    achievements = portfolio.get_achievements(user_type)

    # group skills based on parent (ideal for sorting) and then add children as sub-array under parent
    grouped_skills = []
    for parent_skill in skills:
        # get children based on parent skill's ID
        children = Competency.available_objects.filter(parent_id=parent_skill.id)
        # add sub-dict for each parent skill w/ object of parent skill + children as sub-array
        grouped_skills.append({
            "parent": parent_skill,
            "children": children
        })

    # UQAR: facets (only top 3 levels since rest is done in template using CustomCompetency.has_children & .children
    competency_type = CustomCompetencyType.available_objects.get(code="facets")
    # get 'parent' skills ONLY excluding children to avoid confusion w/ 'code' which resets for children
    facets = CustomCompetency.available_objects.filter(
        type=competency_type,
        parent_id=None
    )

    # Variables
    # Skills dictionaries table (UQAR: mix of 'competency' & 'facets' in same array)
    skill_ops = []
    # Associated skills dictionaries table
    associated_skill_ops = []
    # Rendered achievements
    html_achievements = []
    knowledge_ops = []
    soft_skill_ops = []
    glossary_ops = []
    explicitation_ops = []
    # levels
    total_points = 0
    achievement_validations = []

    #
    # Achievements
    #
    for achievement in achievements:
        if achievement.status == 'Created':
            continue
        achievement_skills = []
        points = 0
        validations = concentration.get_expertise_levels()
        current_level_2 = 'E'

        # get UserAchievement blocks for this achievement
        blocks = (UserAchievement.available_objects
                  .filter(achievement=achievement)
                  .order_by("block__number"))

        for block in blocks:
            if block.attributes is None:
                continue

            for attributes, insert in block.attributes:
                # Get the blots' attributes
                json_ops = json.loads(attributes)
                if json_ops['type'] in ('competency', 'facets'):
                    # Competency
                    skill = json_ops['attributes']['competency_id']
                    achievement_skills.append(skill)
                elif json_ops['type'] == 'associated_skill':
                    # Associated skill
                    process_associated_op(json_ops, associated_skill_ops, achievement.number)
                elif json_ops['type'] == 'knowledge':
                    single_op = process_knowledge_op(json_ops, block)
                    knowledge_ops.append(single_op)
                elif json_ops['type'] == 'soft_skill':
                    single_op = process_knowledge_op(json_ops, block)
                    soft_skill_ops.append(single_op)
                elif json_ops['type'] == 'vocabulary':
                    single_op = _process_vocabulary_op_continuous(json_ops, insert, block, False)
                    glossary_ops.append(single_op)
                elif json_ops['type'] == 'explain':
                    single_op = process_explain_op(json_ops, insert, block)
                    explicitation_ops.append(single_op)
                elif json_ops['type'] == 'expertise_level':
                    if achievement.number > concentration.max_num_achievements:
                        continue
                    current_level_2 = process_level_op(json_ops, current_level_2, validations)

            # Compute points for this achievement
            points = compute_achievement_points(validations)
            total_points += points

        # Append list even if empty for straightforward template processing
        skill_ops.append(achievement_skills)
        validations['total'] = {'score': points}
        achievement_validations.append([achievement, validations])

        # extract & Quill-render 'ops' from each UserAchievement
        block_contents = []
        for block in blocks:
            try:
                ops = json.loads(block.content)['ops']
                quill_rendering = html.render(ops) # uses Delta python library
                block_contents.append({
                    'title': block.block.instruction or "",
                    'content': quill_rendering,
                    'word_count': len(quill_rendering.split()),
                    'max_word_count': block.block.target_words
                })
            except IndexError:
                continue

        html_achievement = {
            'name': achievement.name,
            'number': achievement.number,
            'targeted_competency': achievement.targeted_competency,
            'block_contents': block_contents,
        }
        html_achievements.append(html_achievement)

    scale = concentration.get_credits_scale()
    attribution = get_portfolio_attribution(scale, total_points, concentration.type)

    #
    # Documents
    #
    portfolio_files = Document.available_objects.filter(portfolio=portfolio)
    global_files = {}
    documents = []
    for file in portfolio_files:
        if file.indice_1 == 0:
            global_files[file.indice_2] = file
        else:
            documents.append(file)

    #
    # mode
    #
    if mode == 'read':
        page_break = False
        base_template = 'base.html'
    else:
        page_break = True
        base_template = f'print/{mode}.html'

    #
    # Context
    #
    context = {
        'request': request,
        'user_type': user_type,
        'portfolio': portfolio,
        'achievements': achievements,
        # skills
        'num_achievements': achievements.count(),
        'skills': skills,
        'associated_skill_ops': associated_skill_ops,
        'grouped_skills': grouped_skills,
        'skill_ops': skill_ops,
        # facets
        'facets': facets,
        # achievements
        'html_achievements': html_achievements,
        'knowledge_ops': sorted(knowledge_ops, key=lambda x: locale.strxfrm(x['name'])),
        'soft_skill_ops': sorted(soft_skill_ops, key=lambda x: locale.strxfrm(x['name'])),
        'glossary_ops': sorted(glossary_ops, key=lambda x: locale.strxfrm(x['insert'])),
        'explicitation_ops': explicitation_ops,
        # documents
        'global_files': global_files,
        'documents': documents,
        # levels
        'attribution': attribution,
        'concentration': concentration,
        'scale': scale,
        'total_points': total_points,
        'achievement_validations': achievement_validations,
        # mode
        'mode': mode,
        'base_template': base_template,
        'page_break': page_break,
        # app portfolio mode (continuous VS milestone)
        # NOTE: context_processor's `is_milestone/continuous` vars do NOT work in PDF print, so pass explicitly
        'is_milestone': IS_MILESTONE,
        'is_continuous': IS_CONTINUOUS
    }

    # Manual print
    if mode != 'pdf':
        template = loader.get_template('print/portfolio.html')
        return HttpResponse(template.render(context, request))
    # PDF Export
    else:
        html_string = loader.render_to_string('print/portfolio.html', context)

        html_pdf = HTML(string=html_string)
        html_pdf.write_pdf(target=f'{settings.FILE_UPLOAD_TEMP_DIR}/portfolio.pdf',
                           stylesheets=[f'{settings.STATIC_ROOT}/css/pdf.css'])

        attachment = _('attachment; filename={}_Portfolio.pdf').format(portfolio.user.code_permanent)

        fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
        with fs.open('portfolio.pdf') as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = attachment
            return response


@login_required
def print_portfolio_milestone(request, portfolio_id, mode):
    """Print portfolio type infos for this user

    Args:
        portfolio_id: uuid
        mode: str (read / print / pdf)

    Returns:
        HttpResponse    (print/portfolio.html)
    """

    if request.method != 'GET':
        raise ValueError(request)

    # Models objects
    portfolio = get_portfolio(portfolio_id)
    request_user = request.user
    portfolio_user = portfolio.user

    concentration = portfolio.program.concentration
    # skills = Competency.available_objects.filter(
    #     program=portfolio.program,
    #     parent_id=None)

    # # group skills based on parent (ideal for sorting) and then add children as sub-array under parent
    # grouped_skills = []
    # for parent_skill in skills:
    #     # get children based on parent skill's ID
    #     children = Competency.available_objects.filter(parent_id=parent_skill.id)
    #     # add sub-dict for each parent skill w/ object of parent skill + children as sub-array
    #     grouped_skills.append({
    #         "parent": parent_skill,
    #         "children": children
    #     })

    portfolio_type = active_portfolio_type()
    achievement_types = portfolio_type.get_achievement_types()

    # get all competency types linked to this portfolio program
    # TODO filter by program_type
    custom_competency_types = CustomCompetencyType.available_objects.filter(program=portfolio.program)
    custom_competencies = []
    for ctype in custom_competency_types:
        tmp_competencies = CustomCompetency.available_objects.filter(type=ctype)
        # tuple (<CustomCompetencyType>, <List[CustomCompetency]>)
        custom_competencies.append((ctype, tmp_competencies))

    # MODE=MILESTONE; Due to looping multiple competency types in same function and returning a single
    #   'achievements' variable via context, it's NOT easily possible to use Python-only method (see
    #   'custom_competencies_overview' function) to filter out achievement types by allowed competency types, so
    #   instead we use inline if statement in HTML template (competencies.html) to check against 'get_comp_types_ids'.

    #
    # Achievement types
    #
    html_achievements = []
    skill_ops = []
    glossary_ops = []

    group_milestone = portfolio_user.group.milestone
    group_assignment = portfolio_user.group.assignment

    for achievement_type in achievement_types:
        # ONLY return questions matching portfolio user's group's milestone and assignment
        milestone_blocks = achievement_type.blocks.filter(milestone__lte=group_milestone)
        editor_blocks = []
        for block in milestone_blocks:
            if block.milestone < group_milestone or block.assignment <= group_assignment:
                editor_blocks.append(block)

        block_contents = []
        achievement_skills = {}

        # get each entry per block for this achievement per user
        for block in editor_blocks:
            # each block is an entry for the achievement type for this particular user @ a specific timestamp
            user_block = None
            try:
                # user_achievement = ...
                user_block = UserAchievement.available_objects.get(
                    achievement_type=achievement_type,
                    block=block,
                    portfolio=portfolio
                )
            except ObjectDoesNotExist:
                pass

            # Get achievement contents and render quill ops
            ops = ''
            try:
                ops = json.loads(user_block.content)['ops']
            except (AttributeError, JSONDecodeError, KeyError):
                pass

            # get attributes/tags for mapping w/ custom competencies & skills
            if user_block is not None and user_block.attributes is not None:
                for attributes, insert in user_block.attributes:
                    # Get the blots' attributes
                    json_ops = json.loads(attributes)
                    if json_ops['type'] == 'vocabulary':
                        glossary_ops.append(
                            _process_vocabulary_op_milestone(json_ops, insert, user_block, False)
                        )
                    else:
                        op_attributes = json_ops['attributes']
                        skill_id = op_attributes['competency_id']
                        level = op_attributes.get('level')  # NOT always present; depends on custom competency type
                        attribute_id = str(json_ops['id'])  # a timestamp
                        # if level is present in Quill, then append w/ @ to attribute_id; will do substr on client-side
                        # IMPORTANT: suffix @ works great w/ JS since `parseInt` correctly ignores the @<number> part
                        if level:
                            attribute_id += f"@{level}"

                        # key = "milestone:skill_id"
                        achievement_skills[f'{user_block.block.milestone}:{skill_id}'] = {'id': attribute_id}

            # IMPORTANT: append triple containing title of editor block, actual HTML contents, and word count
            html_ops = html.render(ops)
            block_contents.append((
                block.instruction,      # editor title / instruction
                html_ops,               # HTML contents of Quill editor
                len(html_ops.split()),  # number of words
                block.target_words      # target number of words
            ))

        html_achievement = {
            'name': achievement_type.name,
            'number': achievement_type.number,
            'block_contents': block_contents
        }

        html_achievements.append(html_achievement)
        skill_ops.append(achievement_skills)

    #
    # Documents
    #
    portfolio_files = Document.available_objects.filter(portfolio=portfolio)

    global_files = {}
    documents = []
    for file in portfolio_files:
        if file.indice_1 == 0:
            global_files[file.indice_2] = file
        else:
            documents.append(file)

    #
    # Assessments
    #
    # Put all ratings in dict
    ratings_dict = {}
    for rating in portfolio_type.assessment_ratings.all():
        ratings_dict[rating.weight] = rating

    # Put all user assessments in dict
    user_assessments = UserAssessment.available_objects.filter(portfolio=portfolio)
    user_assessments_dict = {}
    for user_assessment in user_assessments:
        if user_assessment.assessment.code == 'total':
            grade = user_assessment.base100()
        else:
            grade = user_assessment.grade
        user_assessments_dict[f'{user_assessment.assessment.milestone}-{user_assessment.assessment.code}'] = {
            'grade': grade,
            'comment': user_assessment.comment,
        }

    # Get portfolio assessments structured to create tables
    # [milestones
    #     [milestone_assessments
    #         [{assessment
    #           criteria
    #           user_assessment{grade
    #                           comment}}]
    #     ]
    # ]
    assessments = Assessment.available_objects.filter(milestone__lte=portfolio.user.group.milestone)
    milestones = []
    milestone_assessments = []
    current_milestone = 1

    for assessment in assessments:
        if assessment.milestone != current_milestone:
            milestones.append(milestone_assessments)
            milestone_assessments = []
            current_milestone = assessment.milestone
        assessment_dict = {'assessment': assessment}
        if f"{current_milestone}-{assessment.code}" in user_assessments_dict:
            user_assessment = user_assessments_dict[f"{current_milestone}-{assessment.code}"]
            if assessment.code != 'total':
                grade = user_assessment['grade']
                if grade is not None:
                    criteria_code = 'criteria_' + str(int(grade * 100))
                    assessment_dict['criteria'] = getattr(assessment, criteria_code)
                    user_assessment['grade'] = ratings_dict[grade]
                else:
                    assessment_dict['criteria'] = ''
                    user_assessment['grade'] = ''
            assessment_dict['user_assessment'] = user_assessment
        milestone_assessments.append(assessment_dict)
    milestones.append(milestone_assessments)

    #
    # mode
    #
    if mode == 'read':
        page_break = False
        base_template = 'base.html'
    else:
        page_break = True
        base_template = f'print/{mode}.html'

    #
    # Context
    #
    context = {
        'request': request,
        'user_type': request_user.type,
        'portfolio': portfolio,
        'achievements': achievement_types,
        # skills
        'num_achievements': achievement_types.count(),
        # 'skills': skills,
        # 'grouped_skills': grouped_skills,
        'skill_ops': skill_ops,
        # custom competencies
        'custom_competencies': custom_competencies,
        # achievements
        'html_achievements': html_achievements,
        'glossary_ops': sorted(glossary_ops, key=lambda x: locale.strxfrm(x['insert'])),
        # documents
        'global_files': global_files,
        'documents': documents,
        # assessments
        'concentration': concentration,
        'milestones': milestones,
        # mode
        'mode': mode,
        'base_template': base_template,
        'page_break': page_break,
        # global context 'is_milestone' is not available for PDF, so set directly in context
        'is_milestone': IS_MILESTONE
    }

    # DEBUG
    # print(context)

    # Manual print
    if mode != 'pdf':
        template = loader.get_template('print/portfolio.html')
        return HttpResponse(template.render(context, request))
    # PDF Export
    else:
        html_string = loader.render_to_string('print/portfolio.html', context)

        html_pdf = HTML(string=html_string)
        html_pdf.write_pdf(target=f'{settings.FILE_UPLOAD_TEMP_DIR}/portfolio.pdf',
                           stylesheets=[f'{settings.STATIC_ROOT}/css/pdf.css'])

        attachment = _('attachment; filename={}_Portfolio.pdf').format(portfolio_user.code_permanent)

        fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
        with fs.open('portfolio.pdf') as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = attachment
            return response


@login_required
def print_assessment(request, portfolio_id, mode):
    """Print assessment

        Args:
            portfolio_id: uuid
            mode: str (read / print / pdf)

        Returns:
            HttpResponse    (print_assessment/uqar_assessment.html)

        """

    if request.method != 'GET':
        raise ValueError(request)

    user: User = request.user

    # Models objects
    user_type = user.type
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    # get applicant's attribution object
    attribution = Attribution.available_objects.get(applicant_id=portfolio.user.id)

    #
    # mode
    #
    if mode == 'read':
        page_break = False
        base_template = 'base.html'
    else:
        page_break = True
        base_template = f'print/{mode}.html'

    competency_assessments = CompetencyAssessmentUQAR.available_objects.filter(portfolio_id=portfolio.id)
    requested_credits, recommended_credits = calculate_credits(competency_assessments)

    #
    # Context
    #
    context = {
        'request': request,
        'user_type': user_type,
        'portfolio': portfolio,
        'competency_assessments': competency_assessments,
        'requested_credits': requested_credits,
        'recommended_credits': recommended_credits,
        'interview_date': attribution.interview.strftime(DATE_FORMAT) if attribution.interview else '',
        'document_date': attribution.document.strftime(DATE_FORMAT) if attribution.document else '',
        # mode
        'mode': mode,
        'base_template': base_template
    }

    # Manual print
    if mode != 'pdf':
        template = loader.get_template('print/uqar_assessment.html')
        return HttpResponse(template.render(context, request))
    # PDF Export
    else:
        html_string = loader.render_to_string('print/uqar_assessment.html', context)

        html_pdf = HTML(string=html_string)
        html_pdf.write_pdf(target=f'{settings.FILE_UPLOAD_TEMP_DIR}/portfolio_assessment.pdf',
                           stylesheets=[f'{settings.STATIC_ROOT}/css/pdf.css'])

        attachment = _('attachment; filename={}_PortfolioAssessment.pdf').format(portfolio.user.code_permanent)

        fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
        with fs.open('portfolio_assessment.pdf') as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = attachment
            return response


def export_attribution(portfolio):
    """Export credit attribution table in PDF

    Args:
        portfolio: Object

    Returns:
        HttpResponse    (print/attribution.html)

    """

    context = get_portfolio_levels(portfolio)

    html_string = loader.render_to_string('print/attribution.html', context)

    html_pdf = HTML(string=html_string)
    html_pdf.write_pdf(target=f'{settings.FILE_UPLOAD_TEMP_DIR}/portfolio.pdf',
                       stylesheets=[f'{settings.STATIC_ROOT}/css/pdf.css'])

    attachment = _('attachment; filename={}_Attribution.pdf').format(portfolio.user.code_permanent)

    fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
    with fs.open('portfolio.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = attachment
        return response


@login_required
def custom_form(request, form_id):
    """Handle LOADING (GET) & SUBMISSION (POST) of specific custom form. Saves entries as XFormEntry.
    Args:
        request: GET|POST
        form_id: uuid

    Returns:
        HttpResponse (portfolio/custom-form.html)
    """
    response = HttpResponse()

    print("portfolio::custom_form -> {}".format(form_id))

    # current user
    user: User = request.user

    # if 'form_id' not provided, redirect to portfolio index
    if not form_id:
        return redirect('profile')

    # fetch XForm model instance from the URL-provided form ID
    try:
        # Get portfolio object
        portfolio = active_portfolio(request)
        xform = XForm.available_objects.get(id=form_id)
    except ObjectDoesNotExist:
        return redirect('profile')

    # data to pass to form to pre-fill form fields (only relevant on GET)
    form_data = {
        'user_id': user.id
    }

    # (form load) load form w/ latest data for this user
    if request.method == 'GET':
        print("custom_form.GET")

        # get most recent form entry for this form & portfolio
        latest_entry = XFormEntry.available_objects.filter(
            form_id=xform.id,
            portfolio_id=portfolio.id
        ).last()

        if latest_entry is not None:
            # .data is a JSON string, so need to convert string to dict via `json.loads`
            submission_data = json.loads(latest_entry.data)
            form_data.update(submission_data)

    # (form submission) submit data and create new XFormEntry
    elif request.method == 'POST':
        print("custom_form.POST")
        # print(request.POST)

        mutable_request_post: QueryDict = request.POST.copy()
        mutable_request_post.pop('csrfmiddlewaretoken')

        # save XFormEntry
        entry = XFormEntry()
        entry.form_id = xform.id
        entry.portfolio_id = portfolio.id
        entry.data = json.dumps(mutable_request_post)
        entry.save()

        # trigger printing of form as PDF
        print_custom_form(request, xform.id)

        # redirect same page to reload form w/ proper values (forces a GET and avoids double-submission w/ POST)
        # https://stackoverflow.com/questions/68255365/django-reload-current-page-without-using-path#answer-68256274
        return HttpResponseRedirect(request.path_info)


    # build the Django form dynamically

    # start from blank django forms.Form instance
    django_form: forms.Form = Form(form_data)

    # load fields dynamically
    form_fields = xform.fields.all()
    field_names: List[str] = []
    for ff in form_fields:
        # create vanilla form field
        field = forms.CharField()

        # determine widget from field type set per field (textarea, input, number)
        if ff.type == XFieldType.LONG_TEXT:
            field.widget = forms.Textarea()
        elif ff.type == XFieldType.SHORT_TEXT:
            field.widget = forms.TextInput()
        elif ff.type == XFieldType.NUMBER:
            field.widget = forms.FloatField()

        # define other field attributes (label, help, required, etc.)
        field.label = _(ff.name)
        field.help_text = _(ff.desc)
        field.required = ff.required

        # add field to django form using xfield id as field name
        # since id = uuid(), very important to convert to string, else no data match is done
        django_form.fields[str(ff.id)] = field

        # store field names (ids) in separate array in case we need it
        field_names.append(str(ff.id))

    # final context to pass to `custom-form.html` view
    context = {
        'document_active': True,
        'help': get_help_link('applicant_training'),
        'portfolio': portfolio,
        'form': django_form,
        'form_id': xform.id,
        'form_title': xform.name
    }

    template = loader.get_template('portfolio/custom-form.html')
    response.content = template.render(context, request)

    return response

@login_required
def print_custom_form(request, form_id):
    """Given a form ID (uuid) passed as parameter, print custom form entry linked w/ active portfolio

    Args:
        request (QueryDict):
        form_id (object): UUID of the custom form
    """

    portfolio = active_portfolio(request)
    xform = XForm.available_objects.get(id=form_id)
    # print(xform)

    # get latest form entry for this form & portfolio (relies on 'ordering' set in XForm model definition)
    form_entry: XFormEntry = XFormEntry.available_objects.filter(
        form_id=form_id,
        portfolio_id=portfolio.id
    ).last()

    # build list of field names & corresponding value
    final_fields: List[tuple] = []
    for key, value in json.loads(form_entry.data).items():
        xfield = xform.fields.get(id=key)
        final_fields.append((xfield.name, value))

    # print("field names & values")
    # print(final_fields)

    # render form as HTML string (using print-friendly template)
    html_string = loader.render_to_string('print/custom-form.html', {
        'xform': xform,
        'fields': final_fields,
        'entry': form_entry
    })

    # generate unique form name from slugified name + last 12 chars of form ID (uuid)
    unique_form_name = f"{slugify(xform.name)}-{xform.id.__str__()[-12:]}"
    export_file_name = f"form-{unique_form_name}.pdf"

    # convert HTML -> PDF
    dst_tmp_dir = f'{settings.FILE_UPLOAD_TEMP_DIR}/{portfolio.user.id}'
    dst_file_path = f'{dst_tmp_dir}/{export_file_name}'
    pdf_stylesheet = f'{settings.STATIC_ROOT}/css/pdf.css'

    try:
        os.remove(dst_file_path)
    except FileNotFoundError as e:
        print(e)

    try:
        os.makedirs(dst_tmp_dir)
    except FileExistsError as e:
        print(e)


    html_pdf = HTML(string=html_string)
    html_pdf.write_pdf(target=dst_file_path, stylesheets=[pdf_stylesheet])

    # https://stackoverflow.com/questions/3501588/how-to-assign-a-local-file-to-the-filefield-in-django
    # todo save directly to /media/ and then just pass relative path instead of full File object
    local_file = open(dst_file_path, 'rb')
    django_file = File(local_file)

    try:
        document = Document.available_objects.get(
            name=unique_form_name,
            portfolio_id=portfolio.id,
            indice_1=1 # required for 'download all' to include this file
        )
        document.file = django_file
        document.save()
    except ObjectDoesNotExist as e:
        print(e)
        Document.available_objects.create(
            file=django_file,
            name=unique_form_name,
            portfolio_id=portfolio.id,
            indice_1=1 # required for 'download all' to include this file
        )

    # close the local file handle
    local_file.close()

    # write PDF to temp folder & return as HTTP response
    attachment = _('attachment; filename={}').format(export_file_name)
    fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
    with fs.open(dst_file_path) as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = attachment
        return response

