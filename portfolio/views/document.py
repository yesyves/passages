# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale

from os import mkdir, path, remove
from shutil import copyfile, make_archive, rmtree

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.utils.translation import gettext as _

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE, PROJ_DIR

from portfolio.models import Achievement, Document, HelpLink, Portfolio, DocumentType
from user.models import User
from utils.views import get_portfolio, get_help_link, active_portfolio, active_portfolio_type

locale.setlocale(locale.LC_ALL, '')


@login_required
def documents_index(request, portfolio_id, edit):
    """Show or edit portfolio documents

    Args:
        portfolio_id: uuid
        edit: Int (0 = display, 1 = edit)

    Returns:
        HttpResponse
            (portfolio/document.html)
            (portfolio/edit_document.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    user = request.user
    user_type = user.type

    portfolio = get_portfolio(portfolio_id)
    group_milestone = portfolio.user.group.milestone

    if portfolio.status == 'Archived':
        message = [
            _('Archived portfolio'),
            _('Uploaded files are not readily available any longer'),
            '',
            _('Please make a request to the site administrator if you wish to regain access')
        ]
        admin_emails = ','.join([a[1] for a in settings.ADMINS])
        context = {
            'user_type': user_type,
            'message': message,
            'instance': admin_emails,
            'target': 'documents',
        }
        template = loader.get_template('portfolio/restrict.html')
        return HttpResponse(template.render(context, request))

    # milestones = []
    if IS_MILESTONE:
        achievements = portfolio.get_type().get_achievement_types()
        # if portfolio.user.type == 'Applicant':
        #     milestone = group_milestone
        #     year = portfolio.user.group.semester.year
        #     while milestone > 0:
        #         milestones.insert(0, {'milestone': milestone, 'year': year})
        #         milestone -= 1
        #         year -= 1
    else:
        achievements = portfolio.get_achievements(user_type)

    # Document Types
    document_types = DocumentType.available_objects.filter(milestone__lte=group_milestone)
    specific_document_types = []
    document_type_choices = []
    for document_type in document_types:
        if document_type.is_specific:    # and document_type.milestone == group_milestone:
            specific_document_types.append({'type': document_type,
                                            'file': None})
        else:
            document_type_choices.append(document_type)

    # Global portfolio files
    portfolio_files = Document.available_objects.filter(portfolio=portfolio)

    # Bool for template
    has_documents = False
    if portfolio_files:
        has_documents = True

    # global_files = {}
    documents = []
    # loop over all portfolio documents & categorize in global vs special docs
    for file in portfolio_files:
        # if file.indice_1 == 0:
        if file.type is not None and file.type.is_specific:
            # global_files[file.indice_2] = file
            for specific_document_type in specific_document_types:
                if specific_document_type['type'].id == file.type.id:
                    specific_document_type['file'] = file
        else:
            documents.append(file)

    context = {
        'portfolio': portfolio,
        'achievements': achievements,
        'user_type': user_type,
        'num_achievements': achievements.count(),
        'has_documents': has_documents,
        'documents': documents,
        'specific_document_types': specific_document_types,
        'document_type_choices': document_type_choices,
        'document_active': True,
        'help': get_help_link('documents'),
        # due to 'example', need to restrict edition of documents except for 'teachers' and owner
        'editable': user == portfolio.user or user.has_teacher_role()
    }
    if edit:
        template = loader.get_template('portfolio/edit_document.html')
    else:
        template = loader.get_template('portfolio/document.html')

    return HttpResponse(template.render(context, request))


@login_required
def download_all(request, portfolio_id):
    """Download all documents in zipped format

    Args:
        portfolio_id: uuid

    Returns:
        HttpResponse (document.zip)
    """

    if request.method != 'GET':
        raise ValueError(request)

    # global portfolio files
    portfolio_files = Document.available_objects.filter(portfolio_id=portfolio_id)
    # special non-portfolio files
    special_files = Document.available_objects.filter(type__is_specific=True)

    # get user's portfolio either from 1st portfolio-linked document or from session value
    if len(portfolio_files) > 0:
        portfolio = portfolio_files[0].portfolio
    else:
        portfolio = active_portfolio(request)

    max_achievements = portfolio.concentration.max_num_achievements
    code = portfolio.user.code_permanent

    destination = f'{settings.FILE_UPLOAD_TEMP_DIR}/{code}'
    if path.exists(destination):
        rmtree(destination)
    mkdir(destination)

    # prepare all portfolio files for zipping
    for file in portfolio_files:
        # ignore files not linked to a relevant achievement
        if file.indice_1 is None or file.indice_1 > max_achievements:
            continue
        extension = path.splitext(file.file.name)[1]
        file_name = f'{destination}/{file.indice_1}-{file.indice_2} {file.name.replace(u"/", u"∕")}{extension}'
        copyfile(f'{settings.MEDIA_ROOT}/{file.file.name}', file_name)

    # also copy special files for inclusion in zip
    for file in special_files:
        # if NO file is uploaded, do NOT send for compression, else it crashes due to "errno 21: is a directory"
        if not file.file:
            continue

        extension = path.splitext(file.file.name)[1]
        file_name = f'{destination}/{file.name.replace(u"/", u"∕")}{extension}'
        copyfile(f'{settings.MEDIA_ROOT}/{file.file.name}', file_name)

    make_archive(destination, 'zip', destination)
    rmtree(destination)

    attachment = _('attachment; filename={}_Documents.zip').format(code)
    fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
    with fs.open(f'{code}.zip') as zip_file:
        response = HttpResponse(zip_file, content_type='application/zip')
        response['Content-Disposition'] = attachment
        return response


@login_required
def add_global(request):
    """
    Deprecated, use add_annexe for every document type

    Add global portfolio file : CV, Timeline or Interview preparatory sheet

    Args:
        request: (HttpRequest object)
            POST.portfolio_id: uuid
            POST.FILE

    Returns:
        redirect (/portfolio/portfolio_id/documents/1)

    """

    if request.method != 'POST':
        raise ValueError(request)

    portfolio_id = request.POST.get('portfolio_id')
    file_path = request.FILES.get('annexe')
    if file_path is None:
        if portfolio_id is None:
            return redirect('profile')
    else:
        annexe = Document.available_objects.create(
            file=file_path,
            name=request.POST.get('file_name'),
            portfolio_id=portfolio_id,
            indice_1=0,
            indice_2=request.POST.get('file_type'),
        )
        annexe.url = annexe.file.url
        annexe.save()

    return redirect('documents_index', portfolio_id=portfolio_id, edit=1)


@login_required
def add_annexe(request):
    """Add one or more achievement annexes

    Args:
        request: (HttpRequest object)
            POST.portfolio_id: uuid
            POST.FILE

    Returns:
        redirect (/portfolio/portfolio_id/documents/1)
    """

    if request.method != 'POST':
        return redirect('profile')

    portfolio_id = request.POST.get('portfolio_id')
    upload_files = request.FILES.getlist('annexe')
    document_type_id = request.POST.get('document_type')
    file_name = request.POST.get('file_name')

    if not upload_files:
        if portfolio_id is None:
            return redirect('profile')
    else:
        for file in upload_files:
            if file_name is not None:
                name = file_name
            else:
                name = '.'.join(file.name.split('.')[:-1])
            annexe = Document.available_objects.create(
                file=file,
                name=name,
                portfolio_id=portfolio_id,
                indice_1=None,
                type_id=document_type_id,
            )
            annexe.url = annexe.file.url
            annexe.save()

    return redirect('documents_index', portfolio_id=portfolio_id, edit=1)


@login_required
def document_association(request, document_id, achievement_id):
    """Associate or dissociate a document with an achievement

    Call sort_documents() if needed

    Args:
        document_id: uuid
        achievement_id: uuid

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'GET':
        raise ValueError(request)

    achievement = Achievement.available_objects.get(id=achievement_id)
    document = Document.available_objects.get(id=document_id)

    do_sort = True

    # If already present, then remove, else add
    if achievement in document.achievements.all():
        document.achievements.remove(achievement)
        # If no achievement left, clear indices
        if not document.achievements.all():
            document.indice_1 = None
            document.indice_2 = ''
            do_sort = False
    else:
        document.achievements.add(achievement)

    document.save()

    # Check if we need to sort documents
    if do_sort and document.achievements.first().number < achievement.number:
        # Priority association already exists
        do_sort = False
    else:
        sort_documents(achievement.portfolio)

    response = {
        'success': True,
        'do_sort': do_sort,
    }

    return HttpResponse(
        json.dumps(response),
        content_type='application/json',
    )


@login_required
def set_document_type(request):
    """Apply selected type to document

    Args:
        request: (HttpRequest object)
            POST.document_id (uuid)
            POST.document_type_id (uuid)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST' or not request.POST:
        return redirect('profile')

    document_id = request.POST.get('document_id')
    document_type_id = request.POST.get('document_type_id')

    document = Document.objects.get(id=document_id)

    document.type_id = document_type_id
    document.save(update_fields=['type'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


def sort_documents(portfolio):
    """Sort document by associated achievement number

    Args:
        portfolio: Object

    Returns:
        True

    """

    documents = Document.available_objects.filter(portfolio=portfolio).exclude(achievements__isnull=True)
    # Keep track of indices associations { indice_1: indice_2 }
    indices = {}

    for document in documents:
        indice_1 = document.achievements.first().number

        # Update or start new association
        if indice_1 in indices:
            indice_2 = chr(ord(indices[indice_1]) + 1)
        else:
            indice_2 = 'A'

        indices[indice_1] = indice_2

        if document.indice_1 != indice_1 or document.indice_2 != indice_2:
            document.indice_1 = indice_1
            document.indice_2 = indice_2
            document.save()

    return True


@login_required
def move_document(request, document_id, move_up):
    """Swap document order with the previous or the next for an achievement

    Args:
        document_id: uuid
        move_up: Bool

    Returns:
        redirect (/portfolio/current.portfolio.id/documents/1)

    """

    if request.method != 'GET':
        raise ValueError(request)

    current_document = Document.available_objects.get(id=document_id)
    achievement_documents = Document.available_objects.filter(portfolio=current_document.portfolio,
                                                    indice_1=current_document.indice_1)
    i = 0
    for document in achievement_documents:
        if document == current_document:
            indice_2 = document.indice_2
            if move_up:
                swap = achievement_documents[i - 1]
            else:
                swap = achievement_documents[i + 1]
            current_document.indice_2 = swap.indice_2
            current_document.save()
            swap.indice_2 = indice_2
            swap.save()
            break
        i += 1

    return redirect('documents_index', portfolio_id=current_document.portfolio.id, edit=1)


@login_required
def delete_document(request):
    """Delete a document

    Args:
        request: (HttpRequest object)
            POST.document_id: uuid

    Returns:
        redirect (/portfolio/portfolio_id/documents/1)

    """

    if request.method != 'POST':
        raise ValueError(request)

    document_id = request.POST.get('document_id')

    if document_id is None:
        return redirect('profile')

    document = Document.available_objects.get(id=document_id)
    for achievement in document.achievements.all():
        document.achievements.remove(achievement)
    document.delete()

    safe_path = path.realpath(document.file.path)
    base_path = f'{PROJ_DIR}/media/{request.user.id}'
    if path.commonpath([base_path, safe_path]) == base_path:
        remove(safe_path)
    else:
        raise ValueError(document.file.path)

    # .portfolio is optional, so need to check if there's a portfolio associated w/ document, else use session prop;
    # normally, all user-uploaded docs are auto-associated w/ portfolio, and user can only delete these, but still
    # to be safe, we added this condition to prevent fatal error/crash.
    portfolio_id: str
    if not document.portfolio:
        portfolio_id = active_portfolio(request).id
    else:
        portfolio_id = document.portfolio.id

    if document.indice_1:
        sort_documents(portfolio_id)

    return redirect('documents_index', portfolio_id=portfolio_id, edit=1)


@login_required
def rename_document(request):
    """Rename a document

    Args:
        request: (HttpRequest object)
            POST.document_id: uuid
            POST.document_name: Str

    Returns:
        redirect (/portfolio/document.portfolio.id/documents/1)

    """

    if request.method != 'POST':
        raise ValueError(request)

    document_id = request.POST.get('document_id')

    if document_id is None:
        success = False
    else:
        success = True
        document = Document.available_objects.get(id=document_id)

        document.name = request.POST.get('name')
        document.save()

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json',
    )
