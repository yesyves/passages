#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.contrib.admin import display
from django.utils.safestring import mark_safe

from .models import (
    Portfolio, Achievement, AchievementContext, AchievementDevelopment,
    AchievementMotivation, AchievementVersion, Document, Questionnaire, ActiveUser, AchievementTemplate, EditorBlock,
    UserAchievement, PortfolioType, AchievementType, UserAchievementHistory, HelpLink,
    AssessmentRating, Assessment, UserAssessment, XFormField, DocumentType, XForm,
    XFormFieldGroup, XFormEntry, QuestionnaireUQAM, CompetencyAssessmentUQAR
)
from django.apps import AppConfig
from django.contrib import admin


class PortfolioConfig(AppConfig):
    name = 'portfolio'


class PortfolioAdmin(admin.ModelAdmin):
    list_display = ['user', 'program', 'user_expert_consent', 'user_id', 'status']
    list_filter = ('user__group', 'status')
    search_fields = ['user__username', 'user__first_name', 'user__last_name', 'program__id']

    @display(ordering='', description='User expert consent')
    def user_expert_consent(self, obj: Portfolio) -> bool:
        icon = "icon-yes.svg" if obj.user.expert_consent else "icon-no.svg"
        return mark_safe(f'<img src="/static/admin/img/{icon}" alt="True">')


class PortfolioTypeAdmin(admin.ModelAdmin):
    list_display = ('institution', 'credits_editable', 'achievement_types_count')

    @display(ordering='', description='Achievement types')
    def achievement_types_count(self, obj: PortfolioType):
        return obj.achievement_types.count()


class AchievementAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'get_user')
    list_filter = ('portfolio__user__group',)
    search_fields = ['portfolio__user__email', 'portfolio__user__username',
                     'portfolio__user__first_name', 'portfolio__user__last_name']

    @display(ordering='', description='User')
    def get_user(self, obj: Achievement):
        return obj.portfolio.user


class AchievementContextAdmin(admin.ModelAdmin):
    list_filter = ('achievement__number', 'achievement__version')
    search_fields = ['achievement__portfolio__user__email',
                     'achievement__portfolio__user__username',
                     'achievement__portfolio__user__first_name',
                     'achievement__portfolio__user__last_name']


class AchievementMotivationAdmin(admin.ModelAdmin):
    list_filter = ('achievement__number', 'achievement__version')
    search_fields = ['achievement__portfolio__user__email',
                     'achievement__portfolio__user__username',
                     'achievement__portfolio__user__first_name',
                     'achievement__portfolio__user__last_name']


class AchievementDevelopmentAdmin(admin.ModelAdmin):
    list_filter = ('achievement__number', 'achievement__version')
    search_fields = ['achievement__portfolio__user__email',
                     'achievement__portfolio__user__username',
                     'achievement__portfolio__user__first_name',
                     'achievement__portfolio__user__last_name']


class AchievementVersionAdmin(admin.ModelAdmin):
    list_display = ['achievement', 'created']
    list_filter = ('status', 'achievement',)
    search_fields = ['author__email', 'author__username', 'author__first_name', 'author__last_name']


class QuestionnaireUQAMAdmin(admin.ModelAdmin):
    search_fields = ['A1']


class QuestionnaireAdmin(admin.ModelAdmin):
    search_fields = ['A1']


class DocumentTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_specific', 'milestone')


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'type')
    list_filter = ('indice_1', 'portfolio', 'type',)
    search_fields = ['name', 'type',
                     'user__email', 'user__username',
                     'user__first_name', 'user__last_name']

    @display(ordering='', description='User')
    def user(self, obj: Document):
        return obj.portfolio.user


class ActiveUserAdmin(admin.ModelAdmin):
    search_fields = ['achievement__name',
                     'user__email', 'user__username', 'user__first_name', 'user__last_name']


class AchievementTypeAdmin(admin.ModelAdmin):
    list_display = ('number', 'name', 'sub_title', 'template_name')

    # https://stackoverflow.com/questions/163823/can-list-display-in-a-django-modeladmin-display-attributes-of-foreignkey-field
    @display(ordering='template__name', description='Template Name')
    def template_name(self, obj: AchievementType):
        return obj.template.name


class AchievementTemplateAdmin(admin.ModelAdmin):
    pass


class EditorBlockAdmin(admin.ModelAdmin):
    list_display = ('question_block', 'assignment', 'milestone')

    # since 'block' instruction supports HTML, need to format it properly in admin for readability
    @display(ordering='', description='Editor block')
    def question_block(self, obj: EditorBlock):
        return mark_safe('<span style="font-weight:normal">%s</span>' % obj.__str__())


class UserAchievementAdmin(admin.ModelAdmin):
    list_display = ('achievement', 'achievement_type', 'question_block', 'portfolio_user', 'created')
    list_filter = ('block', 'achievement',)

    # since 'block' instruction supports HTML, need to format it properly in admin for readability
    @display(ordering='', description='Editor block')
    def question_block(self, obj: UserAchievement):
        return mark_safe(obj.block)

    @display(ordering='', description='Portfolio User')
    def portfolio_user(self, obj: UserAchievement):
        return obj.portfolio.user


class UserAchievementHistoryAdmin(admin.ModelAdmin):
    list_display = ('user_achievement', 'block', 'author', 'created')
    list_filter = ('user_achievement__achievement', 'user_achievement__achievement_type', 'block',)


class AssessmentRatingAdmin(admin.ModelAdmin):
    list_display = ('description',)


class AssessmentAdmin(admin.ModelAdmin):
    list_display = ('milestone', 'code', 'assessment_object')


class UserAssessmentAdmin(admin.ModelAdmin):
    list_display = ('assessment', 'portfolio_user')

    # @display(ordering='', description='Total grade (%)')
    # def total_grade(self, obj: UserAssessment):
    #     return "%.2f" % (obj.total * 100)

    @display(ordering='', description='User')
    def portfolio_user(self, obj: UserAssessment):
        return obj.portfolio.user


class CompetencyAssessmentUQARAdmin(admin.ModelAdmin):
    list_display = ('competency', 'portfolio_user')

    @display(ordering='', description='User')
    def portfolio_user(self, obj: CompetencyAssessmentUQAR):
        return obj.portfolio.user


class HelpLinkAdmin(admin.ModelAdmin):
    list_display = ['keyword', 'link']


class XFormFieldGroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'id']


class XFormFieldAdmin(admin.ModelAdmin):
    list_display = ['name', 'group', 'order', 'type', 'required']
    list_filter = ('group', 'type',)


class XFormEntryAdmin(admin.ModelAdmin):
    list_display = ['form', 'portfolio', 'created_at']
    list_filter = ('form',)


class XFormAdmin(admin.ModelAdmin):
    list_display = ['name', 'form_url', 'fields_count']

    @display(ordering='', description='Fields (count)')
    def fields_count(self, obj: XForm):
        return obj.fields.count()

    @display(ordering='', description='Relative URL')
    def form_url(self, obj: XForm):
        url = "/portfolio/form/{}".format(obj.id)
        return mark_safe('<a href="{}">{}</a>'.format(url, url))



"""
Register models in Django admin
"""

admin.site.register(Portfolio, PortfolioAdmin)
admin.site.register(PortfolioType, PortfolioTypeAdmin)
admin.site.register(Achievement, AchievementAdmin)
admin.site.register(AchievementTemplate, AchievementTemplateAdmin)
admin.site.register(AchievementType, AchievementTypeAdmin)
admin.site.register(AchievementContext, AchievementContextAdmin)
admin.site.register(AchievementMotivation, AchievementMotivationAdmin)
admin.site.register(AchievementDevelopment, AchievementDevelopmentAdmin)
admin.site.register(AchievementVersion, AchievementVersionAdmin)
# admin.site.register(QuestionnaireUQAM, QuestionnaireUQAMAdmin)
admin.site.register(Questionnaire, QuestionnaireAdmin)
admin.site.register(DocumentType, DocumentTypeAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(ActiveUser, ActiveUserAdmin)
admin.site.register(EditorBlock, EditorBlockAdmin)
admin.site.register(UserAchievement, UserAchievementAdmin)
admin.site.register(UserAchievementHistory, UserAchievementHistoryAdmin)
admin.site.register(AssessmentRating, AssessmentRatingAdmin)
admin.site.register(Assessment, AssessmentAdmin)
admin.site.register(UserAssessment, UserAssessmentAdmin)
admin.site.register(CompetencyAssessmentUQAR, CompetencyAssessmentUQARAdmin)
admin.site.register(HelpLink, HelpLinkAdmin)
admin.site.register(XFormFieldGroup, XFormFieldGroupAdmin)
admin.site.register(XFormField, XFormFieldAdmin)
admin.site.register(XForm, XFormAdmin)
admin.site.register(XFormEntry, XFormEntryAdmin)
