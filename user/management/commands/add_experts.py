#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import csv

from os import getcwd, path

from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from user.models import ProgramExpert, User
from utils.views import get_random_string


class Command(BaseCommand):
    help = 'Add new program experts from data/experts.csv'

    def handle(self, *args, **options):

        username = ''
        previous_username = username
        user = None

        # experts.csv : Code programme; Prénom Nom; Courriel; Établissement
        file_path = path.join(getcwd(), 'data', 'experts.csv')

        with open(file_path, newline='') as csvfile:
            fieldnames = ['program_id', 'name', 'email', 'establishment']
            reader = csv.DictReader(csvfile, fieldnames=fieldnames, delimiter=';')

            print('')

            for row in reader:
                # DEBUG:
                # print(row)

                email = row['email']
                i = email.find('@')
                if i > 0:
                    username = email[0:i]
                else:
                    continue

                name = row['name'].split(' ', 1)
                email = row['email']

                if username != previous_username:
                    previous_username = username
                    print(name)
                    try:
                        user = User.objects.get(email=email)
                    except ObjectDoesNotExist:
                        user = User(
                            username=username,
                            last_name=name[1].replace(u"'", u"’"),
                            first_name=name[0].replace(u"'", u"’"),
                            email=email.lower(),
                            password=make_password(get_random_string(12)),
                            is_superuser=False,
                            is_staff=True,
                            type='Expert',
                            establishment=row['establishment'],
                        )
                        user.save()

                print(row['program_id'])
                expert = ProgramExpert(
                    expert=user,
                    program_id=row['program_id'],
                )
                expert.save()
