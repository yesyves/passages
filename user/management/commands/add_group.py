#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import csv

from os import getcwd, path

from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE
from portfolio.models import Portfolio, PortfolioType
from user.models import Semester, Group, GroupReviewer, User
from utils.views import get_random_string


class Command(BaseCommand):
    help = 'Add new semester and users from data/semester.csv'

    def add_arguments(self, parser):
        parser.add_argument('group', type=str, help='Specify group ID')
        parser.add_argument(
            '-m',
            '--milestone',
            type=int,
            default=1,
            help='Specify Milestone',
        )
        parser.add_argument(
            '-s',
            '--supervisor',
            type=int,
            default=2,
            help='Specify supervisor ID',
        )

    def handle(self, *args, **options):
        #
        # Preliminary checks
        #

        group = options['group']
        milestone = options['milestone']
        supervisor_id = options['supervisor']

        if len(group) != 6 or not group.isdigit():
            print(f'\n\nError: Please user valid group ID!')
            exit(1)

        if IS_MILESTONE and not milestone:
            print(f'\n\nError: Please specify Milestone!')
            exit(1)

        #
        # Create Semester
        #
        try:
            supervisor = User.objects.get(id=supervisor_id)
            if supervisor.type != 'Supervisor':
                print(f'\n\nError: User {supervisor_id} {supervisor} is not a supervisor!')
                exit(1)
        except ObjectDoesNotExist:
            print(f'\n\nError: No user with ID {supervisor_id} exists!')
            exit(1)

        seasons = {
            1: 'Winter',
            2: 'Spring',
            3: 'Summer',
            4: 'Autumn',
        }
        semester = int(group[0-4])
        year = int(group[0:4])
        season = seasons[int(group[4])]

        try:
            this_semester = Semester.available_objects.get(id=semester)
            print(f'\n\nWarning! Semester : {season} {year} already exists')
        except ObjectDoesNotExist:
            print(f'\n\nCreating Semester : {season} {year}')

            this_semester = Semester(
                id=semester,
                year=year,
                season=season,
            )
            this_semester.save()

        #
        # Create Group
        #
        number = int(group[5])
        group = int(group)

        try:
            this_group = Group.available_objects.get(id=group)
            print(f'\n\nWarning! Group : {this_semester} - {number} already exists')
        except ObjectDoesNotExist:
            print(f'\n\nCreating Group : {this_semester} - {number}')
            if IS_MILESTONE:
                print(f'Milestone : {milestone}')

            this_group = Group(
                id=group,
                semester=this_semester,
                code=number,
                supervisor=supervisor,
                milestone=milestone,
            )
            this_group.save()

        #
        # Create users
        #

        file_path = path.join(getcwd(), 'data', 'group.csv')

        with open(file_path, newline='') as csvfile:

            if IS_CONTINUOUS:
                fieldnames = ['code', 'name', 'program_id', 'email']
            else:
                fieldnames = ['code', 'name', 'email']

            reader = csv.DictReader(csvfile, fieldnames=fieldnames, delimiter=';')

            print('')

            print('\nCreating users:\n')

            updated_applicants = 0
            created_users = 0
            created_portfolios = 0
            updated_portfolios = 0
            created_group_reviewers = 0

            for row in reader:
                # DEBUG:
                # print(row)

                email = row['email']
                group_id = group
                user_type = 'Applicant'

                i = email.find('@')
                if i > 0:
                    user = email[0:i]
                else:
                    continue

                if IS_CONTINUOUS:
                    name = row['name'].split(' ', 1)
                    last_name = name[0]
                    first_name = name[1]
                    if row['program_id'] == '7414':
                        concentration = 0
                        code_permanent = row['code']
                    elif row['program_id'] == '7415':
                        concentration = 1
                        code_permanent = row['code']
                    else:
                        code_permanent = ''
                        concentration = None
                        group_id = None
                        user_type = 'Reviewer'
                elif IS_MILESTONE:
                    name = row['name'].split(', ')
                    last_name = name[0]
                    first_name = name[1]
                    if row['code']:
                        code_permanent = row['code']
                        concentration = 1
                        program = '142.H0'
                    else:
                        code_permanent = ''
                        concentration = None
                        group_id = None
                        user_type = 'Reviewer'

                try:
                    # Check if user already exists
                    this_user = User.objects.get(email=email)
                    if user_type == 'Applicant':
                        # Update concentration, semester and reviewer
                        this_user.concentration_id = concentration
                        this_user.group_id = group
                        this_user.reviewer = None
                        this_user.save()
                        updated_applicants += 1
                        if IS_MILESTONE:
                            # Update portfolio status
                            portfolio = Portfolio.available_objects.get(user=this_user)
                            portfolio.status = 'Draft'
                            portfolio.save()
                            updated_portfolios += 1
                except ObjectDoesNotExist:
                    this_user = User(
                        username=user,
                        last_name=last_name.replace(u"'", u"’"),
                        first_name=first_name.replace(u"'", u"’"),
                        email=email,
                        password=make_password(get_random_string(12)),
                        is_superuser=False,
                        is_staff=True,
                        type=user_type,
                        code_permanent=code_permanent,
                        concentration_id=concentration,
                        group_id=group_id,
                    )
                    this_user.save()

                    if IS_MILESTONE and user_type == 'Applicant':
                        portfolio = Portfolio(
                            program_id=program,
                            user=this_user,
                        )
                        portfolio.save()
                        created_portfolios += 1
                    # this_user = None
                    created_users += 1

                user_name = f'{first_name}, {last_name}'
                print(f"{user_name.ljust(40, '_')}{user_type}")

                if user_type == 'Reviewer':
                    # if this_user is None:
                    #     this_user = User.objects.get(email=email)
                    this_reviewer = GroupReviewer(
                        reviewer=this_user,
                        group_id=group,
                    )
                    this_reviewer.save()
                    created_group_reviewers += 1

            print(f'{created_users} users created')
            print(f'{created_portfolios} portfolios created')
            print(f'{updated_applicants} applicants updated')
            print(f'{updated_portfolios} portfolios updated')
            print(f'{created_group_reviewers} group reviewers created')
