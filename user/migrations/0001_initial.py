# Generated by Django 5.0.2 on 2024-03-07 20:27

import django.contrib.auth.models
import django.db.models.deletion
import django.utils.timezone
import django_cryptography.fields
import model_utils.fields
import uuid
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('program', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Semester',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.IntegerField(help_text='Année (XXXX) + saison (1-4) = 20231 = Hiver 2023; 1 (hiver) - 4 (automne)', primary_key=True, serialize=False)),
                ('year', models.IntegerField(verbose_name='Année')),
                ('season', models.CharField(choices=[('Winter', 'Hiver'), ('Spring', 'Printemps'), ('Summer', 'Été'), ('Autumn', 'Automne')], default='Autumn', max_length=10, verbose_name='Saison')),
            ],
            options={
                'verbose_name': 'Semestre',
                'verbose_name_plural': 'Semestres',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='TrainingLink',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('url', models.CharField(default='', help_text='Max 1024 caractères.', max_length=1024, verbose_name='URL')),
                ('desc', models.TextField(default='', help_text='Description du lien (ce à quoi le lien se rapporte)', verbose_name='Description')),
                ('is_video', models.BooleanField(default=False, help_text='Si le lien pointe vers une vidéo YouTube, Vimeo, Panopto ou autre.', verbose_name='S’agit-il d’un lien vers une vidéo ?')),
            ],
            options={
                'verbose_name': 'Lien de formation',
                'verbose_name_plural': 'Liens de formations',
                'ordering': ['url'],
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('email', models.EmailField(max_length=255, unique=True, verbose_name='Courriel')),
                ('username', django_cryptography.fields.encrypt(models.CharField(blank=True, max_length=150, verbose_name='Nom d’usager'))),
                ('first_name', django_cryptography.fields.encrypt(models.CharField(blank=True, max_length=150, verbose_name='Prénom'))),
                ('last_name', django_cryptography.fields.encrypt(models.CharField(blank=True, max_length=150, verbose_name='Nom de famille'))),
                ('code_permanent', django_cryptography.fields.encrypt(models.CharField(blank=True, default='', max_length=255, verbose_name='Code permanent'))),
                ('nb_credits', models.IntegerField(blank=True, default=None, help_text='Nombre de crédits visés', null=True, verbose_name='Nombre de crédits')),
                ('is_teacher', models.BooleanField(default=False, help_text='Vous avez présentement un contrat d’enseignement', verbose_name='Enseigne')),
                ('establishment', django_cryptography.fields.encrypt(models.CharField(blank=True, help_text='Établissement où vous enseignez, sinon où vous avez fait votre dernier stage', max_length=255, null=True, verbose_name='Établissement'))),
                ('type', models.CharField(choices=[('Agent', 'Agent'), ('Applicant', 'Applicant'), ('Expert', 'Expert'), ('Reviewer', 'Reviewer'), ('Supervisor', 'Supervisor'), ('None', 'None')], default='Applicant', max_length=30, verbose_name='Type d’usager de Passages')),
                ('reviewer_consent', models.BooleanField(default=False, help_text='Le réviseur consent à s’engager dans la démarche du portfolio', verbose_name='Consentement du réviseur')),
                ('expert_consent', models.BooleanField(default=False, help_text='L’expert consent à s’engager dans la démarche du portfolio', verbose_name='Consentement de l’expert')),
                ('concentration', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='program.concentration', verbose_name='Concentration')),
                ('expert', models.ForeignKey(blank=True, help_text='L’expert de contenu qui évalue le portfolio', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='applicant_expert', to=settings.AUTH_USER_MODEL, verbose_name='Expert de contenu')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.group', verbose_name='groups')),
                ('reviewer', models.ForeignKey(blank=True, help_text='Réviseur assigné au candidat', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='applicant_reviewer', to=settings.AUTH_USER_MODEL, verbose_name='Réviseur')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Usager',
                'verbose_name_plural': 'Usagers',
                'ordering': ['email'],
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Attribution',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('credits', models.IntegerField(null=True, verbose_name='Nombre de crédits attribués')),
                ('expert', models.CharField(blank=True, default='', max_length=255, verbose_name='Expert du programme')),
                ('interview', models.DateField(blank=True, null=True, verbose_name='Date de l’entrevue')),
                ('document', models.DateField(blank=True, null=True, verbose_name='Date de l’évaluation documentaire')),
                ('applicant', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Candidat')),
            ],
            options={
                'verbose_name': 'Attribution des crédits',
                'verbose_name_plural': 'Attributions des crédits',
                'ordering': ['applicant__group', 'applicant'],
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.IntegerField(help_text='ID du Semestre (ex: 20231) + code du groupe (1-x); ex: 202344 = Automne 2023, Groupe 4', primary_key=True, serialize=False)),
                ('code', models.IntegerField(default=1, verbose_name='Numéro du groupe')),
                ('assignment', models.IntegerField(default=1, verbose_name='Remise du groupe')),
                ('milestone', models.IntegerField(default=1, verbose_name='Étape du groupe')),
                ('description', models.TextField(blank=True, default='', verbose_name='Description')),
                ('supervisor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='group_supervisor', to=settings.AUTH_USER_MODEL, verbose_name='Superviseur pour le groupe')),
                ('semester', models.ForeignKey(help_text='Semestre au cours duquel le candidat fait sa validation des acquis', on_delete=django.db.models.deletion.PROTECT, to='user.semester', verbose_name='Semestre')),
            ],
            options={
                'verbose_name': 'Groupe',
                'verbose_name_plural': 'Groupes',
                'ordering': ['-id'],
            },
        ),
        migrations.AddField(
            model_name='user',
            name='group',
            field=models.ForeignKey(blank=True, help_text='Groupe au sein duquel le candidat fait sa validation des acquis', null=True, on_delete=django.db.models.deletion.PROTECT, to='user.group', verbose_name='Groupe'),
        ),
        migrations.CreateModel(
            name='GroupReviewer',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('group', models.ForeignKey(help_text='Groupe au sein duquel l’usager agit à titre de réviseur', on_delete=django.db.models.deletion.PROTECT, to='user.group', verbose_name='Groupe')),
                ('reviewer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Réviseur')),
            ],
            options={
                'verbose_name': 'Réviseur pour le groupe',
                'verbose_name_plural': 'Réviseurs pour le groupe',
                'ordering': ['group', 'reviewer'],
            },
        ),
        migrations.CreateModel(
            name='ProgramExpert',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('expert', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Expert de contenu')),
                ('program', models.ForeignKey(help_text='Programme pour lequel l’usager est considéré expert', on_delete=django.db.models.deletion.PROTECT, to='program.program', verbose_name='Programme')),
            ],
            options={
                'verbose_name': 'Expert du programme',
                'verbose_name_plural': 'Experts du programme',
                'ordering': ['program', 'expert'],
            },
        ),
        migrations.AddField(
            model_name='user',
            name='personal_consent',
            field=models.ForeignKey(blank=True, default=None, help_text='Je consens à m’engager dans la démarche du portfolio', null=True, on_delete=django.db.models.deletion.PROTECT, to='user.semester', verbose_name='Consentement personnel'),
        ),
        migrations.CreateModel(
            name='TrainingCapsule',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('status', model_utils.fields.StatusField(choices=[('Draft', 'Draft'), ('Created', 'Created'), ('Rejected', 'Rejected'), ('Submitted', 'Submitted'), ('Underreview', 'Underreview'), ('PreReviewed', 'Pre-Reviewed'), ('Reviewed', 'Reviewed'), ('Final', 'Final'), ('PostInterview', 'Post-Interview'), ('PreCertified', 'Pre-Certified'), ('Certified', 'Certified'), ('Archived', 'Archived')], default='Draft', max_length=100, no_check_for_status=True, verbose_name='status')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, monitor='status', verbose_name='status changed')),
                ('is_removed', models.BooleanField(default=False)),
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('title', models.CharField(default='', max_length=255, verbose_name='Titre')),
                ('intro', models.TextField(default='', verbose_name='Introduction')),
                ('user_type', models.CharField(choices=[('Agent', 'Agent'), ('Applicant', 'Applicant'), ('Expert', 'Expert'), ('Reviewer', 'Reviewer'), ('Supervisor', 'Supervisor'), ('None', 'None')], default='Applicant', help_text='Type d’usager auquel cette capsule s’adresse.', max_length=30, verbose_name='Type d’usager ciblé')),
                ('links', models.ManyToManyField(blank=True, to='user.traininglink', verbose_name='Liens')),
            ],
            options={
                'verbose_name': 'Capsule de formation',
                'verbose_name_plural': 'Capsules de formation',
                'ordering': ['title'],
            },
        ),
    ]
