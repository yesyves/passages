# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import gettext_lazy as _

from program.models import Program
from utils.views import active_portfolio_type
from .models import Group, User


class FormAddGroup(forms.Form):
    """
    Add a group
    """

    year = forms.TypedChoiceField(
        coerce=int,
        label=_("Year"),
        choices=(),
    )
    season_number = forms.TypedChoiceField(
        coerce=int,
        label=_("Semester"),
        choices=((1, _('Winter')),
                 (2, _('Spring')),
                 (3, _('Summer')),
                 (4, _('Autumn'))),
    )
    group_number = forms.TypedChoiceField(
        coerce=int,
        label=_("Group number"),
        choices=(),
    )
    supervisor_id = forms.ChoiceField(
        required=False,
        label=_("Supervisor"),
        choices=(),
    )
    new_supervisor_email = forms.EmailField(
        required=False,
        label=_("Add a new supervisor"),
        help_text=_('If supervisor is not in the above list, provide email to register a new supervisor')
    )
    group_list = forms.FileField(
        label=_("List of applicants to register in this group"),
        help_text=_('Upload CSV file (encoded in UTF-8 format) with ordered fields as follow: '),
    )

    def __init__(self, *args, **kwargs):
        super(FormAddGroup, self).__init__(*args, **kwargs)

        start_year = datetime.date.today().year - 1
        year_list = ()
        for count in range(3):
            year = start_year + count
            ajout = ((year, year),)
            year_list = year_list + ajout
        self.fields['year'].choices = year_list

        start_group = 1
        group_list = ()
        for count in range(5):
            group = start_group + count
            ajout = ((group, group),)
            group_list = group_list + ajout
        self.fields['group_number'].choices = group_list

        supervisors = User.objects.filter(type='Supervisor')
        supervisor_list = ()
        for supervisor in supervisors:
            ajout = ((supervisor.id, str(supervisor)),)
            supervisor_list = supervisor_list + ajout
        self.fields['supervisor_id'].choices = supervisor_list

        user_data = active_portfolio_type().user_data
        fieldnames = []
        for data in user_data:
            for key, value in data.items():
                if value['registration'] is None:
                    fieldnames.append(key)
        self.fields['group_list'].help_text += ', '.join(fieldnames)


class FormAddExperts(forms.Form):
    """
    Add program experts
    """

    experts_list = forms.FileField(label=_("List of program experts to register"))

    def __init__(self, *args, **kwargs):
        super(FormAddExperts, self).__init__(*args, **kwargs)

        self.fields['experts_list'].help_text = _(
            'Upload CSV file (encoded in UTF-8 format) with ordered fields as follow: '
            'Program code, Name ({}), Email, Establishment'
        ).format(active_portfolio_type().get_name_format_display())


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ('username', 'email', 'type',)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'type',)


class FormEditApplicantProfileGeneric(forms.Form):
    """
    Edit profile for applicants
    """

    Username = forms.CharField(
        label=_('User name'),
    )
    Given_name = forms.CharField(
        required=True,
        label=_('Given name'),
    )
    Family_name = forms.CharField(
        required=True,
        label=_('Family name'),
    )
    Email = forms.EmailField(
        required=True,
        label=_('Email'),
        help_text=_('(Your login identifier !)'),
    )
    Code_permanent = forms.CharField(
        required=True,
        label=_('Permanent code'),
    )

    def __init__(self, concentration, policy, *args, **kwargs):
        super(FormEditApplicantProfileGeneric, self).__init__(*args, **kwargs)

        programs = (('NONE', _('Choose program')),)
        for program in Program.available_objects.filter(is_active="True", concentration=concentration):
            option = ((program.id, program),)
            programs = programs + option

        if concentration.type == 'E':
            milestones = (('NONE', _('Choose milestone')),)
            for key, val in concentration.get_milestones().items():
                option = ((key, val),)
                milestones = milestones + option


class FormEditApplicantProfileContinuous(FormEditApplicantProfileGeneric):
    """
    Edit profile for applicants
    """
    Is_teacher = forms.BooleanField(
        required=False,
        label=_('Teacher'),
        help_text=_('(Check box if you presently have a teaching contract)'),
        widget=forms.CheckboxInput(attrs={'onchange': 'switch_establishment();'}),
    )
    Establishment = forms.CharField(
        required=True,
        label=_('Establishment'),
        help_text=_('(Establishment where you currently teach or did your last internship)'),
    )
    Program = forms.ChoiceField(
        required=True,
        label=_('Program'),
        choices=(),
    )

    def __init__(self, concentration, policy, *args, **kwargs):
        super(FormEditApplicantProfileContinuous, self).__init__(concentration, policy, *args, **kwargs)

        # create list of all available programs given current user's concentration
        programs = (('NONE', _('Choose program')),)
        for program in Program.available_objects.filter(is_active="True", concentration=concentration):
            option = ((program.id, program),)
            programs = programs + option

        self.fields['Program'].choices = programs

        portfolio_type = active_portfolio_type()

        # E = K-12 teacher (dropdown choices as 'Credits' field)
        if concentration.type == 'E':
            milestones = (('NONE', _('Choose milestone')),)
            for key, val in concentration.get_milestones().items():
                option = ((key, val),)
                milestones = milestones + option

            self.fields['Credits'] = forms.ChoiceField(
                # disable field if PortfolioType does not allow user-editing of credits
                required=portfolio_type.credits_editable == 'Applicant',
                disabled=portfolio_type.credits_editable != 'Applicant',
                label=_('Targeted milestone'),
                choices=milestones
            )
        # P/T = Vocational / Technical Training (number input as 'Credits' field)
        else:
            self.fields['Credits'] = forms.IntegerField(
                # disable field if PortfolioType does not allow user-editing of credits
                required=portfolio_type.credits_editable == 'Applicant',
                disabled=portfolio_type.credits_editable != 'Applicant',
                label=_('Number of RPL credits targeted'),
                widget=forms.NumberInput(
                    attrs={
                        'min': '0',
                        'max': concentration.max_credits,
                        'step': '3'
                    }
                ))

        # personal_consent (teacher consent does not go through this form)
        if policy:
            self.fields['Consents'] = forms.BooleanField(
                required=True,
                label=_('I consent to the following terms'),
                help_text=policy,
                widget=forms.CheckboxInput(attrs={'onchange': 'switch_personal_consent();'}),
            )


class FormEditApplicantProfileMilestone(FormEditApplicantProfileGeneric):
    """
    Edit profile for applicants (milestone mode)
    """
    def __init__(self, concentration, policy, *args, **kwargs):
        super(FormEditApplicantProfileMilestone, self).__init__(concentration, policy, *args, **kwargs)

        # personal_consent (teacher consent does not go through this form)
        if policy:
            self.fields['Consents'] = forms.BooleanField(
                required=True,
                label=_('I consent to the following terms:'),
                help_text=policy,
                widget=forms.CheckboxInput(attrs={'onchange': 'switch_personal_consent();'}),
            )


class FormEditReviewerProfile(forms.Form):
    """
    Edit profile for non applicants
    """

    Username = forms.CharField(
        label=_('User name'),
    )
    Given_name = forms.CharField(
        required=True,
        label=_('Given name'),
    )
    Family_name = forms.CharField(
        required=True,
        label=_('Family name'),
    )
    Email = forms.EmailField(
        required=True,
        label=_('Email'),
        help_text=_('(Your login identifier !)'),
    )
    Establishment = forms.CharField(
        required=True,
        label=_('Establishment'),
        help_text=_('(Main establishment where you teach or work)'),
    )


class FormPickGroup(forms.Form):
    """
    Select group to review
    """

    Group = forms.ChoiceField(
        required=True,
        label=_("Group"),
        choices=(),
        widget=forms.Select(attrs={'onchange': 'this.form.submit();'}),
    )

    def __init__(self, user, *args, **kwargs):
        super(FormPickGroup, self).__init__(*args, **kwargs)

        group_list = ()

        # Original :
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Div(
                'Group',
                css_class='x-formpicksemester',
            )
        )
        self.helper.label_class = 'mr-3'
        # No better :
        # self.helper.label_class = 'col-auto col-form-label'
        # self.helper.field_class = 'col-auto'

        if user.is_superuser:
            groups = Group.available_objects.all()
        elif user.type == 'Supervisor':
            groups = Group.available_objects.filter(supervisor=user)
        elif user.type == 'Agent':
            # single agent for ALL groups (past & present)
            groups = Group.available_objects.all()
        else:   # Reviewer
            groups = Group.available_objects.filter(groupreviewer__reviewer=user)

        # desc-sort groups by ID, meaning more recent year+season shows up first in dropdown
        # https://www.w3schools.com/django/django_queryset_orderby.php
        groups.order_by("-id")

        for group in groups:
            ajout = ((group.id, group),)
            group_list = group_list + ajout

        self.fields['Group'].choices = group_list
