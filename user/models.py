# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import uuid

from django_cryptography.fields import encrypt

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template.loader import get_template
from django.utils.translation import gettext as _

from portfolio.models import Portfolio, PortfolioType
from utils.models import MetaData, UserType
from program.models import Concentration, Program


class Semester(MetaData):
    """
    id = year (XXXX) + season (1-4) ex: 20184 = Autumn 2018
    """
    id = models.IntegerField(
        primary_key=True,
        help_text=_('Year (XXXX) + season (1-4) = 20231 = Winter 2023; 1 (winter) - 4 (autumn)')
    )
    year = models.IntegerField(
        verbose_name=_('Year')
    )

    SEASONS = (
        ('Winter', _('Winter')), # 1
        ('Spring', _('Spring')), # 2
        ('Summer', _('Summer')), # 3
        ('Autumn', _('Autumn')), # 4
    )
    season = models.CharField(
        max_length=10,
        choices=SEASONS,
        default='Autumn',
        verbose_name=_('Season'),
    )

    class Meta:
        ordering = ['-id']
        verbose_name = _('Semester')
        verbose_name_plural = _('Semesters')

    def __str__(self):
        if self.id == 0:
            return _('Pending')
        else:
            return _('{season} {year}').format(season=_(self.season), year=self.year)

    def str_admin(self):
        return _('{original} ({id})').format(original=self.__str__(), id=self.id)


class Group(MetaData):
    """
    Define a Group in a Semester

    id:int = semester_id (XXXXX) + group_code (1-) ex: 201844 = Autumn 2018, Group 4
    """
    id = models.IntegerField(
        primary_key=True,
        help_text=_('Semester ID (e.g. 20231) + group code (1-x); e.g. 202344 = Autumn 2023, Group 4')
    )
    code = models.IntegerField(
        default=1,
        verbose_name=_('Group number'),
    )
    semester = models.ForeignKey(
        Semester,
        on_delete=models.PROTECT,
        verbose_name=_('Semester'),
        help_text=_('Semester in which the applicant is doing his portfolio'),
    )
    assignment = models.IntegerField(
        default=1,
        verbose_name=_('Group assignment'),
    )
    milestone = models.IntegerField(
        default=1,
        verbose_name=_('Group milestone'),
    )
    supervisor = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='group_supervisor',
        verbose_name=_('Group supervisor')
    )
    # revision_timestamp = models.Time
    description = models.TextField(
        verbose_name=_('Description'),
        blank=True,
        null=False,  # todo better to use null=False
        default=''
    )

    class Meta:
        ordering = ['-id']
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        if self.id == 0:
            return _('Pending')
        return _('{semester}, Group {code}').format(semester=self.semester, code=self.code)

    def str_admin(self):
        return _('{original} ({id})').format(original=self.__str__(), id=self.id)

    def get_attribution(self):
        """
        Returns attribution table data for group

        return dict:header[], [rows[]]
        """
        header = [
            '',
            _('Permanent code'),
            _('Student'),
            _('Credits'),
            _('Discipline'),
            _('Institution'),
            _('Prog'),
            _('Expert'),
            _('Interview\ndate'),
        ]

        attributions = Attribution.available_objects.filter(applicant__group=self,
                                                            credits__isnull=False)

        rows = []

        for i, attribution in enumerate(attributions, 1):
            if attribution.applicant.concentration.type == 'P':
                concentration = 7414
            else:
                concentration = 7415
            program = Portfolio.available_objects.filter(user=attribution.applicant).first().program
            if attribution.applicant.establishment is not None:
                establishment = attribution.applicant.establishment
            else:
                establishment = _('None')
            if attribution.expert:
                expert = attribution.expert
            else:
                expert = _('No interview')
            if attribution.interview is not None:
                interview = attribution.interview
            else:
                interview = ''

            row = [
                i,
                attribution.applicant.code_permanent,
                str(attribution.applicant),
                attribution.credits,
                f'{program.id}\n{program.name}',
                establishment,
                concentration,
                expert,
                interview,
            ]

            rows.append(row)

        return {
            'header': header,
            'rows': rows,
        }


class User(AbstractUser):
    """
    Extends Django user model

    https://wsvincent.com/django-custom-user-model-tutorial/
    https://docs.djangoproject.com/en/2.1/topics/auth/customizing/#a-full-example

    N.B. Can't encrypt email without overriding django auth
         Can't use encrypted fields for default ordering
    """
    email = models.EmailField(
        max_length=255,
        unique=True,
        verbose_name=_('Email address'),
    )
    username = encrypt(
        models.CharField(
            max_length=150,
            blank=True,
            verbose_name=_('User name'),
        )
    )
    first_name = encrypt(
        models.CharField(
            max_length=150,
            blank=True,
            verbose_name=_('First name'),
        )
    )
    last_name = encrypt(
        models.CharField(
            max_length=150,
            blank=True,
            verbose_name=_('Last name'),
        )
    )
    code_permanent = encrypt(
        models.CharField(
            max_length=255,
            blank=True,
            default='',
            verbose_name=_('Permanent code'),
        )
    )
    concentration = models.ForeignKey(
        Concentration,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Concentration'),
    )
    nb_credits = models.IntegerField(
        null=True,
        blank=True,
        default=None,
        verbose_name=_('Number of credits'),
        help_text=_('Number of credits targeted'),
    )
    is_teacher = models.BooleanField(
        default=False,
        verbose_name=_('Is a teacher'),
        help_text=_('You currently have a teaching contract'),
    )
    establishment = encrypt(
        models.CharField(
            max_length=255,
            blank=True,
            null=True,
            verbose_name=_('Establishment'),
            help_text=_('Establishment where you teach or did your last internship'),
        )
    )
    group = models.ForeignKey(
        Group,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_('Group'),
        help_text=_('Group in which the applicant is doing his portfolio')
    )
    reviewer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='applicant_reviewer',
        verbose_name=_('Reviewer'),
        help_text=_('Reviewer assigned to applicant'),
    )
    expert = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='applicant_expert',
        verbose_name=_('Content expert'),
        help_text=_("Content expert who evaluates applicant's portfolio"),
    )
    type = models.CharField(
        max_length=30,
        choices=UserType.choices,
        default=UserType.APPLICANT,
        verbose_name=_('Passages user type'),
    )
    personal_consent = models.ForeignKey(
        Semester,
        null=True,
        blank=True,
        default=None,
        on_delete=models.PROTECT,
        verbose_name=_('Personal consent'),
        help_text=_('I consent to engage in the portfolio process')
    )
    reviewer_consent = models.BooleanField(
        default=False,
        verbose_name=_('Reviewer consent'),
        help_text=_('Reviewer consents to engage in the portfolio process')
    )
    expert_consent = models.BooleanField(
        default=False,
        verbose_name=_('Expert consent'),
        help_text=_('Expert consents to engage in the portfolio process')
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', ]

    class Meta:
        ordering = ['email', ]
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    # shortcut to avoid coding "if user.type == 'Supervisor,etc'" every time we check permissions
    def has_teacher_role(self) -> bool:
        return self.type in 'Supervisor,Reviewer,Expert'

    # helper func to directly get the portfolio w/o having to repeat this code everywhere
    def portfolio(self):
        try:
            return Portfolio.available_objects.get(user=self)
        except ObjectDoesNotExist:
            return None

    def send_mail(self, request, subject, message, link=None, from_email=None):
        """
        Sends an email to this User.
        param   HTTPrequest:request
                str:subject
                str:message
                str:link (to add to host)
                User:from_email
                kwargs
        return  None
        """

        # avoid double slash (//) in certain emails (e.g. pwd reset) due to link sometimes having leading slash
        host = request.build_absolute_uri('/')
        if link is not None and link.startswith('/'):
            host = host.strip('/')

        context = {
            'to': self.first_name,
            # important: 'from' MUST always be same domain as app or valid SMTP user
            'from': settings.SERVER_EMAIL,
            'host': host,
            'link': link,
            'message': message,
        }

        print("user.send_email -> which email backend / vars")
        print(settings.EMAIL_BACKEND)
        print(settings.EMAIL_HOST)
        print(settings.EMAIL_PORT)
        print(settings.SERVER_EMAIL)

        msg = EmailMultiAlternatives(
            subject=subject,
            body=get_template('email/base.txt').render(context),
            from_email=settings.SERVER_EMAIL or from_email,
            to=[self.email],
            cc=[request.user.email],
            reply_to=[request.user.email],
        )
        msg.attach_alternative(get_template('email/base.html').render(context), "text/html")
        return msg.send(fail_silently=False)


class ProgramExpert(MetaData):
    """
    Associate expert users with programs
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    program = models.ForeignKey(
        Program,
        verbose_name=_('Program'),
        on_delete=models.PROTECT,
        help_text=_('Program for which user is considered an expert'),
    )
    expert = models.ForeignKey(
        User,
        verbose_name=_('Content expert'),
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ['program', 'expert']
        verbose_name = _('Program expert')
        verbose_name_plural = _('Program Experts')

    def __str__(self):
        return f'{self.expert} -> {self.program}'


class GroupReviewer(MetaData):
    """
    Associate reviewer users with groups
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    group = models.ForeignKey(
        Group,
        verbose_name=_('Group'),
        on_delete=models.PROTECT,
        help_text=_('Group in which user is acting as reviewer')
    )
    reviewer = models.ForeignKey(
        User,
        verbose_name=_('Reviewer'),
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ['group', 'reviewer']
        verbose_name = _('Group reviewer')
        verbose_name_plural = _('Group reviewers')

    def __str__(self):
        return f'{self.reviewer} -> {self.group}'


class Attribution(MetaData):
    """
    Keep a record of credits attributions
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    applicant = models.ForeignKey(
        User,
        verbose_name=_('Applicant'),
        on_delete=models.PROTECT,
    )
    credits = models.IntegerField(
        verbose_name=_('Number of attributed credits'),
        null=True,
    )
    expert = models.CharField(
        max_length=255,
        default='',
        blank=True,
        verbose_name=_('Program expert'),
    )
    interview = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('Interview date'),
    )
    """
    Only expert is authorized to update this field. It is updated when expert submits the summative
    assessments for validation by supervisor.
    """
    document = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('Date of document-based assessment'),
    )

    class Meta:
        ordering = ['applicant__group', 'applicant']
        verbose_name = _('Credits attribution')
        verbose_name_plural = _('Credits attributions')

    def __str__(self):
        return f'{self.applicant} - {self.applicant.group}'


class TrainingLink(MetaData):
    """
    Arbitrary external link pointing to a resource needed for training of either applicant or staff.
    Contains url + explanation of what the link is about and whether it's in video or text/document format.
    """
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    url = models.CharField(
        max_length=1024,
        verbose_name=_('URL'),
        help_text=_('Max 1024 characters.'),
        default='',
        null=False,
        blank=False
    )
    desc = models.TextField(
        verbose_name=_('Description'),
        help_text=_('Description of the link (what the link is about).'),
        default='',
    )
    is_video = models.BooleanField(
        verbose_name=_('Is a video link?'),
        help_text=_('Whether the link points to a YouTube, Vimeo, Panopto, etc., video.'),
        default=False
    )

    class Meta:
        ordering = ['url']
        verbose_name = _('Training link')
        verbose_name_plural = _('Training links')

    # https://stackoverflow.com/questions/10569438/how-to-print-unicode-character-in-python#answer-63213938
    def __str__(self):
        # trim links & desc to only show part of it for easier visual
        # https:// = 8 chars; http:// = 7 chars
        short_link = self.url[8:25] if self.url.startswith('https://') else self.url[:25]
        xellipsis = "" if self.url.endswith(short_link) else "..."
        short_desc = "n/a" if not self.desc or self.desc.isspace() else self.desc[:75]
        icon = "📹" if self.is_video else ""     # \U0001F4F9
        return f"{icon} {short_desc} ({short_link}{xellipsis})"


class TrainingCapsule(MetaData):
    """Training capsule for content experts (PEDs)"""
    id = models.UUIDField(
        default=uuid.uuid4,
        primary_key=True,
    )
    title = models.CharField(
        max_length=255,
        verbose_name=_('Title'),
        default='',
    )
    intro = models.TextField(
        verbose_name=_('Introduction'),
        default='',
    )
    links = models.ManyToManyField(
        TrainingLink,
        blank=True,
        verbose_name=_('Links')
    )
    # uses same choices as User model (user/models.py @ user_type)
    user_type = models.CharField(
        max_length=30,
        choices=UserType.choices,
        default=UserType.APPLICANT,
        verbose_name=_('Target user type'),
        help_text=_('Type of user for whom this training capsule is intended.')
    )

    class Meta:
        ordering = ['title']
        verbose_name = _('Training capsule')
        verbose_name_plural = _('Training capsules')

    def __str__(self):
        short_intro = "n/a" if not self.intro or self.intro.isspace() else self.intro[:75]
        return f'{self.title} : {short_intro} ...'

    def get_links(self):
        return self.links.all()
