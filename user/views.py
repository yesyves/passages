# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import csv
import io
import json
import locale

from datetime import date

from django.conf import settings
from django.contrib.auth import login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.mail import EmailMessage, mail_admins
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext as _
from django.urls import reverse
from ms_identity_web import IdentityContextData

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE, PROGRAM_EMAIL, PROGRAM_CONTACT
from program.models import Program
from portfolio.models import Portfolio, UserAssessment, UserAchievement, EditorBlock, HelpLink
from portfolio.views import create_portfolio
from utils.models import UserType
from utils.views import active_portfolio, get_help_link, get_random_string, active_portfolio_type
from .models import Group, GroupReviewer, ProgramExpert, Semester, User, TrainingCapsule
from .forms import FormEditReviewerProfile, FormPickGroup, FormAddGroup, FormAddExperts, \
    FormEditApplicantProfileContinuous, FormEditApplicantProfileMilestone

locale.setlocale(locale.LC_ALL, '')


# Validate if current session has OpenID-provided (Azure AD) session data related to currently authenticated user
# - https://learn.microsoft.com/en-us/training/modules/msid-django-web-app-sign-in/5-exercise-sign-in-users-to-django-web-app#create-a-context-processor-for-your-templates
# - https://docs.djangoproject.com/en/4.2/topics/auth/default/#how-to-log-a-user-in
#
# 'upn' claim seems more reliable than 'email' because it's always available, whereas I encountered cases where 'email'
# claim was not available. 'preferred_username' is another reliable one, but it's more likely to change than 'upn'.
def validate_aad_session(request):
    print(request)

    # retrieve AAD-provided context data from current request
    context_data: IdentityContextData = request.identity_context_data
    # AzureAD claims (aka tokens); though private var, it's the only way to access AAD-provided claims
    claims = context_data._id_token_claims
    # exclude those unused claims that are provided by default by AAD
    exclude_claims = ['iat', 'exp', 'nbf', 'uti', 'aio', 'rh']
    claims_to_display = {claim: value for claim, value in claims.items() if claim not in exclude_claims}

    print(claims)
    print(claims_to_display)

    # avoid KeyError by using .get() instead of [] notation
    upn_claim: str | None = claims.get('upn')

    # if user is NOT authenticated via AAD OpenID, redirect to default login page
    if not context_data.authenticated or upn_claim is None:
        return redirect('login')
    else:
        upn_claim = upn_claim.lower()

    print("upn = " + upn_claim)

    # try to find local Django user w/ same email as AAD-authenticated user
    try:
        user = User.objects.get(email=upn_claim)
    except ObjectDoesNotExist as ex:
        print(ex)
        user = None

    # if matching local user exists, manually authenticate him via Django auth mechanism
    if user is not None:
        print(user)
        login(request, user)

        # send user back to default home page
        return redirect(reverse('home'))
    else:
        print(user)
        print("user is AAD-authenticated, but there is no corresponding Django account")
        request.session['aad_local_mismatch'] = True
        request.session['aad_upn'] = upn_claim
        request.session['aad_logout'] = reverse('aad_logout')
        response: HttpResponse = redirect(to='login')
        return response


# Logout user of current AAD session: both Microsoft & Local Django account cookies are cleared
# - https://docs.djangoproject.com/en/4.2/topics/auth/default/#how-to-log-a-user-out
def logout_aad_session(request):
    # destroy Django session which was created when authenticating AAD user initially (map to Django user model)
    auth_logout(request)
    # redirect user to default AAD-defined sign-out url (Microsoft URL) - msal_urls (msal_views_and_urls.py)
    return redirect("sign_out")


@login_required
def group_attribution(request, group_id):
    """Show attribution table for group

    Args:
        request: (HttpRequest object)
        group_id: (int)

    Raises:
        ValueError if request is not GET

    Returns:
        HttpResponse (user/attribution.html)

    """
    if request.method != 'GET':
        raise ValueError(request)

    if request.user.type != 'Supervisor':
        return redirect('profile')

    group = Group.available_objects.get(id=group_id)
    attribution = group.get_attribution()

    context = {
        'attribution': attribution,
        'group': group,
    }

    template = loader.get_template('user/attribution.html')
    return HttpResponse(template.render(context, request))


# MODE=MILESTONE: when we EXIT revision mode, remove all 'new_text' attributes from user achievements for group
def _remove_new_text_attributes_milestone(group_id):
    # get all user achievements of users in current group id
    user_achievements = UserAchievement.available_objects.filter(portfolio__user__group_id=group_id)
    print(f"user achievements for semester {group_id}")

    for achievement in user_achievements:
        tmp_ops = json.loads(achievement.content)['ops']
        for op in tmp_ops:
            if 'attributes' not in op:
                continue

            # check if attributes contain 'new_text' and if so delete that attribute
            attributes = op['attributes']
            if 'new_text' in attributes:
                del attributes['new_text']

        # save updated ops w/ removed 'new_text' attributes
        updated_ops = {"ops": tmp_ops}
        achievement.content = json.dumps(updated_ops)
        achievement.save(update_fields=["content"])


# MODE=MILESTONE: toggle status for portfolio of all users in the specified group (in/out of revision)
@login_required
def toggle_revision_mode(request, group_id):
    """Set status to 'Underreview' OR 'Reviewed' for portfolio of all users in the specified group

    Args:
        request: (HttpRequest object)
        group_id: (int)

    Raises:
        ValueError if request is not GET

    Returns:
        HttpResponse (user/attribution.html)

    """
    # print(request)

    if request.method != 'GET':
        raise ValueError(request)

    user = request.user
    if user.type not in 'Supervisor,Reviewer':
        return redirect('profile')

    # if status is already in revision, switch to 'reviewed', and vice versa
    group = Group.available_objects.get(id=group_id)
    if group.status == 'Underreview':
        # increment assignment number each time we EXIT revision mode so proper questions are shown to the applicant
        group.assignment += 1

        # check if this is the last revision
        editor_blocks = EditorBlock.available_objects.filter(milestone=group.milestone)
        last_assignment = 1
        for block in editor_blocks:
            if block.assignment > last_assignment:
                last_assignment = block.assignment

        if group.assignment > last_assignment:
            group.status = 'Final'
            if group.milestone == 3:
                # Apply status to portfolios
                portfolios = Portfolio.available_objects.filter(group=group)
                for portfolio in portfolios:
                    portfolio.status = 'Final'
                    portfolio.save(update_fields=['status'])
        else:
            group.status = 'Reviewed'

        # todo trigger backend function to wipe out all 'new_text' attributes for all UserAchievements for this group
        _remove_new_text_attributes_milestone(group_id)

    elif group.status != 'Final':
        group.status = 'Underreview'

    group.save(update_fields=['status', 'assignment'])

    return HttpResponse(
        json.dumps({
            'group_status': group.status
        }),
        content_type='application/json'
    )


@login_required
def remove_portfolio(request):
    """Remove portfolio from expert

    Args:
        request: (HttpRequest object)
            POST.portfolio_id: uuid

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST' or not request.POST:
        return redirect('profile')

    portfolio_id = request.POST.get('portfolio_id')

    if portfolio_id is None:
        return redirect('profile')

    comment = request.POST.get('comment')
    portfolio = Portfolio.available_objects.get(id=portfolio_id)
    applicant = portfolio.user

    subject = _('Portfolio no longer for evaluation')
    message = [_('is removing your access to the portfolio of {}').format(applicant)]
    if comment:
        message.extend([_('with the following comment:'), comment])

    message.append(_('Thank you for deleting any material you might have downloaded from the portfolio.'))

    applicant.expert.send_mail(request, subject, message)

    portfolio.status = 'Final'
    portfolio.save()
    applicant.expert = None
    applicant.save()

    return redirect('pick_semester')


@login_required
def send_attribution(request):
    """Send attribution table as a csv and html file to modulefp@uqam.ca, cc request.user

    Args:
        request: (HttpRequest object)
            POST.group_id (int)

    Raises:
        ValueError if request is not POST

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    if request.user.type not in 'Supervisor,Reviewer':
        return redirect('profile')

    group = Group.available_objects.get(id=request.POST.get('group_id'))
    additional_message = request.POST.get('message')
    if additional_message:
        additional_message += '\n\n'
    else:
        additional_message = ''

    to_email = [PROGRAM_EMAIL]
    to_name = PROGRAM_CONTACT
    subject = _('Credits attribution - {}').format(group)
    message = _('Hi {name},\n\n'
                'Here is a credit attribution for group {group}\n\n'
                '{additional_message}'
                'Thank you very much and have a great day!\n\n'
                '{user}\n\n').format(name=to_name, group=group, additional_message=additional_message,
                                     user=request.user)

    msg = EmailMessage(
        subject,
        message,
        settings.SERVER_EMAIL,
        to_email,
        cc=[request.user.email],
        reply_to=[request.user.email],
    )

    # Get attribution and add rows for program administration
    attribution = group.get_attribution()
    attribution['header'].extend([_('Validated\ncredits'),
                                  _('Sent to\nregistrar')])
    for row in attribution['rows']:
        row.extend(['', ''])

    # Generate html table
    file_name = f'Attribution_{date.today()}.html'
    context = {
        'attribution': attribution,
        'semester': group.semester,
        'group': group
    }

    msg.attach(
        file_name,
        loader.get_template('email/attribution.html').render(context),
        'text/html'
    )

    # Generate csv table
    file_name = f'Attribution_{date.today()}.csv'
    output = io.StringIO()
    writer = csv.writer(output, dialect='excel', delimiter=';')
    writer.writerow(attribution['header'])
    for row in attribution['rows']:
        writer.writerow(row)

    msg.attach(
        file_name,
        output.getvalue(),
        'text/csv'
    )

    output.close()

    # Send message
    if msg.send(fail_silently=False):
        success = True
    else:
        success = False

    return HttpResponse(
        json.dumps({'success': success}),
        content_type='application/json'
    )


@login_required
def add_group(request):
    """

    Args:
        request:

    Returns:

    """
    if not request.user.is_superuser:
        raise ValueError(request)

    if request.method == 'POST':
        form = FormAddGroup(request.POST, request.FILES)

        if form.is_valid():
            year = form.cleaned_data['year']
            season_number = form.cleaned_data['season_number']
            group_number = form.cleaned_data['group_number']
            # TODO Insert in form
            milestone = 1
            supervisor_id = form.cleaned_data['supervisor_id']
            new_supervisor_email = form.cleaned_data['new_supervisor_email'].lower()

            #
            # Supervisor
            #
            if supervisor_id:
                supervisor = User.objects.get(id=supervisor_id)
            elif new_supervisor_email:
                try:
                    supervisor = User.objects.get(email=new_supervisor_email)
                    supervisor.type = 'Supervisor'
                    supervisor.save()
                except ObjectDoesNotExist:
                    supervisor = User(
                        username=new_supervisor_email[0:new_supervisor_email.find('@')],
                        email=new_supervisor_email,
                        password=make_password(get_random_string(12)),
                        is_superuser=False,
                        is_staff=True,
                        type='Supervisor',
                    )
                    supervisor.save()
            else:
                supervisor = request.user

            #
            # Semester
            #
            seasons = {
                1: 'Winter',
                2: 'Spring',
                3: 'Summer',
                4: 'Autumn',
            }
            semester_id = f'{year}{season_number}'
            season_id = seasons[season_number]

            try:
                semester = Semester.available_objects.get(id=semester_id)
                create_semester = _('N.B. Semester {} already exists').format(semester)
            except ObjectDoesNotExist:
                semester = Semester(
                    id=semester_id,
                    year=year,
                    season=season_id,
                )
                semester.save()

                create_semester = _('Creating Semester {}').format(semester)

            #
            # Group
            #
            group_id = f'{semester_id}{group_number}'

            try:
                group = Group.available_objects.get(id=group_id)
                create_group = _('N.B. Group {} already exists').format(group)
            except ObjectDoesNotExist:
                group = Group(
                    id=group_id,
                    semester=semester,
                    code=group_number,
                    supervisor=supervisor,
                    milestone=milestone,
                )
                group.save()

                create_group = _('Creating Group {}').format(group)
                if IS_MILESTONE:
                    create_group += _(' Milestone {}').format(milestone)

            #
            # Users
            #
            csvfile = request.FILES["group_list"].read().decode('utf-8-sig')

            portfolio_type = active_portfolio_type()
            user_data = portfolio_type.user_data
            fieldnames = []
            new_user = {}

            for data in user_data:
                for key, value in data.items():
                    if value['registration'] is None:
                        fieldnames.append(key)
                    else:
                        new_user[key] = value['registration']

            # if curriculum_code == 'UQAM-BEFPT':
            #     fieldnames = ['code_permanent', 'name', 'concentration_id', 'email']
            #     establishment = ''
            #     nb_credits = None
            #     concentration_id = row['concentration_id']
            # elif curriculum_code == 'CEMP-TRD':
            #     fieldnames = ['code_permanent', 'name', 'email']
            #     concentration_id = 1
            #     establishment = ''
            #     nb_credits = None

            reader = csv.DictReader(csvfile.splitlines(), fieldnames=fieldnames, delimiter=';')

            create_users = ''
            created_portfolios = 0
            updated_applicants = 0
            updated_portfolios = 0
            created_users = 0
            created_group_reviewers = 0
            programs_not_found = []

            name_format = portfolio_type.name_format

            for row in reader:
                # DEBUG:
                # print(row)

                i = row['email'].find('@')
                if i == -1:
                    continue

                for data in user_data:
                    for key, value in data.items():
                        if value['registration'] is None:
                            new_user[key] = row[key]

                email = new_user['email'].lower()

                if new_user['code_permanent'] or portfolio_type.policy_reviewer == 'no_reviewers':
                    user_type = 'Applicant'
                    group = group
                else:
                    user_type = 'Reviewer'
                    group = None

                try:
                    # Check if user already exists
                    user = User.objects.get(email=email)
                    user.concentration_id = new_user['concentration_id']
                    user.group = group
                    user.type = user_type
                    user.reviewer = None
                    user.save()
                    updated_applicants += 1
                except ObjectDoesNotExist:
                    username = row['email'][0:i]

                    if name_format == 'Column':
                        first_name = row['first_name']
                        last_name = row['last_name']
                    elif name_format == 'Comma':
                        name = row['name'].split(', ')
                        first_name = name[1]
                        last_name = name[0]
                    elif name_format == 'Space':
                        name = row['name'].split(' ', 1)
                        first_name = name[0]
                        last_name = name[1]
                    else:
                        raise ValueError(row)

                    user = User(
                        username=username,
                        last_name=last_name.replace(u"'", u"’"),
                        first_name=first_name.replace(u"'", u"’"),
                        email=email,
                        password=make_password(get_random_string(12)),
                        is_superuser=False,
                        is_staff=True,
                        type=user_type,
                        code_permanent=new_user['code_permanent'],
                        concentration_id=new_user['concentration_id'],
                        group=group,
                        establishment=new_user['establishment'],
                        nb_credits=new_user['nb_credits'],
                    )
                    user.save()
                    created_users += 1

                if new_user['discipline']:
                    d = new_user['discipline']
                    if d.find('(') != -1 and d.find(')') != -1:
                        program_id = d[d.find('(') + 1:d.find(')')]
                        try:
                            program = Program.available_objects.get(id=program_id)
                            try:
                                portfolio = Portfolio.available_objects.get(user=user)
                                portfolio.program = program
                                portfolio.save()
                                updated_portfolios += 1
                            except ObjectDoesNotExist:
                                create_portfolio(user, program)
                                created_portfolios += 1
                        except ObjectDoesNotExist:
                            programs_not_found.append(program_id)

                create_users += f"<br>{user}"

            programs_not_found = sorted(set(programs_not_found))
            message = {
                0: _('Group successfully created'),
                1: f'{create_semester}<br>{create_group}',
                2: _('{} users created, {} applicants updated<br>'
                     '{} portfolios created, {} portfolios updated<br>'
                     'Program IDs not found: {}<br>'
                     '{} reviewers created for this group').format(created_users,
                                                                   updated_applicants,
                                                                   created_portfolios,
                                                                   updated_portfolios,
                                                                   ', '.join(programs_not_found),
                                                                   created_group_reviewers),
                3: create_users,
            }

            return render(request, 'portfolio/restrict.html', {'message': message, 'target': 'add_group'})

    else:
        form = FormAddGroup()

    return render(request, "user/add_group.html", {"form": form})


@login_required
def add_experts(request):
    """

    Args:
        request:

    Returns:

    """
    if request.user.type not in ['Agent', 'Supervisor']:
        raise ValueError(request)

    if request.method == 'POST':
        form = FormAddExperts(request.POST, request.FILES)

        if form.is_valid():
            username = ''
            previous_username = username
            user = None

            name_format = active_portfolio_type().name_format

            # experts.csv : Code programme; Prénom Nom; Courriel; Établissement
            csvfile = request.FILES["experts_list"].read().decode('utf-8-sig')

            if name_format == 'Column':
                fieldnames = ['program_id', 'last_name', 'first_name', 'email', 'establishment']
            else:
                fieldnames = ['program_id', 'name', 'email', 'establishment']
            reader = csv.DictReader(csvfile.splitlines(), fieldnames=fieldnames, delimiter=';')

            registered_experts = ''

            for row in reader:
                # DEBUG:
                # print(row)

                email = row['email'].lower()
                i = email.find('@')
                if i > 0:
                    username = email[0:i]
                else:
                    continue

                if name_format == 'Column':
                    first_name = row['first_name']
                    last_name = row['last_name']
                elif name_format == 'Comma':
                    name = row['name'].split(', ')
                    first_name = name[1]
                    last_name = name[0]
                elif name_format == 'Space':
                    name = row['name'].split(' ', 1)
                    first_name = name[0]
                    last_name = name[1]
                else:
                    raise ValueError(row)

                if username != previous_username:
                    previous_username = username
                    registered_experts += f'<br>{row["name"]} : '
                    try:
                        user = User.objects.get(email=email)
                    except ObjectDoesNotExist:
                        user = User(
                            username=username,
                            last_name=last_name.replace(u"'", u"’"),
                            first_name=first_name.replace(u"'", u"’"),
                            email=email,
                            password=make_password(get_random_string(12)),
                            is_superuser=False,
                            is_staff=True,
                            type='Expert',
                            establishment=row['establishment'],
                        )
                        user.save()

                try:
                    expert = ProgramExpert.available_objects.get(expert=user, program_id=row['program_id'])
                except ObjectDoesNotExist:
                    expert = ProgramExpert(
                        expert=user,
                        program_id=row['program_id'],
                    )
                    expert.save()
                    registered_experts += f"{row['program_id']}, "

            message = {
                0: _('Experts successfully registered'),
                3: registered_experts,
            }

            return render(request, 'portfolio/restrict.html', {'message': message, 'target': 'add_experts'})

    else:
        form = FormAddExperts()

    return render(request, "user/add_experts.html", {"form": form})


@login_required
def edit_profile(request):
    """Process profile edition form

    Args:
        request: (HttpRequest object)

    Returns:
        HttpResponse(user/edit_profile.html)

    """

    user = request.user
    user_type = user.type

    help_link = HelpLink.available_objects.get(keyword='profile').link

    portfolio: Portfolio | None = None
    f_program: str = 'NONE'
    if user_type == 'Applicant':
        portfolio = Portfolio.available_objects.filter(user=user).first()
        policy = active_portfolio_type().policy_applicant
        if portfolio:
            f_program = portfolio.program.id
    else:
        policy = None

    if request.method == 'GET':
        # if not 'applicant', meaning staff
        if user_type != 'Applicant':
            form = FormEditReviewerProfile(initial={'Username': user.username,
                                                    'Given_name': user.first_name,
                                                    'Family_name': user.last_name,
                                                    'Email': user.email,
                                                    'Establishment': user.establishment})
        else:
            # user_type = Applicant
            if user.personal_consent is None:
                consent = False
            else:
                consent = True

            initial_data = {
                'Username': user.username,
                'Given_name': user.first_name,
                'Family_name': user.last_name,
                'Email': user.email,
                'Code_permanent': user.code_permanent,
                'Consents': consent
            }

            # (continuous) create profile edit form
            if IS_CONTINUOUS:
                # user's current number of credits
                credits_target = user.nb_credits
                if credits_target is None:
                    if user.concentration.type == 'E':
                        credits_target = 'NONE'
                    else:
                        credits_target = user.concentration.max_credits

                initial_data.update({
                    'Establishment': user.establishment,
                    'Is_teacher': user.is_teacher,
                    'Program': f_program,
                    'Credits': credits_target,
                })
                form = FormEditApplicantProfileContinuous(
                    user.concentration,
                    policy,
                    initial=initial_data
                )

            # (milestone) create profile edit form
            else:
                form = FormEditApplicantProfileMilestone(
                    user.concentration,
                    policy,
                    initial=initial_data
                )

    # when user submits changes to his profile form
    elif request.method == 'POST':
        if user_type != 'Applicant':
            form = FormEditReviewerProfile(request.POST)
        else:
            if IS_CONTINUOUS:
                form = FormEditApplicantProfileContinuous(user.concentration, policy, request.POST)
            else:
                form = FormEditApplicantProfileMilestone(user.concentration, policy, request.POST)

        # validate & parse user form input
        if not form.is_valid():
            context = {
                'form': form,
                'help': help_link,
                'message': _("ERROR")
            }
            return render(request, 'user/edit_profile.html', context)
        else:
            user.username = form.cleaned_data['Username']
            user.email = form.cleaned_data['Email'].lower()
            user.first_name = form.cleaned_data['Given_name'].replace(u"'", u'’')
            user.last_name = form.cleaned_data['Family_name'].replace(u"'", u'’')
            if user_type == 'Applicant':
                user.code_permanent = form.cleaned_data['Code_permanent']
                consent = form.cleaned_data['Consents']
                print(consent)
                if consent:
                    user.personal_consent = user.group.semester
                else:
                    user.personal_consent = None

                    # continuous + staff
            if IS_CONTINUOUS or user_type != 'Applicant':
                user.establishment = form.cleaned_data['Establishment']

            # continuous + applicant
            if IS_CONTINUOUS and user_type == 'Applicant':
                if active_portfolio_type().credits_editable == 'Applicant':
                    user.nb_credits = form.cleaned_data['Credits']
                user.is_teacher = form.cleaned_data['Is_teacher']

                f_program = form.cleaned_data['Program']
                if f_program != 'NONE':
                    new_program = Program.available_objects.get(id=f_program)
                    if not portfolio:
                        # First time : Create portfolio
                        create_portfolio(user, new_program)
                    else:
                        # if user is trying to change his current program (alias: portfolio.type.program)
                        if portfolio.program != new_program:
                            # determine if user is allowed to change program (only if NO skill tags set in achievements)
                            skilled_achievements = []
                            achievements = portfolio.get_achievements(user_type)
                            for achievement in achievements:
                                if achievement.ops:
                                    for op in achievement.ops:
                                        this_op = json.loads(op[0])
                                        if this_op['type'] == 'competency':
                                            skilled_achievements.append(str(achievement.number))
                                            break

                            error: str | None = None

                            # if no user achievement associated w/ skill tags, switch program
                            if not skilled_achievements:
                                portfolio.program = new_program
                                portfolio.save()

                                # update all portfolio's achievements to reset initial targeted competency
                                for achievement in achievements:
                                    achievement.targeted_competency = None
                                    achievement.save()
                            # if user achievements have skill tags, show error message
                            else:
                                if len(skilled_achievements) > 1:
                                    error = _('CANNOT MODIFY PROGRAM, DELETE SKILL TAGS FROM ACHIEVEMENTS ') \
                                            + ', '.join(skilled_achievements)
                                else:
                                    error = _('CANNOT MODIFY PROGRAM, DELETE SKILL TAGS FROM ACHIEVEMENT ') \
                                            + skilled_achievements[0]

                            # reload edit_profile page w/ error message (if any)
                            if error:
                                context = {
                                    'form': form,
                                    'help': help_link,
                                    'message': error
                                }
                                return render(request, 'user/edit_profile.html', context)

            # save user profile modifications, then redirect to user's portfolio
            user.save()
            return redirect('profile')

    # load edit_profile page
    context = {
        'form': form,
        'policy': policy,
        'help': help_link,
    }
    return render(request, 'user/edit_profile.html', context)


@login_required
def pick_semester_milestone(request):
    """Show and process group picker form

    Set 'f_group' cookie
    Show applicants' achievements table for chosen or most recent group
    Show reviewer for group

    Args:
        request: (HttpRequest object)
           POST.form: Group to set as default
           Cookie.f_group (int)

    Returns:
        HttpResponse (user/pick_portfolio.html)

    """
    # semester group form
    form_pick_group: FormPickGroup | None = None
    user = request.user
    user_type = user.type
    if user_type == 'Applicant':
        # Unauthorized access !
        # DEBUG : Monitor users
        mail_admins("In Pick_semester", f"{user} : {user_type}")
        return redirect('profile')

    # Semester form
    form = None
    f_group_id = ''
    cookie_name = 'f_group'
    response = HttpResponse()

    current_milestone = 1

    if request.method == 'POST':
        form_pick_group = FormPickGroup(user, request.POST)
        if form_pick_group.is_valid():
            f_group_id = form_pick_group.cleaned_data['Group']
            form_pick_group = FormPickGroup(user)
            form_pick_group.fields['Group'].initial = f_group_id

            response.set_cookie(cookie_name,
                                f_group_id,
                                max_age=10540800,  # 4 mois (1 semestre)
                                httponly=True,
                                samesite='Strict')
        else:
            raise ValidationError(form_pick_group.errors.as_data())

    # User has a portfolio ?
    try:
        my_portfolio_id = Portfolio.available_objects.get(user=user).id
    except ObjectDoesNotExist:
        my_portfolio_id = None

    # Applicants table
    group: Group | None = None

    if user_type == 'Expert':
        # Expert sees only applicants currently assigned to him
        #   but for every group at once
        applicants = User.objects.filter(expert=user)
    else:
        # Set current group and picker
        group = None

        # "not form" = default group selected in dropdown, so NO user action to switch group
        if not form_pick_group:
            form_pick_group = FormPickGroup(user)
            if request.COOKIES.get(cookie_name):
                f_group_id = request.COOKIES[cookie_name]
            if not f_group_id:
                if user.is_superuser:
                    # Superuser sees every group
                    group = Group.available_objects.first()
                elif user_type == 'Supervisor':
                    group = Group.available_objects.filter(supervisor=user).first()
                elif user_type == 'Agent':
                    group = Group.available_objects.first()
                else:
                    group = GroupReviewer.available_objects.filter(reviewer=user).first().group

                if group is not None:
                    f_group_id = group.id
                else:
                    admin_emails = ','.join([a[1] for a in settings.ADMINS])
                    message = [_('No available semester'),
                               _('You do not have access to any semester right now'),
                               '',
                               _('Please contact the site administrator for more information')]
                    context = {
                        'user_type': user_type,
                        'message': message,
                        'instance': admin_emails,
                        'target': 'pick_semester',
                    }
                    template = loader.get_template('portfolio/restrict.html')
                    return HttpResponse(template.render(context, request))

            form_pick_group.fields['Group'].initial = f_group_id

        if not group:
            group = Group.available_objects.get(id=f_group_id)

        # if user_type == 'Reviewer':
        #     # Reviewer sees applicants assigned to him
        #     applicants = User.objects.filter(group=group,
        #                                      type='Applicant',
        #                                      reviewer=request.user)
        if user_type in ['Supervisor', 'Reviewer', 'Agent']:
            # Supervisor sees all applicants
            applicants = User.objects.filter(group=group, type='Applicant')
        else:
            # Unauthorized access !
            raise ValueError(request)

    # applicant_portfolio = Applicant, Portfolio, Achievements list,
    #                       Program experts, Date of last modification,
    #                       Probable number of achievements based on credits target
    applicant_portfolios = []

    portfolio_type = active_portfolio_type()
    current_milestone: int = 1
    for applicant in applicants:
        portfolio = applicant.portfolio()

        # try to get user's portfolio assessment (if available)
        current_milestone = applicant.group.milestone
        assessment_grades = {}
        for milestone in range(1, current_milestone + 1):
            assessment_grades[milestone] = 0
        user_assessments = UserAssessment.available_objects.filter(portfolio=portfolio,
                                                                   assessment__milestone__lte=current_milestone,
                                                                   assessment__code='total')
        for user_assessment in user_assessments:
            grade = "%.1f" % (user_assessment.grade * 100)
            assessment_grades[user_assessment.assessment.milestone] = grade

        achievements_list = []
        experts = None
        final_achievements = 0
        num_achievements = applicant.concentration.max_num_achievements
        i = 1

        # # This could be refactored with user_achievements but probably better left by alph order
        # if portfolio_type is None:
        #     last_edit = applicant.date_joined
        # else:
        #     achievement_types = portfolio_type.get_achievement_types()
        #     last_edit = portfolio_type.status_changed

        applicant_item = {
            'applicant': applicant,
            'experts': experts,
            'portfolio': portfolio,
            'achievements_list': achievements_list,
            # 'last_edit': last_edit,
            'num_achievements': num_achievements,
            'final_achievements': final_achievements,
            'assessment_grades': assessment_grades,
        }
        applicant_portfolios.append(applicant_item)

    # # Apply sort
    # applicant_portfolio = sorted(applicant_portfolio,
    #                              key=lambda x: x['last_edit'],
    #                              reverse=True)

    # Reviewers
    reviewers = []
    if user_type in ['Supervisor', 'Agent'] and group is not None:
        reviewers.append(group.supervisor)
        reviewers += User.objects.filter(groupreviewer__group=group,
                                         groupreviewer__is_removed=False).order_by('first_name')
    try:
        group_status = Group.available_objects.get(id=f_group_id).status
    except Group.DoesNotExist:
        group_status = 'Draft'

    context = {
        'applicant_portfolios': applicant_portfolios,
        'form_pick_group': form_pick_group,
        'my_portfolio_id': my_portfolio_id,
        'group_id': f_group_id,
        'current_milestone': current_milestone,
        'group_status': group_status,
        'reviewers': reviewers,
        'user_type': user_type,
    }

    template = loader.get_template('user/pick_portfolio.html')
    response.content = template.render(context, request)

    return response


@login_required
def pick_semester_continuous(request):
    """Show and process group picker form

    Set 'f_group' cookie
    Show applicants' achievements table for chosen or most recent group
    Show reviewer for group

    Args:
        request: (HttpRequest object)
           POST.form: Group to set as default
           Cookie.f_group (int)

    Returns:
        HttpResponse (user/pick_portfolio.html)

    """
    user = request.user
    user_type = user.type
    if user_type == 'Applicant':
        # Unauthorized access !
        return redirect('profile')

    portfolio_type = active_portfolio_type()

    # Semester form
    form_pick_group = None
    f_group_id = ''
    cookie_name = 'f_group'
    response = HttpResponse()

    if request.method == 'POST':
        form_pick_group = FormPickGroup(user, request.POST)
        if form_pick_group.is_valid():
            f_group_id = form_pick_group.cleaned_data['Group']
            form_pick_group = FormPickGroup(user)
            form_pick_group.fields['Group'].initial = f_group_id

            response.set_cookie(cookie_name,
                                f_group_id,
                                max_age=10540800,  # 4 mois (1 semestre)
                                httponly=True,
                                samesite='Strict')
        else:
            raise ValidationError(form_pick_group.errors.as_data())

    # User has a portfolio ?
    try:
        my_portfolio_id = Portfolio.available_objects.get(user=user).id
    except ObjectDoesNotExist:
        my_portfolio_id = None

    # Applicants table
    group = None

    if user_type == 'Expert':
        # Expert sees only applicants currently assigned to him
        #   but for every semester at once
        applicants = User.objects.filter(expert=request.user)
    else:
        # Set current group and picker
        # group = None

        # "not form" = default group selected in dropdown, so NO user action to switch group
        if not form_pick_group:
            form_pick_group = FormPickGroup(request.user)
            if request.COOKIES.get(cookie_name):
                f_group_id = request.COOKIES[cookie_name]
            if not f_group_id:
                if request.user.is_superuser:
                    # super-user sees every semester
                    group = Group.available_objects.first()
                elif user_type == 'Supervisor':
                    group = Group.available_objects.filter(supervisor=user).first()
                elif user_type == 'Agent':
                    group = Group.available_objects.first()
                else:
                    group = GroupReviewer.available_objects.filter(reviewer=user).first().group

                if group is not None:
                    f_group_id = group.id

            form_pick_group.fields['Group'].initial = f_group_id

        if not group:
            # DEBUG
            # print(f"f-group-id = {f_group_id}")
            group = Group.available_objects.get(id=f_group_id)

        if user_type == 'Reviewer':
            # Reviewer sees applicants assigned to him
            applicants = User.objects.filter(group=group,
                                             type='Applicant',
                                             reviewer=user)
        elif user_type in ['Supervisor', 'Agent']:
            # Supervisor sees all applicants
            applicants = User.objects.filter(group=group, type='Applicant')
        else:
            # Unauthorized access !
            raise ValueError(request)

    # applicant_portfolio = Applicant, Portfolio, Achievements list,
    #                       Program experts, Date of last modification,
    #                       Probable number of achievements based on credits target
    applicant_portfolios = []
    max_num_achievements = 0

    for applicant in applicants:
        portfolio = Portfolio.available_objects.filter(user=applicant).first()

        achievements_list = []
        experts = None
        final_achievements = 0
        num_achievements = applicant.concentration.max_num_achievements
        if num_achievements > max_num_achievements:
            max_num_achievements = num_achievements
        # available credits for each applicant (editable by agent via pick_portfolio.html)
        available_credits = []
        # avoid NoneType errors if nb_credits is not yet defined
        nb_credits = applicant.nb_credits or 0

        if portfolio_type.curriculum_code == 'UQAM-BEFPT':
            if (
                    nb_credits <= 6 and applicant.concentration.type == 'T'
                    or nb_credits <= 9 and applicant.concentration.type == 'P'
            ):
                num_achievements = 2
            elif (
                    nb_credits <= 9 and applicant.concentration.type == 'T'
                    or nb_credits <= 12 and applicant.concentration.type == 'P'
            ):
                num_achievements = 3
            elif nb_credits <= 18:
                num_achievements = 4
            elif nb_credits <= 21:
                num_achievements = 5

        if portfolio_type.credits_editable == 'Agent':
            # get max number of credits attributable for portfolio type's concentration to populate dropdown
            max_credits = applicant.concentration.max_credits or 3
            # start at 0, up to 'max_credits', skip 3 on each loop iteration
            for x in range(0, max_credits + 1, 3):
                # ignore if NOT a multiple of 3 (it works in steps of 3 credits)
                # if x % 3 != 0: continue
                # e.g. 3, 6, 9, 12, ...
                available_credits.append(x)

        i = 1

        if portfolio is None:
            last_edit = applicant.date_joined
        else:
            achievements = portfolio.get_achievements(user_type)
            last_edit = portfolio.status_changed

            # supervisor & agent see experts related to current portfolio's program
            if user_type in ['Supervisor', 'Agent']:
                experts = User.objects.filter(programexpert__program=portfolio.program,
                                              programexpert__is_removed=False)

            for a in achievements:
                while a.number > i:
                    achievements_list.append(None)
                    i += 1
                achievements_list.append(a)
                if a.status == 'Final':
                    final_achievements += 1
                # Sort by last modified
                if a.status == 'Submitted' and last_edit < a.status_changed:
                    last_edit = a.status_changed
                i += 1

        # Fill every portfolio with empty achievements
        while i <= max_num_achievements:
            achievements_list.append(None)
            i += 1

        applicant_item = {
            'applicant': applicant,
            'experts': experts,
            'portfolio': portfolio,
            'achievements_list': achievements_list,
            'last_edit': last_edit,
            'num_achievements': num_achievements,
            'final_achievements': final_achievements,
            'available_credits': available_credits,
        }
        applicant_portfolios.append(applicant_item)

    # Apply sort
    applicant_portfolios = sorted(applicant_portfolios,
                                  key=lambda ap: ap['last_edit'],
                                  reverse=True)

    # Reviewers
    reviewers = []
    if user_type in ['Supervisor', 'Agent']:
        reviewers.append(group.supervisor)
        reviewers += User.objects.filter(groupreviewer__group=group,
                                         groupreviewer__is_removed=False).order_by('first_name')

    context = {
        'applicant_portfolios': applicant_portfolios,
        'max_num_achievements': max_num_achievements,
        'form_pick_group': form_pick_group,
        'my_portfolio_id': my_portfolio_id,
        'group_id': f_group_id,
        'reviewers': reviewers,
        'user_type': user_type,
        'user_is_admin': user.is_superuser,
    }

    template = loader.get_template('user/pick_portfolio.html')
    response.content = template.render(context, request)

    return response


@login_required
def remove_user(request, user_id):
    """

    Args:
        request:
        user_id:

    Returns:

    """

    if request.method != 'GET':
        raise ValueError(request)

    user = User.objects.get(id=user_id)
    user.group_id = 0
    user.save()

    return redirect('pick_semester')


@login_required
def set_reviewer(request):
    """Associate reviewer to applicant

    Args:
        request: (HttpRequest object)
            POST.applicant_id (int)
            POST.reviewer_id (int)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    applicant_id = request.POST.get('applicant_id')
    reviewer_id = request.POST.get('reviewer_id')

    applicant = User.objects.get(id=applicant_id)
    reviewer = None

    if reviewer_id != 'None':
        reviewer = User.objects.get(id=reviewer_id)

    applicant.reviewer = reviewer
    applicant.save(update_fields=['reviewer'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def set_expert(request):
    """Associate expert to applicant

    Args:
        request: (HttpRequest object)
            POST.applicant_id (int)
            POST.expert_id (int)

    Returns:
        HttpResponse (application/json)
    """
    if request.method != 'POST':
        raise ValueError(request)

    applicant_id = request.POST.get('applicant_id')
    expert_id = request.POST.get('expert_id')

    applicant = User.objects.get(id=applicant_id)
    expert = None

    if expert_id != 'None':
        expert = User.objects.get(id=expert_id)

    applicant.expert = expert
    applicant.save(update_fields=['expert'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def set_target_credits(request):
    """Set target credits to applicant

    Args:
        request: (HttpRequest object)
            POST.applicant_id (int)
            POST.nb_credits (int)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    applicant_id = request.POST.get('applicant_id')
    nb_credits = request.POST.get('nb_credits')

    if applicant_id is not None and nb_credits is not None:
        applicant: User | None = User.objects.get(id=applicant_id)
        applicant.nb_credits = nb_credits
        applicant.save(update_fields=['nb_credits'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def group_notify(request):
    """Send set password link to group users that have not yet logged in

    Args:
        request.POST.group_id (int)
        request.POST.user_type (str)
            This can be a user_type      -> Send to every user of that type associated with this group
            This can be an email         -> Send to this particular email
            This can be anm applicant ID -> Send to this applicant's selected expert
        request.POST.message (str)
        request.POST.my_applicants (checkbox)
        request.POST.set_password (checkbox)

    Returns:
        HttpResponse (application/json)

    """
    # if request.method != 'POST' or (request.user.type != 'Supervisor' and request.user.type != 'Reviewer'):
    allowed_roles = ['Supervisor', 'Reviewer', 'Agent']
    if request.method != 'POST' or request.user.type not in allowed_roles:
        raise ValueError(request)

    group_id = request.POST.get('group_id')

    if request.POST.get('my_applicants') == 'true':
        # Send message to associated applicants
        set_password = False
        users = User.objects.filter(group_id=group_id,
                                    type='Applicant',
                                    reviewer=request.user)
    else:
        user_type = request.POST.get('user_type')

        if '@' in user_type:
            users = User.objects.filter(email=user_type)
        elif user_type.isnumeric():
            applicant = User.objects.get(id=user_type)
            users = [applicant.expert]
        elif user_type == 'Reviewer':
            users = User.objects.filter(groupreviewer__group_id=group_id,
                                        groupreviewer__group__is_removed=False)
        else:
            # user_type is Applicant or Expert:
            users = User.objects.filter(group_id=group_id,
                                        type='Applicant')
            if user_type == 'Expert':
                experts = []
                for user in users:
                    if user.expert is not None and user.expert not in experts:
                        experts.append(user.expert)
                users = experts

        if request.POST.get('set_password') == 'true':
            set_password = True
            if '@' not in user_type:
                no_login = []
                for user in users:
                    if user.last_login is None:
                        no_login.append(user)
                users = no_login
        else:
            set_password = False

    invited_users = ''

    for user in users:
        if set_password:
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = default_token_generator.make_token(user)
            subject = _('Set password on Passages')
            message = [
                _('invites you to set your password for Passages, using the following link '
                  'which is valid for 72 hours (3 days).'),
                _('Please make sure to use email: "{}" for login.').format(user.email),
                request.POST.get('message'),
            ]
            link = reverse('password_reset_confirm', args=[uid, token])
        else:
            subject = _('A message about Passages')
            message = [
                _('sends you the following message:'),
                request.POST.get('message'),
            ]
            link = None

        user.send_mail(request, subject, message, link)
        invited_users += f'{user} ({user.email})\n'

    response = {
        'success': True,
        'users': invited_users,
    }

    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )


@login_required
def set_reviewer_consent(request):
    """Set consent status of teacher for student specified

    Args:
        request: (HttpRequest object)
            POST.applicant_id (int)
            POST.consent (int)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    applicant_id = request.POST.get('applicant_id')
    consent = request.POST.get('consent')

    applicant = User.objects.get(id=applicant_id)
    applicant.reviewer_consent = consent == "true"
    applicant.save(update_fields=['reviewer_consent'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def set_personal_consent(request):
    """Set consent status of student

    Args:
        request: (HttpRequest object)
            POST.applicant_id (int)
            POST.consent (int)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    applicant_id = request.POST.get('applicant_id')
    consent = request.POST.get('consent')

    applicant = User.objects.get(id=applicant_id)
    if consent:
        applicant.personal_consent = applicant.group.semester
    else:
        applicant.personal_consent = None
    applicant.save(update_fields=['personal_consent'])

    # notify student's reviewer that he has consented
    reviewer: User = applicant.reviewer
    if reviewer:
        subject = _('Student "%(first)s %(last)s" %(consent_status)s.' % {
            'first': applicant.first_name,
            'last': applicant.last_name,
            'consent_status':
                _("has consented") if applicant.personal_consent
                else _("has not consented")
        })
        message = [subject]
        link = ''
        reviewer.send_mail(request, subject, message, link)

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def set_expert_personal_consent(request):
    """Set consent status of expert after reading training capsules

    Args:
        request: (HttpRequest object)
            POST.expert_id (int)
            POST.consent (int)

    Returns:
        HttpResponse (application/json)

    """
    if request.method != 'POST':
        raise ValueError(request)

    expert_id = request.POST.get('expert_id')
    consent = request.POST.get('consent')

    semester = Semester.available_objects.all().first()
    expert = User.objects.get(id=expert_id)
    if consent:
        expert.personal_consent = semester
    else:
        expert.personal_consent = None
    expert.save(update_fields=['personal_consent'])

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def view_expert_training(request):
    """Show training capsules (expert)"""
    response = HttpResponse()

    # exclude 'applicant' capsules or any 'draft' or 'archived' capsule
    capsules = (TrainingCapsule.available_objects
                .filter(user_type=UserType.EXPERT)
                .exclude(status__in=('Draft', 'Archived'))
                .all())

    context = {
        'capsules': capsules,
        'target': 'expert-training',
    }

    template = loader.get_template('user/expert-training.html')
    response.content = template.render(context, request)

    return response


@login_required
def view_applicant_training(request):
    """Show training capsules (applicant)"""
    response = HttpResponse()

    # Get portfolio object
    portfolio = active_portfolio(request)

    # exclude 'applicant' capsules or any 'draft' or 'archived' capsule
    capsules = (TrainingCapsule.available_objects
                .filter(user_type=UserType.APPLICANT)
                .exclude(status__in=('Draft', 'Archived'))
                .all())

    context = {
        'training_active': True,
        'help': get_help_link('applicant_training'),
        'portfolio': portfolio,
        'capsules': capsules
    }

    template = loader.get_template('user/applicant-training.html')
    response.content = template.render(context, request)

    return response


@login_required
def view_admin(request):
    """Show admin links (supervisor)"""

    if request.user.type not in ['Agent', 'Supervisor']:
        raise ValueError(request)

    response = HttpResponse()

    # exclude 'applicant' capsules or any 'draft' or 'archived' capsule
    capsules = (
        TrainingCapsule.available_objects.filter(
            user_type='Supervisor'
        ).exclude(
            status__in=('Draft', 'Archived')
        ).all()
    )

    context = {
        'capsules': capsules
    }

    template = loader.get_template('user/admin.html')
    response.content = template.render(context, request)

    return response
