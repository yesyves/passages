#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.contrib import admin
from django.contrib.admin import display
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User, Semester, Group, Attribution, ProgramExpert, GroupReviewer, TrainingCapsule, TrainingLink


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_filter = ('type', 'group')
    list_display = ['id', 'email', 'username', 'get_user_type', 'group',
                    'personal_consent', 'reviewer_consent', 'expert_consent',
                    'code_permanent', 'nb_credits']
    fieldsets = UserAdmin.fieldsets + (
        ('Role', {'fields': ('type',)}),
        ('Information', {
            'fields': (
                'code_permanent',
                # 'semester',
                'group',
                'reviewer',
                'expert',
                'nb_credits',
                'personal_consent',
                'reviewer_consent',
                'expert_consent'
            )
        }),
        ('PGM Information', {
            'fields': (
                'concentration',
                'establishment',
                'is_teacher'
            )
        }),
    )

    def get_user_type(self, instance):
        return instance.type
    get_user_type.short_description = 'User Type'


class GroupReviewerAdmin(admin.ModelAdmin):
    list_display = ('reviewer','group',)
    list_filter = ('group',)
    search_fields = ['reviewer__first_name', 'reviewer__last_name', 'reviewer__email', 'reviewer__username']


class ProgramExpertAdmin(admin.ModelAdmin):
    search_fields = ['program__name', 'program__id', 'expert__first_name', 'expert__last_name', 'expert__email', 'expert__username']


class SemesterAdmin(admin.ModelAdmin):
    list_display = ('nice_name', 'id', 'year', 'status')
    list_filter = ('year',)
    search_fields = ['supervisor__first_name', 'supervisor__last_name', 'supervisor__email', 'supervisor__username']

    @staticmethod
    def nice_name(instance):
        return instance.__str__()


class GroupAdmin(admin.ModelAdmin):
    list_display = ('group', 'id', 'code', 'semester', 'milestone', 'assignment', 'supervisor')
    list_filter = ('semester',)
    search_fields = ['']

    @display(ordering='', description='Group')
    def group(self, obj: Group):
        return "{}, Group {}".format(obj.semester.__str__(), obj.code)


class AttributionAdmin(admin.ModelAdmin):
    search_fields = ['applicant__first_name', 'applicant__last_name', 'applicant__email', 'applicant__username', 'expert']


class TrainingLinkAdmin(admin.ModelAdmin):
    list_display = ['short_link', 'id', 'is_video']
    list_filter = ('is_video',)

    @display(ordering='', description='URL (short)')
    def short_link(self, obj: TrainingLink):
        short_link = obj.url[8:75] if obj.url.startswith('https://') else obj.url[:75]
        xellipsis = "" if obj.url.endswith(short_link) else "..."
        return f"{short_link}{xellipsis}"


class TrainingCapsuleAdmin(admin.ModelAdmin):
    list_display = ['title', 'short_intro', 'user_type', 'links_count', 'status']
    list_filter = ('user_type', 'status')

    @display(ordering='', description='Intro (short)')
    def short_intro(self, obj: TrainingCapsule):
        return "n/a" if not obj.intro or obj.intro.isspace() else obj.intro[:75] + " ..."

    @display(ordering='', description='Links (count)')
    def links_count(self, obj: TrainingCapsule):
        return obj.links.count()


admin.site.register(User, CustomUserAdmin,)
admin.site.register(GroupReviewer, GroupReviewerAdmin,)
admin.site.register(ProgramExpert, ProgramExpertAdmin,)
admin.site.register(Semester, SemesterAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Attribution, AttributionAdmin)
admin.site.register(TrainingLink, TrainingLinkAdmin)
admin.site.register(TrainingCapsule, TrainingCapsuleAdmin)
