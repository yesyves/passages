#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.urls import path

from config.settings.common import IS_CONTINUOUS
from . import views

urlpatterns = [
    path('add_group', views.add_group,
         name='add_group'),
    path('add_experts', views.add_experts,
         name='add_experts'),
    # training capsules (applicant)
    path('applicant-training', views.view_applicant_training,
         name='view_applicant_training'),
    path('edit_profile', views.edit_profile,
         name='edit_profile'),
    # training capsules (expert)
    path('expert_training', views.view_expert_training,
         name='view_expert_training'),
    path('group_notify', views.group_notify,
         name='group_notify'),
    path('pick_semester', views.pick_semester_continuous if IS_CONTINUOUS else views.pick_semester_milestone,
         name='pick_semester'),
    path('remove_portfolio', views.remove_portfolio,
         name='remove_portfolio'),
    path('send_attribution', views.send_attribution,
         name='send_attribution'),
    path('set_expert', views.set_expert,
         name='set_expert'),
    path('set_reviewer', views.set_reviewer,
         name='set_reviewer'),
    path('set_target_credits', views.set_target_credits,
         name='set_target_credits'),
    path('view_admin', views.view_admin,
         name='view_admin'),
    path('attribution/<int:group_id>', views.group_attribution,
         name='group_attribution'),
    path('remove_user/<int:user_id>', views.remove_user,
         name='remove_user'),
    # mode: milestone
    path('switch_to_revision_mode/<int:group_id>', views.toggle_revision_mode,
         name='toggle_revision_mode'),
    path('set_reviewer_consent', views.set_reviewer_consent,
         name='set_reviewer_consent'),
    path('set_personal_consent', views.set_personal_consent,
         name='set_personal_consent'),
    path('set_expert_personal_consent', views.set_expert_personal_consent,
         name='set_expert_personal_consent'),
]
