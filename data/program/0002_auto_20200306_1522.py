#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.contrib.auth.hashers import make_password
from django.db import migrations


"""
Migration for admin user
"""


def create_admin(apps, schema_editor):
    """
    Create admin user : for sandbox legacy portfolio
                      : with temporary password for production
    :param apps:
    :param schema_editor:
    :return:

    """
    # We can't import the User model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    User = apps.get_registered_model('user', 'User')

    db_alias = schema_editor.connection.alias

    User.objects.using(db_alias).create(
        username='',
        email='admin@epassages.ca',
        password=make_password('PassagesDemo'),
        first_name='Yves',
        last_name='de Champlain',
        is_superuser=True,
        is_staff=True,
        user_type=User.UserType.SUPERVISOR
    )


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0001_initial'),
    ]

    # Migrations mostly for Sandbox
    # create_admin can be used with temporary password
    operations = [
        migrations.RunPython(create_admin),
    ]
