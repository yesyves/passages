#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import json
import re

from os import path

from django.db import migrations


"""
Migration for programs
"""


def create_types(apps, schema_editor):
    """
    Add Concentrations
    """

    concentrations = []
    Concentration = apps.get_model('program', 'Concentration')
    db_alias = schema_editor.connection.alias

    # Create Type 'Formation professionnelle'
    concentrations.append(Concentration(id=0,
                                         type='P',
                                         max_credits=27,
                                         max_num_achievements=6))

    # Create Type 'Formation technique'
    concentrations.append(Concentration(id=1,
                                         type='T',
                                         max_credits=15,
                                         max_num_achievements=4))

    Concentration.objects.using(db_alias).bulk_create(concentrations)


def create_dictionaries(apps, schema_editor):
    """
    Add initial data dictionaries for programs/competencies
    :param apps:
    :param schema_editor:
    :return:

    https://docs.djangoproject.com/fr/2.1/topics/migrations/#data-migrations
    https://docs.djangoproject.com/fr/2.1/ref/migration-operations/#django.db.migrations.operations.RunPython
    https://docs.djangoproject.com/fr/2.1/ref/models/querysets/#bulk-create
    """
    programs = []
    competencies = []

    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Concentration = apps.get_registered_model('program', 'Concentration')
    Program = apps.get_model('program', 'Program')
    Competency = apps.get_model('program', 'Competency')
    db_alias = schema_editor.connection.alias

    professionnel = Concentration.objects.using(db_alias).get(type='P')
    technique = Concentration.objects.using(db_alias).get(type='T')

    # Open 2017-11 competencies database
    base_path = path.dirname(__file__)
    file_path = path.abspath(path.join(base_path, '', 'bottin_programmes.json'))
    with open(file_path) as file_handler:
        data = json.load(file_handler)

    for item in data:
        code = item['code']
        # DEBUG:
        # print(code)
        if re.fullmatch('[0-9]{4}', code) is not None:
            type = professionnel.id
        elif re.fullmatch(r'[0-9]{3}\.[A-Z]0', code) is not None:
            type = technique.id
        else:
            print(f'Concentration inconnu !!!! {code}')
            continue
        programs.append(Program(id=code,
                                name=item['name'],
                                version=item['version'],
                                type_id=type))
        for competency in item['competencies']:
            # DEBUG:
            # print(competency['id'])
            competencies.append(Competency(id=f"{item['code']}{competency['id']}",
                                           code=int(competency['id']),
                                           is_key=competency['key'],
                                           name=competency['competency'].replace(u"'", u"’"),
                                           program_id=code))

    Program.objects.using(db_alias).bulk_create(programs)
    Competency.objects.using(db_alias).bulk_create(competencies)


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.RunPython(create_types),
        migrations.RunPython(create_dictionaries),
    ]
