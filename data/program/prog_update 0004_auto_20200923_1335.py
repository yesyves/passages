#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import json

from os import path

from django.db import migrations


"""
Migration to update a program
"""


def update_program(apps):
    """
    Add initial data dictionaries for programs/competencies
    :param apps:
    :return:

    https://docs.djangoproject.com/fr/2.1/topics/migrations/#data-migrations
    https://docs.djangoproject.com/fr/2.1/ref/migration-operations/#django.db.migrations.operations.RunPython
    https://docs.djangoproject.com/fr/2.1/ref/models/querysets/#bulk-create
    """

    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    competency_model = apps.get_model('program', 'Competency')

    # Open 2017-11 competencies database
    base_path = path.dirname(__file__)
    file_path = path.abspath(path.join(base_path, '', 'bottin_add.json'))
    with open(file_path) as file_handler:
        data = json.load(file_handler)

    for item in data:
        for competency in item['competencies']:
            # DEBUG:
            competency_id = f"{item['code']}{competency['id']}"
            this_comp = competency_model.objects.get(id=competency_id)
            this_comp.name = competency['competency'].replace(u"'", u"’")
            print(this_comp)
            this_comp.save()


class Migration(migrations.Migration):
    dependencies = [
        ('data', '0003_auto_20200329_1012'),
    ]

    operations = [
        migrations.RunPython(update_program),
    ]
