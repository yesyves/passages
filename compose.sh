#!/usr/bin/env bash

########################################################################################
# If using custom paths for Yaml config & env files, this shortcut script wraps the
# default `docker compose` commands by automatically adding the proper paths to avoid
# having to manually typing those paths every time or else having to create a script for
# each `docker compose` sub-command (e.g. logs -f, up, down, etc.).
#
# Examples of main `docker compose` commands we may need for this application:
#   - ./compose.sh logs -f
#   - ./compose.sh down
#   - ./compose.sh pull
#   - ./compose.sh up -d --build
#   - ./compose.sh exec django /bin/bash
########################################################################################

# Startup Docker swarm using YAML config, which uses ./Dockerfile;
# file paths are all relative to root of 'passages' folder
X_YAML_CFG="-f docker-compose.yml"
X_ENV_FILE="--env-file .env"

# shortcut for `docker compose` w/ custom Yaml & env file pre-set;
# propagate all user command line arguments directly to `docker compose`
docker compose $X_YAML_CFG $X_ENV_FILE "$@"