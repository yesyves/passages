from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Program


class FormAddProgram(forms.Form):
    program_code = forms.CharField(
        label=_("Program code"),
    )
    program_name = forms.CharField(
        label=_("Program name"),
    )
    version = forms.IntegerField(
        label=_("Year of edition"),
    )
    url = forms.URLField(
        required=False,
        label=_('Program URL'),
        help_text=_('URL in infoRouteFPT (leave blank for vocational training programs)'),
    )
    competencies = forms.FileField(
        label=_("List of competencies for this program"),
        help_text=_('Upload CSV file (encoded in UTF-8 format) with ordered fields:'
                    'Skill number, Skill name, key_skill identifier'),
    )
    deprecated_program = forms.ChoiceField(
        required=False,
        label=_('Program replaced by the new one'),
        choices=(),
    )

    def __init__(self, *args, **kwargs):
        super(FormAddProgram, self).__init__(*args, **kwargs)

        # create list of all available programs given current user's concentration
        programs = (('NONE', _('Choose program')),)
        for program in Program.available_objects.filter(is_active="True"):
            option = ((program.id, program),)
            programs = programs + option

        self.fields['deprecated_program'].choices = programs
