#  © Copyright Yves de Champlain et al. 2018-2023
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import csv
import re

from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.mail import mail_admins
from django.shortcuts import render
from django.utils.translation import gettext as _

from .forms import FormAddProgram
from .models import Concentration, Program, Competency


@login_required
def add_program(request):
    """

    Args:
        request:

    Returns:

    """
    if not request.user.is_superuser:
        raise ValueError(request.user)

    if request.method == 'POST':

        form = FormAddProgram(request.POST, request.FILES)

        if form.is_valid():
            program_code = form.cleaned_data['program_code']
            program_name = form.cleaned_data['program_name']
            version = form.cleaned_data['version']
            url = form.cleaned_data['url']
            deprecated_program_id = form.cleaned_data['deprecated_program']

            results = []

            concentrations = Concentration.available_objects.all()
            valid_concentrations = {}
            for concentration in concentrations:
                valid_concentrations[concentration.type] = concentration

            if re.fullmatch('[0-9]{4}', program_code) is not None:
                concentration = valid_concentrations['P']
                url = ''
            elif re.fullmatch(r'[0-9]{3}\.[A-Z][0-9]', program_code) is not None:
                concentration = valid_concentrations['T']
            else:
                concentration = None

            program = Program(id=program_code,
                              name=program_name,
                              version=version,
                              concentration=concentration,
                              url=url)
            program.save()
            results.append(program)

            csvfile = request.FILES["competencies"].read().decode('utf-8-sig')

            fieldnames = ['competency_code', 'competency_name', 'is_key']
            reader = csv.DictReader(csvfile.splitlines(), fieldnames=fieldnames, delimiter=';')

            for row in reader:
                # DEBUG:
                print(row)

                try:
                    competency_code = int(row['competency_code'])
                except ValueError:
                    continue

                if row['is_key'] == 'clé':
                    is_key = True
                else:
                    is_key = False

                competency = Competency(id=f"{program_code}{competency_code}",
                                        code=competency_code,
                                        is_key=is_key,
                                        name=row['competency_name'].replace(u"'", u"’"),
                                        program=program)
                competency.save()
                results.append(competency)

            mail_admins(f'New program added: {program}',
                        serializers.serialize('json', results))

            if deprecated_program_id != 'NONE':
                deprecated_program = Program.available_objects.get(id=deprecated_program_id)
                deprecated_program.is_active = False
                deprecated_program.save()

            message = {
                0: _('Program successfully created:'),
                1: program,
                2: '',
                3: '',
            }

            return render(request, 'portfolio/restrict.html', {'message': message, 'target': 'add_program'})

    else:
        form = FormAddProgram()

    return render(request, "program/add_program.html", {"form": form})
