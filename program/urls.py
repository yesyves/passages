#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.urls import path

from config.settings.common import IS_CONTINUOUS
from . import views

urlpatterns = [
    path('add_program', views.add_program,
         name='add_program'),
]
