# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

from django.contrib.postgres.fields import ArrayField
from django.core.mail import send_mail
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from config.settings.common import ADMINS
from utils.models import MetaData


class Concentration(MetaData):
    """
    Define the choices of programs CONCENTRATIONS

    Each program defines a credits attribution scale
    available through get_credits_scale()

    id attribute is int for easy semester data migrations
    """
    CONCENTRATIONS = (
        ('P', _('Vocational training')),
        ('T', _('Technical training')),
        ('E', _('K12 Teacher')),
    )
    id = models.IntegerField(
        primary_key=True,
    )
    code = models.CharField(
        default='',
        blank=True,
        verbose_name=_('Concentration code')
    )
    type = models.CharField(
        choices=CONCENTRATIONS,
        max_length=1,
        default='P',
        verbose_name=_('Concentration'),
    )
    code = models.CharField(
        default='',
        verbose_name=_('Concentration code'),
    )
    max_credits = models.IntegerField(
        null=True,
        verbose_name=_('Maximum number of credits'),
        default=27,
        help_text=_('Maximum number of credits that can be attributed'),
    )
    max_num_achievements = models.IntegerField(
        null=True,
        verbose_name=_('Maximum number of achievements'),
        default=6,
        help_text=_('Maximum number of achievements that can be submitted for evaluation'),
    )

    class Meta:
        ordering = ['type']
        verbose_name = _('Concentration')
        verbose_name_plural = _('Concentrations')

    def __str__(self):
        display_code = ''
        if self.code:
            display_code = ' (' + self.code + ')'
        return self.get_type_display() + display_code

    def get_milestones(self):
        """
        Returns:
        """
        if self.type == 'E':
            return {
                1: _('1st year of teacher training completed'),
                2: _('2nd year of teacher training completed'),
                3: _('3rd year of teacher training completed'),
                4: _('Licence obtained'),
                5: _('5 years of professional practice'),
                10: _('10 years of professional practice'),
            }
        else:
            return None

    def get_credits_scale(self):
        """
        Returns the credits attribution scale for the concentration

        Each step consists of a minimum and a maximum threshold
            as well as the number of credits associated

        return scale:[dict]
        """
        # Get number of credits for concentration
        if self.type == 'P':
            scale = [{'min': 210, 'max': 300, 'credits': 27},   # 6 x Complete Level 2
                     {'min': 185, 'max': 209, 'credits': 24},
                     {'min': 160, 'max': 184, 'credits': 21},
                     {'min': 135, 'max': 159, 'credits': 18},
                     {'min': 110, 'max': 134, 'credits': 15},   # 6 x Complete Level 1 = 120
                     {'min': 85, 'max': 109, 'credits': 12},
                     {'min': 60, 'max': 84, 'credits': 9},
                     {'min': 35, 'max': 59, 'credits': 6},
                     {'min': 10, 'max': 34, 'credits': 3},      # Step = 25
                     {'min': 0, 'max': 34, 'credits': 0}]       # Step = 10
        elif self.type == 'T':
            scale = [{'min': 140, 'max': 200, 'credits': 15},   # 4 x Complete Level 2
                     {'min': 115, 'max': 139, 'credits': 12},
                     {'min': 90, 'max': 114, 'credits': 9},
                     {'min': 65, 'max': 89, 'credits': 6},      # 4 x Complete Level 1 = 80
                     {'min': 40, 'max': 64, 'credits': 3},      # Step = 25
                     {'min': 0, 'max': 39, 'credits': 0}]       # Step = 40
        elif self.type == 'E':
            scale = [{'min': 360, 'max': 400, 'credits': 15, 'milestone': _('15 years of professional practice')},      # 8 x 2/3 Level 3
                     {'min': 320, 'max': 359, 'credits': 10, 'milestone': _('10 years of professional practice')},
                     {'min': 280, 'max': 319, 'credits': 5, 'milestone': _('5 years of professional practice')},        # 8 x Complete Level 2
                     {'min': 240, 'max': 279, 'credits': 4, 'milestone': _('Licence obtained')},                        # No practical experience
                     {'min': 200, 'max': 239, 'credits': 3, 'milestone': _('3rd year of teacher training completed')},  # Step = 40
                     {'min': 160, 'max': 199, 'credits': 2, 'milestone': _('2nd year of teacher training completed')},  # 8 x Complete Level 1
                     {'min': 80, 'max': 159, 'credits': 1, 'milestone': _('1st year of teacher training completed')},   # 4 x Complete Level 1
                     {'min': 0, 'max': 79, 'credits': 0, 'milestone': _('No milestone attained')}]                      # Step = 80
        else:
            scale = [{'min': 0, 'max': 0, 'credits': 0}]

        return scale

    @staticmethod
    def get_expertise_levels():
        """
        Return expertise levels

        Levels consists of a dictionary with each level as key
            and default value None

        return expertise_levels:dict
        """
        return {'A': None, 'B': None, 'C': None, 'D': None, 'E': None,
                'F': None, 'G': None, 'H': None, 'I': None, 'J': None}

    @staticmethod
    def get_level_2():
        """
        Return expertise levels 2

        Levels consist of a dictionary with each level as key
            and description

        return level_2:dict
        """
        return {
            'M': _('Develop new method or tool'),
            'N': _('Create a new product'),
            'O': _('Solve a complex situation'),
            'P': _('Transfer knowledge'),
            'Q': _('Participate in planning, logistics'),
            'R': _('Introduce new resources'),
            'S': _('Teach complex or subtle aspects'),
        }


class Program(MetaData):
    """
    Define a program / curriculum

    id:str          to replicate official program codes, ex: 571.A0
    is_active:bool  to keep deprecated programs in DB for old portfolios
                    while not showing them to new users
    """
    id = models.CharField(
        max_length=32,
        primary_key=True,
        verbose_name=_('Program code'),
    )
    name = models.TextField(
        verbose_name=_('Program name'),
    )
    version = models.CharField(
        max_length=32,
        verbose_name=_('Program version'),
    )
    url = models.TextField(
        default='',
        blank=True,
        verbose_name=_('URL'),
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name=_('Program is currently in use'),
    )
    concentration = models.ForeignKey(
        Concentration,
        verbose_name=_('Concentration'),
        on_delete=models.PROTECT,
    )
    # groups = ["category0", "category1"]
    groups = ArrayField(
        models.TextField(),
        null=True,
        blank=True,
        verbose_name=_('Competency groups')
    )

    class Meta:
        ordering = ['name', '-version']
        verbose_name = _('Program')
        verbose_name_plural = _('Programs')

    def __str__(self):
        return f'{self.name} #{self.id} ({self.version})'

    def get_url(self):
        """
        Return current url or compute one based on program ID
        :return: url:str
        """
        url = self.url
        if not url:
            url = 'https://www.inforoutefpt.org/'
            if self.concentration.type == 'P':
                url += f'progSecDet.aspx?sanction=5&prog={self.id}'
            else:
                send_mail(
                    _('Program URL missing'),
                    str(self),
                    None,
                    ADMINS,
                    fail_silently=False,
                )

        return url


class Competency(MetaData):
    """
    Define a program competency / skill

    id:str  Competency ID is a concatenation of Program ID + Competency code'
    """
    id = models.CharField(
        max_length=32,
        primary_key=True,
        verbose_name=_('Competency ID'),
    )
    code = models.IntegerField(
        default=0,
        verbose_name=_('Skill number'),
    )
    is_key = models.BooleanField(
        default=False,
        verbose_name=_('Key skill'),
        help_text=_('Is marked as key competency for program'),
    )
    name = models.TextField(
        verbose_name=_('Skill name'),
    )
    description = models.TextField(
        verbose_name=_('Description'),
        blank=True,
        null=False,     # todo better to use null=False
        default='',
    )
    parent = models.ForeignKey(
        'self',
        verbose_name=_('Parent competency'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    program = models.ForeignKey(
        Program,
        verbose_name=_('Program'),
        blank=False,
        on_delete=models.CASCADE,
    )
    # category = program.groups[group]
    group = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('Competency Group'),
    )

    class Meta:
        ordering = ['program_id', 'code']
        verbose_name = _('Competency')
        verbose_name_plural = _('Competencies')

    def __str__(self):
        return f'{self.program.id} | {self.id} - {self.code}. {self.name}'


class CustomCompetencyType(MetaData):
    """
    Define a program custom competency type / category

    id:str  Competency ID is a concatenation of Program ID + Competency code'
    """
    code = models.CharField(
        default=0,
        verbose_name=_('Type code'),
        help_text=_('Only lowercase letters and underscore are allowed. ONCE SET, DO NOT CHANGE.'),
        max_length=255,
        validators=[RegexValidator(
            r'^[a-z_]*$', _('Only lowercase letters and underscore are allowed')
        )]
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_('Type name'),
    )
    url = models.CharField(
        max_length=255,
        verbose_name=_('Link to resource'),
        default=''
    )
    desc = models.CharField(
        max_length=255,
        verbose_name=_('Description'),
        blank=True,
        default=''
    )
    # TODO mode=milestone-pilote; hard-coded, but in next iteration find way to automatically provide choices from SVG
    icon_name = models.CharField(
        max_length=255,
        verbose_name=_('Icon name'),
        default='etiquette-glossaire',
        choices=[
            ('savoir', 'Habiletés'),
            ('savoir-etre', 'Savoir-être'),
            ('zone-explicitation', 'Profil d\'entrée'),
        ]
    )
    # always link to program since user -> portfolio -> program, and PortfolioType NOT linked to program
    program = models.ForeignKey(
        Program,
        verbose_name=_('Program'),
        blank=True, # optional since competency type may be global across similar programs
        null=True,
        on_delete=models.CASCADE,
    )
    # TODO Should probably be removed ? Because 142.H0 doesn't include program competencies anymore
    private = models.BooleanField(
        verbose_name=_('For internal use only?'),
        help_text=_('Will not be automatically shown to the user in the leftmenu.'),
        default=False,
    )

    # TODO (M) ? should be usable by both Continuous & Milestone modes
    class Meta:
        ordering = ['program_id', 'id']
        verbose_name = _('Competency Type')
        verbose_name_plural = _('Competency Types')

    def __str__(self):
        program_id = "n/a" if not self.program else self.program.id
        return f'{program_id} | {self.id}. {self.name}'


class CustomCompetency(MetaData):
    """
    Define a program custom competency / skill

    id:str  Competency ID is a concatenation of Program ID + Competency code'
    """
    id = models.CharField(
        max_length=32,
        primary_key=True,
        verbose_name=_('Competency ID'),
        help_text=_('ex : type.parent.competency - 1.2.1')
    )
    code = models.IntegerField(
        default=0,
        verbose_name=_('Competency number'),
    )
    is_key = models.BooleanField(
        default=False,
        verbose_name=_('Key competency'),
        help_text=_('Is marked as key competency for program'),
    )
    name = models.TextField(
        verbose_name=_('Competency name'),
    )
    parent = models.ForeignKey(
        'self',
        verbose_name=_('Parent competency'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    type = models.ForeignKey(
        CustomCompetencyType,
        verbose_name=_('Competency type'),
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    # used in cases where custom property 'has_children' cannot be used (e.g. QuillJS facets-tooltip.js)
    child_count = models.IntegerField(
        default=0,
        verbose_name=_('Number of children'),
        help_text=_('How many child competencies are linked to this competency via their "parent" property.')
    )

    class Meta:
        ordering = ['id', 'code']
        verbose_name = _('Custom Competency')
        verbose_name_plural = _('Custom Competencies')

    # update 'child_count' whenever we save a competency
    def save(self, *args, **kwargs):
        print(f"CustomCompetency.save -> {self.id} - {self.name}")
        self.child_count = self.children().count()
        super(CustomCompetency, self).save(*args, **kwargs)

        # update parent when child changes to update child count in both directions;
        # always do this after current object 'super' to make sure everything is persisted to db
        if self.parent:
            parent = CustomCompetency.available_objects.get(id=self.parent_id)
            parent.child_count = parent.children().count()
            parent.save()

    def __str__(self):
        # type_name = f'{self.type.id}. {self.type.name}'
        type_name = f'{self.type.name}'
        # return f'{self.type.program_id} | {type_name} | {self.id}. {self.name}'
        return f'{type_name} | {self.id}. {self.name}'

    # helper to easily get child competencies without having to do complex looping everywhere we use nested competencies
    def children(self):
        return CustomCompetency.available_objects.filter(parent_id=self.id)

    # custom property telling whether this skill has child competencies (especially useful w/ 3 levels deep)
    @property
    def has_children(self):
        return self.children().count() > 0

