#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.test import TestCase

from .models import Concentration, Program, Competency


#
# models
#

class ConcentrationTest(TestCase):

    def create_concentration(self, type):
        return Concentration.available_objects.create(type=type)

    def test_concentration(self):
        pt = self.create_concentration('P')
        self.assertIsInstance(pt, Concentration)
        self.assertEqual(pt.__str__(), pt.get_type_display())
        self.assertEqual(pt.type, 'P')
        self.assertEqual(pt.max_credits, 27)
        self.assertEqual(pt.max_num_achievements, 6)
        self.assertIsInstance(pt.get_credits_scale(), list)
        self.assertIsInstance(pt.get_expertise_levels(), dict)

        ptt = self.create_concentration('T')
        self.assertEqual(ptt.type, 'T')
        self.assertIsInstance(ptt.get_credits_scale(), list)

        ptx = self.create_concentration('X')
        self.assertEqual(ptx.type, 'X')
        self.assertIsInstance(ptx.get_credits_scale(), list)
        # with self.assertRaises(ValueError):
        #     ptx.get_credits_scale()


class ProgramTest(TestCase):

    def create_program(self, id='5220', name='Nom du programme', version='2020'):
        pt = Concentration.available_objects.create()
        return Program.available_objects.create(id=id, name=name, version=version, type_id=1)

    def test_program(self):
        p = self.create_program()
        self.assertIsInstance(p, Program)
        self.assertEqual(p.__str__(), p.name + ' #' + p.id + ' (' + p.version + ')')
        self.assertTrue(p.is_active)
        self.assertEqual(p.concentration.type, 'P')


class CompetencyTest(TestCase):

    def create_competency(self, id='5220', code='1', name='Nom de la competence'):
        pt = Concentration.available_objects.create()
        p = Program.available_objects.create(id='5220', name='Nom du programme', version='2020', type_id=1)
        return Competency.available_objects.create(id=id, code=code, name=name, program_id='5220')

    def test_competency(self):
        c = self.create_competency()
        self.assertIsInstance(c, Competency)
        self.assertEqual(c.__str__(), str(c.id) + ' | ' + str(c.code) + '. ' + c.name)
        self.assertFalse(c.is_key)
        self.assertEqual(c.program.concentration.type, 'P')
