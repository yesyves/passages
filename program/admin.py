#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from .models import Concentration, Program, Competency, CustomCompetency, CustomCompetencyType
from django.apps import AppConfig
from django.contrib import admin


class CoreConfig(AppConfig):
    name = 'program'


class ProgramAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'concentration', 'version', 'is_active']
    search_fields = ['name', 'id']


class CompetencyAdmin(admin.ModelAdmin):
    search_fields = ['program__name', 'program__id']


class CustomCompetencyTypeAdmin(admin.ModelAdmin):
    list_display = ['program_id', 'id', 'name', 'code', 'private']
    # search_fields = ['program__name', 'program__id']


class CustomCompetencyAdmin(admin.ModelAdmin):
    list_display = ['type', 'id', 'name', 'child_count', 'has_children']
    search_fields = ['name', 'id']
    list_filter = ('type',)


admin.site.register(Concentration)
admin.site.register(Program, ProgramAdmin)
admin.site.register(Competency, CompetencyAdmin)
admin.site.register(CustomCompetencyType, CustomCompetencyTypeAdmin)
admin.site.register(CustomCompetency, CustomCompetencyAdmin)
