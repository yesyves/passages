#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import json
import re

from os import getcwd, path

from django.core.management.base import BaseCommand

from program.models import Competency, Program, Concentration


class Command(BaseCommand):
    help = 'Add new programs from data/add_programs.json'

    def add_arguments(self, parser):
        parser.add_argument("program_codes", nargs="+", type=str)

    def handle(self, *args, **options):

        professionnel = Concentration.available_objects.get(type='P')
        technique = Concentration.available_objects.get(type='T')

        for program_code in options['program_codes']:
            file_path = path.join(getcwd(), 'data', f'{program_code}.json')
            with open(file_path) as file_handler:
                data = json.load(file_handler)

            for item in data:
                code = item['code']
                # DEBUG:
                # print(code)
                if re.fullmatch('[0-9]{4}', code) is not None:
                    concentration = professionnel.id
                elif re.fullmatch(r'[0-9]{3}\.[A-Z][01]', code) is not None:
                    concentration = technique.id
                else:
                    print('Concentration inconnu !!!! ' + code)
                    continue

                print(f"\n\nAdd program {code} {item['name']}")
                program = Program(id=code,
                                  name=item['name'],
                                  version=item['version'],
                                  concentration_id=concentration)
                program.save()

                print('Add competencies :')
                for competency in item['competencies']:
                    # DEBUG:
                    # print(competency['id'])
                    print(f"\t{competency['id']} {competency['competency']}")
                    competency = Competency(id=f"{item['code']}{competency['id']}",
                                            code=int(competency['id']),
                                            is_key=competency['key'],
                                            name=competency['competency'].replace(u"'", u"’"),
                                            program_id=code)
                    competency.save()
