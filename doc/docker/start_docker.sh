#!/bin/bash

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

echo "Waiting for postgres..."

while ! nc -z $DB_HOST $DB_PORT; do
    sleep 0.1
done

echo "PostgreSQL started"

cd /web
echo "Making migrations"
python manage.py migrate
echo "Starting gunicorn"
gunicorn config.wsgi:application --workers=4 --bind 0.0.0.0:8000
