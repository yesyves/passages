# Docker documentation (old)

> UPDATE 2023-08-01: Cette version Docker n'est plus à jour et ne sera plus utilisée. Dorénavant, il y aura deux versions Docker pour Passages: DEV et PROD. 
>  
> **DEV**
>   - emplacement: `./Dockerfile` combiné avec `./docker-compose.yml`. 
> 
> Il s'agit d'un setup Docker Compose très simple permettant aisément de rouler Passages pour du développement local.
> 
> **PROD**
>   - emplacement: https://bitbucket.org/yesyves/docker.git (répertoire Git dédié)
> 
> Il s'agit d'un installateur Docker 100% autonome et prêt pour la production.


### Older documentation

Cette version docker de Passages utilise des conteneurs construits a partir de trois images docker:

 - passages_nginx : Serveur web nginx qui transfère les requêtes à gunicorn qui roule dans le conteneur DJANGO
 - passages_web   : service gunicorn fournissant l'application django PASSAGES
 - postgres:11.5-alpine : Serveur postgresql configuré de telle sorte qu'il sauvegarde sa base de
                          données dans le répertoire /data/passages de la machine hôte assurant ainsi
                          la persistance des données après l'arrêt du conteneur postgres.


Étapes de mise en  place:

1 - Créer un répertoire /data/passages:
      sudo mkdir -p /data/passages

2 - Créer un volume docker:
      sudo docker volume create --driver local --opt type=none --opt device=/data/passages --opt o=bind passages_postgres_data

3 - Redémarrer le service docker:
      sudo systemctl restart docker      (sous LINUX)
      Redémarrer Docker.app              (sous MacOS)

4-  Modifier ( au besoin ) les fichiers :
      - start_docker.sh
      - docker-compose.yml
          pour y mettre les bons parametres de connexion BD

5 - Construire l'image :

    - Faire le ménage des migrations si nécessaire (voir devtools/reset_db.sh):
        find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete
        find . -path "*/migrations/*.pyc"  -delete
        python manage.py makemigrations

    - Produire les fichiers de traduction:
        ./devtools/translate.sh

    - Centraliser les fichiers statiques sous staticfiles :
        python manage.py collectstatic
        
    - Créer les trois images et lancer les conteneurs:
	      sudo docker-compose -f docker-compose.yml up -d --build

Pour arrêter les conteneurs :
  sudo docker-compose -f docker-compose.yml down

Pour redémarrer les conteneurs en mode daemon sans les reconstruire :
  sudo docker-compose -f docker-compose.yml up -d

Pour redémarrer les conteneurs sans les reconstruire et afficher leurs logs à l'écran :
  sudo docker-compose -f docker-compose.yml up

  Pour arrêter les conteneurs : CTL-C



Pour visualiser les attributs des différents volumes docker:

   sudo docker volume ls
    DRIVER              VOLUME NAME
    local               1ac6159a4301400f999b3ecf2024b33e1c71a7947b7f48101c5f839196d0b240
    local               216fdc6dc8a77e21191b7a1d19c443558b40548d462dc34f9a3ef77557936959
         ...
    local               passages_postgres_data
    local               passages_static_volume


  sudo docker volume inspect passages_postgres_data
   [
     {
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/passages_postgres_data/_data",
        "Name": "passages_postgres_data",
        "Options": {
            "device": "/data/passages",
            "o": "bind",
            "type": "none"
        },
        "Scope": "local"
    }
  ]

Pour vérifier que postgresql consrve bien ses données sous /data/passages:

sudo ls -al /data/passages/
total 60
drwx------ 19 avahi avahi  4096 11 sep 21:39 .
drwxr-xr-x  3 root  root     20 10 sep 07:19 ..
drwx------  5 avahi avahi    41 11 sep 21:34 base
drwx------  2 avahi avahi  4096 11 sep 21:39 global
drwx------  2 avahi avahi     6 11 sep 21:34 pg_commit_ts
drwx------  2 avahi avahi     6 11 sep 21:34 pg_dynshmem
-rw-------  1 avahi avahi  4535 11 sep 21:34 pg_hba.conf
-rw-------  1 avahi avahi  1636 11 sep 21:34 pg_ident.conf
drwx------  4 avahi avahi    68 11 sep 21:44 pg_logical
drwx------  4 avahi avahi    36 11 sep 21:34 pg_multixact
drwx------  2 avahi avahi    18 11 sep 21:39 pg_notify
drwx------  2 avahi avahi     6 11 sep 21:34 pg_replslot
drwx------  2 avahi avahi     6 11 sep 21:34 pg_serial
drwx------  2 avahi avahi     6 11 sep 21:34 pg_snapshots
drwx------  2 avahi avahi     6 11 sep 21:39 pg_stat
drwx------  2 avahi avahi    63 11 sep 22:02 pg_stat_tmp
drwx------  2 avahi avahi    18 11 sep 21:34 pg_subtrans
drwx------  2 avahi avahi     6 11 sep 21:34 pg_tblspc
drwx------  2 avahi avahi     6 11 sep 21:34 pg_twophase
-rw-------  1 avahi avahi     3 11 sep 21:34 PG_VERSION
drwx------  3 avahi avahi    60 11 sep 21:34 pg_wal
drwx------  2 avahi avahi    18 11 sep 21:34 pg_xact
-rw-------  1 avahi avahi    88 11 sep 21:34 postgresql.auto.conf
-rw-------  1 avahi avahi 23841 11 sep 21:34 postgresql.conf
-rw-------  1 avahi avahi    24 11 sep 21:39 postmaster.opts
-rw-------  1 avahi avahi    94 11 sep 21:39 postmaster.pid

N.B. Fonctionnement sous LINUX. Différent sous MacOS.
