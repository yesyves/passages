## Example of installing and setting the system environment with MacPorts

sudo port install py311-pip postgresql13-server pango graphviz
sudo port select --set python python311
sudo port select --set python3 python311
sudo port select --set pip pip311
sudo port select --set pip3 pip311
sudo port select --set postgresql postgresql13
sudo port load postgresql13-server


## Example of shell profile to set up the user environment with MacPorts.

export PATH=/opt/local/bin:/opt/local/sbin:$PATH

py_version=3.11

export PATH=$HOME/Library/Python/$py_version/bin:$PATH
source $HOME/Library/Python/$py_version/bin/virtualenvwrapper.sh

export CPPFLAGS=-I/opt/local/include
export CFLAGS=-I/opt/local/include
export LDFLAGS=-L/opt/local/lib

export EDITOR=nano
