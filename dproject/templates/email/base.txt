{% comment %}
  © Copyright Yves de Champlain et al. 2018-2022
    For more informations: https://doc.epassages.ca/index.php/contributions/

  SPDX-License-Identifier: LiLiQ-Rplus-1.1
  License-Filename: LICENCE
{% endcomment %}{% load i18n %}{% blocktranslate with to=to %}Dear {{ to }}{% endblocktranslate %}

{{ from }} {% for part in message %}{{ part }}

{% endfor %}
{% if link %}{% translate 'Follow the link:' %} {{ host }}{{ link }}{% endif %}

{{ host }}

{% translate 'Passages' %}
