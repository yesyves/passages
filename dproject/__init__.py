#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.utils.translation import gettext as _


__name__ = 'Passages'
__description__ = _('Recognition of experiential prior learning')

__author__ = 'de Champlain'
__authors__ = ['Yves de Champlain', 'Stéphane Lapointe', 'Abdoulaye Fall',
               'James Caveen', 'Brad Covey']

__copyright__ = 'Copyright © ' + __author__ + ' <i>et al.</i> 2018-2023'

__email__ = 'support@epassages.ca'
__licence__ = 'LiLiQ-R+ 1.1'
__status__ = 'Development'

__contact__ = __email__
__maintainer__ = __author__

__editor_version__ = '2.9.1'
__icons_version__ = '2.0'

__version__ = '2.9.1'
__version_info__ = tuple([int(num) if num.isdigit() else num for num in __version__.replace('-', '.', 1).split('.')])
