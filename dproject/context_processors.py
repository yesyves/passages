#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE
from django.template.defaulttags import url
from ms_identity_web import IdentityContextData

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE, USE_SSO, env
from user.models import TrainingCapsule
from utils.views import active_portfolio, active_portfolio_type
from . import (
    __contact__, __copyright__, __description__,
    __licence__, __name__,
    __editor_version__, __icons_version__, __version__,
)


def app_info(request):
    if USE_SSO:
        # whether there is an active AAD session (based on request context data)
        aad_context_data: IdentityContextData = request.identity_context_data
        is_aad_authenticated = aad_context_data.authenticated
    else:
        is_aad_authenticated = False

    admin_capsules = (
        TrainingCapsule.available_objects.filter(
            user_type='Supervisor'
        ).exclude(
            status__in=('Draft', 'Archived')
        ).all()
    )
    if admin_capsules:
        admin_link = 'view_admin'
    else:
        admin_link = 'admin:index'

    return {
        'app_contact': __contact__,
        'app_copyright': __copyright__,
        'app_description': __description__,
        'app_licence': __licence__,
        'app_name': __name__,
        'editor_version': __editor_version__,
        'icons_version': __icons_version__,
        'admin_link': admin_link,
        'app_version': __version__,
        # mode set for this server instance
        'is_continuous': bool(IS_CONTINUOUS),
        'is_milestone': bool(IS_MILESTONE),
        # program contact email
        'help_email': env("PROGRAM_EMAIL_TO", default=''),
        # home page
        'home_url': env("HOME_URL", default='/'),
        # institution logo & url
        'institution_logo': env("INSTITUTION_LOGO", default=''),
        'institution_url': env("INSTITUTION_URL", default=''),
        'logo_style': env("LOGO_STYLE", default=''),
        'logo_width': env("LOGO_WIDTH", default='180px'),
        # whether current user is authenticated via AAD
        'use_sso': USE_SSO,
        'is_aad_authenticated': is_aad_authenticated,
        # name of the logout view based on whether user is authenticated via AAD or Django
        'logout_view': 'aad_logout' if is_aad_authenticated else 'logout'
    }


# Make portfolio-specific data available in all Django templates (e.g. for use in left menu)
# e.g. works whether continuous vs milestone mode (portfolio), competency types, etc.
def portfolio_info(request):
    portfolio_type = active_portfolio_type()
    # competency types are attached to a portfolio; many-to-many relationship (QuerySet)
    competency_types = []
    if IS_MILESTONE:
        portfolio = active_portfolio(request)
        if portfolio is not None:
            competency_types = portfolio_type.competency_types.filter(private=False)

    return {
        'competency_types': competency_types,
        'portfolio_type': portfolio_type,
    }