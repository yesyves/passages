/**
 * Simple way of parsing Django boolean strings (True, False) as JS-friendly booleans.
 * Works well also if null or undefined is passed.
 * - https://www.freecodecamp.org/news/javascript-string-to-boolean/
 * @param {String} value
 */
export const xbool = (value) => {
    return (/true/i).test(value);
}