import {xbool} from "./utils.js";

export class PickPortfolio {
    constructor() {
        console.info(`esm.PickPortfolio`);
    }

    /**
     * Class initialization should always happen in dedicated init() function instead of constructor,
     * in order to comply w/ SonarLint rule `javascript:S1848` about useless object creation.
     * rule: Objects should not be created to be dropped immediately without being used
     */
    init() {
        // DOM elements

        /** @type {HTMLBodyElement|null} */
        this.body = document.body;
        /** @type {HTMLElement|null} */
        this.main = document.querySelector("main#page-pick-portfolio");

        /** @type {NodeListOf<HTMLAnchorElement>} */
        this.portfolioLinks = this.main.querySelectorAll("a.portfolio-id");

        /** @type {HTMLDivElement|null} */
        this.modalExpertConsent = this.main.querySelector("#modal-portfolio-expert-consent");
        /** @type {HTMLDivElement|null} */
        this.modalPolicyContainer = this.modalExpertConsent?.querySelector(".scrollable-content");
        /** @type {HTMLButtonElement|null} */
        this.btnCloseModal = this.modalExpertConsent?.querySelector("button.close");
        /** @type {HTMLButtonElement|null} */
        this.btnConfirmConsent = this.modalExpertConsent?.querySelector("button.confirm-consent");
        /** @type {HTMLInputElement|null} */
        this.hiddenInputPortfolioId = this.modalExpertConsent?.querySelector("[name='portfolio_id']");

        /** @type {string} */
        this.csrftoken = $("[name='csrfmiddlewaretoken']").val();

        // register event listeners on DOM elements
        this.registerEvents();
    }

    /**
     * IMPORTANT: either `.bind(this)` or use arrow function to propagate this to event handlers
     */
    registerEvents() {
        // detect click on individual portfolio links
        this.portfolioLinks.forEach((node) => {
            node.addEventListener("click", this.onClickPortfolio.bind(this));
        });

        // register modal-specific event handlers
        if (this.modalExpertConsent) {
            // modal: register close event listener
            this.btnCloseModal.addEventListener("click", this.onCloseConsentModal.bind(this));

            // detect modal close event in order to unregister 'I confirm' event handler
            $(this.modalExpertConsent).on('hidden.bs.modal', this.onHiddenBsModal.bind(this));
        }
    }

    /**
     * @param {string} portfolioId
     * @return string
     */
    getPortfolioTypeExpertPolicy(portfolioId) {
        /** @type {Element|null} */
        const policyText = this.main.querySelector(`.hidden.expert-policy_${portfolioId}`);
        return policyText?.innerHTML ?? "";
    }

    /**
     * By default, Bootstrap automatically closes the modal on clicking the 'x' close icon, so this JS
     * function may not be necessary.
     * - https://getbootstrap.com/docs/4.0/components/modal/
     * @param {Event} e
     */
    onCloseConsentModal(e) {
        if (!this.modalExpertConsent) {
            return;
        }
        $(this.modalExpertConsent).modal("hide");
    }

    /**
     * Bootstrap modal event: `hidden.bs.modal`
     * - https://getbootstrap.com/docs/4.0/components/modal/#events
     * @param {CustomEvent} e
     */
    onHiddenBsModal(e) {
        console.info(`PickPortfolio.onHiddenBsModal`, e);
        this.btnConfirmConsent?.removeEventListener("click", this.onConfirmConsent.bind(this));
    }

    /**
     * ONLY show consent modal to 'expert' user if there is no consent for that particular portfolio.
     * NOTE: use presence of modal DOM element as way of knowing if we're dealing w/ an expert vs other
     *  staff roles (e.g. reviewer, supervisor, etc.).
     *
     * @param {Event} e
     * @param {HTMLAnchorElement} e.currentTarget
     * @param {DOMStringMap} e.currentTarget.dataset
     * @param {string} e.currentTarget.dataset.portfolioId
     * @param {string} e.currentTarget.dataset.expertConsent
     */
    onClickPortfolio(e) {
        const data = e.currentTarget.dataset;
        const portfolioId = data.portfolioId;
        const hasExpertConsent = xbool(data.expertConsent);
        const policyText = this.getPortfolioTypeExpertPolicy(portfolioId);

        console.info(`clicked portfolio`, {
            portfolio: portfolioId,
            consent: hasExpertConsent,
            modal: this.modalExpertConsent,
            policy: policyText
        });

        // modal only shown to 'expert' role if no consent for this portfolio (user.expert_consent)
        if (this.modalExpertConsent && !hasExpertConsent) {
            // prevent default link navigation + remove href
            e.preventDefault();

            // show the modal
            $(this.modalExpertConsent).modal("show");

            // set portfolio ID in hidden input value so 'submit' button will send it along in POST request
            this.hiddenInputPortfolioId.value = portfolioId;

            // set policy text in modal content
            if (this.modalPolicyContainer) {
                this.modalPolicyContainer.innerHTML = policyText;
            }

            // register event listener for when the user will click 'I confirm'
            this.btnConfirmConsent?.removeEventListener("click", this.onConfirmConsent);
            this.btnConfirmConsent?.addEventListener("click", this.onConfirmConsent.bind(this));
        }
    }

    /**
     *
     * @param {Event} e
     */
    onConfirmConsent(e) {
        console.info(`I do consent !!!`, e.currentTarget);
    }
}

/**
 * DOM-ready hook
 */
$(function () {
    new PickPortfolio().init();
})