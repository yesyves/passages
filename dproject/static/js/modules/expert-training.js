import {nativePost, XFormData} from "./fetch-helpers.js";

export class ExpertTraining {
    constructor() {
        console.info(`esm.ExpertTraining`);
    }

    /**
     * Class initialization should always happen in dedicated init() function instead of constructor,
     * in order to comply w/ SonarLint rule `javascript:S1848` about useless object creation.
     * rule: Objects should not be created to be dropped immediately without being used
     */
    init() {
        // DOM elements

        /** @type {HTMLBodyElement|null} */
        this.body = document.body;
        /** @type {HTMLElement|null} */
        this.main = document.querySelector("main#page-expert-training");
        /** @type {HTMLInputElement|null} */
        this.consentCheckbox = this.main.querySelector("input#personal-consent");

        /** @type {string} */
        this.csrftoken = $("[name='csrfmiddlewaretoken']").val();
        /** @type {string|number} */
        this.userId = this.consentCheckbox?.dataset.userId ?? null;

        // register event listeners on DOM elements
        this.registerEvents();
    }

    /**
     * IMPORTANT: either `.bind(this)` or use arrow function to propagate this to event handlers
     */
    registerEvents() {
        this.consentCheckbox?.addEventListener("change", this.onConsentToggle.bind(this));
    }

    onConsentToggle(e) {
        console.info(this)

        const formData = new XFormData({
            csrfmiddlewaretoken: this.csrftoken,
            expert_id: this.userId,
            consent: e.currentTarget.checked
        }).get();

        nativePost("/user/set_expert_personal_consent", formData)
            .then(response => {});
    }
}

/**
 * DOM-ready hook
 */
$(function () {
    new ExpertTraining().init();
})