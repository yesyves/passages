/**
 *
 */
export class XFormData {
    /** @type {FormData} */
    formData;

    /**
     * @param {Object} object
     */
    constructor(object) {
        this.formData = new FormData();
        for (const [k, v] of Object.entries(object)) {
            this.formData.append(k, v);
        }
    }

    get() {
        return this.formData;
    }
}

/**
 * @source
 *  - https://ridwanray.medium.com/django-and-fetch-api-form-submissions-without-page-reloading-dc5106598005
 * @param {string} url
 * @param {String|Object|Array|FormData} body
 */
export const nativePost = async (url, body) => {
    console.info(`nativePost.body`, body)

    // validate body to make sure it's in proper format for `fetch` api
    /** @type {String|FormData} */
    let preparedBody = body;

    if (body instanceof FormData) {
        console.info(`nativePost.body.type -> FormData`);
    } else if (body instanceof String) {
        console.info(`nativePost.body.type -> String`);
    } else if (body instanceof Object || body instanceof Array) {
        console.info(`nativePost.body.type -> Object|Array`);
        preparedBody = JSON.stringify(body);
    }

    const response = await fetch(url, {
        method: "POST",
        body: preparedBody
    });
    const json = await response.json();
    console.info(`nativePost`, { url, body, json });
    return json;
}