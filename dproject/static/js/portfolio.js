/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/**
 * Achievement
 */

function changeNumber(caller) {
    const achievementNumber = $("#changeNumberInput").val();
    if (achievementNumber > 0) {
        caller = typeof caller !== 'undefined' ? "/" + caller : "";
        $.get("/portfolio/set_achievement_number/" + achievementId + "/" + achievementNumber + caller,
            (response) => {
                if (response.success) {
                    $("#achievementNumber").text(achievementNumber + ".");
                    $("#changeNumberModal").modal("hide");
                } else {
                    alert(response.message);
                }
            }
        );
    }
}


/**
 * Attribution
 */

function sendAttribution(groupId) {
    $.post("/user/send_attribution", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        group_id: groupId,
        message: $("#message").val()
    }, (response) => {
        if (response.success) {
            const confirmObjet = $("#confirmSend");
            confirmObjet.fadeIn();
            confirmObjet.css("visibility", "visible");
            confirmObjet.fadeOut(3000);
        }
    });
}


/**
 * Base
 */

function setLang(langCode) {
    $("#language").val(langCode);
    $("#set_language").submit();
}


/**
 * Edit Document
 */

// Values for document currently being renamed
let currentDocumentID = null;
let currentName = null;
// Show "Do document sort" button
let doSort = false;

function validateAnnexes(fileType) {
    const filePointer = "#file-" + fileType;

    // Validate a file was chosen
    if (!$(filePointer).val()) {
        if (fileType === 3) {
            alert(gettext("Please select at least 1 file"));
        } else {
            alert(gettext("Please select a file"));
        }
        return false;
    }

    // Validate file type
    const files = $(filePointer).prop("files");

    for (const f of files) {
        // console.info(f)
        // console.info(f.type)
        // console.info(f.size)

        // do NOT allow video or audio upload
        if (
            f.type.startsWith("video")
            || f.type.startsWith("application/ogg")
            || f.type.startsWith("audio")
        ) {
            alert(gettext("Error ! Video & audio files are not allowed !"));
            return false;
        }
    }

    // Validate file extensions
    const names = $.map(files, function (val) {
        return val.name;
    });
    for (let i = 0; i < names.length; i++) {
        if (names[i].indexOf(".") === -1) {
            alert(
                gettext('Error ! File "') + names[i] + gettext('" has no extension !')
            );
            return false;
        }
    }

    // Validate document type
    if (fileType === 3 && $("#documentType option:selected").val() === "") {
        alert(
            gettext('Choose document type from list')
        );
        return false;
    }

    return true;
}

function setDocumentType(select, documentID) {
    $.post("/portfolio/set_document_type", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        document_id: documentID,
        document_type_id: select.value
    });
}

function setFileType(typeName, fileType) {
    // let fileName;
    let addFile = gettext("Add") + " " + typeName;
    // switch (fileType) {
    //     case "A":
    //         fileName = gettext("Curriculum vitae");
    //         addFile = gettext("Add Curriculum vitae");
    //         break;
    //     case "B":
    //         fileName = gettext("Timeline");
    //         addFile = gettext("Add Timeline");
    //         break;
    //     case "C":
    //         fileName = gettext("Interview preparatory sheet");
    //         addFile = gettext("Add Interview preparatory sheet");
    //         break;
    // }
    $("#addGlobalName").text(addFile);
    $("#documentType").val(fileType);
    $("#fileName").val(typeName);
}

function setLink(documentID, achievementID) {
    const linkID = "#" + documentID + achievementID;
    const anchor = $(linkID);
    anchor.attr("disabled", true);
    $.get("/portfolio/document_association/" + documentID + "/" + achievementID,
        (response) => {
            if (response.success) {
                // Swap icons and data attribute
                const icon = $(linkID + "-icon");
                if (anchor.attr("data-link") === "true") {
                    anchor.attr("data-link", "false");
                    icon.html(
                        "<svg class='icon ajouter orange' width=25 height=25>" +
                        "  <use xlink:href='/static/icons/icons-" + iconsVersion + ".svg#ajouter'></use>" +
                        "</svg>"
                    );
                } else {
                    anchor.attr("data-link", "true");
                    icon.html(
                        "<svg class='icon retirer orange' width=25 height=25>" +
                        "  <use xlink:href='/static/icons/icons-" + iconsVersion + ".svg#retirer'></use>" +
                        "</svg>"
                    );
                }
                // If documents sort has changed, remove arrows and add button
                if (!doSort && response.do_sort) {
                    doSort = true;
                    $(".up_or_down").css("display", "none");
                    $("#do_sort").css("display", "inline");
                }
            }
            anchor.attr("disabled", false);
        });
}

function setRenameValues(documentID) {
    currentDocumentID = documentID;
    currentName = $("#" + currentDocumentID).text();
    $("#documentName").val(currentName);
}

function renameDocument() {
    const name = $("#documentName").val();
    if (name !== currentName) {
        $.post("/portfolio/rename_document", {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            document_id: currentDocumentID,
            name: name
        }, (response) => {
            if (response.success) {
                $("#" + currentDocumentID).text(name);
                $("#renameDocumentModal").modal("hide");
            }
        });
    }
}

function setDeleteValues(documentID) {
    $("#deleteID").val(documentID);
    $("#deleteName").text($("#" + documentID).text());
}


/**
 * Edit Profile
 */

function switch_establishment() {
    const hint = $("#hint_id_Establishment");
    let establishment = gettext("Training center");
    let action = gettext("where you did your last internship");
    if (concentration === "T") {
        establishment = gettext("College");
    }
    if ($("#id_Is_teacher").prop("checked")) {
        action = gettext("where you currently teach");
    }
    hint.text("(" + establishment + " " + action + ")");
}

/**
 *
 */
function switch_personal_consent(firstLoad = false) {
    const consent = document.querySelector("input#id_Consents")
    if (consent !== null) {
        const menuPortfolio = document.querySelector(".navbar-nav a[data-id='portfolio']")
        const logoPortfolio = document.querySelector(".navbar-primary a[data-id='logo-portfolio']")
        const btnSave = $("button#edit-profile-save")
        const btnCancel = $("button#edit-profile-cancel")

        if (consent?.checked) {
            menuPortfolio.setAttribute("href", menuPortfolio.dataset.href)
            logoPortfolio.setAttribute("href", logoPortfolio.dataset.href)
            btnSave.show()
            btnCancel.show()
        } else {
            // is_Consents comes from FormEditApplicantProfileMilestone; so only hide buttons if checkbox is there
            // and unchecked, NOT when there's no checkbox (continuous)
            if (!consent) {
                return;
            }

            menuPortfolio.dataset.href = menuPortfolio.href
            menuPortfolio.removeAttribute("href")
            logoPortfolio.dataset.href = logoPortfolio.href
            logoPortfolio.removeAttribute("href")
            btnSave.hide()
            btnCancel.hide()
        }

        // change student consent status in server WITHOUT requiring Save button
        // NOTE: ONLY call this function when toggling the checkbox, NOT on page load
        if (!firstLoad) {
            setPersonalConsent(consent, userId)
        }
    }
}


/**
 * Index
 */

/**
 *
 * @param field_selector
 * @returns {boolean}
 */
function validateAchievementName(field_selector) {
    const nameInput = $(field_selector);
    const achievementName = nameInput.val().trim();
    if (!achievementName) {
        alert(gettext("Please specify a title for this achievement."));
        return false;
    } else {
        nameInput.val(achievementName);
        return true;
    }
}


/**
 * Knowledge / Soft Skills
 */

function toggleSort(sort) {
    if (sort === "k") {
        $("#sort_k").show();
        $("#sort_a").hide();
    } else {
        $("#sort_k").hide();
        $("#sort_a").show();
    }
}


/**
 * Levels
 */

function setDate() {
    const now = new Date();
    const day = ("0" + now.getDate()).slice(-2);
    const month = ("0" + (now.getMonth() + 1)).slice(-2);
    const today = now.getFullYear() + "-" + month + "-" + day;

    $("#date").val(today);
}

function completePost() {
    if ($("#export").prop("checked")) {
        setTimeout(function () {
            location.reload();
        }, 5000);
    }
}

function validateSubmit() {
    if ($("#revised").prop("checked")) {
        $("#finalizePortfolio").attr("disabled", false);
    } else {
        $("#finalizePortfolio").attr("disabled", true);
    }
}


/**
 * Login
 */

function normalizeEmail() {
    let email = $(emailInputId).val();
    $(emailInputId).val(email.toLowerCase());
}


/**
 * Pick Portfolio
 */

let targetedUser = "Applicant";

function adjustNotify(change = null) {
    // noinspection JSIncompatibleTypesComparison
    if (change === "set_password") {
        if ($("#set_password").prop("checked")) {
            $("#my_applicants").prop("disabled", true);
        } else {
            $("#my_applicants").prop("disabled", false);
        }
    } else if (change === "my_applicants") {
        if ($("#my_applicants").prop("checked")) {
            $("#set_password").prop("disabled", true);
        } else {
            $("#set_password").prop("disabled", false);
        }
    } else {
        if (userType === "Reviewer") {
            $("#setPasswordDiv").hide();
            $("#myApplicantsDiv").hide();
            $("#my_applicants").prop("checked", true);
        }
    }
}

function notifyUsers() {
    let alertMessage = "";
    const message = $("#message").val();
    const set_password = $("#set_password").prop("checked");
    if (message || set_password) {
        $.post("/user/group_notify", {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            // semester_id: semesterID, // todo remove eventually
            group_id: groupID,
            user_type: targetedUser,
            message: message,
            set_password: set_password,
            my_applicants: $("#my_applicants").prop("checked")
        }, (response) => {
            if (response.success) {
                alertMessage = gettext("Message sent to:\n\n") + response.users;
            } else {
                alertMessage = gettext("Sending failed!");
            }
            alert(alertMessage);
            $("#semesterNotifyModal").modal("hide");
        });
    }
}

function setTargetedUser(thisTargetedUser) {
    targetedUser = thisTargetedUser;
    if (thisTargetedUser !== "Applicant") {
        $("#myApplicantsDiv").hide();
        $("#my_applicants").prop("checked", false);
        $("#set_password").prop("disabled", false);
    } else if (userType != "Reviewer") {
        $("#myApplicantsDiv").show();
    }
}

function setReviewer(select, ApplicantId) {
    $.post("/user/set_reviewer", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        applicant_id: ApplicantId,
        reviewer_id: select.value
    });
}

function setExpert(select, ApplicantId, portfolioStatus) {
    const expert = select.value;
    $.post("/user/set_expert", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        applicant_id: ApplicantId,
        expert_id: expert
    }, (response) => {
        if (response.success) {
            if (expert === "None") {
                $("#send_" + ApplicantId).attr("disabled", true);
                $("#showNotifyExpert_" + ApplicantId).css("visibility", "hidden");
            } else {
                $("#showNotifyExpert_" + ApplicantId).css("visibility", "visible");
                if (portfolioStatus === "Final") {
                    $("#send_" + ApplicantId).attr("disabled", false);
                }
            }
        }
    });
}

/**
 * @param {HTMLSelectElement} select
 * @param {string} ApplicantId
 */
function setTargetCredits(select, ApplicantId) {
    console.info("nb_credits", select.value)

    $.post("/user/set_target_credits", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        applicant_id: ApplicantId,
        nb_credits: select.value
    });
}

function removePortfolio(portfolioID) {
    $("#portfolioRemoveID").val(portfolioID);
    const expertName = $("#select_expert_" + portfolioID + " option:selected").text();
    $("#expertRemoveName").text(expertName);
}

function sendPortfolio(portfolioID) {
    $("#portfolioID").val(portfolioID);
    const expertName = $("#select_expert_" + portfolioID + " option:selected").text();
    $("#expertName").text(expertName);
}

/**
 * Sets boolean for Applicant's 'reviewer_consent' variable
 * @param chkbox
 * @param ApplicantId
 */
function setReviewerConsent(chkbox, ApplicantId) {
    $.post("/user/set_reviewer_consent", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        applicant_id: ApplicantId,
        consent: chkbox.checked
    });
}

/**
 * Sets boolean for Applicant's 'personal_consent' variable
 * @param chkbox
 * @param ApplicantId
 */
function setPersonalConsent(chkbox, ApplicantId) {
    $.post("/user/set_personal_consent", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        applicant_id: ApplicantId,
        consent: chkbox.checked
    });
}


/**
 * Questionnaire
 */

// Question currently focused
let currentQuestion = '';

function setCurrentQuestion(name) {
    currentQuestion = name;
}

function setTargetedSkill() {
    const newSkill = $("#id_targeted_skill").val();
    console.info("portfolio.js@setTargetedSkill", newSkill);

    $.post("/portfolio/set_targeted_skill", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        achievement_id: $("#achievement_id").val(),
        targeted_skill: newSkill
    });
}

function setDevelopmentLevel() {
    const newLevel = $("#id_development_level").val();
    console.info("portfolio.js@setDevelopmentLevel", newLevel);

    $.post("/portfolio/set_development_level", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        achievement_id: $("#achievement_id").val(),
        development_level: newLevel
    });
}


/**
 * NOTE: `this.event` is automatically passed to every event handler function
 * @param name
 * @returns {Promise<*|boolean>}
 */
async function saveQuestionnaire(name = "") {
    if (!name) {
        name = currentQuestion;
    }

    // overwrite contents of <textarea> to put back the original answer;
    // needed because `validateForm` checks for questions 1 - 9 to be filled in
    if (name === "A1" && !validateAchievementName('#id_A1')) {
        this.event && this.event.preventDefault()

        document.querySelector('#id_A1').value = this.event.target.textContent
        return Promise.resolve(false);
    }

    // in all other cases, save the question
    return await $.post({
        url: "/portfolio/save_questionnaire",
        data: {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            achievement_id: $("#achievement_id").val(),
            element_name: name,
            answer: $("#id_" + name).val()
        }
    });
}

async function validateForm() {
    let firstLimit = 9;
    let lastLimit = 41;
    let extension = "";
    if (achievementNumber <= 2) {
        firstLimit = 29;
        lastLimit = 38;
        extension = gettext(" as well as questions 38 to 40");
    }
    const $inputs = $("#questionnaire :input");
    let values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    for (let key in values) {
        if (key.substring(1) > firstLimit && key.substring(1) < lastLimit) {
            continue;
        }
        if (!values[key] || values[key] === "NONE") {
            alert(
                gettext("Please answer at least questions 1 to ") + firstLimit + extension
            );
            // this prevents occasional redirection when alert dialog is closed
            this.event && this.event.preventDefault()
            return Promise.resolve(false);
        }
    }
    // Save last answer
    return await saveQuestionnaire();
}

/**
 *
 * @param submit
 * @returns {Promise<boolean>}
 */
async function calculateAssessmentGrades(submit = false) {

    const ratingsDom = document.querySelectorAll(".rating-dd")
    // e.g. { a1: '0.8', ..., c1: '1.0' }
    const ratings = {}
    for (const node of ratingsDom) {
        ratings[node.dataset.rubricId] = node.value
    }
    // DEBUG
    // console.log("Ratings :")
    // console.info(ratings)

    const criteriaDom = document.querySelectorAll(".rating-criteria")
    for (const node of criteriaDom) {
        const rubricId = node.dataset.rubricId
        let rating = ratings[rubricId]
        let criteria = null
        if (rating !== "") {
            rating = Math.round(rating * 100)
            criteria = criterias[`${rubricId}_${rating}`] ?? ""
            if (rating !== 0 && criteria === "") {
                alert(gettext("Error! Invalid rating for this assessment object: ") + $(`#assessment-${rubricId}`).text());
                node.innerHTML = "";
                return false
            }
        } else {
            criteria = ""
        }
        node.innerHTML = criteria
    }

    // accumulate sub-grades per parent rubric
    const grades = {}
    for (const key in ratings) {
        // take 1st number and letter (e.g. 1_a1 = 1_a, 2_b2 = 2_b)
        const gradeGroup = key.substring(0, 3)
        // append sub-grades to each parent grade rubric; deals w/ initial empty array, then append (one-liner)
        grades[gradeGroup] = [...grades[gradeGroup] || [], parseFloat(ratings[key]) || 0]
    }
    // DEBUG
    // console.log("Grades :")
    // console.info(grades)

    // calculate weights per category
    // e.g. { a: [.5, .8, .6, .1], b: [1], ... }
    const finalGrades = {}
    for (const group in grades) {
        // actual array containing sub grades for group A, for instance
        const subGrades = grades[group]
        // add up all sub-grades per grade group
        const sugGradesTotal = subGrades.reduce((a, b) => a + b, 0)
        const avgSubGrade = sugGradesTotal / subGrades.length
        try {
            // get the weight (e.g. 0.15, 0.35) from the data attribute assigned to
            const gradeGroupWeight = document
                .querySelector(`[data-grade-group="${group}"]`)
                .dataset.gradeWeight

            // calculate final grade for assessment
            // e.g. [(.5+.8+.6+1)/4] * 0.15 (15%) = 0.10875 = 10%
            finalGrades[group] = avgSubGrade * parseFloat(gradeGroupWeight)
        } catch (e) {
            console.error(e)
            return false
        }
    }

    // DEBUG
    // console.log("Final Grades :")
    // console.info(finalGrades)

    // Distinguish milestones
    const milestones = []
    for (const key in finalGrades) {
        // take 1st number (e.g. 1_a1 = 1, 2_b2 = 2)
        let milestone = key.substring(0, 1)
        if (!milestones.includes(milestone)) {
            milestones.push(milestone)
        }
    }
    // DEBUG
    // console.log("Milestones :")
    // console.log(milestones)

    let currentFinalGrade = 0.0
    for (const count in milestones) {
        // calculate final grade
        let finalGrade = 0.0 // decimal, (.88, NOT 88%)
        let finalGradePerc = 0 // in percentage (88%, NOT .88)

        for (const group in finalGrades) {
            if (group[0] == milestones[count]) {
                finalGrade += finalGrades[group]
            }
        }
        // e.g. 0.985 = 98.5%
        finalGradePerc = Math.round(finalGrade * 1000) / 10
        currentFinalGrade = finalGrade

        // DEBUG
        // console.info(finalGrade)
        // console.info(`${finalGradePerc}%`)

        // update HTML to display newly calculated sum
        $(`#${milestones[count]}_final-grade`).html(`${finalGradePerc}%`)
    }

    if (!submit)
        return true

    // capture comments per rating for saving to server
    const ratingCommentsDom = document.querySelectorAll(".rating-comment")
    // e.g. { a1: '<text>', ..., c1: '<text>' }
    const ratingComments = {}
    for (const node of ratingCommentsDom) {
        ratingComments[`${node.dataset.rubricId}`] = node.value
    }
    // DEBUG
    // console.log("Comments")
    // console.info(ratingComments)

    const btnSubmit = $("#btn-assessment-submit")
    const milestone = btnSubmit.data("milestone")

    // accumulate sub-grades per parent rubric
    const assessments = {}
    for (const key in ratings) {
        // take 1st number and letter (e.g. 1_a1 = 1, 2_b2 = 2)
        if (key.substring(0, 1) == milestone) {
            assessments[key.substring(2, 4)] = [ratings[key], ratingComments[key]]
        }
    }
    assessments['total'] = [currentFinalGrade, ""]
    // DEBUG
    // console.log("Assessments :")
    // console.info(assessments)

    // submit assessment to server for storage in database
    $.post("/portfolio/submit_user_assessment", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        portfolio: btnSubmit.data("portfolio"),
        milestone: milestone,
        assessments: JSON.stringify(assessments)
    }, (response) => {
        // DEBUG
        // console.info(response)
        if (response.success) {
            alert(gettext("The assessment has been saved."))
            return true
        } else {
            alert(gettext("An error occurred while trying to save this assessment."))
            return false
        }
    })
}

/**
 * Pre-fill the assessment table w/ existing values
 */
function populateExistingAssessment() {
    if (!currentAssessment)
        return

    // get all current dropdowns
    const ratingsDom = document.querySelectorAll(".rating-dd")
    const ratingCommentsDom = document.querySelectorAll(".rating-comment")

    // set grade value based on UserAssessment object
    for (const node of ratingsDom) {
        const rubricId = node.dataset.rubricId
        let grade = parseFloat(currentAssessment[rubricId]) ?? ""
        if (grade === 1 || grade === 0) {
            grade = grade.toFixed(1)
        }
        node.value = grade
    }

    // set comment value based on UserAssessment object
    for (const node of ratingCommentsDom) {
        const rubricId = node.dataset.rubricId
        const isReadOnly = node.hasAttribute("data-read-only")
        let comment = currentAssessment[`${rubricId}_comment`] ?? ""
        // read-only (student) -> div -> append comment to prefix "Commentaire : " already in the div component
        if (isReadOnly) {
            node.innerHTML += comment
        }
        // editable (admin) -> textarea
        else {
            node.value = comment
        }
    }
}


/**
 TODO On pourrait éventuellement optimiser ce code:
 - calculateAssessmentGrades() ne calcule les milestones précédentes qu'à sa première utilisation
 - les notes sont conservées dans une variable globale var ratings = {}
 - les notes et critères associés sont mis à jour à la pièce via des appels onchange() discrets
 */
function handleUserAssessment() {
    // pre-populate entries based on current user portfolio assessment (if available)
    populateExistingAssessment()
    calculateAssessmentGrades(false)

    // handle click on 'Submit' button
    $("#btn-assessment-submit > button").on("click", function (e) {
        return calculateAssessmentGrades(true)
    })

    // handle live changing of grade via dropdown so we compute final grade automatically (w/o sending it)
    $(".rating-dd").on("change", function (e) {
        return calculateAssessmentGrades(false)
    })
}

/**
 * Document.ready()
 */

$(document).ready((event) => {
    /**
     * base : Help link swap icon
     */

    $("#help-link")
        .mouseenter(function () {
            let iconPath = is_continuous
                ? `/static/icons/icons-${iconsVersion}.svg#interrogation-hover`
                : `/static/icons/icons-${iconsVersion}.svg#enveloppe-orange`
            $(this).find("use").attr("xlink:href", iconPath);
        })
        .mouseleave(function () {
            let iconPath = is_continuous
                ? `/static/icons/icons-${iconsVersion}.svg#interrogation-blanc`
                : `/static/icons/icons-${iconsVersion}.svg#enveloppe-blanc`
            $(this).find("use").attr("xlink:href", iconPath);
        });


    /**
     * dashboard : left sidebar menu hiders
     */

    $(".x-menu-minimize > div.fixed").on("click", function (e) {
        let node = e.currentTarget;
        // console.info(node.classList)

        // const maxMinBtn = $(".x-menu-minimize");
        const sidebarRoot = $("nav#side-nav-bar");
        const menuItemsContainer = $("nav#side-nav-bar > ul.navbar-nav");

        const icnMinimize = $(".x-menu-minimize svg[data-iconid='icn-minimize']");
        const icnMaximize = $(".x-menu-minimize svg[data-iconid='icn-maximize']");

        // if minimized, maximize it
        if (sidebarRoot[0].classList.contains("minimized")) {
            sidebarRoot.removeClass("minimized");
            $("main.ps-main-section").removeClass("no-sidebar");
            menuItemsContainer.show();

            icnMaximize.hide();
            icnMinimize.show();
        }
        // else minimize the menu
        else {
            sidebarRoot.addClass("minimized");
            $("main.ps-main-section").addClass("no-sidebar");
            menuItemsContainer.hide();

            icnMinimize.hide();
            icnMaximize.show();
        }
    });


    /**
     * Location specific calls begin here
     */

    const url = $(location).attr("href");


    /**
     * Change achievement number : Editor + Questionnaire
     */

    if (url.includes("editor") && userType === "Applicant") {
        $("#changeNumber").on("click", (event) => {
            changeNumber();
        });
        $(document).keypress((e) => {
            if ($("#changeNumberModal").hasClass("show") && (e.keycode === 13 || e.which === 13)) {
                changeNumber();
            }
        });
    } else if (url.includes("/questionnaire") && userType === "Applicant") {
        $("#changeNumber").on("click", (event) => {
            changeNumber("questionnaire");
        });
        $(document).keypress((e) => {
            if ($("#changeNumberModal").hasClass("show") && (e.keycode === 13 || e.which === 13)) {
                changeNumber("questionnaire");
            }
        });
    }


    /**
     * edit-document : Modal form monitoring
     */

    else if (url.includes("documents/1")) {
        $("#renameDocument").on("click", (event) => {
            renameDocument();
        });
        $(document).keypress((e) => {
            if ($("#renameDocumentModal").hasClass("show") && (e.keycode === 13 || e.which === 13)) {
                renameDocument();
            }
        });
    }


    /**
     * edit-profile : Set Establishment
     */

    else if (url.includes("edit_profile") && userType === "Applicant") {
        switch_establishment();
    }


    /**
     * skills : Initialize tooltips
     */

    else if (url.includes("skills")) {
        $("[data-bs-toggle='tooltip']").tooltip();
        // var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        // var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        //     return new bootstrap.Tooltip(tooltipTriggerEl)
        // })
    }


    /**
     * knowledge / soft-skills : Hide alternative sort view
     */

    else if (url.includes("knowledge") || url.includes("soft-skill")) {
        $("#sort_a").hide();
    }


    /**
     * levels : Show finalization warnings
     */

    else if (url.includes("levels")) {
        $("#finalizePortfolioModal").on("show.bs.modal", function () {
            let message = "";
            if (!cv || !ips || !proofed || comments.length > 0) {
                if (!cv) {
                    message = gettext(
                        "Please upload your CV in order to finalize your portfolio.\n\n"
                    );
                }
                if (!proofed) {
                    message += gettext(
                        "Please make sure every achievement is associated to " +
                        "at least one document in order to finalize your portfolio.\n\n"
                    );
                }
                if (comments.length > 0) {
                    if (comments.length === 1) {
                        message += gettext("Please make sure to remove remaining comments from achievement ") +
                            comments[0] +
                            gettext(" in order to finalize your portfolio.\n\n");
                    } else {
                        message += gettext("Please make sure to remove remaining comments from achievements ") +
                            comments.toString() +
                            gettext(" in order to finalize your portfolio.\n\n");
                    }
                }
                if (!ips) {
                    message += gettext(
                        "Please upload your Interview preparation sheet " +
                        "in order to finalize your portfolio."
                    );
                }
                alert(message);
                return false;
            } else if (!finalized) {
                message = gettext(
                    "Every achievement must be marked as final\n" +
                    "before the portfolio can be submitted for evaluation."
                );
                alert(message);
                return false;
            } else if (!validated) {
                message = gettext(
                    "Your portfolio does not meet your credits target.\n" +
                    "Are you sure you want to submit your portfolio for evaluation?"
                );
                return confirm(message);
            }
        });

        $("#certifyPortfolioModal").on("show.bs.modal", function () {
            if (userType === "Expert") {
                setDate();
            }
        });

    }


    /**
     * pick_semester : Initialize tooltips
     */

    else if (url.includes("pick_semester")) {
        if (isContinuous) {
            // Enable tooltips
            $("[data-bs-toggle='tooltip']").tooltip();
        } else {
            $("#btn-toggle-revision-mode").on("click", async function (e) {
                e.preventDefault();
                const res = await fetch(toggleURL + "?csrfmiddlewaretoken=" + $("[name='csrfmiddlewaretoken']").val());
                const data = await res.json();
                // console.log(data);
                window.location.reload();
            })
        }

        // Set notification default values
        adjustNotify();

        // Listen
        $("#notifyUsers").on("click", (event) => {
            notifyUsers();
        });

    }

        // Location specific calls

    /**
     * Deal w/ assessment submission & other things
     */
    else if (url.includes("assessment")) {
        handleUserAssessment()
    }

    /**
     * Deal w/ progress and summative evaluation forms
     */
    else if (url.includes("evaluations")) {
        const tabs = $('.competency-tab')
        const explanation_overestimate = $('#explanation-overestimate')
        const explanation_underestimate = $('#explanation-underestimate')
        const explanation_good_estimate = $('#explanation-good-estimate')
        const progressCandidateLevel = $('#id_progress_candidate_level')
        const summativeCandidateLevel = $('#id_summative_candidate_level')
        const summativeExpertLevel = $('#id_summative_expert_level')
        const summativeCredits = $('#id_credits')
        const dateInterviewAssessment = $('#interview-assessment-date')
        const btnSubmitPortfolio = $('#submit-portfolio')

        const actions = {
            FINALIZE: 'finalize',
            CERTIFY: 'certify',
            REVIEW: 'review'
        }

        const assessmentTypes = {
            PROGRESS: 'progress',
            SUMMATIVE: 'summative'
        }

        /**
         * Show a message to highlight the discrepancy (or not) between candidate's and expert's development levels.
         * Update number of credits according to development level selected by expert in summative assessment.
         */
        const updateSummativeFeedback = () => {
            if (summativeCandidateLevel.val() !== '' && summativeExpertLevel.val() !== '') {
                const diff = summativeExpertLevel.val().charAt(2) - summativeCandidateLevel.val().charAt(2)
                if (diff > 0) { // candidate underestimates
                    explanation_overestimate.hide()
                    explanation_underestimate.show()
                    explanation_good_estimate.hide()
                } else if (diff < 0) { // candidate overestimates
                    explanation_overestimate.show()
                    explanation_underestimate.hide()
                    explanation_good_estimate.hide()
                } else { // candidate properly estimates
                    explanation_overestimate.hide()
                    explanation_underestimate.hide()
                    explanation_good_estimate.show()
                }
            } else {
                explanation_overestimate.hide()
                explanation_underestimate.hide()
                explanation_good_estimate.hide()
            }

            switch (summativeExpertLevel.val().charAt(2)) {
                case '1':
                    summativeCredits.val('2');
                    break;
                case '2':
                    summativeCredits.val('4');
                    break;
                case '3':
                    summativeCredits.val('6');
                    break;
                default:
                    summativeCredits.val('');
            }
        }
        progressCandidateLevel.change(() => {
            summativeCandidateLevel.val(progressCandidateLevel.val())
        })
        if (summativeExpertLevel.length && summativeCandidateLevel.length) {
            updateSummativeFeedback()
            summativeCandidateLevel.change(updateSummativeFeedback)
            summativeExpertLevel.change(updateSummativeFeedback)
        }

        /**
         * load page with competency assessment data when user clicks on a competency
         */
        tabs.each((i, el) => {
            $(el).click((e) => {
                const competencyId = $(e.currentTarget).data('competencyId')
                window.location.href = '/portfolio/evaluations?competency_id=' + competencyId
            })
        })

        /**
         * update status of all assessments of same type (progress/summative) on
         * finalization, review or certification by expert/supervisor
         */
        const updateAssessmentStatus = (assessmentType, action) => {
            $.get(`/portfolio/${action}_assessments/${assessmentType}`, (response) => {
                if (response.success) {
                    console.log("success")
                } else {
                    alert("error");
                }
            })
            window.location.href = '/portfolio/evaluations'
        }
        $('#finalize-progress-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.PROGRESS, actions.FINALIZE)
        })
        $('#finalize-summative-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.SUMMATIVE, actions.FINALIZE)
        })
        $('#review-progress-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.PROGRESS, actions.REVIEW)
        })
        $('#review-summative-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.SUMMATIVE, actions.REVIEW)
        })
        $('#certify-progress-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.PROGRESS, actions.CERTIFY)
        })
        $('#certify-summative-assessments').click(() => {
            updateAssessmentStatus(assessmentTypes.SUMMATIVE, actions.CERTIFY)
        })
        btnSubmitPortfolio.click(() => {
            $.get('/portfolio/assess_portfolio', (response) => {
                if (response.success) {
                    btnSubmitPortfolio.hide()
                    confirm(gettext("The portfolio has been submitted to the expert for review."))
                } else {
                    alert(gettext("The submission of the portfolio has failed. Try again."));
                }
            })
        })

        /**
         * update portfolio's interview_assessment_date field on change of date picker by expert
         */
        dateInterviewAssessment.change((e) => {
            const dateStr = e.currentTarget.value;
            const interviewDate = Date.parse(dateStr);
            // if invalid date entered, do not save
            if (isNaN(interviewDate))
                return;
            // do not allow accidentally setting very old date like 1500s or 1900s - when typing year manually
            if (new Date(interviewDate).getFullYear() < new Date().getFullYear() - 1)
                return;

            // when date is valid, persist it
            $.get(`/portfolio/set_interview_assessment_date/${dateStr}`, (response) => {
                if (response.success) {
                    return confirm(gettext("The date of the interview has successfully been updated."));
                } else {
                    alert(gettext("A problem occurred during the saving of the interview date. Please try again later."));
                }
            });
        })
    }

    /**
     * Make table sections in custom competencies collapsible
     * - https://stackoverflow.com/questions/64098304/html-collapsible-table-sections-using-css-and-js
     */
    if (document.querySelectorAll("table[data-type='custom-competency']").length > 0) {
        // whenever a parent row is clicked, toggle CSS class for all its siblings until the next parent competency
        $('table').on('click', 'tr.parent-competency:not(.top-parent)', function () {
            $(this).toggleClass('collapsed')
            $(this).nextUntil('.parent-competency').toggleClass('row-open')
        })
        // handle collapse-all / expand-all on top row
        // IMPORTANT: this handles cases where user manually collapses / expands some sub-competencies
        $('table thead > tr.competency-type-row').on('click', function () {
            // used to change icon to reflect new state
            let wasCollapsed = $(this).hasClass('collapsed')
            $(this).toggleClass('collapsed')

            let parentTable = $(this).closest('table')
            let parentCompetencies = parentTable.find('tr.parent-competency:not(.top-parent)')
            let childCompetencies = parentTable.find('.child-competency')
            if (wasCollapsed) {
                parentCompetencies.removeClass('collapsed')
                childCompetencies.addClass('row-open')
            } else {
                parentCompetencies.addClass('collapsed')
                childCompetencies.removeClass('row-open')
            }
        })

        // count number of <a> tags in child competencies to display total number of instances of a competency group
        // in a specific achievement; number of times in the achievement any of a competency group's children are used
        const parentCompetencyRows = document.querySelectorAll("tr.parent-competency")
        parentCompetencyRows.forEach(function (row) {
            // for each parent row, get the 'achievement' cells <td> that have this custom data attribute
            const achievementCells = row.querySelectorAll("td[data-achievement-number]")
            // for each 'achievement' cell, count the number of <a> siblings for this achievement number
            achievementCells.forEach(function (n) {
                // 1 - get siblings until next parent competency

                // todo no milestone for UQAR
                // 2 - find <a> that have data attribute with this achievement number
                // const compCount = $(row)
                //   .nextUntil('.parent-competency')
                //   .find(`a[data-achievement-number='${n.dataset.achievementNumber}'][data-tab='${n.dataset.tab}']`)
                //   .length;
                const compCount = $(row)
                    .nextUntil('.parent-competency')
                    .find(`a[data-achievement-number='${n.dataset.achievementNumber}']`)
                    .length;
                console.info("achievement-cells", {compCount, n})

                // IMPORTANT: need to diff. between skills & custom competencies since skills are NOT all w/
                //  new structure and also a parent skill can be used in Quill as tag (unlike custom competency).

                // display the number of instances in the parent competency's achievement cells
                if (!row.classList.contains('original-skill')) // custom competency
                    n.innerHTML = compCount > 0 ? `(${compCount})` : "";
                else if (compCount > 0) // original skills, BUT make sure we got children, else leave parent check
                    n.innerHTML = `(${compCount})`;
            })
        })
    }

}); // Document.ready()
