/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import AssociatedSkill from "./formats/associated_skill.js";
import Competency from "./formats/competency.js";
import ExpertiseLevel from "./formats/expertise_level.js";
import Explain from "./formats/explain.js";
import Knowledge from "./formats/knowledge.js";
import SimpleComment from "./formats/simple_comment.js";
import SoftSkill from "./formats/soft_skill.js";
import Vocabulary from "./formats/vocabulary.js";
import Spellcheck from "./formats/spellcheck.js";
import MediaDoc from "./formats/media_doc.js";
import MediaImg from "./formats/media_img.js";
import NewText from "./formats/new_text.js";

import AssociatedSkillComment from "./modules/associated_skill-comment.js";
import CompetencyComment from "./modules/competency-comment.js";
import ExplainComment from "./modules/explain-comment.js";
import ExpertiseLevelComment from "./modules/expertise_level-comment.js";
import KnowledgeComment from "./modules/knowledge-comment.js";
import SimpleCommentComment from "./modules/simple_comment-comment.js";
import SoftSkillComment from "./modules/soft_skill-comment.js";
import VocabularyComment from "./modules/vocabulary-comment.js";
import MediaImgComment from "./modules/media_img-comment.js";
import MediaDocComment from "./modules/media_doc-comment.js";

import Icons from "./themes/icons.js";
import CertificationTheme from "./themes/certification.js";
import CEMPTheme from "./themes/cemp.js";
import Habilete from "./formats/competency-types/habilete.js";
import HabileteComment from "./modules/competency-types/habilete-comment.js";
import SavoirEtre from "./formats/competency-types/savoir_etre.js";
import ProfilEntree from "./formats/competency-types/profil_entree.js";
import SavoirEtreComment from "./modules/competency-types/savoir_etre-comment.js";
import ProfilEntreeComment from "./modules/competency-types/profil_entree-comment.js";

// UQAR
import Facets from "./formats/competency-types/facets.js";
import FacetsComment from "./modules/competency-types/facets-comment.js";
import FacetsTooltip from "./ui/competency-types/facets-tooltip.js";

import SpellcheckComment from "./modules/spellcheck-comment.js";

Quill.register({
    "formats/associated_skill": AssociatedSkill,
    "formats/competency": Competency,
    "formats/expertise_level": ExpertiseLevel,
    "formats/explain": Explain,
    "formats/knowledge": Knowledge,
    "formats/simple_comment": SimpleComment,
    "formats/soft_skill": SoftSkill,
    "formats/vocabulary": Vocabulary,
    "formats/spellcheck": Spellcheck,
    "formats/media_doc": MediaDoc,
    "formats/media_img": MediaImg,
    "formats/new_text": NewText,

    // MODE = MILESTONE
    "formats/habilete": Habilete,
    "formats/savoir_etre": SavoirEtre,
    "formats/profil_entree": ProfilEntree,

    // UQAR
    "formats/facets": Facets,

    "modules/associated_skill_comment": AssociatedSkillComment,
    "modules/competency_comment": CompetencyComment,
    "modules/explain_comment": ExplainComment,
    "modules/expertise_level_comment": ExpertiseLevelComment,
    "modules/knowledge_comment": KnowledgeComment,
    "modules/simple_comment_comment": SimpleCommentComment,
    "modules/spellcheck_comment": SpellcheckComment,
    "modules/soft_skill_comment": SoftSkillComment,
    "modules/vocabulary_comment": VocabularyComment,
    "modules/media_img_comment": MediaImgComment,
    "modules/media_doc_comment": MediaDocComment,

    // MODE=MILESTONE
    "modules/habilete_comment": HabileteComment,
    "modules/savoir_etre_comment": SavoirEtreComment,
    "modules/profil_entree_comment": ProfilEntreeComment,

    // UQAR
    "modules/facets_comment": FacetsComment,

    "ui/icons": Icons, // Overwriting Quill modules/icons
    "themes/certification": CertificationTheme, // continuous: UQAM, UQAR
    "themes/cemp": CEMPTheme // milestone: CEMP
  },
  // Suppress warning(s) (Default: False)
  true
);
