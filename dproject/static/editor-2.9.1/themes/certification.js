/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/*
 * Creates custom theme overriding snow theme.
 *
 * References;
 *   - https://api.jquery.com/jquery.extend/
 */

const Snow = Quill.import("themes/snow");

let toolbarOptions = [
  [{ "list": "ordered" }, { "list": "bullet" }],
  ["link", "video"],
  // Custom formats
  ["media_img", "media_doc"],
  ["simple_comment"],
  // TODO UQAR REMOVED
  // ["competency", "associated_skill"],
  ["facets"],
  ["knowledge", "soft_skill"],
  ["vocabulary", "explain"],
  // TODO UQAR REMOVED
  // ["expertise_level"],
  ["clean"]
];

const defaultQuillFormats = [
  'list', 'link', 'video', 'image',
  'media_img', 'media_doc',
  'simple_comment',
  // TODO UQAR REMOVED
  // 'competency', 'associated_skill',
  'facets',
  'knowledge', 'soft_skill',
  'vocabulary', 'explain',
  // TODO UQAR REMOVED
  // 'expertise_level',
  'clean', 'new_text'
]


class CertificationTheme extends Snow {
  constructor(quill, options) {
    if (
      options.modules.toolbar != null &&
      options.modules.toolbar.container == null
    ) {
      options.modules.toolbar.container = toolbarOptions;
    }
    super(quill, options);
    this.quill.container.classList.add("ql-certification");
  }
}

CertificationTheme.DEFAULTS = $.extend(true, {}, Snow.DEFAULTS, {
  // default formats to allow (used to prevent copying invalid attributes from Word or other text editors)
  formats: defaultQuillFormats,
  modules: {
    associated_skill_comment: true,
    competency_comment: true,
    explain_comment: true,
    expertise_level_comment: true,
    knowledge_comment: true,
    simple_comment_comment: true,
    soft_skill_comment: true,
    vocabulary_comment: true,
    media_img_comment: true,
    media_doc_comment: true,
    // UQAR
    facets_comment: true
  }
});

export default CertificationTheme;
