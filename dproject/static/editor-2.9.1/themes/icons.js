/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/**
 * Merges all Quill default icons with new ones.
 */

let icons = Quill.import("ui/icons");

let svg_defs_file = "/static/icons/icons-" + iconsVersion + ".svg";

icons.list.ordered = `<svg class="icon ajouter-liste-numerote" width="40"><use xlink:href="${svg_defs_file}#ajouter-liste-numerote"></use></svg>`;
icons.list.bullet = `<svg class="icon ajouter-liste" width="40"><use xlink:href="${svg_defs_file}#ajouter-liste"></use></svg>`;

icons.link = `<svg class="icon ajouter-lien" width="40"><use xlink:href="${svg_defs_file}#ajouter-lien"></use></svg>`;
// icons.image = `<svg class="icon ajouter-photo" width="40"><use xlink:href="${svg_defs_file}#ajouter-photo"></use></svg>`;
icons.video = `<svg class="icon ajouter-video" width="40"><use xlink:href="${svg_defs_file}#ajouter-video"></use></svg>`;

icons.media_img = `<svg class="icon ajouter-photo" width="40"><use xlink:href="${svg_defs_file}#ajouter-photo"></use></svg>`;
icons.media_doc = `<svg class="icon document" width="40"><use xlink:href="${svg_defs_file}#document"></use></svg>`;

icons.simple_comment = `<svg class="icon ajouter-commentaire" width="40"><use xlink:href="${svg_defs_file}#ajouter-commentaire"></use></svg>`;
icons.spellcheck = `<svg class="icon spellcheck-orange-outlined" width="40"><use xlink:href="${svg_defs_file}#spellcheck-orange-outlined"></use></svg>`;

icons.competency = `<svg class="icon etiquette-competence-cle" width="40"><use xlink:href="${svg_defs_file}#etiquette-competence-cle"></use></svg>`;
icons.associated_skill = `<svg class="icon etiquette-competence-associe" width="40"><use xlink:href="${svg_defs_file}#etiquette-competence-associe"></use></svg>`;

icons.knowledge = `<svg class="icon etiquette-savoir" width="40"><use xlink:href="${svg_defs_file}#etiquette-savoir"></use></svg>`;
icons.soft_skill = `<svg class="icon etiquette-savoir-etre" width="40"><use xlink:href="${svg_defs_file}#etiquette-savoir-etre"></use></svg>`;

icons.vocabulary = `<svg class="icon etiquette-glossaire" width="40"><use xlink:href="${svg_defs_file}#etiquette-glossaire"></use></svg>`;
icons.explain = `<svg class="icon etiquette-zone-explicitation" width="40"><use xlink:href="${svg_defs_file}#etiquette-zone-explicitation"></use></svg>`;

icons.expertise_level = `<svg class="icon etiquette-niveaux-expertise" width="40"><use xlink:href="${svg_defs_file}#etiquette-niveaux-expertise"></use></svg>`;

// icons.clean = `<svg class="icon ajouter-texte" width="40"><use xlink:href="${svg_defs_file}#ajouter-texte"></use></svg>`;
// NEW icon for 'clean-formatting'
icons.clean = `<svg class="icon clean-formatting" width="35"><use xlink:href="${svg_defs_file}#clean-formatting"></use></svg>`

// MODE=MILESTONE manually specify those icons (reuse hard-coded existing ones)
if (is_milestone) {
  icons.habilete = icons.knowledge
  icons.savoir_etre = icons.soft_skill
  icons.profil_entree = icons.explain
}

// UQAR
icons.facets = icons.expertise_level

export default icons;
