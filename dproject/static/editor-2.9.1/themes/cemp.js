/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/*
 * Creates custom theme overriding snow theme.
 *
 * References;
 *   - https://api.jquery.com/jquery.extend/
 */

const Snow = Quill.import("themes/snow");

var toolbarOptions = [
  ["bold", { "list": "ordered" }, { "list": "bullet" }],
  ["link", "video"],
  // Custom formats
  ["media_img"],
  ["simple_comment", "vocabulary", "spellcheck"],
];
var qlCleanOption = ["clean"]

const defaultQuillFormats = [
  'bold',
  'list', 'link', 'video', 'image',
  'media_img',
  'simple_comment',
  // 'competency',
  'vocabulary', 'spellcheck',
  'habilete', 'savoir_etre',
  'profil_entree',
  'clean', 'new_text'
]


class CEMPTheme extends Snow {
  constructor(quill, options) {
    let blockId = quill.container.dataset.block_id
    // e.g. "habilete,savoir_etre"
    let editorOpts = quill.container.dataset.editor_opts

    // DEBUG
    // console.info(`The Constructor`, blockId)
    // console.info(quill.container.dataset)

    if (
      options.modules.toolbar != null &&
      options.modules.toolbar.container == null
    ) {
      options.modules.toolbar.container = [
        ...toolbarOptions,
        // [""] causes blank space in Quill w/ ql-<missing> class, while [] completely eliminates DOM object
        editorOpts ? editorOpts.split(",") : [],
        qlCleanOption
      ];
    }
    super(quill, options);
    this.quill.container.classList.add("ql-certification");
  }
}

CEMPTheme.DEFAULTS = $.extend(true, {}, Snow.DEFAULTS, {
  formats: defaultQuillFormats,
  modules: {
    // associated_skill_comment: true,
    // competency_comment: true,
    explain_comment: true,
    // expertise_level_comment: true,
    // knowledge_comment: true,
    simple_comment_comment: true,
    // soft_skill_comment: true,
    vocabulary_comment: true,
    spellcheck_comment: true,
    media_img_comment: true,
    media_doc_comment: true,
    // MODE=MILESTONE
    habilete_comment: true,
    savoir_etre_comment: true,
    profil_entree_comment: true
  }
});

export default CEMPTheme;
