/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/* Variables declarations */

// Global variables for Quill version editors
let quill_version_am;
let quill_version_ac;
let quill_version_ad;

function setEditorSize() {
    const versionEditor = $("#version_editor");
    versionEditor.css("height", "auto");
    versionEditor.css("width", ("auto"));
    const editorObject = $("#editor");
    editorObject.css("height", "auto");
    editorObject.css("width", ("auto"));
}


$(document).ready((event) => {

    /** Editor setup */

    /** New Quill Editor for version development tab */
    quill_version_ad = new Quill("#version_editor", {
        modules: {
            toolbar: false,
        },
        theme: "certification",
        bounds: "#version_editor"
    });


    /** New Quill for version motivation tab */
    quill_version_am = new Quill("#version_editor_am", {
        modules: {
            toolbar: false,
        },
        theme: "certification",
        bounds: "#version_editor_am",
    });

    /** New Quill for version context tab */
    quill_version_ac = new Quill("#version_editor_ac", {
        modules: {
            toolbar: false,
        },
        theme: "certification",
        bounds: "#version_editor_ac"
    });

    /** Set version editors contents and count words */
    if (versionDevelopmentContents) {
        quill_version_ad.setContents(versionDevelopmentContents, "api");
        editorText = quill_version_ad.getText();
        editorText = editorText.trim();
        $("#version_counter")[0].textContent = editorText.split(/\s+/).length + gettext(" words (target: ") + "1200-1500)";
    }
    if (versionMotivationContents) {
        quill_version_am.setContents(versionMotivationContents, "api");
        editorText = quill_version_am.getText();
        editorText = editorText.trim();
        $("#version_counter_am")[0].textContent = editorText.split(/\s+/).length + gettext(" words (target: ") + "80-100)";
    }
    if (versionContextContents) {
        quill_version_ac.setContents(versionContextContents, "api");
        editorText = quill_version_ac.getText();
        editorText = editorText.trim();
        $("#version_counter_ac")[0].textContent = editorText.split(/\s+/).length + gettext(" words (target: ") + "100-120)";
    }
});
