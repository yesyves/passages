/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Delta = Quill.import('delta') // https://github.com/quilljs/quill/issues/1573

/**
 * Global variables for Quill editors
 *
 * var => registered in window[]
 */

// CONTINUOUS: individual Quill editors for 3 main tabs
// let quillMotivation;
// let quillContext;
// let quillDevelopment;
let editorText;

// array of all the active quill editors in current achievement
let quills = [];

const tooltips = {
    associated_skill: gettext("Associated skill"),
    clean: gettext("Clear formatting /\nRemove attributes"),
    competency: gettext("Skill"),
    expertise_level: gettext("Expertise level"),
    explain: gettext("Explicitation zone"),
    knowledge: gettext("Knowledge"),
    link: gettext("Insert an URL"),
    media_doc: gettext("Create a link to a document"),
    media_img: gettext("Insert an image"),
    simple_comment: gettext("Comment"),
    spellcheck: gettext("Spell check"),
    soft_skill: gettext("Soft-Skill"),
    video: gettext("Insert a video"),
    vocabulary: gettext("References"),
    // MILESTONE
    habilete: gettext("Ability"),
    savoir_etre: gettext("Soft-Skill"),
    profil_entree: gettext("Entry Profile")
};


/**
 * Achievement modifications
 */

function renameAchievement() {
    // force achievement title to be filled in
    if (!validateAchievementName('#renameAchievementNameInput'))
        return false

    const achievementName = $("#renameAchievementNameInput").val();
    // const dest = "/portfolio/rename_achievement";
    $.post("/portfolio/rename_achievement", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        achievement_id: achievementId,
        achievement_name: achievementName
    }, (response) => {
        if (response.success) {
            $("#achievementName").text(achievementName);
        }
        // location.assign("/editor/" + achievementId + "/#resume");
        $("#renameAchievementModal").modal("hide");
    });
}


function setTargetedSkill() {
    const targetedSkill = $("#id_targeted_skill").val();
    // const dest = "/portfolio/set_targeted_skill";
    $.post("/portfolio/set_targeted_skill", {
        csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
        achievement_id: achievementId,
        targeted_skill: targetedSkill
    }, (response) => {
        if (response.success) {
            if (targetedSkill === "NONE") {
                $("#targetedSkill").text(gettext("None"));
            } else {
                for (const element of competencies) {
                    if (element.pk === targetedSkill) {
                        $("#targetedSkill").text(element.fields.name);
                        break;
                    }
                }
            }
        }
    });
}


/**
 * Achievement save
 */

// Track state changes
// let contextChanged = false;
// let developmentChanged = false;
// let motivationChanged = false;
// let developmentSaved = false;
let emptyAutosave = 0;
let saveCounter = 0;

// generic var to indicate if contents of the quill editor blocks has changed (given multiple blocks per achievement);
let contentsChanged = {}; // boolean for each editor block to detect those whose content was changed
let contentsSaved = false;


function undoLastSave() {
    if (confirm(gettext(
        "Are you sure?\n\n" +
        "Clicking this button means you lost a big chunk of text even after reloading the page.\n\n" +
        "Go on?"
    ))) {
        location.href = "/editor/" + achievementId + "/undo"
    }
}

/**
 * Works for both milestone & continuous modes
 * @param blockId
 */
function confirmSave(blockId) {
    // const confirmObject = (blockId && is_milestone)
    const confirmObject = blockId
        ? $(`#confirmSave-${blockId}`)
        : $(`[id^="confirmSave"]`);

    confirmObject.fadeIn();
    confirmObject.css("display", "block");
    confirmObject.fadeOut(3000);
    resetCount();

    // styling works a bit differently for milestone (multiple editor blocks) vs continuous (single editor block)
    // if (is_milestone)
    //     confirmObject.css("display", "block");
    // else
    //     confirmObject.css("visibility", "visible");
    // confirmObject.fadeOut(3000);
}


function failSave(error, insert, blockId) {
    // const errorBlock = is_milestone ? $(`#fail-error-${blockId}`) : $("#fail-error")
    // const insertBlock = is_milestone ? $(`#fail-insert-${blockId}`) : $("#fail-insert")
    // const failObject = is_milestone ? $(`#failSave-${blockId}`) : $("#failSave")

    const errorBlock = $(`#fail-error-${blockId}`)
    const insertBlock = $(`#fail-insert-${blockId}`)
    const failObject = $(`#failSave-${blockId}`)

    errorBlock.text(error);
    insertBlock[0].innerHTML = insert.replace(/\n/, "<br>");
    failObject.fadeIn();
    failObject.css("display", "block")

    // if (is_milestone)
    //     failObject.css("display", "block")
    // else
    //     failObject.css("visibility", "visible");
}

/**
 * Handle network error (403, 500, 404, etc.) from AJAX operation in achievement
 */
function failSaveNetworkError(statusCode, errMsg, blockId) {
    // DEBUG
    // console.info(`${statusCode} :: ${errMsg}`)
    // const failObject = is_milestone
    //   ? $(`#failSaveNetworkError-${blockId}`)
    //   : $("#failSaveNetworkError")

    const failObject = $(`#failSaveNetworkError-${blockId}`);
    failObject.fadeIn()

    let achievementLink = "/editor/" + achievementId;
    if (is_milestone) {
        achievementLink += "/3/" + activePortfolio;
    }

    failObject.css("display", "block")

    // if (is_milestone)
    //     failObject.css("display", "block")
    // else
    //     failObject.css("visibility", "visible")

    if (statusCode !== 500) {
        // Refresh the page to force authentification, after a delay of 3 seconds to let user see the error message
        setTimeout(function(){
            location.reload();
        }, 3000); // 3000 milliseconds = 3 seconds
    }
}


// Start counter : Trigger submit_achievement every 4 minutes
function startCount() {
    saveCounter = setInterval(() => {
        submitAchievement(true)
    }, 240000);
}


// Reset and restart counter
function resetCount() {
    clearInterval(saveCounter);
    startCount();
}

/**
 * Parent function dispatching to sub-function depending on app mode
 * @param autoSave
 */
function submitAchievement(autoSave = false) {
    if (is_milestone)
        _submitAchievementMilestone(autoSave)
    else
        _submitAchievementContinuous(autoSave)
}

function _submitAchievementMilestone(autoSave = false) {
    // extract editor contents, checking if any block contents were modified
    let blockContents = {}
    quills.forEach((quill) => {
        let _dataset = quill.container.dataset
        let blockId = _dataset.block_id
        let milestone = _dataset.milestone // aka 'tab'
        if (contentsChanged[blockId]) {
            blockContents[blockId] = {
                milestone: milestone, // send milestone number for this block along w/ contents of the quill editor
                quill: quill.getContents()
            }
        }
    })
    // DEBUG
    // console.info(blockContents)
    // console.info(JSON.stringify(blockContents))

    // ONLY send if at least 1 editor's content was modified
    if (Object.keys(blockContents).length > 0) {
        // Reset tracker
        emptyAutosave = 0;

        $.post("/editor/submit_achievement", {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            achievement_id: achievementId,
            portfolio_id: activePortfolio,
            contents: JSON.stringify(blockContents)
        }, (response) => {
            // DEBUG
            // console.info(response)

            // iterate over response per editor block
            for (const blockId in response.blocks) {
                const blockContent = response.blocks[blockId]
                const quillEditor = quills.find((quill) => quill.container.dataset.block_id === blockId)

                if (blockContent && quillEditor) {
                    // ONLY if error will 'success' exist and be 'false'
                    if (blockContent.success === false) {
                        if (typeof blockContent.insert === "string") {
                            // Achievement was processed and an error was found in one of the tags
                            failSave(blockContent.error, blockContent.insert, blockId);
                        } else {
                            // Something went wrong along the way, most probably a 302
                            failSaveNetworkError(
                                1,
                                gettext("Save has failed! Please make sure you are logged in!"),
                                blockId
                            )
                        }
                    } else {
                        const newContents = JSON.parse(blockContent)
                        quillEditor.setContents(newContents, "api")

                        // Tell user save was successful
                        confirmSave(blockId);
                    }
                } else {
                    // means this block was successfully saved
                    confirmSave(blockId)
                }
            }

            // Reset states
            contentsChanged = {};
            contentsSaved = true
        })
            .fail(function (xhr, status, error) {
                // Request raised an exception at server level
                failSaveNetworkError(xhr.status, error)
            });
    } else if (autoSave) {
        // Update tracker
        // If it is being updated for the 3rd time (12 minutes since the last change) : release achievement
        if (userType !== "Expert" && emptyAutosave++ > 1) {
            $.post("/editor/release_achievement", {
                csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
                achievement_id: achievementId
            }, (response) => {
                window.location.href = "/portfolio/";
            });
        }
    } else {
        // Nothing new but tell user save was successful anyway
        confirmSave();
    }
}

function _submitAchievementContinuous(autoSave = false) {
    // extract editor contents, checking if any block contents were modified
    let blockContents = {}
    quills.forEach((quill) => {
        let _dataset = quill.container.dataset

        console.info(`submit-achievement-continuous`, _dataset)

        let blockId = _dataset.block_id
        let tab = _dataset.tabNumber
        if (contentsChanged[blockId]) {
            blockContents[blockId] = {
                tab: tab,
                quill: quill.getContents(),
            }
        }
    })

    if (Object.keys(blockContents).length > 0) {
        emptyAutosave = 0;

        $.post("/editor/submit_achievement", {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            achievement_id: achievementId,
            portfolio_id: activePortfolio,
            contents: JSON.stringify(blockContents)
        }, (response) => {
            for (const blockId in response.blocks) {
                const blockContent = response.blocks[blockId]
                const quillEditor = quills.find((quill) => quill.container.dataset.block_id === blockId)

                if (blockContent && quillEditor) {
                    // ONLY if error will 'success' exist and be 'false'
                    if (blockContent.success === false) {
                        if (typeof blockContent.insert === "string") {
                            // achievement was processed and error was found in one of the tags
                            failSave(blockContent.error, blockContent.insert, blockId);
                        } else {
                            // something went wrong along the way, most probably a 302
                            failSaveNetworkError(
                                1,
                                gettext("Save has failed! Please make sure you are logged in!"),
                                blockId
                            )
                        }
                    } else {
                        const newContents = JSON.parse(blockContent)
                        quillEditor.setContents(newContents, "api")
                        // tell user save was successful
                        confirmSave(blockId)
                    }
                } else {
                    // means this block was successfully saved
                    confirmSave(blockId)
                }
            }

            // reset states
            contentsChanged = {};
            contentsSaved = true;
        }).fail(function (xhr, status, error) {
            // request raised an exception at server level
            failSaveNetworkError(xhr.status, error)
        });
    } else if (autoSave) {
        // Update tracker
        // If it is being updated for the 3rd time (12 minutes since the last change) : release achievement
        if (userType !== "Expert" && emptyAutosave++ > 1) {
            $.post("/editor/release_achievement", {
                csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
                achievement_id: achievementId
            }, (response) => {
                window.location.href = "/portfolio/";
            });
        }
    } else {
        // Nothing new but tell user save was successful anyway
        confirmSave();
    }
}

function _submitAchievementContinuousOld(autoSave = false) {
    if (motivationChanged || contextChanged || developmentChanged) {
        // Reset tracker
        emptyAutosave = 0;
        // Update contents
        if (motivationChanged) {
            motivationContents = JSON.stringify(quillMotivation.getContents());
        } else {
            motivationContents = "";
        }
        if (contextChanged) {
            contextContents = JSON.stringify(quillContext.getContents());
        } else {
            contextContents = "";
        }
        if (developmentChanged) {
            developmentContents = JSON.stringify(quillDevelopment.getContents());
            developmentSaved = true;
        } else {
            developmentContents = "";
        }

        $.post("/editor/submit_achievement", {
            csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
            achievement_id: achievementId,
            motivation_contents: motivationContents,
            context_contents: contextContents,
            development_contents: developmentContents
        }, (response) => {
            // reload page if content modified by parse_editor_contents()
            if (response.motivation_contents) {
                quillMotivation.setContents(JSON.parse(response.motivation_contents), "api")
            }
            if (response.context_contents) {
                quillContext.setContents(JSON.parse(response.context_contents), "api")
            }
            if (response.success) {
                // Tell user save was successful
                confirmSave();

                // reload page if content modified by parse_editor_contents()
                if (response.development_contents) {
                    quillDevelopment.setContents(JSON.parse(response.development_contents), "api")
                }
            } else {
                if (typeof response.insert === "string") {
                    // Achievement was processed and an error was found in one of the tags
                    failSave(response.error, response.insert);
                } else {
                    // Something went wrong along the way, most probably a 302
                    failSaveNetworkError(1, gettext("Save has failed! Please make sure you are logged in!"))
                }

            }
            // Reset states
            motivationChanged = false;
            contextChanged = false;
            developmentChanged = false;
        })
            .fail(function (xhr, status, error) {
                // Request raised an exception at server level
                failSaveNetworkError(xhr.status, error)
            });
    } else if (autoSave) {
        // Update tracker
        // If it is being updated for the 3rd time (12 minutes since the last change) : release achievement
        if (userType !== "Expert" && emptyAutosave++ > 1) {
            $.post("/editor/release_achievement", {
                csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
                achievement_id: achievementId
            }, (response) => {
                window.location.href = "/portfolio/";
            });
        }
    } else {
        // Nothing new but tell user save was successful anyway
        confirmSave();
    }
}


/**
 * Image handler
 */

// regex expression defining an image based on file extension
// https://www.geeksforgeeks.org/how-to-validate-image-file-extension-using-regular-expression/
// Make it case-insensitive
var IMG_REGEX = /\S.(jpe?g|png|gif|bmp|tiff?)$/i

// given a URL w/ file extension, determine if an image or not
function isImg(url) {
    return IMG_REGEX.test(url)
}


/**
 * Tag handling
 *
 * Triggers on page load to (automatically) directly access the tag from the views.
 *
 * Uses the URL's search terms to pass the ID of the tag & the class of the tag
 *  so we can then scroll directly to that place and highlight the text of interest.
 *
 * @sources
 *  - https://getbootstrap.com/docs/4.0/components/navs/
 *  - https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes
 *  - https://stackoverflow.com/questions/12102118/scrollintoview-animation
 */

function onLoadSpecificTag() {
    // if NO search terms, just load the page as usual
    if (!location.search)
        return;

    // get the search terms (e.g. URLSearchParams { tag → "1672252858849", cls → "ql-competency" })
    const searchTerms = new URLSearchParams(location.search)
    const tag = searchTerms.get("tag") // e.g. tag=<id>
    const cls = searchTerms.get("cls") // e.g. cls=<cls-name>
    const tab = searchTerms.get("tab") // e.g. tab=<number> -- OPTIONAL, both milestone & continuous

    // if tag is provided, then dynamically switch tab and scroll to specific use of tag in Quill editors
    if (tag) {
        showSpecificTag(tag, cls, tab);
    }
    // when ONLY milestone is provided, we need to ONLY switch tabs
    else if (tab) {
        // DEBUG
        // console.info(`milestone`, milestone)
        $(`#nav-tab-development-${tab}`).tab("show")
    }
}

/**
 *
 * @param _tag - e.g. 1672252858849 -> string|null
 * @param _cls - e.g. ql-competency -> string|null
 * @param _tab - e.g. 2 -> number (corresponds to tabs) - milestone or achievement type
 */
function showSpecificTag(_tag, _cls, _tab) {
    // Remove previous highlight
    $(".editor-highlight").removeClass("editor-highlight");

    // programmatically activate "Development / Déroulement" tab
    $(`#nav-tab-development-${_tab}`).tab("show")

    // find all <span> tags w/ classname specified by 'cls' to avoid duplicate IDs in different classes
    $(".editor").find(`span[class^=${_cls ?? "ql-"}]`).each((key, dom_elem) => {

        // just a safety: if no "data-value" attribute, skip; else undefined/null errors
        if (!dom_elem.dataset.value)
            return

        const val = JSON.parse(dom_elem.dataset.value);
        // console.info(val)

        // console.info(dom_elem.classList)

        // if the "id" from "data-value" matches the ID passed via the "tag" search term, ...
        // NOTE: in 'milestone', _1stTerm may be '1646339162097@3', but parseInt properly ignores the '@3' part
        if (val.id === parseInt(_tag)) {
            // console.info(dom_elem)

            // "editor-highlight" adds bigger font + bold styling to anchor we want to focus on;
            //  "transition" is optional in case we want to smoothly return to original style after X seconds
            dom_elem.style.transition = "all 2s";
            dom_elem.classList.add("editor-highlight");
            // console.info(dom_elem)

            // OPTIONAL: after X milliseconds, remove class to return to normal styling of the anchored tag
            // setTimeout(() => {
            //   dom_elem.classList.remove("editor-highlight")
            // }, 3000);

            // wait .5 second before smoothly scrolling to the anchor specified by "tag"; wait is important
            // to ensure proper scrolling, else scroll does not work because something else not yet initialized
            // NOTE: behavior: "smooth" does NOT work on Safari, but on all others, yes
            setTimeout(() => {
                dom_elem.scrollIntoView({
                    behavior: "smooth",
                    block: "center"
                })
            }, 500);
        }
    });
}


/**
 * Summary update
 *
 * When the "Summary" tab is clicked, we want to refresh the contents of "Summary" so that it
 *  reflects the latest changes just done in "Development". Otherwise, we would have to refresh
 *  the page manually or automatically.
 * This uses AJAX to re-fetch the current page (e.g. /editor/<guid>). Then we parse the
 *  returned HTML, extract the section related to "Summary", and overwrite the HTML of
 *  the current "Summary" section. This is quite fast and does not visibly affect performance
 *  or UX.
 *
 * @sources:
 *  - https://gomakethings.com/getting-html-with-fetch-in-vanilla-js/
 */
function onTabSummaryOpened() {
    $("#nav-tab-resume").click(function (e) {
        // console.info("Reload "Summary" ops ...")
        fetch(location.pathname)
            // this returns the response contents as raw text (text/html)
            .then(function (r) {
                return r.text()
            })
            // parse HTML as DOM object
            .then(function (html) {
                let parser = new DOMParser()
                let doc = parser.parseFromString(html, "text/html");

                // extract HTML section from "Summary" id
                let summary = doc.querySelector("#nav-tab-pane-resume")

                // overwrite inner html of current "Summary" section
                document.getElementById("nav-tab-pane-resume").innerHTML = summary.innerHTML
            })
    })
}

/**
 * Listen for clicks on 4 tabs (Relevance, Key, Dev, Summary) so we can append the #<id> as a URL query param
 * to then auto-select proper tab on page reload
 */
function handleTabsClickAndReload() {
    // on clicking tabs, add query params
    $(".nav-tabs a").on("click", function (e) {
        history.replaceState({}, "", `?editor-tab=${e.target.id}`)
    })

    // try to get the query param '?editor-tab=XXX` from current URL
    const activeTab = new URLSearchParams(location.search).get('editor-tab')
    if (!activeTab)
        return

    // programmatically activate tab indicated in search params
    $(`#${activeTab}`).tab("show");
}


/**
 * Quill Monitoring
 *
 * Helper function to add Ctrl+S binding to a Quill editor
 * @source
 *  - https://quilljs.com/docs/modules/keyboard/
 * @param quillObj
 */
function quillAddCtrlSaveBinding(quillObj) {
    quillObj.keyboard.addBinding({
        key: "s",
        shortKey: true
    }, function (range, context) {
        submitAchievement();
    });
}


function quillException(e) {
    console.error("ERROR formatting text", e);
    alert(gettext(
        "Warning!\n" +
        "This operation has deleted only part of the tag.\n" +
        "Please make sure to delete the other parts as well " +
        "in order to avoid undesirable effects."
    ));
    // $.post("/utils/notify_exception", {
    //     csrfmiddlewaretoken: $("[name='csrfmiddlewaretoken']").val(),
    //     subject: "Quill exception",
    //     event: e,
    //     link: "/editor/" + achievementId
    // });
}


/**
 * Given a Quill instance, apply format 'new_text' to current selection taking into account
 * NOT to re-apply same format if already in 'new_text' range
 *
 * This works great, BUT we really need to rely on `selection-change` event to trigger `.format`
 *  - https://quilljs.com/docs/api/#selection-change
 *  - https://quilljs.com/docs/api/#formatting
 *  - https://stackoverflow.com/questions/62955576/ngx-quill-set-default-format-for-editor
 *  - https://github.com/KillerCodeMonkey/ngx-quill/issues/971
 *  - BUG: https://github.com/quilljs/quill/issues/2197
 *  - http://billauer.co.il/blog/2021/12/quill-cursor-jump/
 *
 * @param quill
 */
function quillApplyFormat_NewText(quill) {
    // DEBUG
    // window.prev_selection = Date.now()
    quill.on('selection-change', function (range, oldRange, source) {
        // DEBUG
        // console.info(`selection-change`, {
        //     time: Date.now() - window.prev_selection,
        //     current: range?.index,
        //     previous: oldRange?.index,
        //     source
        // })
        if (range === null) {
            return
        }

        const currentFormat = quill.getFormat()
        const mayApplyNewText = range?.length === 0 && !currentFormat.new_text

        if (mayApplyNewText) {
            quill.format('new_text', 'true')
        }
        // DEBUG
        // window.prev_selection = Date.now()
    })
}


/**
 * Set proper height for Quill editors
 * NOTE: if `quillEditor` object is passed, this will set size ONLY for this specific editor (mostly used in MILESTONE)
 * @param quillEditor - jQuery object (e.g. $(quillWrapper))
 */
function setEditorSize(quillEditor) {
    // noinspection JSJQueryEfficiency
    // .editor is generic for all instances of Quill editors, both CONTINUOUS and MILESTONE
    const oEditors = quillEditor || $(".editor");

    // in CONTINUOUS mode, resizing worked find since ONLY 1 Quill editor per page
    if (is_continuous) {
        // previous method of resizing editor area dynamically; however, empty Quills were very big (wasted space)
        oEditors.css("height", (($(window).height() - 355) + "px"));
    }
    // in MILESTONE mode, many Quill editors, so need to make empty ones smaller and rest auto-adjust properly
    else {
        // NOTE: `this` refers to the Quill editor content from `editorObjects`
        const maxHeight = $(window).height() - 355
        oEditors.css("height", function (idx, val) {
            let totalHeight = 0;
            this.querySelectorAll("p").forEach((e) => {
                totalHeight += e.offsetHeight
            })
            return totalHeight > maxHeight ? maxHeight : totalHeight + 75
        })
    }

    // todo this is not used for now since CSS forces 'width' back to 'auto' in certification.css:49
    if ($(window).width() >= 800) {
        oEditors.css("width", (($(window).width() - $("#side-nav-bar").width() - 60) + "px"));
    } else {
        oEditors.css("width", "500");
    }
}

/**
 * Handle the 'Enter' keypress (code 13) to prevent its default newline behaviour and instead
 * do same as TAB and navigate to next <textarea> and then to 'Edit'/'Define' `.ql-action` button.
 */
function qlTooltipReturnKeypressHandler() {
    // listen for keypress anywhere inside .ql-tooltip
    $(".ql-tooltip").on("keypress", (e) => {
        // 13 = ENTER key, BUT allow `Shift + Enter` for newline
        if (e.keyCode === 13 && !e.shiftKey) {
            // prevent 'newline'
            e.preventDefault()

            // get parent (i.e. `.ql-tooltip`)
            const parent = $(e.currentTarget)
            // get the next <textarea> that is NOT the current/first <textarea> instance
            const nextInput = parent.find("textarea:not(:first-child)")

            // NO nextInput : ONLY 1 <textarea> in the tooltip
            // nextInput === e.target : last <textarea> in tooltip
            if (!nextInput[0] || nextInput[0] === e.target) {
                // get the <a> action button (either Edit or Define)
                const actionBtn = parent.find("a.ql-action")
                // click the button if it exists (to avoid undefined errors)
                if (actionBtn[0])
                    actionBtn[0].click()
            }
            // if 1st <textarea> AND nextInput available, focus it
            else {
                nextInput.focus()
            }
        }
    })
}

/**
 * MODE=MILESTONE
 * Allow toggling explanation for Quill editor block
 */
function handleBlockExplanationToggling() {
    $("a.toggle-explanation").on("click", function (e) {
        e.preventDefault()
        // DEBUG
        // console.info(e.currentTarget.dataset)
        const blockId = e.currentTarget.dataset.blockId
        $(`#explanation-${blockId}`).toggle()
    })
}

/**
 * Handles loading of example portfolio question ID (EditorBlock) via query string
 *
 * e.g. /editor/ce038020-49e4-4033-8c09-5241b65b43c9/0/f7a55edc-6e64-4857-a162-3d40a00e5ef9#editor-15
 *  - /editor/<achievement-id>/<mode>/<portfolio-id>#<editor-id>
 *  - take the # part and smoothly scroll it into view (if available)
 */
function highlightExamplePortfolioQuestion() {
    // e.g. #editor-15
    const editorBlockId = location.hash
    if (!editorBlockId)
        return

    const instructionDom = document
        .querySelector(`[data-qinstruct-id="${editorBlockId}"]`)

    // add class to highlight the question being viewed in the example
    instructionDom.style.transition = "all 2s";
    instructionDom.classList.add("question-highlight");

    // OPTIONAL: after X milliseconds, remove class to return to normal styling of the anchored tag
    setTimeout(() => {
        instructionDom.classList.remove("question-highlight")
    }, 3000);

    // wait .5 second before smoothly scrolling to the editor block; wait is important to ensure proper scrolling,
    // else scroll does not work because something else not yet initialized
    // NOTE: behavior: "smooth" does NOT work on Safari, but on all others, yes
    setTimeout(() => {
        document.querySelector(editorBlockId).scrollIntoView({
            behavior: "smooth",
            block: "center"
        })
    }, 500);
}

/**
 * Helper function to generate an <option> child of <select>
 * @param value
 * @param txt
 * @returns {HTMLOptionElement}
 */
function generateOption(value, txt) {
    const option = document.createElement("option")
    option.value = value
    option.text = txt;
    return option
}


/**
 * Setup + Listen
 */
$(document).ready((event) => {

    /****************
     * Editor setup *
     ****************/

    /**
     * Automatically load all Quill editor block wrappers and then initiate QuillJS on each
     * editor <div>, storing the resulting Quill instance in an array so we can later reference
     * the Quills by block ID for saving/editing.
     * @type {NodeListOf<Element>}
     */
    let editorBlocks = document.querySelectorAll(".achievement-block-editor")
    editorBlocks.forEach((node) => {
        // DEBUG
        // console.info(node.dataset)
        let quillWrapper = node.querySelector(".editor")
        // prevent drag-&-drop of image in Firefox
        quillWrapper?.addEventListener("dragover", (e) => e.preventDefault());
        quillWrapper?.addEventListener("drop", (e) => e.preventDefault());

        let options = {
            modules: {
                toolbar: true,
                // https://quilljs.com/docs/modules/clipboard/#addmatcher
                clipboard: {
                    matchers: [
                        // when new text is pasted in quill, apply 'new_text' format to this
                        [Node.TEXT_NODE, function (node, delta) {
                            // DEBUG
                            // console.info("quilljs -> pasted text", { node, delta })
                            return delta.compose(new Delta().retain(delta.length(), {new_text: true}))
                        }]
                    ]
                }
            },
            theme: is_milestone ? "cemp" : "certification",
            bounds: `#${quillWrapper.id}`
        }

        let theQuill = new Quill(`#${quillWrapper.id}`, options)

        theQuill.on('text-change', function (delta, oldDelta, source) {
            setEditorSize($(quillWrapper))
        })

        quillApplyFormat_NewText(theQuill)

        quills.push(theQuill)
    })

    /**
     * Tooltips for editor_ad buttons
     *
     * @param el
     */

    const showTooltip = (el) => {
        const tool = el.className.replace("ql-", "");
        if (tooltips[tool]) {
            $(el).attr("title", tooltips[tool]);
        }
    }

    // CONTINUOUS and MILESTONE
    const toolbarElements = document.querySelectorAll(".ql-toolbar");
    if (toolbarElements.length > 0) {
        for (const toolbarElement of toolbarElements) {
            const matches = toolbarElement.querySelectorAll("button");
            for (const el of matches) {
                showTooltip(el);
            }
        }
    }

    /**
     * Set editors contents and count words
     */
    if (blockContents) {
        for (const blockId in blockContents) {
            const content = blockContents[blockId]
            const quillEditor = quills.find((quill) => quill.container.dataset.block_id === blockId)

            if (content && quillEditor) {
                // find the custom 'target words' expected for this editor block
                let targetWords = quillEditor.container.dataset.target_words

                // set the contents from the db
                quillEditor.setContents(JSON.parse(content), "api")

                // count words
                const editorText = quillEditor.getText().trim()
                $(`#counter-${blockId}`)[0].textContent =
                    editorText.split(/\s+/).length + gettext(" words (target: ") + `${targetWords})`;
            }
        }
    }

    /**
     * Set editor size
     */

    setEditorSize();
    $(window).on("resize", function () {
        setEditorSize();
    });


    /**********
     * Listen *
     **********/

    /**
     * Active tab monitoring
     */

    $(".nav-tabs a").on("shown.bs.tab", function (event) {
        // active_tab = $(event.target)[0].id;         // active tab
        // active_tab = active_tab.replace("nav-tab-", "#");
        // Hide #editorButtons when resume tab is active
        if ($(event.target)[0].id === "nav-tab-resume") {
            $("#editorButtons").hide();
        } else {
            $("#editorButtons").show();
        }

        // re-adjust proper Quill editor size when switching tabs; otherwise ONLY default-active tab is resized
        setEditorSize()
    });


    /**
     * Toggle switch
     */

    $("#myonoffswitch").change(function () {
        const tagSelector = $(".ql-editor [class^='ql-']:not(.ql-simple_comment)")
        if ($("#myonoffswitch").is(":checked")) {
            tagSelector.addClass("ql-tag-Normal")
        } else {
            tagSelector.removeClass("ql-tag-Normal")
        }
    });


    /**
     * Tag handling
     * deal w/ specific tag loaded via the URL (e.g ?tag=<tag-id>)
     */

    onLoadSpecificTag()

    // is_milestone && handleBlockExplanationToggling()
    handleBlockExplanationToggling()

    /**
     * MILESTONE: handle going to specific question of example portfolio;
     */
    if (is_milestone && document.querySelector("[data-portfolio-type='example']")) {
        highlightExamplePortfolioQuestion()
    }


    if (editable) {

        /**
         * start autosave
         */

        startCount();

        /**
         * Dynamic word count for all quill editors in the current achievement
         */
        quills.forEach((quill) => {
            quill.on("text-change", () => {
                // container is the HTML wrapper (<div>) which has custom data attributes
                let blockId = quill.container.dataset.block_id
                let targetWords = quill.container.dataset.target_words

                // track content changed status at editor block level; else we send all blocks even if not modified
                contentsChanged[blockId] = true

                let editorText = quill.getText().trim()

                // set word count + target word count dynamically from data attributes
                $(`#counter-${blockId}`)[0].textContent =
                    editorText.split(/\s+/).length + gettext(" words (target: ") + `${targetWords})`;
            })
        })

        /**
         * Dynamic word count
         */

        /**
         * Buttons monitoring
         */

        $("button#submitAchievementButton").on("click", (e) => {
            submitAchievement();
        });

        if (is_continuous) {
            $("#renameAchievement").on("click", (event) => {
                renameAchievement();
            });
            $("#submitVersionModal").on("show.bs.modal", function () {
                submitAchievement();
            });
        }


        /**
         * Keybindings in editors
         */
        quills.forEach((quill) => quillAddCtrlSaveBinding(quill))

        /**
         * Keybindings outside of editors
         */

        Mousetrap.unbind("mod+s");
        Mousetrap.bind("mod+s", function (e) {
            // this is required; otherwise, Ctrl+S triggers saving the current HTML page
            e.preventDefault();
            submitAchievement();
        });

        /**
         * Keybindings in modals
         */

        $(document).keypress((e) => {
            if ($("#renameAchievementModal").hasClass("show") && (e.keycode === 13 || e.which === 13)) {
                renameAchievement();
            }
        });

        // functions to execute when mode is 'continuous'
        if (is_continuous) {
            /**
             * listen to click events on tab "Summary"
             */
            onTabSummaryOpened()

            /**
             * Listen for click events on any of 4 tabs
             */
            handleTabsClickAndReload()
        }

        /**
         * Handle 'Enter' key in tag tooltips in Quill editor
         */
        qlTooltipReturnKeypressHandler();
    }
});


/**
 * Unload
 *
 * https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload
 * NOTE: custom text support is NOT supported, ONLY in Internet Explorer
 */

if (editable) {
    window.addEventListener("beforeunload", (event) => {
        if (Object.values(contentsChanged).length > 0) {
            event.preventDefault();
            event.returnValue = "";
        }
    });
}
