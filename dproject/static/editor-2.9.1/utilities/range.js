/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

class Range {
  constructor(index, length = 0) {
    this.index = index;
    this.length = length;
  }
}

export default Range;
