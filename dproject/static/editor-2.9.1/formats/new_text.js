/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class NewText extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // simple attribute, ONLY use boolean for value of "new_text" key in parent "attributes" object in op
    return super.create(true);
  }
}

NewText.blotName = "new_text";
NewText.tagName = "SPAN";
NewText.className = "ql-new_text";

export default NewText;
