/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const BlotBlockEmbed = Quill.import("blots/block/embed");

/**
 * NOTE: Using our custom format "media_img" caused problems when inserting the link and did not
 *  allow to click the link to open it like the normal link. So we are NOT using "media_img"
 *  specifically but rather "image" which works great.
 *  However, if we remove formats/media_img.js, the toolbar button stops working.
 */
class MediaImg extends BlotBlockEmbed {
  // Creates corresponding DOM node
  static create(imgUrl) {
    // create main node
    return super.create(imgUrl)
  }

  static getAttribute(domNode) {
    // console.info(domNode.getAttribute("src"))
    return JSON.parse(domNode.getAttribute("data-value"));
  }
}

MediaImg.blotName = "media_img";
MediaImg.className = "ql-media-img";
MediaImg.tagName = "IMG";

export default MediaImg;
