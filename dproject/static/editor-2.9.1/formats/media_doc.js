/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const BlotInline = Quill.import("blots/inline");

/**
 * NOTE: Using our custom format "media_doc" caused problems when inserting the link and did not
 *  allow to click the link to open it like the normal link. So we are NOT using "media_doc"
 *  specifically but rather "link" which works great.
 *  However, if we remove formats/media_doc.js, the toolbar button stops working.
 *
 * @sources
 *  - https://www.geeksforgeeks.org/how-to-validate-image-file-extension-using-regular-expression/
 */
class MediaDoc extends BlotInline {
  // Creates corresponding DOM node
  static create(value) {
    // create main node
    return super.create(value);
  }

  static getAttribute(domNode) {
    // console.info("DOM Node", domNode)
    return JSON.parse(domNode.getAttribute("data-value"));
  }
}

MediaDoc.blotName = "media_doc";
MediaDoc.className = "ql-media-doc";
MediaDoc.tagName = "A";

export default MediaDoc;
