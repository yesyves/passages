/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class Explain extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search("ql-explain") > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      value = value.attributes.explanation;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "explain",
      "class": "ql-explain",
      "attributes": {"explanation": value}
    });
  }

  static getAttribute(domNode) {
    const explain_tag = JSON.parse(domNode.getAttribute("data-value"));
    return explain_tag.attributes.explanation;
  }
}

Explain.blotName = "explain";
Explain.tagName = "SPAN";
Explain.className = "ql-explain";

export default Explain;
