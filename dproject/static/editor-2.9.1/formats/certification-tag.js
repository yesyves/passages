/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

/*
 * Creates custom format for certification tags.
 *
 * References;
 *   - https://github.com/quilljs/parchment
 *   - https://developer.mozilla.org/fr/docs/Web/API/HTMLElement/dataset
 */

const Inline = Quill.import("blots/inline");

class CertificationTag extends Inline {
  // Creates corresponding DOM node
  static create(value) {
    if (typeof value === "string") {
        value = JSON.parse(value);
    }
    const node = super.create(value);
    node.dataset.value = JSON.stringify(value); // value.value?
    return node;
  }

  /** Formattable blots only **/

  // Returns format values represented by domNode if it is this Blot"s type
  // No checking that domNode is this Blot"s type is required.
  static formats(domNode) {
    return domNode.dataset.value;
  }
  // Apply format to blot. Should not pass onto child or other blot.
  format(name, value) {
    super.format(name, value);
  }
}

export default CertificationTag;
