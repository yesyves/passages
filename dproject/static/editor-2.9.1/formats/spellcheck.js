/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class Spellcheck extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // NOTE: if saving the comment, value is the content of the textarea
    // Make the difference between json string and simple string
    let comment, date, author = null;
    let attribute_id;

    if (typeof value === "string" && value.search("ql-spellcheck") > 0) {
      let values = JSON.parse(value);
      attribute_id = values.id;
      comment = values.attributes.comment || "";
      date = values.attributes.date || "";
      author = values.attributes.author || "";
    } else {
      comment = value;
    }

    // NOTE: need to provide both "author" & "userName" to "author" field to properly save real author name;
    // otherwise, w/o userName, creating a simple comment may have author "null"
    return super.create({
      "id": attribute_id || Date.now(),
      "type": "spellcheck",
      "class": "ql-spellcheck",
      "attributes": {
        "comment": comment,
        "date": date || Date.now(),
        "author": author || userName
      }
    });
  }

  static getAttributes(domNode) {
    const spellcheck_tag = this.getDataValue(domNode);
    return spellcheck_tag.attributes;
  }

  static getComment(domNode) {
    const spellcheck_tag = this.getDataValue(domNode);
    return spellcheck_tag.attributes.comment;
  }

  static getAuthor(domNode) {
    const spellcheck_tag = this.getDataValue(domNode);
    return spellcheck_tag.attributes.author;
  }

  static getDate(domNode) {
    const spellcheck_tag = this.getDataValue(domNode);
    return spellcheck_tag.attributes.date;
  }

  static getDataValue(domNode) {
    return JSON.parse(domNode.getAttribute("data-value"));
  }
}

Spellcheck.blotName = "spellcheck";
Spellcheck.tagName = "SPAN";
Spellcheck.className = "ql-spellcheck";

export default Spellcheck;
