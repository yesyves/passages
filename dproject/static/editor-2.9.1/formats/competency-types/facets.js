/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "../certification-tag.js";

class Facets extends CertificationTag {
  // to avoid repeating the string several times in the same file
  static modName = "facets"

  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search(`ql-${this.modName}`) > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      value = value.attributes.competency_id;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": this.modName,
      "class": `ql-${this.modName}`,
      "attributes": {"competency_id": value}
    });
  }

  static getAttribute(domNode) {
      const competency_tag = JSON.parse(domNode.getAttribute("data-value"));
      return competency_tag.attributes.competency_id;
  }
}

Facets.blotName = Facets.modName;
Facets.tagName = "SPAN";
Facets.className = `ql-${Facets.modName}`;

export default Facets;
