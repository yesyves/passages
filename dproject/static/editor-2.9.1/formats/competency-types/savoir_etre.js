/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "../certification-tag.js";

class SavoirEtre extends CertificationTag {
  // to avoid repeating the string several times in the same file
  static modName = "savoir_etre"

  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;
    let competency_id;
    let level;

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search(`ql-${this.modName}`) > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      competency_id = value.attributes.competency_id;
      level = value.attributes.level;
    } else {
      competency_id = value.competency_id;
      level = value.level;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": this.modName,
      "class": `ql-${this.modName}`,
      "attributes": {
        "competency_id": competency_id,
        "level": level
      }
    });
  }

  static getAttribute(domNode) {
      const competency_tag = JSON.parse(domNode.getAttribute("data-value"));
      // return competency_tag.attributes.competency_id;
      return competency_tag.attributes;
  }
}

SavoirEtre.blotName = SavoirEtre.modName;
SavoirEtre.tagName = "SPAN";
SavoirEtre.className = `ql-${SavoirEtre.modName}`;

export default SavoirEtre;
