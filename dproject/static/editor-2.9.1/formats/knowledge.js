/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class Knowledge extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // Make the difference between srring and object
    let attribute_id;
    let attribute_name;
    let attribute_explanation;

    if (typeof value === "string") {
      value = JSON.parse(value);
      attribute_id = value.id;
      attribute_name = value.attributes.name;
      attribute_explanation = value.attributes.explanation;
    } else {
      attribute_name = value.name;
      attribute_explanation = value.explanation;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "knowledge",
      "class": "ql-knowledge",
      "attributes": {"name": attribute_name, "explanation": attribute_explanation}
    });
  }

  static getAttribute(domNode) {
      const knowledge_tag = JSON.parse(domNode.getAttribute("data-value"));
      return knowledge_tag.attributes;
  }
}

Knowledge.blotName = "knowledge";
Knowledge.tagName = "SPAN";
Knowledge.className = "ql-knowledge";

export default Knowledge;
