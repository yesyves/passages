/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class ExpertiseLevelBlot extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // Make the difference between srring and object
    let attribute_id;
    let attribute_level;
    let attribute_validation;

    if (typeof value === "string") {
      value = JSON.parse(value);
      attribute_id = value.id;
      attribute_level = value.attributes.level;
      attribute_validation = value.attributes.validation;
    } else {
      attribute_level = value.level;
      attribute_validation = value.validation;
    }

    const node = super.create({
      "id": attribute_id || Date.now(),
      "type": "expertise_level",
      "class": "ql-expertise_level",
      "attributes": {"level": attribute_level, "validation": attribute_validation}
    });

    // add CSS class depending on the sub-level of expertise
    node.classList.add((() => {
      let level = 0
      if (["A", "B", "C", "D"].includes(attribute_level))
        level = 1
      else if (["E", "F", "G", "M", "N", "O", "P", "Q", "R", "S"].includes(attribute_level))
        level = 2
      else if (["H", "I", "J"].includes(attribute_level))
        level = 3
      return `ql-expertise_level-${level}`
    })())

    return node
  }

  static getAttribute(domNode) {
    const expertise_level_tag = JSON.parse(domNode.getAttribute("data-value"));
    return expertise_level_tag.attributes;
  }
}

ExpertiseLevelBlot.blotName = "expertise_level";
ExpertiseLevelBlot.tagName = "SPAN";
ExpertiseLevelBlot.className = "ql-expertise_level";

export default ExpertiseLevelBlot;
