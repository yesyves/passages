/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class Competency extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;

    // DEBUG
    // console.info(`Competency::value`)

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search("ql-competency") > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      value = value.attributes.competency_id;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "competency",
      "class": "ql-competency",
      "attributes": {"competency_id": value}
    });
  }

  static getAttribute(domNode) {
      const competency_tag = JSON.parse(domNode.getAttribute("data-value"));
      return competency_tag.attributes.competency_id;
  }
}

Competency.blotName = "competency";
Competency.tagName = "SPAN";
Competency.className = "ql-competency";

export default Competency;
