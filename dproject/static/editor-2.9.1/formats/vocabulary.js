/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class Vocabulary extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search("ql-vocabulary") > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      value = value.attributes.definition;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "vocabulary",
      "class": "ql-vocabulary",
      "attributes": {"definition": value}
    });
  }

  static getAttribute(domNode) {
    const vocabulary_tag = JSON.parse(domNode.getAttribute("data-value"));
    return vocabulary_tag.attributes.definition;
  }
}

Vocabulary.blotName = "vocabulary";
Vocabulary.tagName = "SPAN";
Vocabulary.className = "ql-vocabulary";

export default Vocabulary;
