/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class SoftSkill extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    // Make the difference between srring and object
    let attribute_id;
    let attribute_name;
    let attribute_explanation;

    if (typeof value === "string") {
      value = JSON.parse(value);
      attribute_id = value.id;
      attribute_name = value.attributes.name;
      attribute_explanation = value.attributes.explanation;
    } else {
      attribute_name = value.name;
      attribute_explanation = value.explanation;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "soft_skill",
      "class": "ql-soft_skill",
      "attributes": {"name": attribute_name, "explanation": attribute_explanation}
    });
  }

  static getAttribute(domNode) {
    const soft_skill_tag = JSON.parse(domNode.getAttribute("data-value"));
    return soft_skill_tag.attributes;
  }
}

SoftSkill.blotName = "soft_skill";
SoftSkill.tagName = "SPAN";
SoftSkill.className = "ql-soft_skill";

export default SoftSkill;
