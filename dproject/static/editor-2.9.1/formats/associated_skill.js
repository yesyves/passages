/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import CertificationTag from "./certification-tag.js";

class AssociatedSkill extends CertificationTag {
  // Creates corresponding DOM node
  static create(value) {
    let attribute_id;

    // Make the difference between json string and simple string
    if (typeof value === "string" && value.search("ql-associated_skill") > 0) {
      value = JSON.parse(value);
      attribute_id = value.id;
      value = value.attributes.name;
    }

    return super.create({
      "id": attribute_id || Date.now(),
      "type": "associated_skill",
      "class": "ql-associated_skill",
      "attributes": {"name": value}
    });
  }

  static getAttribute(domNode) {
    const associated_skill_tag = JSON.parse(domNode.getAttribute("data-value"));
    return associated_skill_tag.attributes.name;
  }
}

AssociatedSkill.blotName = "associated_skill";
AssociatedSkill.tagName = "SPAN";
AssociatedSkill.className = "ql-associated_skill";

export default AssociatedSkill;
