/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import AssociatedSkill from "../formats/associated_skill.js";
import Range from "../utilities/range.js";

class AssociatedSkillTooltip extends Tooltip {
  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Get the tooltip textarea value
    this.textarea = this.root.querySelector(".text");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.root.setAttribute("data-mode", "associated_skill");
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add interaction to tooltip when escape or enter key is pressed down
    this.textarea.addEventListener("keydown", event => {
      if (event.key === "Escape") {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      // if (this.root.classList.contains("ql-editing")) {
      //   this.save();
      // } else {
      //   // Récupérer la valeur à éditer
      //   this.edit(this.textarea.value);
      // }
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector("a.ql-remove").addEventListener("click", event => {
      try {
        if (this.range != null) {
          const range = this.range;
          this.restoreFocus();
          this.quill.formatText(range, "associated_skill", false, Quill.sources.USER);
          delete this.range;
        }
        event.preventDefault();
        this.hide();
      } catch (e) {
        quillException(e)
      }
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        if (range == null) {
            // Ceci pose problème si on clique sur le tooltip mais pas sur un des boutons d'action YD
            this.preview.textContent = "";
            return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            AssociatedSkill,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            // const preview = AssociatedSkillBlot.formats(blot.domNode);
            const preview = AssociatedSkill.getAttribute(blot.domNode);
            // this.preview.textContent = preview;
            // this.show();
            this.edit(preview);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(preview = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    if (preview != null) {
      this.root.classList.remove("ql-editing");
      this.textarea.value = preview;
    } else {
      this.textarea.value = "";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
    this.textarea.select();
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }

  save() {
    let { value } = this.textarea;

    try {
      const {scrollTop} = this.quill.root;
      if (this.range) {
        // When created
        this.quill.formatText(
          this.range,
          "associated_skill",
          value,
          Quill.sources.USER,
        );
        delete this.range;
      } else {
        // When edited
        this.restoreFocus();
        this.quill.format("associated_skill", value, Quill.sources.USER);
      }
      this.quill.root.scrollTop = scrollTop;

      this.textarea.value = "";
      this.hide();
    } catch (e) {
      quillException(e)
    }
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// textarea = libellé
AssociatedSkillTooltip.TEMPLATE = [
  "<p><span class='ql-preview'></span></p>",
  "<p><textarea class='text form-control form-control-tooltip' rows='1' cols='45' maxlength='50'></textarea></p>",
  "<a class='ql-action'></a>",
  "<a class='ql-remove'></a>",
].join("");

export default AssociatedSkillTooltip;
