/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import SpellcheckBlot from "../formats/spellcheck.js";
import Range from "../utilities/range.js";

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString
 * @type {{month: string, year: string, weekday: string, day: string}}
 */
let dateFormat = {
  // weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
};
// try to get the navigator's language or else set "undefined" as default (should automatically use browser locale)
// `navigator.language` is always supposed to be available, so quite reliable
let lang = navigator.language || undefined;

class SpellcheckTooltip extends Tooltip {
  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Ajouter le textarea
    this.textarea = this.root.querySelector("textarea");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.comment_author = this.root.querySelector("div > span.ql-psg-comment-user");
    this.comment_date = this.root.querySelector("div > span.ql-psg-comment-date");
    this.root.setAttribute("data-mode", "spellcheck");
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add escape key to textarea
    this.textarea.addEventListener("keydown", event => {
      if (event.key === "Escape") {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector("a.ql-remove").addEventListener("click", event => {
      try {
        if (this.range != null) {
          const range = this.range;
          this.restoreFocus();
          this.quill.formatText(range, "spellcheck", false, Quill.sources.USER);
          delete this.range;
        }
        event.preventDefault();
        this.hide();
      } catch (e) {
        quillException(e)
      }
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        if (range == null) {
            this.preview.textContent = "";
            return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            SpellcheckBlot,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            const attributes = SpellcheckBlot.getAttributes(blot.domNode);
            this.edit(attributes);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(attributes = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    // Ajoute le texte s'il y en a déjà
    if (attributes != null && attributes.comment != null && attributes.comment !== "") {
      this.textarea.value = attributes.comment;
      this.root.classList.remove("ql-editing");
    } else {
      this.textarea.value = "";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
    this.textarea.select();

    // display the comment's author and date
    if (attributes != null) {
      this.comment_author.textContent = attributes.author;
      this.comment_date.textContent = new Date(attributes.date).toLocaleDateString(lang, dateFormat);
    }
    // set default values when creating a new comment
    else {
      this.comment_author.textContent = userName;
      this.comment_date.textContent = new Date(Date.now()).toLocaleDateString(lang, dateFormat);
    }
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }


  save() {
    let { value } = this.textarea;
    const defaultValue = " " // single space is sufficient to allow tag to be created

    try {
      const {scrollTop} = this.quill.root;
      if (this.range) {
        // When created
        this.quill.formatText(
          this.range,
          "spellcheck",
          value || defaultValue,
          Quill.sources.USER,
        );
        delete this.range;
      } else {
        // When edited
        this.restoreFocus();
        this.quill.format("spellcheck", value || defaultValue, Quill.sources.USER);
      }
      this.quill.root.scrollTop = scrollTop;

      this.textarea.value = "";

      this.hide();
    } catch (e) {
      quillException(e)
    }
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// SPECS
// textarea = explanation
SpellcheckTooltip.TEMPLATE = [
  "<p><span class='ql-preview'></span></p>",
  "<div class='mb-1 flex'>" +
    "<div class='flex-auto'>" +
      "<svg class='icon user pr-2' width=25 height=25><use xlink:href='/static/icons/icons-" + iconsVersion + ".svg#user'></use></svg>" +
      "<span class='ql-psg-comment-user mr-3'></span>" +
    "</div>" +
    "<div class='flex-auto text-right'>" +
      "<svg class='icon horloge pr-2' width=25 height=25><use xlink:href='/static/icons/icons-" + iconsVersion + ".svg#horloge'></use></svg>" +
      "<span class='ql-psg-comment-date'></span>" +
    "</div>" +
  "</div>",
  "<textarea class='form-control form-control-tooltip' rows='6' cols='40'></textarea>",
  "<a class='ql-action'></a>",
  "<a class='ql-remove'></a>",
].join("");

export default SpellcheckTooltip;
