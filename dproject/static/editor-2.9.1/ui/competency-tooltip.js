/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import Competency from "../formats/competency.js";
import Range from "../utilities/range.js";

class CompetencyTooltip extends Tooltip {
  isComp2Level = false
  modName = "competency"

  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Get the tooltip select value
    this.select = this.root.querySelector("select");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.root.setAttribute("data-mode", this.modName);

    // Build select for TEMPLATE
    let optGroups = {}

    function create_group(compId) {
      if (optGroups[compId])
        return optGroups[compId]

      // if NO optgroup defined yet, ...
      const parent = competencies.find((c) => c.pk === compId)
      if (parent) {
        const grp = document.createElement("optgroup")
        grp.label = `${parent.fields.code}. ${parent.fields.name}`
        optGroups[compId] = grp
        return grp
      }
    }

    for (const competency of competencies) {
      const compId = competency.pk

      // if parent is null, this means it's a parent competency
      if (!competency.fields.parent) {
        // check if this parent has any children competencies
        const children = competencies.filter((e) => e.fields.parent === compId)
        if (children.length === 0) {
          const option = generateOption(compId, `${competency.fields.code}. ${competency.fields.name}`)
          this.select.add(option)
        }
        // if has children, create optgroup, and tag it w/ `compId` so we add children to proper parent <optgroup>
        else {
          const grp = create_group(compId)
          // console.info(grp)
          this.select.add(grp)
        }
      }
      // for all child competencies OR standalone competences
      else {
        this.isComp2Level = true

        const parent = competency.fields.parent
        const parentCode = competencies.find((c) => c.pk === parent).fields.code
        const option = generateOption(competency.pk, `${parentCode}.${competency.fields.code}. ${competency.fields.name}`)

        const grp = create_group(parent)
        if (grp)
          grp.appendChild(option)
        else
          this.select.add(option)
      }
    }

    // manually change the text of the 1st 'none' label according to whether it's 1 or 2 levels
    this.root.querySelector('[data-id="none-label"]').innerHTML =
      this.isComp2Level ? gettext("Choose skill dimension") : gettext("Choose skill")
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add interaction to tooltip when escape or enter key is pressed down
    this.select.addEventListener("keydown", event => {
      if (event.key === "Enter") {
        this.save();
        event.preventDefault();
      } else if (event.key === "Escape") {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector("a.ql-remove").addEventListener("click", event => {
      try {
        if (this.range != null) {
          const range = this.range;
          this.restoreFocus();
          this.quill.formatText(range, this.modName, false, Quill.sources.USER);
          delete this.range;
        }
        event.preventDefault();
        this.hide();
      } catch (e) {
        quillException(e)
      }
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        if (range == null) {
            // Ceci pose problème si on clique sur le tooltip mais pas sur un des boutons d'action YD
            this.preview.textContent = "";
            return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            Competency,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            const preview = Competency.getAttribute(blot.domNode);
            this.edit(preview);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(preview = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    if (preview != null) {
      this.root.classList.remove("ql-editing");
      this.select.value = preview;
    } else {
      this.select.value = "";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }

  save() {
    let { value } = this.select;
    // console.info(value)

    try {
      const {scrollTop} = this.quill.root;
      if (this.range) {
        // When created
        this.quill.formatText(
          this.range,
          this.modName,
          value,
          Quill.sources.USER,
        );
        delete this.range;
      } else {
        // When edited
        this.restoreFocus();
        this.quill.format(this.modName, value, Quill.sources.USER);
      }
      this.quill.root.scrollTop = scrollTop;

      this.select.value = "";
      this.hide();
    } catch (e) {
      quillException(e)
    }
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// input = libellé
CompetencyTooltip.TEMPLATE = [
  "<span class='ql-preview'></span>",
  "<select class='form-control form-control-sm form-control-tooltip form-select'>",
  "<option data-id='none-label' value=''></option>",
  "</select>",
  "<a class='ql-action'></a>",
  "<a class='ql-remove'></a>",
].join("");

export default CompetencyTooltip;
