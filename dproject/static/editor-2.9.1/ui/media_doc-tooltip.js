/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import MediaDoc from "../formats/media_doc.js";

import Range from "../utilities/range.js";

class MediaDocTooltip extends Tooltip {

  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Get the tooltip select value
    this.select = this.root.querySelector("select");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.root.setAttribute("data-mode", "media_doc");

    // Loop in achievement"s documents and keep ONLY the non-image documents
    if (!documents) {
      return;
    }
    for (const doc of documents) {
      // discard all non-document media
      // if (isImg(doc.url))
      //   continue;

      const option = document.createElement("option");
      option.value = doc.id;
      option.text = doc.name;
      option.setAttribute("data-url", doc.url);
      this.select.add(option);
    }
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add interaction to tooltip when escape or enter key is pressed down
    this.select.addEventListener("keydown", event => {
      if (event.key === "Enter") {
        this.save();
        event.preventDefault();
      } else if (event.key === "Escape") {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      this.save();
      event.preventDefault();
    });

    // hide the modal whenever we click outside or change the selection
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        this.hide()
      }
    )
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(preview = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    if (preview != null) {
      this.root.classList.remove("ql-editing");
      this.select.value = preview;
    } else {
      this.select.value = "";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));

    // focus on dropdown so ESC key works to close the modal
    this.select.focus()
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }

  save() {
    let { selectedIndex } = this.select;

    // if no user selection, just exit save; nothing to save
    if (selectedIndex === 0) {
      return
    }

    // get the full <option> that was selected in the document list
    let selOpt = this.select.options[selectedIndex]

    // extract values of interest from selected <option> node
    const docId = selOpt.value
    const docUrl = selOpt.getAttribute("data-url")
    const docName = selOpt.innerText

    const { scrollTop } = this.quill.root;

    // When edited
    this.restoreFocus();

    // apply "link" format given "value" to current cursor position;
    // using "link" built-in format ensures proper formatting of the link
    try {
      this.quill.format(
        "link",
        `${location.origin}${docUrl}`,
        Quill.sources.USER
      );
    } catch (e) {
      quillException(e)
    }

    this.quill.root.scrollTop = scrollTop;

    this.select.value = "";
    this.hide();
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// input = libellé
MediaDocTooltip.TEMPLATE = [
  "<span class='ql-preview'></span>",
  "<select class='form-control form-control-sm form-control-tooltip form-select'><option value=''>",
  gettext("Choose document"),
  "</option></select>",
  "<a class='ql-action'></a>",
  "<a class='ql-remove'></a>",
].join("");

export default MediaDocTooltip;
