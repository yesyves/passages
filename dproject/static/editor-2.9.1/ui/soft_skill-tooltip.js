/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import SoftSkill from "../formats/soft_skill.js";
import Range from "../utilities/range.js";

class SoftSkillTooltip extends Tooltip {
  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Get the tooltip textbox value
    this.textbox = this.root.querySelector("textarea.textbox");
    // Ajouter le textarea
    this.textarea = this.root.querySelector("textarea.textarea");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.root.setAttribute("data-mode", "soft_skill");
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add escape key to textarea
    this.textarea.addEventListener("keydown", event => {
      if (event.key === "Escape") {
        this.cancel();
        event.preventDefault();
      }
    });
    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector("a.ql-remove").addEventListener("click", event => {
      try {
        if (this.range != null) {
          const range = this.range;
          this.restoreFocus();
          this.quill.formatText(range, "soft_skill", false, Quill.sources.USER);
          delete this.range;
        }
        event.preventDefault();
        this.hide();
      } catch (e) {
        quillException(e)
      }
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        if (range == null) {
            this.preview.textContent = "";
            return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            SoftSkill,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            const preview = SoftSkill.getAttribute(blot.domNode);
            this.edit(preview.name, preview.explanation);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(textbox = null, textarea = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    // Ajoute le texte s'il y en a déjà
    if (textbox != null) {
      this.root.classList.remove("ql-editing");
      this.textbox.value = textbox;
    } else {
      this.textbox.value = "";
    }
    if (textarea != null) {
      this.textarea.value = textarea;
    } else {
      this.textarea.value = "";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
    this.textbox.select();
  }

  restoreFocus() {
    const { scrollTop } = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }


  save() {
    let value = {
      name: this.textbox.value,
      explanation: this.textarea.value
    };

    try {
      const {scrollTop} = this.quill.root;
      if (this.range) {
        // When created
        this.quill.formatText(
          this.range,
          "soft_skill",
          value,
          Quill.sources.USER,
        );
        delete this.range;
      } else {
        // When edited
        this.restoreFocus();
        this.quill.format("soft_skill", value, Quill.sources.USER);
      }
      this.quill.root.scrollTop = scrollTop;

      this.textbox.value = "";
      this.textarea.value = "";
      this.hide();
    } catch (e) {
      quillException(e)
    }
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

// HTML DOM tooltip
// SPECS
// textbox = name
// textarea = explanation
SoftSkillTooltip.TEMPLATE = [
  "<p><span class='ql-preview'></span></p>",
  "<p><textarea class='textbox form-control form-control-tooltip' rows='1' maxlength='40'></textarea></p>",
  "<textarea class='textarea form-control form-control-tooltip' rows='6' cols='40'></textarea>",
  "<a class='ql-action'></a>",
  "<a class='ql-remove'></a>",
].join("");

export default SoftSkillTooltip;
