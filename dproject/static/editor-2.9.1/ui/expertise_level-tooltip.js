/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const Tooltip = Quill.import("ui/tooltip");

import ExpertiseLevelBlot from "../formats/expertise_level.js";
import Range from "../utilities/range.js";

class ExpertiseLevelTooltip extends Tooltip {
  modName = "expertise_level"

  constructor(quill, boundsContainer) {
    super(quill, boundsContainer);
    // Get the tooltip select value
    this.expertise_level = this.root.querySelector(".expertise_level");
    this.validation_level = this.root.querySelector(".validation_level");
    // Add HTML DOM event listeners
    this.listen();
    // Get HTML DOM element ql-preview to later interact with
    this.preview = this.root.querySelector("span.ql-preview");
    this.root.setAttribute("data-mode", this.modName);
  }

  /**
   * What happens when "keydown" is pressed in any of the 2 dropdowns
   * @param event
   */
  keydownActionHandler(event) {
    if (event.key === "Enter") {
      this.save();
      event.preventDefault();
    } else if (event.key === "Escape") {
      this.cancel();
      event.preventDefault();
    }
  }

  // Add HTML DOM event listeners
  listen() {
    // (base)
    // Add interaction to tooltip when escape or enter key is pressed down
    this.expertise_level.addEventListener("keydown",
        event => this.keydownActionHandler(event));
    this.validation_level.addEventListener("keydown",
        event => this.keydownActionHandler(event));

    // (snow)
    // Add event to ql-action button click.
    // The behavior (Edit/Save) is based on tooltip class ql-editing
    this.root.querySelector("a.ql-action").addEventListener("click", event => {
      this.save();
      event.preventDefault();
    });

    // Remove format at user’s current selection
    this.root.querySelector("a.ql-remove").addEventListener("click", event => {
      try {
        if (this.range != null) {
          const range = this.range;
          this.restoreFocus();
          this.quill.formatText(range, this.modName, false, Quill.sources.USER);
          delete this.range;
        }
        event.preventDefault();
        this.hide();
      } catch (e) {
        quillException(e)
      }
    });

    // https://quilljs.com/docs/api/#selection-change
    this.quill.on(
      "selection-change",
      (range, oldRange, source) => {
        if (range == null) {
          // Ceci pose problème si on clique sur le tooltip mais pas sur un des boutons d'action YD
          this.preview.textContent = "";
          return;
        }
        // There is no selection, only the cursor inside the editor
        if (range.length === 0 && source === Quill.sources.USER) {
          const [blot, offset] = this.quill.scroll.descendant(
            ExpertiseLevelBlot,
            range.index,
          );
          if (blot != null) {
            this.range = new Range(range.index - offset, blot.length());
            const preview = ExpertiseLevelBlot.getAttribute(blot.domNode);
            this.edit(preview.level, preview.validation);
            this.position(this.quill.getBounds(this.range));
            return;
          }
        } else {
          delete this.range;
        }
        this.hide();
      },
    );
  }

  // Event triggered when the escape key is pressed while the tooltip has focus
  cancel() {
    this.hide();
    this.restoreFocus();
  }

  edit(level = null, validation = null) {
    // Make the HTML DOM element
    this.root.classList.remove("ql-hidden");
    this.root.classList.add("ql-editing");
    if (level != null) {
      this.root.classList.remove("ql-editing");
      this.expertise_level.value = level;
    } else {
      this.expertise_level.value = "";
    }
    if (validation != null) {
      this.validation_level.value = validation;
    } else {
      this.validation_level.value = "5";
    }
    // Fix position relative to selected bolt in the editor
    this.position(this.quill.getBounds(this.quill.selection.savedRange));
  }

  restoreFocus() {
    const {scrollTop} = this.quill.scrollingContainer;
    this.quill.focus();
    this.quill.scrollingContainer.scrollTop = scrollTop;
  }

  save() {
    let value = {
      level: this.expertise_level.value,
      validation: this.validation_level.value
    };

    try {
      const {scrollTop} = this.quill.root;
      if (this.range) {
        // When created
        this.quill.formatText(
          this.range,
          "expertise_level",
          value,
          Quill.sources.USER,
        );
        delete this.range;
      } else {
        // When edited
        this.restoreFocus();
        this.quill.format("expertise_level", value, Quill.sources.USER);
      }
      this.quill.root.scrollTop = scrollTop;

      this.expertise_level.value = "";
      this.validation_level.value = "";
      this.hide();
    } catch (e) {
      quillException(e)
    }
  }

  // Show tooltip without editing functionality
  show() {
    super.show();
  }
}

const level_1 = {
  empty: gettext("Choose level"),
  label: gettext("Level 1"),
  A: gettext("A: Quality work"),
  B: gettext("B: Technical and human adjustments"),
  C: gettext("C: Internal and external resources"),
  D: gettext("D: Problem analysis and decision making"),
};

const level_2_label = gettext("Level 2")
const level_3_label = gettext("Level 3")

// HTML DOM tooltip
// input = libellé
ExpertiseLevelTooltip.TEMPLATE = [
  `<span class='ql-preview'></span>`,
  `<div class='row g-3'><div class='col-md-auto'>`,
  `<select class='expertise_level form-control form-control-sm form-control-tooltip form-select'>`,
  `<option value=''>${level_1.empty}</option>`,
  `<optgroup label="${level_1.label}">`,
  `<option value='A'>${level_1.A}</option>`,
  `<option value='B'>${level_1.B}</option>`,
  `<option value='C'>${level_1.C}</option>`,
  `<option value='D'>${level_1.D}</option>`,
  `</optgroup>`,
].join("");


// New Level 2
// HTML DOM tooltip
// input = libellé
ExpertiseLevelTooltip.TEMPLATE += [
  `<optgroup label="${level_2_label}">`,
  `<option value='M'>${level_2.M}</option>`,
  `<option value='N'>${level_2.N}</option>`,
  `<option value='O'>${level_2.O}</option>`,
  `<option value='P'>${level_2.P}</option>`,
  `<option value='Q'>${level_2.Q}</option>`,
  `<option value='R'>${level_2.R}</option>`,
  `<option value='S'>${level_2.S}</option>`,
  `</optgroup>`,
].join("");

const level_3 = {
  H: gettext("H: Hierarchic responsibility"),
  I: gettext("I: Improvements in workplace"),
  J: gettext("J: Contribution to the community"),
  X: gettext("Complete"),
  Y: gettext("Partial"),
  Z: gettext("Absent"),
};

// HTML DOM tooltip
// input = libellé
ExpertiseLevelTooltip.TEMPLATE += [
  `<optgroup label="${level_3_label}">`,
  `<option value='H'>${level_3.H}</option>`,
  `<option value='I'>${level_3.I}</option>`,
  `<option value='J'>${level_3.J}</option>`,
  `</optgroup>`,
  `</select>`,
  `</div><div class='col-md-auto pl-3'>`,
  `<select class='validation_level form-control form-control-sm form-control-tooltip form-select'>`,
  `<option value='5'>${level_3.X}</option>`,
  `<option value='3'>${level_3.Y}</option>`,
  `<option value='0'>${level_3.Z}</option>`,
  `</select>`,
  `</div></div>`,
  `<a class='ql-action'></a>`,
  `<a class='ql-remove'></a>`,
].join("");

// const e_levels = Object.assign({}, level_1, level_2, level_3);

export default ExpertiseLevelTooltip;
