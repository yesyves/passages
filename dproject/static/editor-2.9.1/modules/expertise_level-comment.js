/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import ExpertiseLevelTooltip from "../ui/expertise_level-tooltip.js";

// Add Associated Skill certification tag to toolbar.
// Binds a ExpertiseLevel certification tag to a expertise_level tooltip.
class ExpertiseLevelComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new ExpertiseLevelTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("expertise_level", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      if (value) {
        const range = this.quill.getSelection();
        if (range == null || range.length === 0) return;
        this.tooltip.edit(null, null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("expertise_level", false);
      }
    } catch (e) {
      quillException(e)
    }
  }
}

export default ExpertiseLevelComment;
