/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import SoftSkillTooltip from "../ui/soft_skill-tooltip.js";

// Add soft_skill certification tag to toolbar.
// Binds a soft_skill certification tag to a soft_skill tooltip.
class SoftSkillComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new SoftSkillTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("soft_skill", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      if (value) {
        const range = this.quill.getSelection();
        if (range == null || range.length === 0) return;
        this.tooltip.edit(null, null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("soft_skill", false);
      }
    } catch (e) {
      quillException(e)
    }
  }
}

export default SoftSkillComment;
