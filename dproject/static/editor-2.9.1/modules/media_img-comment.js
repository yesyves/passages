/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import MediaImgTooltip from "../ui/media_img-tooltip.js";

/**
 * Module to allow inserting an image from the current achievement's attached media
 */
class MediaImgComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new MediaImgTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("media_img", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      // NOTE: "value" is only a boolean, true or false
      if (value) {
        const range = this.quill.getSelection();

        // only allow adding image when NO text is selected
        if (range && range.length > 0)
          return

        this.tooltip.edit(null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("image", false);
      }
    } catch (e) {
      quillException(e)
    }
  }
}

export default MediaImgComment;
