/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import MediaDocTooltip from "../ui/media_doc-tooltip.js";

/**
 * Allows to insert document from achievement's attachments when clicking on paper clip icon in toolbar
 */
class MediaDocComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new MediaDocTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("media_doc", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      // NOTE: "value" is only a boolean, true or false
      if (value) {
        const range = this.quill.getSelection();

        // for link, MUST absolutely be on a selection, NOT alone
        if (range === null || range.length === 0)
          return

        this.tooltip.edit(null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("link", false);
      }
    } catch (e) {
      quillException(e)
    }
  }
}

export default MediaDocComment;
