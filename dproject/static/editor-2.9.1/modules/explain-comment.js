/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import ExplainTooltip from "../ui/explain-tooltip.js";

// Add explain certification tag to toolbar.
// Binds a explain certification tag to a explain tooltip.
class ExplainComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new ExplainTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("explain", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      if (value) {
        const range = this.quill.getSelection();
        if (range == null || range.length === 0) return;
        this.tooltip.edit(null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("explain", false);
      }
    } catch (e) {
      quillException(e);
    }
  }
}

export default ExplainComment;
