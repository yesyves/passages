/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

const QuillModule = Quill.import("core/module");
import VocabularyTooltip from "../ui/vocabulary-tooltip.js";

// Add vocabulary certification tag to toolbar.
// Binds a vocabulary certification tag to a vocabulary tooltip.
class VocabularyComment extends QuillModule {
  constructor(quill, options) {
    super(quill, options);
    this.tooltip = new VocabularyTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      toolbar.addHandler("vocabulary", this.comment.bind(this));
    }
  }

  comment(value) {
    try {
      if (value) {
        const range = this.quill.getSelection();
        if (range == null || range.length === 0) return;
        this.tooltip.edit(null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format("vocabulary", false);
      }
    } catch (e) {
      quillException(e);
    }
  }
}

export default VocabularyComment;
