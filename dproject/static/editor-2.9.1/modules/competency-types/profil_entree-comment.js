/*
 * © Copyright Yves de Champlain et al. 2018-2022
 *     For more informations, see https://doc.epassages.ca/index.php/contributions
 * SPDX-License-Identifier: LiLiQ-Rplus-1.1
 * License-Filename: LICENCE
 */

import ProfilEntreeTooltip from "../../ui/competency-types/profil_entree-tooltip.js";

const QuillModule = Quill.import("core/module");

// Add Associated Skill certification tag to toolbar.
// Binds a Competency certification tag to a competency tooltip.
class ProfilEntreeComment extends QuillModule {
  modName = "profil_entree"

  constructor(quill, options) {
    // get the array of competency types indicating which toolbar options to load
    // MODE=MILESTONE; can we use data attributes to know more about which options to load in Quill
    const comp_types = quill.container.dataset.editor_opts.split(',')

    super(quill, options);
    this.tooltip = new ProfilEntreeTooltip(this.quill, options.bounds || this.quill.options.bounds);
    const toolbar = this.quill.getModule("toolbar");
    if (toolbar) {
      toolbar.container.classList.add("ql-certification");
      // MODE=MILESTONE; trying to see if possible to dynamically add handler given multiple editor opts
      // DEBUG
      // for (const type of comp_types) {
      //   console.info(type)
      // }
      toolbar.addHandler(this.modName, this.comment.bind(this));
    }

  }

  comment(value) {
    try {
      if (value) {
        const range = this.quill.getSelection();
        if (range == null || range.length === 0) return;
        this.tooltip.edit(null);
      } else {
        // https://quilljs.com/docs/api/#format
        this.quill.format(this.modName, false);
      }
    } catch (e) {
      quillException(e)
    }
  }
}

export default ProfilEntreeComment;
