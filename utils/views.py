# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale
import random
import string

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import mail_admins
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt

from config.settings.common import SESSION_ACTIVE_PORTFOLIO
from portfolio.models import Portfolio, HelpLink, PortfolioType
from program.models import Program
from user.models import User

locale.setlocale(locale.LC_ALL, '')


def get_random_string(length):
    """Return random lowercase ascii string of specified length

    Args:
        length: (int)

    Returns:
        String

    """
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str


def active_portfolio(request) -> Portfolio|None:
    """Use session variable to always have quick access to active portfolio, whether in admin or user mode.
    Returns: Portfolio / None
    """

    try:
        # retrieve active portfolio ID stored in session
        active_portfolio_id = request.session.get(SESSION_ACTIVE_PORTFOLIO)

        # if session has valid portfolio ID, return it, else return None
        if active_portfolio_id is not None:
            portfolio = Portfolio.available_objects.get(id=active_portfolio_id)
            return portfolio
    except KeyError as k:
        mail_admins('KeyError in active_portfolio()',
                    '{}\n\n{}\n\n{}\n\n{}\n\n{}\n\n{}'.format(k,
                                                              request.user,
                                                              request.META,
                                                              request.COOKIES,
                                                              request.POST,
                                                              request.GET))

    return None


def get_portfolio(portfolio_id) -> Portfolio|None:
    """Helper to retrieve a Portfolio object given its ID to avoid code duplication"""
    try:
        return Portfolio.available_objects.get(id=portfolio_id)
    except ObjectDoesNotExist as e:
        print(e)
        return None


def active_portfolio_type() -> PortfolioType|None:
    """Return currently active PortfolioType; single PortfolioType per Passages instance, so that is the way to go"""
    try:
        return PortfolioType.available_objects.first()
    except ObjectDoesNotExist as e:
        print(e)
        return None


@login_required
def check_restriction(request, instance, target=None):
    """Validate access to specific parts of the portfolio depending on user type and
    portfolio status or active achievement user

    Args:
        request: (HttpRequest object)
        instance: (Portfolio or Achievement object) Object to which the check applies
        target: (Optional[str]) Target to validate, can be 'achievement'

    Returns:
        HttpResponse (portfolio/restrict.html) if restriction applies, False otherwise

    """

    user: User = request.user
    user_type = user.type
    message = False
    if target == 'achievement':
        achievement = instance
        portfolio = achievement.portfolio
    else:
        portfolio = instance

    print("portfolio-not-finalized")
    print(portfolio)

    if user_type == 'Expert':
        # Show portfolio to Expert only if portfolio is under review
        if portfolio.status == 'Certified' or portfolio.status == 'Archived':
            message = [_('Portfolio certified'),
                       _('Portfolio evaluation for {} has been completed.').format(portfolio.user),
                       _('Thank you!')]
        elif portfolio.status != 'Underreview' and portfolio.status != 'Reviewed':
            message = [_('Portfolio not finalized'),
                       _('Portfolio of {} is not ready for evaluation').format(portfolio.user),
                       _('You will be notified when it is ready')]
    else:
        if (
            user_type == 'Applicant'
            and (
                portfolio.status == 'Underreview'
                or portfolio.status == 'Pre-Reviewed'
                or portfolio.status == 'Post-Interview'
                or portfolio.status == 'Reviewed'
                or user.group.status == 'Underreview'
            )
        ):
            message = [_('Portfolio under evaluation'),
                       _('Your portfolio is currently being evaluated'),
                       _('You will be notified when the evaluation is completed')]
        # TODO Implement between 2 semesters
        # elif (
        #     user_type == 'Applicant'
        #     and target == 'achievement'
        #     and achievement.number != 1
        #     and achievement.status != 'Final'
        # ):
        #     if achievement.number == 2:
        #         up_to = 1
        #     else:
        #         up_to = 2
        #     achievements = Achievement.available_objects.filter(portfolio=portfolio, number__lte=up_to)
        #     for achievement in achievements:
        #         if achievement.status != 'Final':
        #             return redirect('show_questionnaire', achievement_id=achievement.id)
        elif target == 'achievement':
            message = achievement.check_active_user(user)

    if message:
        context = {
            'user_type': user_type,
            'message': message,
            'instance': instance,
            'target': target,
        }
        template = loader.get_template('portfolio/restrict.html')
        return HttpResponse(template.render(context, request))
    else:
        return False


def notify_exception(request):
    """Let client-side notify ADMINS of an exception

    Args:
        request: (HttpRequest object)
            POST.subject: (str) Type of exception
            POST.event: (str) Details about the exception
            POST.link (str) Triggering URL

    Raises:
        ValueError if request method is not POST

    Returns:
        HttpResponse (application/json) : True if message was sent, False otherwise

    """
    if request.method != 'POST':
        raise ValueError(request)

    msg = mail_admins(
        request.POST.get('subject'),
        '{}\n\nIn : https://{}{}'.format(request.POST.get('event'),
                                         request.META.get('HTTP_HOST'),
                                         request.POST.get('link')),
    )
    response = {'success': True} if msg else {'success': False}

    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )


# given keyword, try to find matching help link (URL) or return # if no HelpLink exists for that keyword
def get_help_link(keyword: str) -> str:
    try:
        help_link = HelpLink.available_objects.get(keyword=keyword).link
    except ObjectDoesNotExist:
        help_link = "#"
    return help_link


# Deprecated

# def is_json(string):
#     """
#     https://stackoverflow.com/questions/5508509/how-do-i-check-if-a-string-is-valid-json-in-python
#     Check if a given string is valid JSON. json.loads crashes if a string is passed to it vs a valid
#     JSON object.
#     """
#     try:
#         json_object = json.loads(string)
#     except ValueError as e:
#         return False
#     return True


