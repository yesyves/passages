#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django import template
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from dproject import __icons_version__

register = template.Library()


@register.simple_tag(name='svg_icon_psg')
def svg_icon_psg(icon_name, extra_class='', data_iconid='', width="100%", height="100%"):
    """Streamline use of <svg> tags based on icons-1.1.0.svg
    https://mirellavanteulingen.nl/blog/svg-icons-django-template.html

    Args:
        icon_name: (str)
        extra_class: (Optional[str])
        data_iconid: (Optional[str]) Enable getElementByID
        width: (Optional[str])
        height: (Optional[str])

    Returns:
        Html svg tag

    """
    svg_tag = format_html(
        '<svg data-iconid="{data_iconid}" class="icon {name} {extra}" width={width} height={height}>'
        '<use xlink:href="/static/icons/icons-{icons_version}.svg#{name}"></use>'
        '</svg>',
        data_iconid=data_iconid,
        name=icon_name,
        extra=extra_class,
        width=width,
        height=height,
        icons_version=__icons_version__)

    return mark_safe(svg_tag)

