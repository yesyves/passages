#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django import template
from django.utils.translation import gettext_lazy as _

register = template.Library()


@register.filter
def concat(arg1, arg2):
    """Concatenate arg1 & arg2

    Args:
        arg1:
        arg2:

    Returns: String

    """
    return str(arg1) + str(arg2)


@register.filter
def cut_from(data: str, key: str):
    """given String & separator, return last part after separator

    Args:
        data: (str) Data structure
        key: (str) string separator

    Returns:
        Data value

    Example:
        {{ your_data|cut_from:"@" }}
    """
    subs = data.split(key)
    if len(subs) > 1:
        return subs[len(subs) - 1]
    else:
        return ""


@register.simple_tag(name='compare_development_levels')
def compare_development_levels(candidate_level, expert_level):
    try:
        expert_level = int(expert_level[2])
        candidate_level = int(candidate_level[2])
    except IndexError:
        print("Index error")
        expert_level = 0
        candidate_level = 0

    if expert_level > candidate_level:
        return _("The written and oral explanation did not allow us to conclude on the level of development targeted "
                 "for the student. The level selected is higher than that required.")
    elif expert_level < candidate_level:
        return _("The written and oral explanation did not allow us to conclude on the level of development targeted "
                 "for the student. The level selected is lower than that required.")
    else:
        return _("The written and oral explanation did allow to conclude on the targeted level of development "
                 "for the student.")
