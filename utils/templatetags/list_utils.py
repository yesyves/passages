#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django import template


register = template.Library()


@register.filter
def get_value_with_key(data, key):
    """Get a value with key when the dot notation won't work, e.g. list.forloop.counter0

    Args:
        data: (dictionary or list) Data structure
        key: (int or str) Data index

    Returns:
        Data value

    Example:
        {{ your_data|get_value_with_key:your_key }}
    """
    try:
        return data[key]
    except KeyError:
        return False
    except IndexError:
        return False


@register.filter
def following(some_list, current_index):
    """Returns the following element from the current index of a list if it exists.
    Otherwise returns an empty string.

    Args:
        some_list: (list)
        current_index: (int)

    Returns:
        List element if it exists, empty string otherwise

    """
    try:
        return some_list[int(current_index) + 1]    # access the next element
    except IndexError:
        return ''   # return empty string in case of exception


@register.filter
def previous(some_list, current_index):
    """Returns the previous element of the list from the current index if it exists.
    Otherwise returns an empty string.

    Args:
        some_list: (list)
        current_index: (int)

    Returns:
        List element if it exists, empty string otherwise

    """
    if current_index < 1:
        # There can't be a previous element
        return ''
    else:
        try:
            return some_list[int(current_index) - 1]  # access the previous element
        except IndexError:
            return ''   # return empty string in case of exception


@register.filter
def xrange(number):
    # Django range tag to loop over list of numbers, starting from 1
    # https://stackoverflow.com/questions/1107737/numeric-for-loop-in-django-templates#answer-12797728
    # https://django.cowhite.com/blog/numeric-for-loop-in-django-templates/
    return range(1, number+1)


"""
Usage example :
{% for element in list %}
    # get the next element
    {% with next_element=list|following:forloop.counter0 %}
        # get the previous element
        {% with previous_element=list|previous:forloop.counter0 %}
            {% if element.is_child and not previous_element.is_child %}
                <div class="children-block">
            {% endif %}
                {{ element.title }}
            {% if element.is_child and not next_element.is_child %}
                </div>
            {% endif %}
        {% endwith %}
    {% endwith %}
{% endfor %}
"""
