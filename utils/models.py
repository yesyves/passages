# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

from model_utils.models import SoftDeletableModel, StatusModel, TimeStampedModel
from model_utils import Choices

from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy
from django.db import models


class MetaData(TimeStampedModel, StatusModel, SoftDeletableModel):
    """Abstract class to implement TimeStamp, Status and SoftDelete in models

    TimeStampedModel:
        Attributes:
            created: (datetime) Self-updating
            modified: (datetime) Self-updating

    StatusModel:
        Attributes:
            Status: (Choice)
            status_modified: (Datetime) Self-updating

    SoftDeletableModel:
        Attributes:
            is_removed: (bool) Default = False, set True on delete

    """

    STATUS = Choices(('Draft', _('Draft')),
                     ('Created', _('Created')),
                     ('Rejected', _('Rejected')),
                     ('Submitted', _('Submitted')),
                     ('Underreview', pgettext_lazy('portfolio', 'Underreview')),
                     ('PreReviewed', _('Pre-Reviewed')),
                     ('Reviewed', _('Reviewed')),
                     ('Final', _('Final')),
                     ('PostInterview', _('Post-Interview')),
                     ('PreCertified', _('Pre-Certified')),
                     ('Certified',  pgettext_lazy('portfolio', 'Certified')),
                     ('Archived', pgettext_lazy('portfolio', 'Archived')),)

    STATUS_APPLICANT = Choices(('Submitted', _('Submitted')),)
    STATUS_REVIEWER = Choices(('Reviewed', _('Reviewed')),
                              ('Rejected', _('Rejected')),
                              ('Final', _('Final')),)

    # Portfolio translations
    draft = pgettext_lazy('portfolio', 'Draft')
    reviewed = pgettext_lazy('portfolio', 'Reviewed')
    final = pgettext_lazy('portfolio', 'Final')

    class Meta:
        abstract = True


# list of user types available throughout Passages models
# NOTE: cannot put that class in `user/models.py` due to circular dependency:
#   - `user.models` -> `portfolio.models` (Portfolio, PortfolioType)
#   - `portfolio.models` -> `user.models` (UserType)
class UserType(models.TextChoices):
    AGENT = 'Agent', _('Agent')
    APPLICANT = 'Applicant', _('Applicant')
    EXPERT = 'Expert', _('Expert')
    REVIEWER = 'Reviewer', _('Reviewer')
    SUPERVISOR = 'Supervisor', _('Supervisor')
    NONE = 'None', _('None')
