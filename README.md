# Passages
#### Un portfolio transdisciplinaire pour la reconnaissance des acquis expérientiels

#### A transdisciplinary portfolio to assist the recognition of experiential prior learning

#### https://epassages.ca/

## Installation

### Setting up the Environment (local)
- Development is done with current Python 3 and PostgreSQL 13
- _Specific notes for macOS are provided in doc_

1. Create the Python Environment
   ```
   pip install virtualenvwrapper
   mkvirtualenv passages
   workon passages
   ./tools/requirements/upgrade.sh
   ```

2. Create config/settings/.env
   ```
   cp doc/env.example config/settings/.env
   ```

3. Set up the DB
   ```
   python manage.py makemigrations
   python manage.py migrate portfolio
   python manage.py migrate
   ```

4. Generate and compile locale files, compress static files
   ```
   ./tools/translate.sh -c
   ```

5. Start the development server
   ```
   ./tools/runserver.sh
   ```
   
### Setting up the environment (local Docker env)

1. `docker compose up -d --build`
2. `docker compose exec django /bin/bash`
3. `./tools/db-reset.dev.sh`
4. `./tools/runserver.sh`

## Development tools

1. Modify / Update the Python Environment
   ```
   ./tools/requirements/upgrade.sh
   ```
   IMPORTANT: If `weasyprint` causes error `OSError: cannot load library 'gobject-2.0-0': gobject-2.0-0: cannot open shared object file: No such file or directory`, this is due to missing `glib` (GTK-related libs), so install Glib (e.g. `conda install glib`).


2. Reset DB structure and contents
   ```
   ./tools/reset-migrations.sh
   ./tools/reset-contents.sh
   ```

3. Run tests with Coverage
   ```
   ./tools/test.sh
   ```

4. Generate and compile locale files
   ```
   ./tools/translate.sh
   ```

5. Compress static files
   ```
   ./tools/compress.sh
   ```

6. Validate python and html code
   ```
   ./tools/validate.sh
   ```

[comment]: <> (7. Update `quill-delta` &#40;bundling for browser&#41;)
[comment]: <> (- Download https://www.jsdelivr.com/package/npm/quill-delta)
[comment]: <> (   ```)
[comment]: <> (   tar zxf quill-delta-X.Y.Z.tgz)
[comment]: <> (   cd package)
[comment]: <> (   browserify dist/Delta.js --standalone Delta -o dist/delta-browser.js)
[comment]: <> (   cp dist/delta-browser.js ../passages/dproject/static/vendor/quill-delta-X.Y.Z)
[comment]: <> (   ```)

### Deploying in Docker
- See doc/docker


## Issues & Notes

### Migration error: Relation <X> already exists

- https://stackoverflow.com/questions/29830928/django-db-utils-programmingerror-relation-already-exists
- `./manage.py migrate --fake <app-nane>`, e.g. `./manage.py migrate --fake portfolio`