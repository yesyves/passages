/** @type {import('tailwindcss').Config} */
module.exports = {
  important: false,
  content: [
    './dproject/static/**/*.{html,js}',
    './dproject/templates/**/*.{html,js}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

/**
 * IMPORTANT: This file must remain at project's root in order for IDE CSS class auto-complete to work;
 * otherwise, it cannot detect active Tailwind config and thus will not auto-complete Tailwind classes.
 */