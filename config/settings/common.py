# -*- coding: utf-8 -*-
#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE
import os
from datetime import datetime

import environ
import sys

from email.utils import parseaddr
from ms_identity_web.configuration import AADConfig
from ms_identity_web import IdentityWebPython

from config.aad_config import XAADConfig, aad_config_structure

# base_dir/config/settings/common.py - 3 = base_dir/
ROOT_DIR = environ.Path(__file__) - 3
PROJ_DIR = ROOT_DIR.path('dproject')

# read from OS environment (either set via Docker or by sourcing local .env file);
env = environ.Env()
# no longer read from config/settings/.env since we're relying ONLY on OS env variables
# env.read_env()

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('DJANGO_SECRET_KEY')

# This ensures that Django will be able to detect a secure connection
# https://stackoverflow.com/questions/62047354/build-absolute-uri-with-https-behind-reverse-proxy
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
)

THIRD_PARTY_APPS = (
    'compressor',
    'crispy_forms',
    'crispy_bootstrap5',
    'statici18n',
    # 'anymail'
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'utils',
    'program',
    'user',
    'portfolio',
    'editor',
)
# Include 'data' if not running tests
# if 'test' not in sys.argv and 'test_coverage' not in sys.argv:
#     LOCAL_APPS += ('data', )

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# Add USE_SSO in .env
USE_SSO = env.bool('USE_SSO', False)

if USE_SSO:
    # Microsoft Azure AD authentication connection
    # AAD_CONFIG = AADConfig.parse_json(file_path='aad.config.json')
    AAD_CONFIG = XAADConfig.parse_dict(aad_dict_config=aad_config_structure)

    # TODO for debugging only
    # print("\n$: Loading AAD dict config -> " + datetime.now().__str__())
    # print(AAD_CONFIG)
    # print("")

    MS_IDENTITY_WEB = IdentityWebPython(AAD_CONFIG)
    ERROR_TEMPLATE = 'auth/{}.html' # for rendering 401 or other errors from msal_middleware
    MIDDLEWARE.append('ms_identity_web.django.middleware.MsalMiddleware')

# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
# https://anymail.dev/en/stable/quickstart/
# ANYMAIL = {
#     "MAILGUN_API_KEY": env('MAILGUN_API_KEY', default=''),
#     "MAILGUN_SENDER_DOMAIN": env('MAILGUN_SENDER_DOMAIN', default='')
# }
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')
EMAIL_HOST = env('DJANGO_EMAIL_HOST', default='localhost')
EMAIL_PORT = env('DJANGO_EMAIL_PORT', default=25)
EMAIL_HOST_USER = env('DJANGO_EMAIL_HOST_USER', default='test')
EMAIL_HOST_PASSWORD = env('DJANGO_EMAIL_HOST_PASSWORD', default='test')
EMAIL_USE_TLS = env('DJANGO_EMAIL_USE_TLS', default=True)
DEFAULT_FROM_EMAIL = env('DJANGO_DEFAULT_FROM_EMAIL', default='')
SERVER_EMAIL = env('DJANGO_SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)

PROGRAM_EMAIL = env('PROGRAM_EMAIL_TO', default='')
PROGRAM_CONTACT = env('PROGRAM_CONTACT_NAME', default='')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = tuple(parseaddr(email) for email in env.list('DJANGO_ADMINS', default=[None]))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
SQLITE_URL = "sqlite:///" + str(ROOT_DIR) + "/sqlite.db"
DATABASES = {
    'default': env.db('DATABASE_URL', default=SQLITE_URL)
}

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = env('DJANGO_TIME_ZONE', default='Etc/UTC')

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
LOCALE_PATHS = [PROJ_DIR('locale')]

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'fr-ca'

LANGUAGES = [
    ('en', 'English'),
    ('fr-ca', 'Français (Canada)'),
    ('fr-fr', 'Français (France)'),
]

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [str(PROJ_DIR.path('templates'))],
    'APP_DIRS': False,
    'OPTIONS': {
        # https://docs.djangoproject.com/en/dev/topics/templates/#django.template.backends.django.DjangoTemplates
        # https://docs.djangoproject.com/en/dev/ref/templates/api/#template-loaders
        # Remove loaders : https://github.com/jazzband/django-debug-toolbar/issues/1550
        # 'loaders': ['django.template.loaders.filesystem.Loader',
        #             'django.template.loaders.app_directories.Loader'],
        # https://docs.djangoproject.com/en/dev/ref/templates/api/#django.template.RequestContext
        'context_processors': [
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'django.template.context_processors.i18n',
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.tz',
            # custom context processors to provide global info across views
            'dproject.context_processors.app_info',
            'dproject.context_processors.portfolio_info'
        ],
        # 'libraries': {
        #     'sorl_thumbnail': 'sorl.thumbnail.templatetags.thumbnail',
        # },
    },
}]


# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(PROJ_DIR('staticfiles'))
FILE_UPLOAD_TEMP_DIR = str(PROJ_DIR('tmp'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [str(PROJ_DIR.path('static'))]

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
]

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(PROJ_DIR('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'
LOGIN_URL = '/accounts/login/'
LOGOUT_URL = '/accounts/logout/'
LOGIN_REDIRECT_URL = '/portfolio/'
LOGOUT_REDIRECT_URL = '/accounts/login/'

# Custom user model
AUTH_USER_MODEL = 'user.User'
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'


# PASSWORD VALIDATION
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# Session that expire as soon as the user closes their browser
# see https://docs.djangoproject.com/en/3.0/topics/http/sessions/
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Your common stuff: Below this line define 3rd party library settings
# ------------------------------------------------------------------------------

# https://django-compressor.readthedocs.io/en/stable/settings/
# NOTE: COMPRESS_ENABLED default value = not DEBUG
COMPRESS_ENABLED = not DEBUG
COMPRESS_OFFLINE = True
COMPRESS_OFFLINE_CONTEXT = 'config.views.offline_context'
COMPRESS_FILTERS = {
    'css': ['compressor.filters.css_default.CssAbsoluteFilter',
            'compressor.filters.cssmin.rCSSMinFilter'],
    'js': ['compressor.filters.jsmin.JSMinFilter']
}


# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap5"
CRISPY_TEMPLATE_PACK = 'bootstrap5'


GOOGLE_RECAPTCHA_SECRET_KEY = env('DJANGO_RECAPTCHA_SECRET_KEY')

# name of session variable to be used to keep track of active portfolio;
# Due to how 'milestone' mode works w/ achievement types instead of instances, we need a way to easily keep track
# of which portfolio is active since AchievementType has NO reference to portfolio or user.
# - individual achievement types not linked to user achievement, so no portfolio link
# - need to block student from accessing his achievements when 'underreview'
# - dynamic display of custom competency types in left menu based on user portfolio's type (not available in template)
# - sometimes it's easier to just have a 'global' variable instead of always tracing down variable in templates
SESSION_ACTIVE_PORTFOLIO = "active_portfolio"

# store this environment variable as constant accessible everywhere else in the code
IS_CONTINUOUS = env.bool('MODE_CONTINUOUS')
IS_MILESTONE = not IS_CONTINUOUS
