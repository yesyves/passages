# -*- coding: utf-8 -*-
#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

"""
Local settings
- Run in Debug mode
- Use console backend for emails
- Add Django Debug Toolbar
- Add django-extensions as app
"""

import sys

from .common import *  # noqa


ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['*'])
CSRF_TRUSTED_ORIGINS = env.list('DJANGO_CSRF_TRUSTED_ORIGINS', default=[None])

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = True
TEMPLATES[0]['APP_DIRS'] = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG
TEMPLATES[0]['OPTIONS']['context_processors'].append('django.template.context_processors.debug')


# Mail settings
# ------------------------------------------------------------------------------
# todo setup Mailpit as part of Docker to simplify things and avoid using console.EmailBackend
# todo set those variables exclusively in common.py to avoid conflicting variable assignments
# EMAIL_HOST = 'localhost'
# EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
# EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# print("local.py @ email_backend")
# print(EMAIL_BACKEND)


# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}


# TESTING
# ------------------------------------------------------------------------------
if 'test' in sys.argv or 'test_coverage' in sys.argv:
    # Covers regular testing and django-coverage
    DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'
    TEST_RUNNER = 'django.test.runner.DiscoverRunner'
    INSTALLED_APPS += ('coverage', )


# # django-debug-toolbar
# # ------------------------------------------------------------------------------
# INTERNAL_IPS = ['127.0.0.1']
# INSTALLED_APPS += ('debug_toolbar', )
# MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
# TEMPLATES[0]['APP_DIRS'] = True
#
# DEBUG_TOOLBAR_CONFIG = {
#     'DISABLE_PANELS': ['debug_toolbar.panels.redirects.RedirectsPanel'],
#     'SHOW_TEMPLATE_CONTEXT': True,
# }


# django-extensions
# ------------------------------------------------------------------------------
INSTALLED_APPS += ('django_extensions', )


# django-html-validator
# ------------------------------------------------------------------------------
HTMLVALIDATOR_ENABLED = False
HTMLVALIDATOR_FAILFAST = False
HTMLVALIDATOR_DUMPDIR = '~/Documents/validationerrors/'
HTMLVALIDATOR_VNU_URL = 'http://localhost:8888/'
# vnu/bin/java -cp vnu.jar nu.validator.servlet.Main 8888
if HTMLVALIDATOR_ENABLED:
    MIDDLEWARE += ("htmlvalidator.middleware.HTMLValidator",)


# admindocs
# ------------------------------------------------------------------------------
INSTALLED_APPS += ('django.contrib.admindocs', )
MIDDLEWARE += ("django.contrib.admindocs.middleware.XViewMiddleware",)

# force http -> https redirection (mainly helpful w/ MS AAD redirect_uri to force it into HTTPS)
SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=False)