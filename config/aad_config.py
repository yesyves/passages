import os
from types import SimpleNamespace

from ms_identity_web.configuration import AADConfig

# Extend original [AADConfig] to provide method to parse config from dictionary instead of a JSON file
class XAADConfig(AADConfig):
  @staticmethod
  def parse_dict(aad_dict_config: dict):
    from types import SimpleNamespace
    parsed_config = SimpleNamespace(**aad_dict_config)
    AADConfig.sanity_check_configs(parsed_config)
    return parsed_config


# Actual dictionary representing JSON AAD config structure (each sub-dict must be wrapped in SimpleNamespace)
aad_config_structure = {
  "type": SimpleNamespace(**{
    "client_type": "CONFIDENTIAL",
    "authority_type": "SINGLE_TENANT",
    "framework": "DJANGO"
  }),
  "client": SimpleNamespace(**{
    "client_id": os.getenv("AAD_CLIENT_ID"),
    "client_credential": os.getenv("AAD_CLIENT_SECRET"),
    "authority": os.getenv("AAD_LOGIN_URL")
  }),
  "auth_request": SimpleNamespace(**{
    "redirect_uri": os.getenv("AAD_REDIRECT_URI"),
    "scopes": [],
    "response_type": "code"
  }),
  "flask": None,
  "django": SimpleNamespace(**{
    "id_web_configs": "MS_ID_WEB_CONFIGS",
    "auth_endpoints": SimpleNamespace(**{
      "prefix": "auth",
      "sign_in": "sign_in",
      "edit_profile": "edit_profile_aad",
      "redirect": "redirect",
      "sign_out": "sign_out",
      "post_sign_out": "post_sign_out"
    })
  })
}