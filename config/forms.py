#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import requests

from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.core.mail import mail_admins
from django.utils.translation import gettext_lazy as _


class LoginForm(AuthenticationForm):
    remember_me = forms.BooleanField(label=_('Stay connected'),
                                     initial=False,
                                     required=False)


class PasswordResetGoogleRecaptchaForm(PasswordResetForm):
    recaptcha = forms.CharField(
        widget=forms.HiddenInput(),
        max_length=1024,
        required=False
    )

    def clean(self):
        cleaned_data = super(PasswordResetGoogleRecaptchaForm, self).clean()
        recaptcha_token = cleaned_data.get('recaptcha')
        recaptcha_data = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_token
        }
        r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=recaptcha_data)
        result = r.json()
        if result.get('success') and result.get('score') > 0.5:
            # client is human
            # print(result)
            pass
        else:
            mail_admins(
                _('Problem resetting password'),
                _('With email {}').format(cleaned_data.get('email')),
            )
            raise forms.ValidationError(
                _('You seem to be having some difficulties. Please do not hesitate to ask for help!')
            )
