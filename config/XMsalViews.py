import os

from django.shortcuts import redirect
from django.urls import reverse
from ms_identity_web.django.msal_views_and_urls import MsalViews

class XMsalViews(MsalViews):
    def __init__(self, ms_identity_web):
        super().__init__(ms_identity_web)

    def custom_redirect_uri(self, request):
        return os.getenv("AAD_REDIRECT_URI") or request.build_absolute_uri(reverse(self.endpoints.redirect))

    def sign_in(self, request):
        self.logger.debug(f"{self.prefix}{self.endpoints.sign_in}: request received. will redirect browser to login")
        redirect_uri = self.custom_redirect_uri(request)
        auth_url = self.ms_identity_web.get_auth_url(redirect_uri=redirect_uri)
        print(f"XMsalViews.redirect_uri -> {redirect_uri}")
        print(f"XMsalViews.auth_url -> {auth_url}")
        return redirect(auth_url)

    def aad_redirect(self, request):
        self.logger.debug(f"{self.prefix}{self.endpoints.redirect}: request received. will process params")
        redirect_uri = self.custom_redirect_uri(request)
        print(f"XMsalViews.redirect_uri -> {redirect_uri}")
        return self.ms_identity_web.process_auth_redirect(
            redirect_uri=redirect_uri,
            afterwards_go_to_url=reverse('index'))
