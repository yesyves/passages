#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.conf import settings
from django.contrib.auth import views as auth_views, login
from django.contrib.auth.views import PasswordResetView
from django.shortcuts import redirect

from config.forms import PasswordResetGoogleRecaptchaForm
from dproject import __version__, __editor_version__, __icons_version__


class Login(auth_views.LoginView):

    def form_valid(self, form):
        """
        Set persistent login
        """
        login(self.request, form.get_user())
        remember = form.data.get('remember_me', False)
        if remember:
            self.request.session.set_expiry(28800)    # 8 hours
        return redirect(self.get_success_url())


class PasswordResetGoogleRecaptchaView(PasswordResetView):
    form_class = PasswordResetGoogleRecaptchaForm


def offline_context():
    """
    Generate COMPRESS_OFFLINE_CONTEXT
    :return: context:[{}]
    """
    for code, description in settings.LANGUAGES:
        for base in ('base', 'print/print'):
            yield {'app_version': __version__,
                   'editor_version': __editor_version__,
                   'icons_version': __icons_version__,
                   'base_template': f'{base}.html',
                   'LANGUAGE_CODE': code,
                   'STATIC_URL': settings.STATIC_URL}
