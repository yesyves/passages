#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path, re_path
from django.views.generic.base import RedirectView

import user.views
from config.forms import LoginForm
from config.settings.common import USE_SSO
from . import views
from .XMsalViews import XMsalViews

if USE_SSO:
    # AzureAD imports (python MSAL library)
    # aad: import default MSAL views
    from ms_identity_web.django.msal_views_and_urls import MsalViews
    # aad: get default MSAL url patterns for AzureAD endpoints
    # msal_urls = MsalViews(settings.MS_IDENTITY_WEB).url_patterns()
    msal_urls = XMsalViews(settings.MS_IDENTITY_WEB).url_patterns()

urlpatterns = [
    # Index
    path('', RedirectView.as_view(url='/portfolio/'), name='home'),
    # Django Admin (doc needs to be first)
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    # Django Authentification
    path('accounts/login/',
         views.Login.as_view(authentication_form=LoginForm,),
         name='login'),
    # Needs to be after login for LoginForm
    path('accounts/', include('django.contrib.auth.urls')),
    path('password_reset/', views.PasswordResetGoogleRecaptchaView.as_view(),
         name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    # Internationalisation
    path('i18n/', include('django.conf.urls.i18n')),
    # path('jsi18n/', JavaScriptCatalog.as_view(),
    #      name='javascript-catalog'),
    # Favicon
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('icons/favicons/favicon.ico'))),
    # Apps
    path('editor/', include('editor.urls')),
    path('portfolio/', include('portfolio.urls')),
    path('program/', include('program.urls')),
    path('user/', include('user.urls')),
    path('utils/', include('utils.urls')),
]
if USE_SSO:
    urlpatterns += [
    # Azure AD login URLs - prefix = auth (e.g. /auth/sign_in, /auth/sign_out, etc.)
    path(f'{settings.AAD_CONFIG.django.auth_endpoints.prefix}/', include(msal_urls)),

    # Root path for AAD to redirect to after successful authentication (alias must be 'index' to match w/ MSAL code);
    # /ms_identity_web/django/msal_views_and_urls.py @ aad_redirect (AAD uses 'index' as internal alias)
    path('aad-redirect', user.views.validate_aad_session, name='index'),    # user/views
    path('aad-logout', user.views.logout_aad_session, name='aad_logout'),   # user/views
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
