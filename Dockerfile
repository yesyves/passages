FROM python:3

# required to fix file ownership issues between host ($USER) and docker (root);
# defined in docker-compose.yml
ARG user
ARG uid

# RUN FOLLOWING COMMANDS AS DEFAULT DOCKER ROOT
# system updates + install graphviz-dev which is required for WeasyPrint
RUN apt update && \
    apt upgrade -y && \
    apt install -y gettext graphviz-dev git vim postgresql-client

# create user w/ same username & uid as on host and assign it to 'root' group + create home directory for this user
RUN useradd -G root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user && chown -R $user:$user /home/$user

# create /home/passages folder where the code will reside in the container
RUN mkdir /home/passages
# set /home/passages as work directory for the Docker environment
WORKDIR /home/passages
# copy Python requirements folder to the image
COPY ./tools/requirements/ .
# install base & dev requirements via pip
RUN pip install -r base.txt && \
    pip install -r dev.txt

# IMPORTANT: run after pip installs to minimize Docker build time when making changes to .bashrc or other minor details
# install starship.rs & configure 'll' alias for better shell experience
# https://stackoverflow.com/questions/74399476/how-to-pass-yes-to-sh-command-like-curl-url-sh-sh
RUN curl -sS https://starship.rs/install.sh | sh -s -- -y && \
    echo "alias ll='ls \$LS_OPTIONS -l'" >> /home/$user/.bashrc && \
    echo 'eval "$(starship init bash)"' >> /home/$user/.bashrc

# copy entire current directory (root of 'passages') to image;
# NOTE: this has ZERO effect on subsequent Docker restarts, ONLY 1st time; so consider removing it and rely only on
# local shared volume set in docker-compose.yml
#COPY . .

# change owner of /home/passages to eliminate 'root' permission issues
RUN chown -R $user:$user .

# SWITCH TO HOST-FRIENDLY USER FROM THAT POINT ON (when we do a docker exec, we'll be connected as that user)
USER $user

# open shell by default instead of default python image's Python repl
CMD ["/bin/bash"]