#!/usr/bin/env bash

# todo for debugging purposes only
#printenv

echo "**********************************************************"
echo "PASSAGES: RECOGNITION OF EXPERIENTIAL PRIOR LEARNING"
echo "**********************************************************"
echo ""

# discard any local changes to migrations, locales, etc.; otherwise, `git pull` will fail
git restore . # or git stash
# update git repository
git pull

# always update pip + deps on container restart since the one in Dockerfile is run only on first init
pip install --upgrade pip
pip install -r ./tools/requirements/production.txt

# presence of this 'lockfile' indicates db is already set, so do NOT overwrite it on each restart;
# this file being persisted in the volume, it will only be lost if we destroy the volume
CONTAINER_FIRST_STARTUP="CONTAINER_FIRST_STARTUP"

# if 'lockfile' not found, this means we need to do a 1st-time full db initialization
if [ ! -e ./$CONTAINER_FIRST_STARTUP ]; then
  echo "Passages -> first database initialization"

  # create 'lockfile' to indicate on next startup to skip full db reset
  touch ./$CONTAINER_FIRST_STARTUP;

  # run first-init script - creates db structure & fill in minimal test data
  ./tools/db-first-init.prod.sh

# if 'lockfile' not found, just run migrations (no risk there)
else
  echo "Passages -> database already initialized"

  # todo do NOT wipe migrations on each restart, only run 'makemigrations'
  # note: wipe all local migration files to avoid migration conflicts
  # find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete
  #python manage.py makemigrations
  python manage.py migrate
fi

# re-run translations & compression on every restart (since non-destructive)
#./tools/translate.sh
python manage.py compilemessages
./tools/compress.sh --force

# copy logo to destination (must be JPG) - always after `compress.sh` since it wipes staticfiles
if [ -e ./institution-logo.jpg ]; then
  cp -f ./institution-logo.jpg ./dproject/staticfiles/institution-logo.jpg
fi

# start Django gunicorn service
gunicorn config.wsgi:application --worker-class=gevent --workers=4 --bind 0.0.0.0:8000
#gunicorn config.wsgi:application --bind 0.0.0.0:8000
