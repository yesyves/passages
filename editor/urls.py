#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from django.urls import path

from . import views

urlpatterns = [
    path('release_achievement', views.release_achievement,
         name="release_achievement"),
    path('submit_achievement', views.submit_achievement,
         name='submit_achievement'),
    path('<slug:instance_id>', views.get_achievement,
         name="get_achievement"),
    path('<slug:achievement_id>/undo', views.undo_save,
         name="undo_save"),
    path('<slug:version_id>/compare', views.compare_version,
         name="compare_version"),
    path('<slug:instance_id>/<int:mode>', views.get_achievement,
         name="get_achievement_mode"),
    path('<slug:achievement_id>/<str:mode>', views.print_achievement,
         name="print_achievement"),
    # get achievement from specified portfolio
    path('<slug:instance_id>/<int:mode>/<str:portfolio_id>', views.get_achievement,
         name="get_achievement_by_portfolio")
]
