# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations: https://doc.epassages.ca/index.php/contributions/
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

import json
import locale

from delta import html
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage
from django.core.mail import mail_admins
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.utils.translation import gettext as _
from urlextract import URLExtract
from weasyprint import HTML

from config.settings.common import IS_CONTINUOUS, IS_MILESTONE, SESSION_ACTIVE_PORTFOLIO
from portfolio.forms import (
    FormTargetedSkill, FormAchievementVersionApplicant, FormAchievementVersionReviewer
)
from portfolio.models import (
    AchievementDevelopment, AchievementContext, AchievementMotivation, AchievementVersion, Document,
    UserAchievement, EditorBlock, AchievementType, Portfolio, Achievement, HelpLink, UserAchievementHistory
)
from program.models import Competency, CustomCompetencyType, CustomCompetency, Program
from user.models import User
from utils.views import active_portfolio, check_restriction, get_portfolio, get_help_link, active_portfolio_type

locale.setlocale(locale.LC_ALL, '')


@login_required
def get_achievement(request, instance_id, mode=0, portfolio_id=None):
    """Call edit_achievement or view_version depending on what id points to

    Args:
        request
        instance_id: uuid
        mode: 0 = achievement_id
              1 = version_id
              2 = ignore restriction
              3 = set as active portfolio (direct link - portfolio_id must be given)
        portfolio_id: uuid|None

    Returns:
        HttpResponse (editor/index.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    if IS_CONTINUOUS:
        return _get_achievement_continuous(request, instance_id, mode)
    else:
        return _get_achievement_milestone(request, instance_id, mode, portfolio_id)


def _get_achievement_continuous(request, instance_id, mode=0):
    """Call edit_achievement or view_version depending on what id points to

    Args:
        instance_id: uuid
        mode: 0 = achievement_id
              1 = version_id
              2 = ignore restriction

    Returns:
        HttpResponse (editor/index.html)

    """

    user_type = request.user.type
    context = {
        'help': get_help_link('editor'),
        'previous_revision': False,
    }

    if mode == 1:
        version = AchievementVersion.available_objects.get(id=instance_id)
        context.update(
            view_version(version, user_type, compare=False)
        )
    else:
        achievement = Achievement.available_objects.get(id=instance_id)

        # Created status means no achievement contents yet
        if achievement.status == 'Created':
            return redirect('show_questionnaire', achievement_id=instance_id)

        if mode != 2 and achievement.portfolio.status != 'Certified' and achievement.portfolio.status != 'Archived':
            restrict = check_restriction(request, achievement, 'achievement')
            if restrict:
                return restrict

        context.update(_edit_achievement_continuous(achievement, user_type))

    # todo try to get current tab from URL in order to load proper tab by default instead of via JavaScript
    # print("_get_achievement_continuous -> final context")
    # print(context.keys())
    # print(request.GET)

    template = loader.get_template('editor/index.html')
    return HttpResponse(template.render(context, request))


def _get_achievement_milestone(request, instance_id, mode, portfolio_id):

    request_user = request.user

    # if 'portfolio_id' is passed, this is the 'example' (mode=0) or a direct link (mode=3),
    # so bypass active portfolio
    if portfolio_id is not None:
        portfolio = get_portfolio(portfolio_id)
        if mode == 3:
            request.session[SESSION_ACTIVE_PORTFOLIO] = str(portfolio.id)
    else:
        portfolio = active_portfolio(request)
        if portfolio is None:
            return redirect('home')

    # make sure student is NOT able to view achievement when under review (milestone)
    restrict = check_restriction(request, portfolio)
    if restrict:
        return restrict

    # try to get one shared portfolio example
    example_portfolio = None
    try:
        example_portfolio = Portfolio.available_objects.get(
            share=True, program=portfolio.program)
        example_portfolio = example_portfolio.id
    except ObjectDoesNotExist as e:
        print(e)

    # only allow editing portfolio if logged-in user is owner of portfolio OR admin
    editable = request_user == portfolio.user or request_user.has_teacher_role()
    if portfolio.user.group.status == 'Final' and not request_user.has_teacher_role():
        editable = False

    if editable:
        #
        # Versions
        if request_user.type == 'Applicant':
            f_version = FormAchievementVersionApplicant()
        else:
            f_version = FormAchievementVersionReviewer()
            f_version.initial['portfolio_id'] = portfolio.id
        f_version.initial['achievement_id'] = instance_id
    else:
        f_version = None

    context = {
        'help': get_help_link('editor'),
        'editable': editable,
        'f_version': f_version,
        # id of the example portfolio (if exists)
        'example_portfolio': example_portfolio,
        # whether current portfolio being viewed by user is indeed the example portfolio
        'is_example_portfolio': example_portfolio is not None and example_portfolio == portfolio.id
    }

    # get the achievement type, generic to all users
    achievement = AchievementType.available_objects.get(id=instance_id)

    context.update(_edit_achievement_milestone(achievement, portfolio.user))

    # load all achievement types for current user's portfolio type so we can implement next/previous
    portfolio_achievements = map(
        lambda x: x,
        active_portfolio_type().achievement_types.all())
    # convert to hashmap/dict <index>:<value>
    portfolio_achievements = dict(enumerate(portfolio_achievements))

    # get the next / previous achievement OR return None if invalid index
    achievement_next = portfolio_achievements.get(achievement.number, None)
    achievement_prev = portfolio_achievements.get(achievement.number - 2, None)

    context.update({'achievement_next': achievement_next,
                    'achievement_prev': achievement_prev})

    # DEBUG
    # print(context)

    template = loader.get_template('editor/index.html')
    return HttpResponse(template.render(context, request))


@login_required
def print_achievement(request, achievement_id, mode):
    """Print achievement

    Calls edit_achievement

    Args:
        achievement_id: uuid
        mode: read / print / pdf

    Returns:
        HttpResponse (print/achievement.html)

    """

    if request.method != 'GET':
        raise ValueError(request)

    achievement = Achievement.available_objects.get(id=achievement_id)
    context = _edit_achievement_continuous(achievement, request.user.type, printing=True)

    # Render quill ops

    if mode == 'read':
        base_template = 'base.html'
    else:
        base_template = f'print/{mode}.html'

    context.update({
        'mode': mode,
        'base_template': base_template,
    })

    # Read or Print
    if mode != 'pdf':
        template = loader.get_template('print/achievement.html')
        return HttpResponse(template.render(context, request))
    # PDF Export
    else:
        # IMPORTANT: when printing, 'is_continuous' (context processor) is lost, so we need to set 'is_continuous'
        # directly as part of the context so it is available to print/contents.html; else 'milestone' is assumed,
        # which bugs the pdf printing since variables do not match between modes. (print/contents.html:37)
        context.update({
            'is_continuous': IS_CONTINUOUS
        })

        html_string = loader.render_to_string('print/achievement.html', context)

        html_pdf = HTML(string=html_string)
        html_pdf.write_pdf(target=f'{settings.FILE_UPLOAD_TEMP_DIR}/achievement.pdf',
                           stylesheets=[f'{settings.STATIC_ROOT}/css/pdf.css'])

        attachment = _('attachment; filename=Achievement_{}.pdf').format(achievement.number)

        fs = FileSystemStorage(settings.FILE_UPLOAD_TEMP_DIR)
        with fs.open('achievement.pdf') as pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            response['Content-Disposition'] = attachment
            return response


@login_required()
def undo_save(request, achievement_id):
    """Delete last save

    Args:
        achievement_id: uuid

    Returns:
        redirect(editor/achievement_id)
    """

    if request.method != 'GET':
        raise ValueError(request)

    motivations = AchievementMotivation.available_objects.filter(achievement_id=achievement_id)
    contexts = AchievementContext.available_objects.filter(achievement_id=achievement_id)
    developments = AchievementDevelopment.available_objects.filter(achievement_id=achievement_id)
    last_save = None
    if developments.count() > 1:
        last_save = developments[0]
    if contexts.count() > 1 and (last_save is None
                                 or contexts[0].created > last_save.created):
        last_save = contexts[0]
    if motivations.count() > 1 and (last_save is None
                                    or motivations[0].created > last_save.created):
        last_save = motivations[0]
    if last_save is not None:
        last_save.delete()

    return redirect('get_achievement', instance_id=achievement_id)


@login_required
def compare_version(request, version_id):
    """Compare current version with last revision

    Args:
        version_id: uuid

    Returns:
        HttpResponse (editor/compare.html)
    """

    if request.method != 'GET':
        raise ValueError(request)

    version = AchievementVersion.available_objects.get(id=version_id)
    user_type = request.user.type

    context = {
        'current': _edit_achievement_continuous(version.achievement, user_type),
        'version': view_version(version, user_type, compare=True),
    }

    template = loader.get_template('editor/compare.html')
    return HttpResponse(template.render(context, request))


def _edit_achievement_continuous(achievement, user_type, printing=False):
    """Load editor contents for achievement

    Calls load_editor()

    Args:
        achievement: Object
        user_type: str

    Returns:
        Context

    """

    # get current portfolio type and its achievement types (tabs)
    portfolio_type = active_portfolio_type()
    tabs = portfolio_type.get_achievement_types()

    # active blocks = editor blocks associated with each achievement type (aka tab)
    active_blocks = []
    for tab in tabs:
        blocks = list(tab.editor_blocks())
        active_blocks += blocks # list concatenation

    # store display-friendly info about UserAchievement blocks (mostly for print/contents.html)
    full_blocks = []
    # store actual JSON ops (raw)
    block_contents = {}

    # get each entry per block for this achievement per portfolio_user
    for block in active_blocks:
        # each block is an entry for the achievement for this particular portfolio_user @ a specific timestamp
        try:
            user_achievement = UserAchievement.available_objects.get(
                achievement=achievement,
                block=block,
                portfolio=achievement.portfolio
            )

            if printing:
                ops = json.loads(user_achievement.content)['ops']
                try:
                    quill_rendering = html.render(ops)
                except Exception as e:
                    # DEBUG
                    # print("ERROR rendering HTML for block")
                    # print(ops)
                    # print(e)
                    quill_rendering = "" + e.__str__()
                full_blocks.append({
                    'title': user_achievement.block.instruction or "",
                    'content': quill_rendering,
                    'word_count': len(quill_rendering.split()),
                    'max_word_count': user_achievement.block.target_words
                })

            # json ops
            block_contents[block.id] = user_achievement.content or ""

        except ObjectDoesNotExist:
            continue

    context = {
        'achievement': achievement,
        'tabs': tabs, # AchievementType
        'contents': block_contents,
        'block_contents': full_blocks,
        'active_editor_blocks': active_blocks,
        'version': None,
    }

    ops = []

    editor_context = _load_editor_continuous(achievement, user_type, ops, is_version=False, compare=False)
    context.update(editor_context)

    return context


def _edit_achievement_milestone(achievement: AchievementType, portfolio_user):
    """Load editor contents for achievement

    Calls load_editor()

    Args:
        achievement: portfolio.AchievementType
        portfolio_user: User

    Returns:
        Context
    """

    # get editor blocks related to this achievement type
    # ONLY load editor blocks matching current portfolio_user's group's milestone & assignment

    group_milestone = portfolio_user.group.milestone
    group_assignment = portfolio_user.group.assignment
    portfolio = portfolio_user.portfolio()

    milestone_blocks = achievement.blocks.filter(milestone__lte=group_milestone)

    active_blocks = []
    for block in milestone_blocks:
        if block.milestone < group_milestone or block.assignment <= group_assignment:
            active_blocks.append(block)

    # DEBUG
    # print("editor-blocks")
    # print(editor_blocks)

    block_contents = {}
    # get each entry per block for this achievement per portfolio_user
    for block in active_blocks:
        # each block is an entry for the achievement type for this particular portfolio_user @ a specific timestamp
        block_entry = ''
        try:
            # todo (conceptual) make sure ONLY ONE UserAchievement per achievement-type/block/portfolio/!step
            block_entry = UserAchievement.available_objects.filter(
                achievement_type=achievement,
                block=block,
                portfolio=portfolio
            )[0].content
        except IndexError:
            pass
        block_contents[block.id] = block_entry

    context = {
        'achievement': achievement,
        # 'portfolio_id': portfolio.id,
        # 'semester': portfolio_user.group.semester.id,
        'group': portfolio_user.group.id,
        'group_milestone': group_milestone,      # use |add:"0" or |add:0 in template to coerce int->str, str->int
        'group_assignment': group_assignment,    # use |add:"0" or |add:0 in template to coerce int->str, str->int
        'contents': block_contents,
        'active_editor_blocks': active_blocks,
        'version': None,
    }

    ops = []
    context.update(
        _load_editor_milestone(achievement, portfolio_user, ops, is_version=False, compare=False)
    )

    return context


def view_version(version, user_type, compare):
    """Load editor contents for a specific achievement version

    Calls load_editor()

    Args:
        version: Object
        user_type: str
        compare: Is is a version to compare ? (bool)

    Returns:
        Context
    """

    # get current portfolio type and its achievement types (tabs)
    portfolio_type = active_portfolio_type()
    tabs = portfolio_type.get_achievement_types()

    # active blocks = editor blocks associated with each achievement type (aka tab)
    active_blocks = []
    for tab in tabs:
        blocks = list(tab.editor_blocks())
        active_blocks += blocks # list concatenation

    # store display-friendly info about UserAchievement blocks (mostly for print/contents.html)
    # Versions don't print
    # full_blocks = []
    # store actual JSON ops (raw)
    block_contents = {}

    # get each entry per block for this achievement per portfolio_user
    for block in active_blocks:
        # each block is an entry for the achievement for this particular portfolio_user @ a specific timestamp
        try:
            user_achievement_history: UserAchievementHistory = version.user_blocks.get(block=block)

            # ops = json.loads(user_achievement_history.content)['ops']
            # quill_rendering = html.render(ops)
            # full_blocks.append({
            #     'title': user_achievement_history.block.instruction or "",
            #     'content': quill_rendering,
            #     'word_count': len(quill_rendering.split()),
            #     'max_word_count': user_achievement_history.block.target_words
            # })

            # json ops
            block_contents[block.id] = user_achievement_history.content or ""

        except ObjectDoesNotExist:
            continue

    context = {
        'achievement': version.achievement,
        'tabs': tabs,
        'contents': block_contents,
        # 'block_contents': full_blocks,
        'active_editor_blocks': active_blocks,
        'version': version,
    }

    ops = []

    context.update(
        _load_editor_continuous(version.achievement, user_type, ops, is_version=True, compare=compare)
    )

    return context


def _load_editor_continuous(achievement, user_type, ops, is_version, compare):
    """Returns editor context

    Calls load_ops()

    Args:
        achievement: Object
        user_type: str
        ops: list
        is_version: True => Load uneditable version (bool)
        compare: True => Load for comparison only (bool)

    Returns:
        Context

    """

    # GET
    portfolio = achievement.portfolio

    #
    # Editable
    editable = True
    if is_version or portfolio.status == 'Certified' or portfolio.status == 'Archived':
        editable = False

    competencies = Competency.available_objects.filter(program=portfolio.program)
    serialized_competencies = []

    for competency in competencies:
        competency.htmlName = competency.name.replace(' ', '-')
        serialized_competencies.append(serializers.serialize('json', [competency]))

    # UQAR: get facets for this achievement's development level
    tmp_competencies = CustomCompetency.available_objects.filter(
        type__code="facets",
        parent__isnull=False # only from 2nd level down
        # parent__code=achievement.development_level or 1
    )
    tmp_competencies_for_level = []
    for tmp_comp in tmp_competencies:
        id_parts = tmp_comp.id.split(".")
        # print(id_parts) # ['9', '1', '2', '3']

        # index @1 = development level of competency and MUST match dev level of achievement
        if len(id_parts) > 0 and id_parts[1] != str(achievement.development_level):
            continue
        tmp_competencies_for_level.append(tmp_comp)

    custom_competencies = {
        "facets": serializers.serialize('json', tmp_competencies_for_level)
    }

    achievement_versions = None
    previous_revision = None
    f_version = None
    f_target = None
    serialized_documents = []

    if not compare:
        #
        # Targeted skill
        f_target = FormTargetedSkill(competencies)
        f_target.initial['achievement_id'] = achievement.id
        if achievement.targeted_competency is not None:
            f_target.fields['targeted_skill'].initial = achievement.targeted_competency.id

        #
        # Versions
        if user_type == 'Applicant':
            f_version = FormAchievementVersionApplicant()
        else:
            f_version = FormAchievementVersionReviewer()
        f_version.initial['achievement_id'] = achievement.id
        f_version.initial['portfolio_id'] = portfolio.id

        if user_type != 'Expert':
            achievement_versions = AchievementVersion.available_objects.filter(achievement=achievement)
            if achievement_versions and not is_version:
                for version in achievement_versions:
                    if version.status == 'Reviewed':
                        previous_revision = version
                        break

        #
        # Documents
        documents = Document.objects.filter(achievements__id=achievement.id)
        # convert the documents as JSON
        for doc in documents:
            serialized_documents.append(serializers.serialize('json', [doc]))

    context = {
        'achievement_versions': achievement_versions,
        'competencies': competencies,
        'previous_revision': previous_revision,
        'dev_mode': True,                          # Activate Quill Ops view
        'editable': editable,
        'f_target': f_target,
        'f_version': f_version,
        'portfolio': portfolio,
        'custom_competencies': custom_competencies,
        'serialized_competencies': serialized_competencies,
        'serialized_documents': serialized_documents
    }

    context.update(
        load_ops(competencies, ops)
    )

    return context


def _load_editor_milestone(achievement_type, user, ops, is_version, compare):
    """Returns editor context

    Calls load_ops()

    Args:
        achievement_type: AchievementType
        user: User
        ops: list
        is_version: True => Load uneditable version (bool)
        compare: True => Load for comparison only (bool)

    Returns:
        Context
    """

    # GET
    portfolio = user.portfolio()

    competencies = Competency.available_objects.filter(program=portfolio.program)

    # get all competency types linked to this portfolio program
    custom_competency_types = CustomCompetencyType.available_objects.filter(program=portfolio.program)
    ctype: CustomCompetencyType
    custom_competencies = {}
    for ctype in custom_competency_types:
        tmp_competencies = CustomCompetency.available_objects.filter(type=ctype)
        custom_competencies[ctype.code] = serializers.serialize('json', tmp_competencies)

    # original competencies related to the program; not customizable per institution
    serialized_competencies = []
    for competency in competencies:
        competency.htmlName = competency.name.replace(' ', '-')
        serialized_competencies.append(serializers.serialize('json', [competency]))

    # achievement_versions = None
    # previous_revision = None
    # f_version = None
    # f_target = None
    serialized_documents = []

    # if not compare:
    #     #
    #     # Targeted skill
    #     f_target = FormTargetedSkill(competencies)
    #     f_target.initial['achievement_id'] = achievement_type.id
    #     if achievement_type.targeted_competency is not None:
    #         f_target.fields['targeted_skill'].initial = achievement_type.targeted_competency.id

    #
    # Documents
    documents = Document.available_objects.filter(portfolio=portfolio)
    # convert the documents as JSON
    for doc in documents:
        serialized_documents.append(serializers.serialize('json', [doc]))

    context = {
        # 'achievement': achievement_type,
        # 'achievement_versions': achievement_versions,
        # 'competencies': competencies,
        # 'editable': editable,
        # 'previous_revision': previous_revision,
        'dev_mode': True,                          # Activate Quill Ops view
        # 'f_target': f_target,
        # 'f_version': f_version,
        'portfolio': portfolio,
        'serialized_competencies': serialized_competencies,
        'serialized_documents': serialized_documents,
        'custom_competencies': custom_competencies
    }

    # if ops and ops is not None:
    #     ops = ()
    #
    # context.update(
    #     load_ops(competencies, ops)
    # )

    return context


def load_ops(competencies, ops):
    """Process ops for Résumé Tab

    Args:
        competencies: list
        ops: list

    Returns:
        Context
    """

    #
    # Resume
    skills = {}
    associated_skills = []
    levels = {}
    knowledges = []
    soft_skills = []
    vocabulary = []
    current_level_2 = 'E'
    if ops:
        for attributes, insert in ops:
            # Get the blots' attributes
            json_ops = json.loads(attributes)
            # Check attribute type and put in table
            if json_ops['type'] == 'competency':
                skills[json_ops['attributes']['competency_id']] = json_ops['id']
            elif json_ops['type'] == 'associated_skill':
                associated_skill = {
                    'id': json_ops['id'],
                    'cls': json_ops['class'],
                    'name': json_ops['attributes']['name'],
                }
                associated_skills.append(associated_skill)
            elif json_ops['type'] == 'expertise_level':
                level = json_ops['attributes']['level']
                if not level:
                    continue
                if ord(level) >= ord('M'):
                    if current_level_2 == 'H':
                        raise ValueError(json_ops)
                    level = current_level_2
                    current_level_2 = chr(ord(current_level_2) + 1)
                levels[level] = json_ops['id']
            elif json_ops['type'] == 'knowledge':
                knowledge = {
                    'id': json_ops['id'],
                    'cls': json_ops['class'],
                    'name': json_ops['attributes']['name'],
                    'explanation': json_ops['attributes']['explanation'],
                }
                knowledges.append(knowledge)
            elif json_ops['type'] == 'soft_skill':
                soft_skill = {
                    'id': json_ops['id'],
                    'cls': json_ops['class'],
                    'name': json_ops['attributes']['name'],
                    'explanation': json_ops['attributes']['explanation'],
                }
                soft_skills.append(soft_skill)
            elif json_ops['type'] == 'vocabulary':
                extractor = URLExtract()
                definition = json_ops['attributes']['definition']
                for url in extractor.gen_urls(definition):
                    if 'http' in definition:
                        href = f'<a href="{url}">{url}</a>'
                    else:
                        href = f'<a href="https://{url}">{url}</a>'
                    definition = definition.replace(url, href)
                definition = definition.replace('\n', '<br>')
                expression = {
                    'id': json_ops['id'],
                    'cls': json_ops['class'],
                    'name': insert.capitalize(),
                    'explanation': definition,
                }
                vocabulary.append(expression)

        # if ops ...
        for competency in competencies:
            if str(competency.id) in skills:
                skills[competency.code] = skills.pop(competency.id)

    context = {
        'skills': skills,
        'associated_skills': associated_skills,
        'levels': sorted(levels.items()),
        'knowledges': knowledges,
        'soft_skills': soft_skills,
        'vocabulary': vocabulary,
    }

    return context


@login_required
def release_achievement(request):
    """ Reset active_user timestamp for achievement

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    # since this func is called in both CONTINUOUS and MILESTONE modes, safer to apply condition
    if IS_CONTINUOUS:
        achievement = Achievement.available_objects.get(
            id=request.POST.get('achievement_id')
        )
        achievement.update_active_user()

    return HttpResponse(
        json.dumps({'success': True}),
        content_type='application/json'
    )


@login_required
def submit_achievement(request):
    """Save achievement and update status

    Args:
        request: (HttpRequest object)
            POST.achievement_id: uuid

    Returns:
        HttpResponse (application/json)

    """

    if request.method != 'POST':
        raise ValueError(request)

    if IS_CONTINUOUS:
        return _submit_achievement_continuous(request)
    else:
        return _submit_achievement_milestone(request)


def _submit_achievement_continuous(request):
    user: User = request.user
    achievement = Achievement.available_objects.get(id=request.POST.get('achievement_id'))
    portfolio = achievement.portfolio

    response = {
        'success': True,
        'blocks': {}
    }

    contents = request.POST.get('contents')
    contents_json = json.loads(contents)

    if user.type == 'Applicant':
        if achievement.status == "Reviewed" or achievement.status == "Rejected":
            achievement.status = "Draft"
            achievement.save()

    for block_id in contents_json:
        # e.g. { quill: [{ ops: {} }] }
        block_content = contents_json[block_id]
        block_content_quill = block_content['quill']
        block_content_tab = block_content['tab']

        # todo maybe use achievement_type.id instead of .number since .number is user-defined, so less unique-safe
        achievement_type = AchievementType.available_objects.get(number=block_content_tab)
        user_achievement, created = UserAchievement.available_objects.get_or_create(
            achievement_id=achievement.id,
            achievement_type=achievement_type,
            block_id=block_id,
            portfolio_id=portfolio.id
        )

        if user_achievement is not None:
            # important to avoid double quotes which are invalid JSON
            user_achievement.content = json.dumps(block_content_quill)
            user_achievement.save()

        # keep track of edits
        user_achievement_history = UserAchievementHistory(
            user_achievement_id=user_achievement.id,
            block_id=block_id,
            content=json.dumps(block_content_quill),
            author=user.id
        )
        user_achievement_history.save()

        response["blocks"][block_id] = _parse_editor_contents_continuous(
            editor_contents=block_content_quill,
            achievement_part=user_achievement
        )


    # if contents := request.POST.get('motivation_contents'):
    #     achievement_part = AchievementMotivation(content=contents, achievement=achievement)
    #     achievement_part.save()
    #     response['motivation_contents'] = _parse_editor_contents_continuous(contents, achievement_part, 'motivation')
    #
    # if contents := request.POST.get('context_contents'):
    #     achievement_part = AchievementContext(content=contents, achievement=achievement)
    #     achievement_part.save()
    #     response['context_contents'] = _parse_editor_contents_continuous(contents, achievement_part, 'context')
    #
    # if contents := request.POST.get('development_contents'):
    #     achievement_part = AchievementDevelopment(content=contents, achievement=achievement)
    #     achievement_part.save()
    #     response.update(
    #         _parse_editor_contents_continuous(contents, achievement_part, 'development')
    #     )

    achievement.update_active_user(request.user)

    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )


def _submit_achievement_milestone(request):
    """

    Args:
        request:

    Returns:

    """
    achievement_type = AchievementType.available_objects.get(
        id=request.POST.get('achievement_id')
    )
    # ID of portfolio to which this achievement belongs
    owner_portfolio = request.POST.get('portfolio_id')

    response = {
        'success': True,
        'blocks': {}
    }

    user = request.user

    # get the POST 'contents' as JSON string, then convert to real JSON
    contents = request.POST.get('contents')
    contents_json = json.loads(contents)

    # iterate over keys (aka editor block IDs)
    for block_id in contents_json:
        block_content = contents_json[block_id]     # { milestone: 1, quill: [{ ops: {} }] }
        block_content_quill = block_content['quill']
        block_content_milestone = block_content['milestone']

        # get full object for the block ID
        block_obj = EditorBlock.available_objects.get(id=block_id)

        # try to see if 'UserAchievement' exists for this combination of type, block, user;
        # create if does not exist; ensures single entry per achievement type per user (owner) per block
        user_achievement, created = UserAchievement.available_objects.get_or_create(
            achievement_type=achievement_type,
            block=block_obj,
            portfolio_id=owner_portfolio
        )

        # update 'content'/'step' w/ JSON ops from Quill
        if user_achievement is not None:
            # important to avoid double quotes which are invalid JSON
            user_achievement.content = json.dumps(block_content_quill)
            user_achievement.save()

        # # NOTE: each save creates a new entry in the database; create new UserAchievementHistory instance ...
        # # keeps version history of who edited which block for which UserAchievement
        # user_achievement_history = UserAchievementHistory(
        #     user_achievement_id=user_achievement.id,
        #     block=block_obj,
        #     content=json.dumps(block_content_quill), # important to avoid double quotes which are invalid JSON
        #     author=user.id # person editing the user achievement entry
        # )
        # # ... and save it
        # user_achievement_history.save()

        response["blocks"][block_id] = _parse_editor_contents_milestone(block_content_quill, user_achievement)

    return HttpResponse(
        json.dumps(response),
        content_type='application/json'
    )


def get_insert(insert):
    """Returns op's associated text

    Args:
        insert: str / dictionary

    Returns:
        str

    """
    if isinstance(insert, str):
        return insert
    if 'image' in insert:
        return _('Image: {}').format(insert['image'])
    if 'video' in insert:
        return _('Video: {}').format(insert['video'])
    mail_admins('Unexpected insert', str(insert))
    return ''


def parse_op(json_ops, i, ops_count, attribute_type):
    """Check if op is a new one, if so, concatenate insert from following ops as needed, else ignore

    Args:
        json_ops: ops array (list)
        i: current index in json_ops (int)
        ops_count: total number of ops (int)

    Returns:
        insert (str)
        False (bool)

    """

    # attribute_type = list(json_ops[i]['attributes'])[0]
    if (
        i - 1 >= 0
        and 'attributes' in json_ops[i - 1]
        and attribute_type in list(json_ops[i - 1]['attributes'])[0]
    ):
        # op is a continuation, ignore
        return False
    # New op, concat and return insert
    c = 1
    insert = get_insert(json_ops[i]['insert'])
    while (
        i + c < ops_count
        and 'attributes' in json_ops[i + c]
        and attribute_type in list(json_ops[i + c]['attributes'])
    ):
        insert += get_insert(json_ops[i + c]['insert'])
        c += 1
    return insert

def _validate_op_continuous(attribute_type, json_op, op_tracker, concentration, level_2):
    """Validate competency and level ops constraints

    Must only be called for relevant attribute types : competency / level

    Args:
        attribute_type: str
        json_op: op to validate (json object)
        op_tracker: lists of already found competencies and levels (dict)
        concentration: (char)
        level_2: list of already found levels 2 (dict)

    Returns:
        True (bool)
        delete_op (str)
        response (dict)

    """

    if attribute_type == 'competency':
        op = json.loads(json_op['attributes']['competency'])
        this_op = op['attributes']['competency_id']
        if this_op in op_tracker['competency_found']:
            # Duplicate competency
            if concentration == 'T':
                skill_number = this_op[6:]
            else:
                skill_number = this_op[4:]
            return {
                'success': False,
                'error': _('Skill number {} is declared for a second time').format(skill_number),
                'insert': json_op['insert'],
            }
        else:
            op_tracker['competency_found'].append(this_op)
    elif attribute_type == 'expertise_level':
        op = json.loads(json_op['attributes']['expertise_level'])
        this_op = op['attributes']['level']
        if not this_op:
            return 'delete_op'
        elif this_op in op_tracker['level_found']:
            # Duplicate level
            if this_op in 'MNOPQRS':
                level_definition = level_2[this_op]
            else:
                level_definition = this_op
            return {
                'success': False,
                'error': _('Expertise level "{}" is declared for a second time').format(level_definition),
                'insert': json_op['insert'],
            }
        op_tracker['level_found'] += this_op
        if this_op in 'MNOPQRS':
            if op_tracker['level_2_found'] == 3:
                # Too many level 2
                level_definition = level_2[this_op]
                return {
                    'success': False,
                    'error': _('No more than 3 level 2 are allowed, a fourth "{}" is declared').format(level_definition),
                    'insert': json_op['insert'],
                }
            else:
                op_tracker['level_2_found'] += 1
    return True

def _validate_op_milestone(attribute_type, json_op, op_tracker, concentration):
    """Validate competency and level ops constraints

    Must only be called for relevant attribute types : competency / level

    Args:
        attribute_type: str (dynamic)
        json_op: op to validate (json object)
        op_tracker: lists of already found competencies and levels (dict)
        concentration: (char)

    Returns:
        True (bool)
        delete_op (str)
        response (dict)

    """

    # deal w/ 'vocabulary' attributes separately; else causes error due to 'competency_id' NOT present for glossary
    if attribute_type == 'vocabulary':
        return True

    op = json.loads(json_op['attributes'][attribute_type])
    this_op = op['attributes']['competency_id']

    if this_op in op_tracker['competency_found']:
        # Duplicate competency
        if concentration == 'T':
            skill_number = this_op[6:] if attribute_type == "competency" else this_op
        else:
            skill_number = this_op[4:] if attribute_type == "competency" else this_op
        return {
            'success': False,
            'error': _('Skill number {} is declared for a second time').format(skill_number),
            'insert': json_op['insert'],
        }
    else:
        op_tracker['competency_found'].append(this_op)

    return True


def _parse_editor_contents_continuous(editor_contents, achievement_part: UserAchievement):
    """Parse editor (motivation, context, development) contents for Ops

    Save cert attributes into the database along with corresponding text inserts
    Return an error if ops are overlapping
    Clean foreign op attributes
    NB : Insert is a useless for now, but a good idea nonetheless

    Args:
        editor_contents: contents to be parsed (str)
        achievement_part: Object

    Returns:
        response (dict)

    """

    # DEBUG
    # print(editor_contents)

    achievement = achievement_part.achievement

    _portfolio: Portfolio = achievement.portfolio
    concentration = _portfolio.concentration.type
    level_2 = _portfolio.concentration.get_level_2()

    # Build a list of certification ops for this achievement
    achievement_ops = []

    # Check duplicable tags
    op_tracker = {
        'competency_found': [],
        'level_found': '',
        'level_2_found': 0,
    }

    # Attributes we want to parse
    cert_attributes = [
        'associated_skill',
        'competency',
        'expertise_level',
        'explain',
        'knowledge',
        'soft_skill',
        'vocabulary',
        'facets'
    ]

    # Attributes that are OK but we leave them alone
    neutral_attributes = ['list', 'link', 'new_text']
    neutral_set = set(neutral_attributes)

    # Store comment-related ops separately from main achievement ops to avoid conflict w/ default 'cert' attributes
    comments_ops = []
    # 'simple_comment' has its own view like 'vocabulary', BUT it's neither neutral nor cert attribute
    comment_attribute = 'simple_comment'

    # All ops in editor contents
    json_ops = editor_contents['ops']
    # Make a copy for parsing
    parse_ops = json_ops
    # Number of items in the json_ops (used to prevent index out of range errors w/ i+x)
    ops_count = len(json_ops)
    # Flag to update raw content if outsider attributes have been deleted
    update_raw = False

    for i, json_op in enumerate(parse_ops):
        if 'attributes' in json_op:
            # Op contains any attribute
            incoming_attributes = json_op['attributes']
            # DEBUG
            # print(f'{i} incoming_attributes:{incoming_attributes}')
            for attribute_type in incoming_attributes:
                # for j, attribute_type in enumerate(incoming_attributes):
                #     j = index of attribute in Op
                # DEBUG
                # print(f'{i} attribute_type:{attribute_type}')         # competency
                if attribute_type in neutral_attributes:
                    continue
                if attribute_type in cert_attributes:
                    # attribute type is cert attribute and first in Op
                    # DEBUG
                    # print(f'{i} parsed_attribute:{json.loads(incoming_attributes[attribute_type])}')

                    # Remove new_text attribute
                    if 'new_text' in incoming_attributes:
                        # DEBUG
                        # print('new_text in cert_attributes: ' + str(json_op['attributes']))
                        del json_op['attributes']['new_text']
                        # print('new_text deleted ?: ' + str(json_op['attributes']))

                    # Check for tags across paragraphs ..
                    if (
                        i+2 < ops_count
                        and json_ops[i + 1]['insert'] == '\n'
                        and 'attributes' in json_ops[i + 2]
                        and attribute_type == list(json_ops[i + 2]['attributes'])[0]
                    ):
                        return {
                            'success': False,
                            'error': _('There is a tag across 2 paragraphs'),
                            'insert': get_insert(json_op['insert']) + '\n' + get_insert(json_ops[i + 2]['insert']),
                        }

                    # Check for overlapping tags ...
                    if len(json_op['attributes']) > 2:
                        # Too much overlapping: Exit... Stage Left
                        # DEBUG
                        # print(json_op)
                        # DEBUG
                        # todo pass tab number (aka achievement type)
                        mail_admins(
                            'More than 2 tags error',
                            '{}\n\nIn : https://passages.uqar.ca/editor/{}?editor-tab=nav-tab-{}'.format(json_op,
                                                                                                         achievement.id,
                                                                                                         "todo"),
                        )
                        return {
                            'success': False,
                            'error': _('There are more than 2 overlapping tags'),
                            'insert': json_op['insert'],
                        }
                    if len(json_op['attributes']) == 2:
                        # 2 attributes: reject if it is anything else than a nested vocabulary or neutral op overlap
                        # DEBUG
                        # print(json_op)
                        attributes = list(json_op['attributes'])
                        attribute_set = set(attributes)
                        if 'vocabulary' in attributes:
                            if attribute_type != 'vocabulary':
                                # Add vocabulary since the other op will be inserted as single attribute
                                achievement_ops.append([incoming_attributes['vocabulary'], json_op['insert']])
                        elif not attribute_set & neutral_set:
                            # attributes not neutral
                            return {
                                'success': False,
                                'error': _('There are 2 overlapping tags'),
                                'insert': json_op['insert'],
                            }

                    # Single attribute to parse
                    insert = parse_op(json_ops, i, ops_count, attribute_type)
                    if insert:
                        response = _validate_op_continuous(attribute_type, json_op, op_tracker, concentration, level_2)
                        if response is True:
                            achievement_ops.append([incoming_attributes[attribute_type], insert])
                        elif response == 'delete_op':
                            # DEBUG
                            # print('parse_ops / json_op: ' + str(json_op))
                            # print('json_ops / i: ' + str(json_ops[i]))
                            del json_ops[i]['attributes']
                            update_raw = True
                        else:
                            return response

                # if attribute is a 'simple_comment', add it to 'achievement_ops'
                elif attribute_type == comment_attribute:
                    insert = parse_op(json_ops, i, ops_count, attribute_type)
                    if insert:
                        comments_ops.append([incoming_attributes[attribute_type], insert])

                # if attribute NOT a neutral attribute ...
                elif attribute_type not in neutral_attributes:
                    # Attribute neither in cert or neutral attributes
                    #     => We have outsider attributes so remove them
                    if 'attributes' in json_op:
                        # DEBUG
                        # print('parse_ops / json_op: ' + str(json_op))
                        # print('json_ops / i: ' + str(json_ops[i]))
                        del json_ops[i]['attributes']
                        # Wait until the end of the loop to actually update the DB
                        update_raw = True
                # endif attribute in cert_attributes

                # Never go further than first item
                break
            # endfor j, attribute in enumerate(incoming_attributes)
        elif (
                'image' in json_op['insert']
                and isinstance(json_op['insert'], dict)
                and 'base64' in json_op['insert']['image']
        ):
            mail_admins(
                'base64 Image in',
                '{}\n\nIn : https://passages.uqar.ca/editor/{}?editor-tab=nav-tab-development-{}'.format(
                    achievement.portfolio,
                    achievement.id,
                    achievement_part.achievement_type.number
                )
            )
            # DEBUG
            # print('parse_ops / json_op: ' + str(json_op))
            # print('json_ops / i: ' + str(json_ops[i]))
            del json_ops[i]
            update_raw = True
        # endif 'attributes' in json_op
    # endfor i, json_op in enumerate(json_ops)

    if update_raw:
        updated_contents = '{"ops":' + json.dumps(json_ops) + '}'
        achievement_part.content = updated_contents
        achievement_part.save(update_fields=["content"])
    else:
        updated_contents = None

    # save user achievement ops
    achievement_part.attributes = achievement_ops
    # save comments_ops
    achievement_part.comments = comments_ops
    # save both user achievement ops and comments ops
    achievement_part.save(update_fields=["attributes", "comments"])

    return updated_contents


def _parse_editor_contents_milestone(editor_contents, achievement_part: UserAchievement):
    """Parse editor (motivation, context, development) contents for Ops

    Save cert attributes into the database along with corresponding text inserts
    Return an error if ops are overlapping
    Clean foreign op attributes
    NB : Insert is a useless for now, but a good idea nonetheless

    Args:
        editor_contents: contents to be parsed (JSON dict)
        achievement_part: UserAchievement
        is_development: True | False

    Returns:
        response (dict)

    """

    _portfolio: Portfolio = achievement_part.portfolio
    concentration = _portfolio.concentration.type

    # Build a list of certification ops for this achievement
    achievement_ops = []
    # Store comment-related ops separately from main achievement ops to avoid conflict w/ default 'cert' attributes
    comments_ops = []

    # Check duplicable tags
    op_tracker = {
        'competency_found': [],
    }

    # No certification attributes outside of development
    # NOTE: must validate allowed attributes on EditorBlock (tags_options) and/or AchievementType (tags_options)
    editor_block: EditorBlock = achievement_part.block
    cert_attributes = editor_block.get_comp_types() \
        if editor_block.tags_options.count() > 0 \
        else achievement_part.achievement_type.get_comp_types()
    cert_attributes.append('vocabulary') # glossary

    # Attributes that are OK but we leave them alone
    # neutral = does NOT appear in views/reports like 'References' (vocabulary) or 'Comments' (simple_comment)
    neutral_attributes = ['bold', 'list', 'link', 'spellcheck', 'new_text']
    neutral_set = set(neutral_attributes)
    # 'simple_comment' has its own view like 'vocabulary', BUT it's neither neutral nor cert attribute
    comment_attribute = 'simple_comment'
    # All ops in editor contents
    json_ops = editor_contents['ops']
    # Make a copy for parsing
    parse_ops = json_ops
    # Number of items in the json_ops (used to prevent index out of range errors w/ i+x)
    ops_count = len(json_ops)
    # Flag to update raw content if outsider attributes have been deleted
    update_raw = False

    for i, json_op in enumerate(parse_ops):
        if 'attributes' in json_op:
            # Op contains any attribute
            incoming_attributes = json_op['attributes']
            # DEBUG
            # print(f'{i} incoming_attributes:{incoming_attributes}')
            for attribute_type in incoming_attributes:
                # for j, attribute_type in enumerate(incoming_attributes):
                #     j = index of attribute in Op
                # DEBUG
                # print(f'{i} attribute_type:{attribute_type}')         # competency
                if attribute_type in neutral_attributes:
                    continue
                if attribute_type in cert_attributes:
                    # attribute type is cert attribute and first in Op
                    # DEBUG
                    # print(f'{i} parsed_attribute:{json.loads(incoming_attributes[attribute_type])}')

                    # Remove new_text attribute
                    if 'new_text' in incoming_attributes:
                        # DEBUG
                        # print('new_text in cert_attributes: ' + str(json_op['attributes']))
                        del json_op['attributes']['new_text']
                        # print('new_text deleted ?: ' + str(json_op['attributes']))

                    # Check for tags across paragraphs ..
                    if (
                        i+2 < ops_count
                        and json_ops[i + 1]['insert'] == '\n'
                        and 'attributes' in json_ops[i + 2]
                        and attribute_type == list(json_ops[i + 2]['attributes'])[0]
                    ):
                        return {
                            'success': False,
                            'error': _('There is a tag across 2 paragraphs'),
                            'insert': get_insert(json_op['insert']) + '\n' + get_insert(json_ops[i + 2]['insert']),
                        }

                    # Check for overlapping tags ...
                    if len(json_op['attributes']) > 2:
                        # Too much overlapping: Exit... Stage Left
                        return {
                            'success': False,
                            'error': _('There are more than 2 overlapping tags'),
                            'insert': json_op['insert'],
                        }
                    if len(json_op['attributes']) == 2:
                        # 2 attributes: reject if it is anything else than a nested vocabulary or neutral op overlap
                        attributes = list(json_op['attributes'])
                        attribute_set = set(attributes)
                        if 'vocabulary' in attributes:
                            if attribute_type != 'vocabulary':
                                # Add vocabulary since the other op will be inserted as single attribute
                                achievement_ops.append([incoming_attributes['vocabulary'], json_op['insert']])
                        elif not attribute_set & neutral_set:
                            return {
                                'success': False,
                                'error': _('There are 2 overlapping tags'),
                                'insert': json_op['insert'],
                            }

                    # Single attribute to parse
                    insert = parse_op(json_ops, i, ops_count, attribute_type)
                    if insert:
                        response = _validate_op_milestone(attribute_type, json_op, op_tracker, concentration)
                        if response is True:
                            achievement_ops.append([incoming_attributes[attribute_type], insert])
                        elif response == 'delete_op':
                            # DEBUG
                            # print('parse_ops / json_op: ' + str(json_op))
                            # print('json_ops / i: ' + str(json_ops[i]))
                            del json_ops[i]['attributes']
                            update_raw = True
                        else:
                            return response

                # if attribute is a 'simple_comment', add it to 'achievement_ops'
                elif attribute_type == comment_attribute:
                    insert = parse_op(json_ops, i, ops_count, attribute_type)
                    if insert:
                        comments_ops.append([incoming_attributes[attribute_type], insert])

                # if attribute NOT a neutral attribute ...
                elif attribute_type not in neutral_attributes:
                    # Attribute neither in cert or neutral attributes
                    #     => We have outsider attributes so remove them
                    if 'attributes' in json_op:
                        # DEBUG
                        # print('parse_ops / json_op: ' + str(json_op))
                        # print('json_ops / i: ' + str(json_ops[i]))
                        del json_ops[i]['attributes']
                        # Wait until the end of the loop to actually update the DB
                        update_raw = True
                # endif attribute in cert_attributes

                # Never go further than first item
                break
            # endfor j, attribute in enumerate(incoming_attributes)
        # endif 'attributes' in json_op
    # endfor i, json_op in enumerate(json_ops)

    if update_raw:
        updated_contents = '{"ops":' + json.dumps(json_ops) + '}'
        achievement_part.content = updated_contents
        achievement_part.save(update_fields=["content"])
    else:
        updated_contents = None

    # save user achievement ops
    achievement_part.attributes = achievement_ops
    # save `achievement_part.comments = comments_ops`
    achievement_part.comments = comments_ops
    # save both user achievement ops AND comments ops
    achievement_part.save(update_fields=["attributes", "comments"])

    # print(achievement_part.attributes)

    return updated_contents
