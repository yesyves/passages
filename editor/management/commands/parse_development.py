#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import json

from django.core.management.base import BaseCommand

from portfolio.models import AchievementDevelopment


class Command(BaseCommand):
    help = """Check for bas64 images in ops and delete them
        """

    def handle(self, *args, **options):

        developments = AchievementDevelopment.available_objects.all()
        for development in developments:
            if 'base64' in development.content:
                json_ops = json.loads(development.content)['ops']
                for i, json_op in enumerate(json_ops):
                    if 'image' in json_op['insert'] and 'base64' in json_op['insert']['image']:
                        print(f'base64 in json_op: {development.achievement} - {development.achievement.portfolio.user}')
                        del json_op['insert']
                updated_contents = '{"ops":' + json.dumps(json_ops) + '}'
                development.content = updated_contents
                development.save(update_fields=["content"])
