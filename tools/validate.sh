#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# Validation tools

. tools/sh/set-env.sh

# Flake8
flake8 > flakes.py
$EDITOR flakes.py

# # html-validator
# cd ~/Documents
# vnu/bin/java -cp vnu.jar nu.validator.servlet.Main 8888 &
# echo "HTML validator listening on port 8888"
# $EDITOR validationerrors/

# W3C css validator
# https://jigsaw.w3.org/css-validator/

# js validator
# atom-jshint
