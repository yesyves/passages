#!/usr/bin/env bash

echo "ALWAYS run from project root, NEVER from './tools/tailwindcss'"
echo " - pass \`--watch\` to run tailwind-cli in watch mode instead of single-run"
echo " - pass any other cli parameter after \`./tools/tailwindcss/built-tailwind.sh\` as needed"
echo ""

#MODE=development
MODE=production

# check latest Tailwind version on https://tailwindcss.com/
TAILWIND_VERSION=3.3.3
TAILWIND_PATH=./dproject/static/vendor/tailwind-$TAILWIND_VERSION

# NOTE: w/ `$@`, we can pass other cli parameters (e.g. `--watch`) which will be
# appended to the initial command, thus simplifying script setup (no need for if conditions)
# https://tailwindcss.com/docs/installation
NODE_ENV=$MODE npx tailwindcss@latest \
  -c ./tailwind.config.js \
  -i $TAILWIND_PATH/tailwind.css \
  -o $TAILWIND_PATH/tailwind.build.min.css \
  --minify "$@"

# TODO Tailwind's built-in `--minify` is sufficient for now, but we could also use dedicated CSSNano
#echo " - 2. minify w/ CSSNano"
# https://cssnano.co/docs/getting-started
#npx postcss-cli \
#  $TAILWIND_PATH/tailwind.build.css \
#  > $TAILWIND_PATH/tailwind.build.min.css