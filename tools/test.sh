#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

. tools/sh/set-env.sh

coverage run manage.py test -v 2
coverage html
open -a Safari.app htmlcov/index.html
