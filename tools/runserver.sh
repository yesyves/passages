#!/usr/bin/env bash

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# Run Django server in DEV mode - e.g. ./tools/runserver.sh (listens on port 8000 by default)


# run Django in local dev mode

. ./tools/set-env.sh

python manage.py runserver 0.0.0.0:8000 --settings=config.settings.local
