#!/usr/bin/env python3

#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

from itertools import islice

import json
import sys
import unicodedata


if len(sys.argv) == 1:
    program_codes = ['program']
else:
    program_codes = sys.argv[1:]

print(program_codes)
programs = []

# bottin_programmes.csv :
# Exporter tous les champs du bottin dans l'ordre par défaut
# (√) Grouper par Programme

for program_code in program_codes:
    with open(f'{program_code}.csv', mode='r') as file_handler:
        # readline() might be a simpler equivalent
        lines = list(islice(file_handler, None))

        for line in lines:
            # DEBUG:
            # print(line)
            elements = line.split('","')

            # Eventually use collections.OrderedDict for json output readability
            program = {
                'code': elements[2],
                'name': unicodedata.normalize('NFC', elements[10]),
                'sector': elements[11],
                'version': elements[17][0:-2],  # Remove last character of string: "
                'comment': elements[3],
                'total_key_unit': elements[14],
                'total_unit': elements[13],
                'total_key': elements[15],
                'total_module': elements[16],
                'prog_length': elements[6],
                'threshold': elements[12],
                'corr_factor': elements[7]
            }

            ids = elements[9].split('\x1D')
            keys = elements[1].split('\x1D')
            competencies = elements[4].split('\x1D')
            categories = elements[0][1:].split('\x1D')  # Remove first character of string : "
            lengths = elements[5].split('\x1D')
            links = elements[8].split('\x1D')

            program['competencies'] = []
            for index, competency in enumerate(competencies):
                if not competency or ids[index] == '':
                    continue
                if keys[index] == '0':
                    keys[index] = 'True'
                else:
                    keys[index] = 'False'
                program['competencies'].append({
                    'id': ids[index],
                    'competency': unicodedata.normalize('NFC', competencies[index]),
                    'category': categories[index],
                    'length': lengths[index],
                    'link': links[index],
                    'key': keys[index]
                })

            programs.append(program)

    with open(f'data/{elements[2]}.json', mode='w') as file_handler:
        file_handler.write(json.dumps(programs, sort_keys=True, indent=4))
