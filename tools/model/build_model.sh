#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# Note : graphviz package needed

python manage.py graph_models --dot -a -g -o tools/model/passages_model.gv
dot -Tpdf tools/model/passages_model.gv > doc/passages_model.pdf

open doc/passages_model.pdf
