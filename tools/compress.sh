#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# Run offline compressor

. ./tools/set-env.sh

rm -rf dproject/staticfiles

python manage.py collectstatic

python manage.py compilejsi18n

python manage.py compress
