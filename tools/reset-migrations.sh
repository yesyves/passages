#!/bin/sh

###########################################################################################
# This script does NOT work in Docker DEV environment, so use `db-reset.dev.sh` instead.
###########################################################################################

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

. ./tools/set-env.sh

# IMPORTANT: run this line to avoid errors when running `./manage.py migrate` after code pull
find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete

if [ -e sqlite.db ]
  then
    rm sqlite.db
fi

if [ -z $DB_NAME ]
  then
    DB_NAME=`grep DATABASE_URL .env | cut -d'/' -f 4 | cut -d'"' -f 1`
fi

if [ -z $DB_USER ]
  then
    DB_USER=`grep DATABASE_URL .env | cut -d'/' -f 3 | cut -d':' -f 1`
fi

echo "DB_NAME: $DB_NAME"
echo "DB_USER: $DB_USER"
sleep 3

if [ -n `which psql` ]
  then
    psql -U $DB_USER $DB_NAME -a -f tools/sql/drop_tables.sql
fi

python manage.py makemigrations
python manage.py migrate

./tools/reset-contents.sh
