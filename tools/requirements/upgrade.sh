#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

echo '\n-------------------- upgrade pip and base packages ---------------------\n'

# for package in $(awk '{ print $0 "\n" }' < tools/requirements/base.txt); do
for package in `cat tools/requirements/base.txt | cut -d= -f1` ; do
  echo "$package"
  pip install -U $package | grep -v satisfied
done

echo '\n-------------------- upgrade development packages ----------------------\n'

for package in `cat tools/requirements/dev.txt` ; do
  echo "$package"
  pip install -U $package | grep -v satisfied
done

echo '\n-------------------- upgrade production packages -----------------------\n'

for package in `cat tools/requirements/production.txt | grep -v '#\|-\|^$' | cut -d= -f1` ; do
  echo "$package"
  pip install -U $package | grep -v satisfied
done
