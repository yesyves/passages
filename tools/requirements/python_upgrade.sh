#!/bin/sh

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: python_upgrade.sh current_version new_version"
  exit 0
fi

current_version=$1
new_version=$2
mp_version=${2//./}

source $HOME/Library/Python/$current_version/bin/virtualenvwrapper.sh

# Environment should not be activated
# deactivate does not work here
rmvirtualenv passages

sudo port select --set python python$mp_version
sudo port select --set python3 python$mp_version
sudo port select --set pip pip$mp_version
sudo port select --set pip3 pip$mp_version

/opt/local/Library/Frameworks/Python.framework/Versions/$new_version/bin/pip install virtualenvwrapper

export PATH=$HOME/Library/Python/$new_version/bin:/opt/local/Library/Frameworks/Python.framework/Versions/$new_version/bin:$PATH
source $HOME/Library/Python/$new_version/bin/virtualenvwrapper.sh

mkvirtualenv passages
workon passages

# Install requirements
pip install -r tools/requirements/base.txt
#./tools/requirements/upgrade.sh

echo -n "Make sure to restart your shell with the new python environment"
for i in `seq 1 4`; do
  sleep 1
  echo -n '.'
done
echo '.'

nano ~/.profile