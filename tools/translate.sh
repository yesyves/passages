#!/usr/bin/env bash

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

. ./tools/set-env.sh

if [ ${1+x} ] && [ $1 != '-c' ] && [ $1 != '--compress' ]; then
  echo "Usage: translate.sh [-c|--compress]"
  exit 0
fi

# Create and compile locale files

python manage.py makemessages --all --no-obsolete --ignore static --ignore node_modules
python manage.py makemessages -d djangojs --all --no-obsolete --ignore vendor --ignore node_modules

CA_path="dproject/locale/fr_CA/LC_MESSAGES"
FR_path="dproject/locale/fr_FR/LC_MESSAGES"
cmds=(-e "s/Réalisations/Situations de travail/g" -e "s/réalisations/situations de travail/g"
      -e "s/Réalisation/Situation de travail/g" -e "s/réalisation/situation de travail/g")
for file in django djangojs
  do
    sed "${cmds[@]}" $CA_path/$file.po > $CA_path/$file.tmp
    mv -f $CA_path/$file.tmp $CA_path/$file.po

    # Use almost same translations for fr_*
    sed -e "s/Reconnaissance/Validation/g" -e "s/reconnaissance/validation/g" -e "s/RAC/VAE/g" \
      $CA_path/$file.po > $FR_path/$file.po
done
python manage.py compilemessages

if [ "$1" = '-c' ] || [ "$1" = '--compress' ]; then
  ./tools/compress.sh
fi
