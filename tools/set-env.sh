#!/usr/bin/env bash

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# Set OS environment variables by sourcing root .env file (./.env) instead of reading directly
# from ./config/settings/.env. This is needed to make this code Docker-proof since in Docker
# PROD setup, NO .env file can be read and therefore env vars are made available as OS vars.
# - https://stackoverflow.com/questions/19331497/set-environment-variables-from-file-of-key-value-pairs#answer-30969768
# - https://stackoverflow.com/questions/19331497/set-environment-variables-from-file-of-key-value-pairs#answer-45971167
# - https://gist.github.com/mihow/9c7f559807069a03e302605691f85572#gistcomment-4172996
# - https://stackoverflow.com/questions/13702425/source-command-not-found-in-sh-shell#answer-13702462
# - http://gnu.org/software/bash/manual/html_node/The-Set-Builtin.html

# NOTE: `source` does not work in /bin/sh, but only in /bin/bash, so use `.` since compatible in both;
# also, always use relative path `./.env`, never `.env` since in /bin/sh it results in `.env not found`

# TODO do we need a conditional ? setting variables is idempotent
if [ -z ${DJANGO_SECRET_KEY+x} ]
  then
    set -o allexport
    #source ./.env
    . ./.env
    set +o allexport

    # print env vars to confirm that .env was properly imported
    echo "OS Environment Variables:"
    printenv | grep -E "(DJANGO|AAD)"
    echo ""
fi
