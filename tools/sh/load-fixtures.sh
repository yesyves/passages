#!/bin/sh

if [ "$1" = "uqam" ]
  then
    for fix in \
      uqam_concentration fp_program ft_program \
      uqam_help_link uqam_document_type \
      uqam_semester_user uqam_portfolio
      do
        echo "$fix"
        python manage.py loaddata "$fix"
    done

    cp -vr data/media/7 dproject/media

elif [ "$1" = "radio" ]
  then
    for fix in  \
      radio_concentration ft_program radio_custom_competency_type radio_custom_competency \
      radio_achievement_template radio_document_type radio_assessment_rating radio_editor_block \
      radio_achievement_type radio_portfolio_type radio_assessment help_link radio_semester_user radio_portfolio
      do
        echo $fix
        python manage.py loaddata $fix
    done

    cp -vr data/media/0 dproject/media

elif [ "$1" = "uqar" ]
  then
    for fix in \
      uqar_concentration fp_program uqar_custom_competency_type uqar_custom_competency \
      assessment_rating uqar_achievement_template uqar_editor_block uqar_achievement_type \
      uqar_xform help_link uqar_portfolio_type assessment uqar_document_types \
      uqar_semester_user uqar_portfolio
      do
        echo $fix
        python manage.py loaddata $fix
    done

elif [ "$1" = "rcpe" ]
  then
    for fix in \
      rcpe \
      uqam_help_link \
      rcpe_semester_user rcpe_portfolio
      do
        echo $fix
        python manage.py loaddata $fix
    done

else
  echo "No valid fixtures specified"

fi
