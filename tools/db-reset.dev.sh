#!/bin/sh

###########################################################################################
# DEV-only script to fully reset db by dropping all tables, running migrations, and
# using fixtures to populate default dummy data for the db.
# NEVER run this script in PROD.
# Uses `reset-contents.sh` for populating db from fixtures, but not `reset-migrations.sh`
###########################################################################################

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# IMPORTANT: run this line to avoid errors when running `./manage.py migrate` after code pull
# USE WITH CAUTION: when dealing w/ a live system (in prod), running this in dev corrupts all migrations and causes errors down the line
#   because it resets all local migrations, which will no longer match those in prod, so we would need to run 'git restore .' to ignore
#   those migrations, but then existing local migrations no longer work. So best to just reset contents of db, not touch migrations, 
#   and then run 'manage.py migrate' from existing db structure. If needed, though, for new deployment, we can use this line to cleanup migrations.

#find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete

echo "DB_NAME: $POSTGRES_DB"
echo "DB_USER: $POSTGRES_USER"
sleep 3

# wipe all db tables (note: in docker, the db container is not on same host, so need to pass -h <host> also)
# https://dba.stackexchange.com/questions/14740/how-to-use-psql-with-no-password-prompt
if [ -n `which psql` ]
  then
    PGPASSWORD=$POSTGRES_PASSWORD psql -h $POSTGRES_HOST \
      -U $POSTGRES_USER $POSTGRES_DB \
      -a -f ./tools/sql/drop_tables.sql
fi

# CAUTION: better to run this manually as needed rather than automatically when resetting the db
#python manage.py makemigrations

# recreate db structure from migrations
python manage.py migrate

# seed db w/ default dummy contents
./tools/reset-contents.sh
