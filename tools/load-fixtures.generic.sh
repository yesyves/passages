#!/usr/bin/env bash

for fix in \
  fpt_program fp_program rcpe \
  custom_competency_type custom_competency assessment_rating \
  achievement_template editor_block achievement_type
  do
    echo $fix
    python manage.py loaddata $fix
done

# remove local media files (user uploads)
rm -rf dproject/media/*

# check if 'MODE_CONTINUOUS' is set in os env vars
VAR_MODE=${MODE_CONTINUOUS:-True}
if [[ "${VAR_MODE}" == "True" ]]
then
  for fix in fpt_help_link uqar_portfolio_type uqar_semester_user uqar_portfolio
    do
      echo $fix
      python manage.py loaddata $fix
  done
  cp -vr data/media/7 dproject/media
else
  for fix in help_link radio_portfolio_type radio_semester_user radio_portfolio
    do
      echo $fix
      python manage.py loaddata $fix
  done
  cp -vr data/media/0 dproject/media
fi

# load 'assessment' fixture
python manage.py loaddata assessment
