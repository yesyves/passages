#!/usr/bin/env bash

for fix in \
  uqar_concentration fp_program uqar_custom_competency_type uqar_custom_competency \
  uqar_achievement_template uqar_editor_block uqar_achievement_type \
  uqar_xform help_link uqar_portfolio_type uqar_document_types \
  uqar_semester_user uqar_portfolio
  do
    echo $fix
    python manage.py loaddata $fix
done
