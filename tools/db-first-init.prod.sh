#!/bin/bash

#####################################################################################
# IMPORTANT: This script is assumed to run ONLY ONCE in PROD Docker environments
# on first initialization of the container & volume. On subsequent restarts of
# the container, no such script will ever be executed. It also assumes we start
# from a clean database w/ no tables created since it's a new Docker volume.
#####################################################################################

# © Copyright Yves de Champlain et al. 2018-2022
#     For more informations, see https://doc.epassages.ca/index.php/contributions
# SPDX-License-Identifier: LiLiQ-Rplus-1.1
# License-Filename: LICENCE

# wait 3 seconds to make sure PGSQL is up; else it may happen that db is not fully available
sleep 3

# IMPORTANT: run this line to avoid errors when running `./manage.py migrate` after code pull
find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete
find . -path "*.pyc"  -delete

# run migrations = create tables (db structure) - no data yet
python manage.py makemigrations
python manage.py migrate
python manage.py flush --no-input

# one script per institution to load different fixtures by default
echo "Load data from :"
#./tools/load-fixtures.generic.sh
./tools/load-fixtures.uqar.sh